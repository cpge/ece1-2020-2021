## Pour le futur
\objectifintro (intro + tableofcontents)
\begin{extrait}[Auteur Date ). \emph{Titre livre}] ... \end{extrait}
\objectifs + les objectifs (fait une newpage avant et après)

## ECE1 - 2020-2021 - A faire


### Chapitre 13

* Objectifs : OK
* Lien exercices : OK
* Correction exercices :
* Ajouter exercices :
* Relecture :


### Chapitre 14   **OK**

* Objectifs : OK
* Lien exercices : OK
* Correction exercices : OK
* Ajouter exercices : NON
* Relecture : OK

### Chapitre 15 : **Étude élémentaire sur les séries**

* Objectifs : OK
* Lien exercices : OK
* Correction exercices : OK
* Ajouter exercices : oui type concours
* Relecture :

### Chapitre 16 : **Espaces probabilisés infinis**

* Objectifs :  OK
* Lien exercices : OK
* Correction exercices : OK 
* Ajouter exercices :
* Relecture :

### Chapitre 17 : Intégration  

* Objectif : 
* Lien exercices :
* Correction exercices
* Ajouter exercices :
* Relecture :

### Chapitre 18 

* Objectifs : OK
* Lien exercices : OK
* Correction exercices : dernier à faire.
* Ajouter exercices :
* Relecture :

