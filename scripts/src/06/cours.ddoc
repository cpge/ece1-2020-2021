chapter:6
title:Probabilités sur un ensemble fini

[intro]
Nous allons dans ce chapitre revoir les notions de base sur les probabilités, vues depuis la classe de troisième, en les rendant plus rigoureuses. Nous revenons également les probabilités conditionnelles et la formule des probabilités totales.
[/]

[objectifs]

[=num]
* Concernant les généralités sur les probabilités :
[=item]
* \hyperref[objectif-06-1]{Déterminer des probabilités en situation d'équiprobabilité}\dotfill $\Box$
* \hyperref[objectif-06-2]{Utiliser la formule du crible de Poincaré}\dotfill $\Box$
* \hyperref[objectif-06-3]{Justifier qu'une famille est un système complet d'événements}\dotfill $\Box$
* \hyperref[objectif-06-4]{Utiliser les formules des probabilités composées et des probabilités totales}\dotfill $\Box$
* \hyperref[objectif-06-5]{Utiliser les formules de Bayes}\dotfill $\Box$
* \hyperref[objectif-06-6]{Démontrer que deux événements sont indépendants}\dotfill $\Box$
* \hyperref[objectif-06-7]{Utiliser la mutuelle indépendance d'une famille d'événements}\dotfill $\Box$
[item=]
* Concernant les variables aléatoires :
[=item]
* \hyperref[objectif-06-8]{Déterminer le support et la loi de probabilités d'une variable aléatoire}\dotfill $\Box$
* \hyperref[objectif-06-9]{Calculer espérance et variance d'une variable aléatoire}\dotfill $\Box$
[item=]
[num=]

[extrait Henri Poincaré (1854 -- 1912)]
Le nom seul de calcul des probabilités est un paradoxe : la probabilité, opposée à la certitude, c'est ce qu'on ne sait pas, et comment peut-on calculer ce que l'on ne connaît pas ?
[/extrait]
[NP]

[= Espace probabilisé fini]

[== Espace probabilisable]

[.definition]
Soit $\Omega$ un ensemble \underline{fini}, et $\mathcal{P}(\Omega)$ l'ensemble des parties de $\Omega$. On dit que $(\Omega, \mathcal{P}(\Omega) )$ est un \textbf{espace probabilisable}. Les éléments de $\mathcal{P}(\Omega)$ sont appelés \textbf{événements}. Les événements $\{\omega\}$ (pour $\omega \in \Omega$) sont appelés \textbf{événements élémentaires}.
[definition.]

[.remarque]
$\partie(\Omega)$ vérifie les propriétés suivantes, que nous reverrons plus tard :
\begin{itemize}[label=\textbullet]
    \item $\Omega \in \partie(\Omega)$
    \item $\forall~A \in \partie(\Omega),\quad\overline{A} \in \partie(\Omega)$
    \item $\forall, A_1, \cdots, A_n \in \partie(\Omega), A_1\cup\cdots\cup A_n \in \partie(\Omega)$.
\end{itemize}
[remarque.]

[.remarque]
L'événement $\vide$ est appelé \textbf{événement impossible}, et l'événement $\Omega$ l'\textbf{événement certain}.
[remarque.]

[.definition]
  Deux événements $A$ et $B$ de l'espace probabilisable $(\Omega, \mathcal{P}(\Omega))$ sont dits \textbf{incompatibles} si $A\cap B=\vide$.
[definition.]

[== Loi de probabilité]

[.definition]
Soit $(\Omega, \partie(\Omega))$ un espace probabilisable. On appelle \textbf{probabilité} sur $(\Omega, \partie(\Omega))$ toute application $\PP : \partie(\Omega) \rightarrow [0;1]$ vérifiant :
[=item]
* $\PP(\Omega)=1$
* Pour tous événements incompatibles $A$ et $B$, $\PP(A\cup B)=\PP(A)+\PP(B)$.
[item=]
[definition.]

[.remarque]
  Le triplet $(\Omega, \partie(\Omega), \PP)$ est alors appelé \textbf{espace probabilisé fini}, et pour tout $A \in \partie(\Omega)$, $\PP(A)$ s'appelle la \textbf{probabilité} de l'événement $A$.
[remarque.]

[.propriete]
Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini, et $A, B$ deux événements. Alors, on a :
[=item]
* $\PP(\overline{A})=1-\PP(A)$. Ainsi, $\PP(\vide)=1-\PP(\Omega)=0$.\vspace*{1mm}
* $\PP(B) = \PP(A\cap B) + \PP(\overline{A}\cap B)$.\vspace*{1mm}
* Si $A \subset B$ alors $\PP(A)\leq \PP(B)$.\vspace*{1mm}
* $\PP(A\cup B) = \PP(A)+\PP(B)-\PP(A\cap B)$.
[item=]
[propriete.]

[prof]
[.demonstration]
[=item]
* Par définition, $A$ et $\overline{A}$ sont incompatibles, et $A\cup \overline{A}= \Omega$. Donc par définition
    \[1=\PP(\Omega)=P(A\cup \overline{A})=\PP(A)+\PP(\overline{A})\]
* $A\cap B$ et $\overline{A}\cap B$ sont incompatibles, et $(A \cap B) \cup (\overline{A}\cap B) = B$. Par définition,
    \[\PP(B) = \PP((A \cap B) \cup (\overline{A}\cap B) =\PP(A \cap B) + \PP(\overline{A}\cap B) \]
* Si $A\subset B$, on peut écrire $B = A \cup (B\cap \overline{A})$. Les événements $A$ et $B\cap \overline{A}$ étant incompatibles, par définition
    \[\PP(B)=\PP(A \cup (B\cap \overline{A}))=\PP(A)+\PP(B\cap \overline{A})\geq \PP(A)\]
* On constate que $A\cup B = A \cup (B\cap \overline{A})$. Puisque $A$ et $B\cap \overline{A}$ sont incompatibles, par définition
    \[\PP(A\cup B)= \PP(A \cup (B\cap \overline{A}))=\PP(A)+\PP(B\cap \overline{A})\underbrace{=}_{\textrm{d'après pt 2}} \PP(A)+\PP(B)-\PP(A\cap B)\]
[item=]
[demonstration.]
[/prof 10]

[.propriete Crible de Poincaré]
Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Si $A_1,\cdots,A_n$ sont des événements deux à deux incompatibles ($k\geq 1$), on a \[\PP\left(\bigcup_{k=1}^n A_k\right)=\sum_{k=1}^n \PP(A_k)\]
Et si $A,B$ et $C$ sont trois événements quelconques, on a
\[\PP(A\cup B\cup C) = \PP(A)+\PP(B)+\PP(C)-\PP(A\cap B) -\PP(A \cap C) -\PP(B\cap C)+\PP(A\cap B \cap C)\]
[propriete.]

[.demonstration]
Ce résultat se démontre par récurrence sur $n$.
[demonstration.]

[.remarque]
  Il existe un crible de Poincaré plus général : pour tous événements $(A_1,\cdots, A_n$), on a
\[\PP\left(\bigcup_{k=1}^n A_k\right)=\sum_{k=1}^n \PP(A_k) - \sum_{i<j} \PP(A_i\cap A_j) + \sum_{i< j < k} \PP(A_i\cap A_j \cap A_k) \cdots + (-1)^{n+1} \PP(A_1\cap A_2\cap \cdots \cap A_n)\]
[remarque.]

[== Loi équiprobable]

[.definition]
  Dans le cas où toutes les issues ont la même probabilité, on dit qu'elles sont \textbf{équiprobables}, ou que la loi de probabilité $\PP$ est \textbf{uniforme}.\\
Si $\Omega=\{x_1;\cdots;x_n\}$, alors la probabilité de chaque issue est \[p=\frac{1}{\card(\Omega)}=\frac{1}{n}\]
[definition.]

[.theoreme]
  Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini en situation d'équiprobabilité. Alors la probabilité d'un événement $A$ est donnée par
\[\PP(A)=\frac{\card(A)}{\card(\Omega)}=\frac{\textrm{nombre de cas favorables}}{\textrm{nombre de cas possibles}}\]
[theoreme.]

[.exo]
  On lance un dé à 12 faces bien équilibré. On note $A$ l'événement ``obtenir un multiple de 3'' et $B$ l'événement ``obtenir un multiple de 4''. Déterminer $\PP(A)$ et $\PP(B)$.
[exo.]

[prof]
[.soluce]
Puisque le dé est bien équilibré, la loi de probabilité est uniforme. Ainsi, $\Omega=\llbracket 1;12\rrbracket$ et $\Card(\Omega)=12$. \\
Puisque $A=\{3;6;9;12\}$ et $B=\{4;8;12\}$, on a
\[\PP(A)=\frac{4}{12}=\frac{1}{3} \textrm{  et  } \PP(B)=\frac{3}{12}=\frac{1}{4}\]
[soluce.]
[/prof 4]

[= Probabilités conditionnelles]

[== Exemple]

Dans un sac, on possède $10$ jetons : $6$ jetons rouges, numérotés $1,1,1,2,2,4$ et quatre jetons verts, numérotés $2,2,4,4$.
On tire au hasard un jeton du sac.

On note $R$ l'événement ``obtenir un jeton rouge'', $V$ l'événement ``obtenir un jeton vert'', $1$ l'événement ``obtenir un jeton $1$'', ...

Cette expérience peut être représentée par un \textbf{arbre pondéré} :

[.center]
	\includegraphics[width=10cm]{tex/Chap06/pic/arbres_pond}
[center.]

De cet arbre, on peut lire ainsi que $\PP(R)=\frac{6}{10}$, $\PP(V)=\frac{4}{10}$.

La branche $ - R - 2$ indique que l'on a obtenu un jeton rouge \textbf{et} numéroté $2$.

[.propriete]
[=item]
* Loi des noeuds : La somme des probabilités inscrites sur les branches issues d'un même noeud est égale à $1$.
* La probabilité de l'événement représenté par un chemin est égale au produit des probabilités inscrites sur les branches de ce chemin.
[item=]
[propriete.]
%

 Ainsi, la probabilité de $R \cap 2$ est égale à \[\PP(R\cap 2)= \frac{6}{10} \times \frac{1}{3} = \PP(R) \times \PP_R(2)\]

[= Probabilités conditionnelles]

[.definition]
  Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soit $A$ un événement de probabilité non nulle. Alors, l'application $\PP_A$ définie sur $\partie(\Omega)$ par \[\forall~B\in\partie(\Omega),~\PP_A(B)=\frac{\PP(A\cap B)}{\PP(A)}\] est une probabilité sur $(\Omega, \partie(\Omega))$ appelée \textbf{probabilité sachant} $A$.
[definition.]

[.remarque]
  $\PP_A(B)$ est également notée $\PP(B|A)$. Puisque $\PP_A$ est une probabilité sur $(\Omega, \partie(\Omega))$, toutes les propriétés des probabilités s'appliquent à $\PP_A$.
[remarque.]

[.exemple]
Dans l'exemple précédent, $\PP_V(2)=\frac{1}{2}$.
[exemple.]

[.remarque]
  Dans la suite du cours, et plus généralement tout au long de l'année, on essaiera de se passer des arbres. D'une part, parce qu'il est plus important de comprendre les formules sous-jacentes; et d'autre part parce que, souvent, les expériences aléatoires que l'on fera ne se modéliseront pas facilement avec un arbre.
[remarque.]

[== Formule des probabilités composées]

[.theoreme Probabilités composées] 
On utilise souvent les probabilités conditionnelles pour déterminer la probabilité de l'intersection : $\PP(A\cap B)=\PP(A) \times \PP_A(B)$.

On peut d'ailleurs généraliser cette formule : pour tous événements $A_1,\cdots,A_n$, on a
\[\PP(A_1\cap\cdots \cap A_n) = \PP(A_1) \PP_{A_1}(A_2) \PP_{A_1\cap A_2}(A_3)\cdots \PP_{A_1\cap \cdots \cap A_{n-1}}(A_n)\]
[theoreme.]

[.exemple]
Toujours dans l'exemple précédent, $\PP(R\cap 4)=\PP(R)\times \PP_R(4)=\frac{6}{10}.\frac{1}{6}=\frac{1}{10}$.
[exemple.]

[.remarque]
Cette formule est très importante. C'est celle qu'on utilisera souvent pour déterminer la probabilité d'une intersection.
[remarque.]

[= Probabilités totales]

[== Système complet d'événements]

[.definition]
Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $(A_1,\cdots,A_n)$ une famille d'événements. On dit que cette famille est un \textbf{système complet d'événements} si
[=item]
* les événements $A_1,\cdots,A_n$ sont deux à deux incompatibles ($\forall~i\neq j, A_i\cap A_j=\vide$).
* $\ds{\bigcup_{k=1}^n A_k=\Omega}$.
[item=]
[definition.]

[.exemple]
On lance un dé à $6$ faces, et on note $A:$``obtenir un nombre pair'', et $B:$``obtenir un nombre impair''. Alors $\Omega= \llbracket 1,6 \rrbracket$, $A\cup B=\Omega$ et $A\cap B=\vide$. Donc $(A,B)$ forme un système complet d'événements.
[exemple.]

[.remarque]
Si $A$ est un événement de $(\Omega, \partie(\Omega), \PP)$, alors $(A,\overline{A})$ est un système complet de deux événements.
[remarque.]

[== Formules des probabilités totales]

[.theoreme]
Soit  $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soit $(A_1,\cdots,A_n)$ un système complet d'événements de $\Omega$, tel que pour tout $i\in \llbracket 1,n \rrbracket, \PP(A_i)\neq 0$. Alors, pour tout événement $B$, on a
\[\PP(B)=\sum_{k=1}^n \PP(B\cap A_k)=\sum_{k=1}^n \PP(A_k) \PP_{A_k}(B)\]
[theoreme.]

[prof]
[.demonstration]
On peut écrire \[B=B\cap \bigcap_{k=1}^n A_k = \bigcap_{k=1}^n B\cap A_k\]
Or les événements $B\cap A_k$ sont des événements deux à deux incompatibles, puisque les $A_k$ le sont. Donc
\[\PP(B)= \PP\left(\bigcap_{k=1}^n B\cap A_k\right)=\sum_{k=1}^n \PP(B\cap A_k)\]
[demonstration.]
[/prof 6]

[.propriete]
Dans un arbre pondéré, la probabilité d'un événement $E$ est la somme des probabilités des chemins qui aboutissent à $E$.
[propriete.]

[.exemple]
Dans l'exemple de début, $(R,V)$ est un système complet d'événements. La probabilité de l'événement $2$ vaut donc, d'après la formule des probabilités totales,
\[\PP(2)=\PP_R(2)\times \PP(R) + \PP_V(2)\times \PP(V)=\frac{1}{3}.\frac{6}{10}+\frac{1}{2}.\frac{4}{10}=\frac{4}{10}\]
[exemple.]

[= Formule de Bayes]

[.theoreme Formule de Bayes] 
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A$ et $B$ deux événements de probabilité non nulle. Alors
\[\PP_A(B)= \frac{\PP(B)}{\PP(A)}\PP_B(A) \textrm{ et }
\PP(B)=\frac{\PP(A)\PP_A(B)}{\PP_B(A)}
\]
[theoreme.]

[prof]
[.demonstration]
En effet, \[\frac{\PP(B)}{\PP(A)}\PP_B(A)=\frac{\PP(B)}{\PP(A)}\frac{\PP(B\cap A)}{\PP(B)} = \frac{\PP(B\cap A)}{\PP(A)}=\PP_A(B)\]
[demonstration.]
[/prof 3]

[.exemple]
Dans l'exemple du début, la probabilité, sachant qu'on a eu un jeton $2$, que celui-ci soit vert, vaut
\[\PP_2(V)= \frac{\PP(V)}{\PP(2)}\PP_V(2)=\frac{\frac{4}{10}}{\frac{4}{10}}\frac{1}{2}=\frac{1}{2}\]
[exemple.]

[= Indépendance de deux évènements]

[== Indépendance de deux événements]

[.definition]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A$ et $B$ deux événements. On dit que $A$ et $B$ sont \textbf{indépendants} lorsque $\PP(A\cap B)=\PP(A)\times \PP(B)$.
[definition.]

[.exemple]
Dans l'exemple du II., les événements $V$ et $R$ sont indépendants.
[exemple.]

[.propriete]
Si $A$ et $B$ sont des événements de probabilité non nulle, il y a équivalence entre les propositions suivantes :
[=item]
* $A$ et $B$ sont indépendants.
* $\PP_A(B)=\PP(B)$
* $\PP_B(A)=\PP(A)$
[item=]
[propriete.]

[.theoreme]
Soient deux évènements  indépendants $A$ et $B$. Alors $\overline{A}$ et $B$ sont aussi indépendants.
[theoreme.]

[prof]
[.demonstration]
Puisque $A$ et $B$ sont indépendants, on a $\PP(A\cap B)=\PP(A)\PP(B)$.  Or, d'après la formule des probabilités totales :
\[\PP(B)=\PP(A\cap B)+\PP(\overline{A}\cap B)=\PP(A)\PP(B)+\PP(\overline{A}\cap B)\]
On a donc $\PP(\overline{A}\cap B)=\PP(B)-\PP(A)\PP(B)=(1-\PP(A))\PP(B)=\PP(\overline{A})\PP(B)$.
[demonstration.]
[/prof 5]

[== Indépendance d'une famille d'événements]

[.definition]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A_1,\cdots, A_n$ des événements.
[=item]
* On dit que $A_1,\cdots,A_n$ sont \textbf{deux à deux indépendants} pour la probabilité $\PP$ si
    \[\forall~(i,j)\in \llbracket 1,n \rrbracket, i\neq j, ~\PP(A_i\cap A_j)=\PP(A_i)\PP(A_j)\]
* On dit que $A_1, \cdots, A_n$ sont \textbf{mutuellement indépendants} pour la probabilité $\PP$ si
    \[\forall~I \subset \llbracket 1,n \rrbracket,~ \PP\left(\bigcap_{i\in I} A_i\right) = \prod_{i\in I} \PP(A_i)\]
    En particulier, $\PP(A_1 \cap \cdots \cap A_n)=\PP(A_1)\times\cdots \times\PP(A_n)$.
[item=]
[definition.]

[.theoreme]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. Soient $A_1,\cdots, A_n$ des événements mutuellement indépendants pour la probabilité $\PP$. Soit $B_1,\cdots, B_n$ des événements tels que, pour tout $i \in \llbracket 1,n \rrbracket$, $B_i=A_i \textrm{ ou } \overline{A_i}$. Alors les événements $B_1,\cdots, B_n$ sont encore mutuellement indépendants.
[theoreme.]

[= Variables aléatoires - Rappels]

 \textit{Ceci n'est qu'un rappel de première et terminale. Nous définirons en détail la notion de variable aléatoire plus tard dans l'année.}

[= Définitions]

[.definition]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini. On appelle \textbf{variable aléatoire} sur $\Omega$ toute application $X$ définie sur $\Omega$ et à valeur dans $\R$. $X(\Omega)$ est appelé le \textbf{support} de la variable aléatoire $X$.\\
Si on note $X(\Omega)=\{x_1; \cdots; x_n\}$, on appelle \textbf{loi de probabilité de la variable aléatoire} $X$, l'application qui à tout élément $x_i$ fait correspondre la probabilité de l'événement ``$X$ prend la valeur $x_i$'', que l'on note $\PP(X=x_i)$.
[definition.]

[.remarque]
[=item]
* L'événement $(X=x_i)$ est composé des issues pour lesquelles la variable aléatoire $X$ prend la valeur $x_i$.
* Déterminer la loi de probabilité d'une variable aléatoire $X$, c'est donner l'ensemble des probabilités $\PP(X=x_1),\cdots,\PP(X=x_n)$ (sous la forme d'un tableau par exemple).
[item=]
[remarque.]

[.exo]
On lance un dé équilibré. On gagne $1$ euro si on tombe sur $1$ ou $6$, et on perd $1$ euro sinon. Déterminer le support et la loi de probabilité de la variable aléatoire représentant le gain.
\label{variablealea1}
[exo.]

[prof]
[.soluce]
En notant $X$ le gain, alors les valeurs possibles de $X$ sont $1$ et $-1$. On a donc
\[(X=1)=\{1;6\}\textrm{ et } (X=-1)=\{2;3;4;5\}\] et donc $\PP(X=1)=\frac{2}{6}=\frac{1}{3}$ et $\PP(X=-1)=\frac{4}{6}=\frac{2}{3}$.
[soluce.]
[/prof 3]

[== Paramètres d'une variable aléatoire]

[.definition]
Soit $(\Omega, \partie(\Omega), \PP)$ un espace probabilisé fini, et $X$ une variable aléatoire. On note $X(\Omega)=\{x_1;\cdots;x_n\}$.
[=item]
* On appelle \textbf{espérance mathématique} de $X$ le nombre réel
	\[\esperance(X)=x_1\times \PP(X=x_1)+ \cdots + x_n\times \PP(X=x_n)= \sum_{i=1}^n x_i\times \PP(X=x_i)\]
* On appelle \textbf{variance} de $X$ le nombre réel positif
	\[\Var(X)=(x_1-\esperance(X))^2 \times \PP(X=x_1)+ \cdots + (x_n-\esperance(X))^2 \times \PP(X=x_n)= \sum_{i=1}^n (x_i-\esperance(X))^2 \times \PP(X=x_i)\]
* On appelle\textbf{ écart-type} de $X$ le nombre réel positif $\sigma(X)=\sqrt{\Var(X)}$.
[item=]
[definition.]

[.proposition]
Soient $a$ et $b$ deux réels, et $X$ une variable aléatoire. Alors \[\esperance(aX+b)=a\esperance(X)+b \textrm{ et }\Var(aX+b)=a^2\Var(X)\]
[proposition.]

[.exo]
Déterminer espérance et variance de la variable aléatoire définie dans l'exercice \ref{variablealea1}.
[exo.]

[prof]
[.soluce]
Par définition, on a
	\[\esperance(X)=1\times \PP(X=1)+(-1)\times \PP(X=-1)=1\times \frac{1}{3}-1\times \frac{2}{3}=-\frac{1}{3}\]
	De même, on a :
	\[\Var(X)=(1-\esperance(X))^2\PP(X=1)+(-1-\esperance(X))^2\PP(X=-1)=\left(1+\frac{1}{3}\right)^2\times \frac{1}{3} + \left(-1+\frac{1}{3}\right)^2\times \frac{2}{3}=\frac{16}{27}+\frac{8}{27}=\frac{24}{27}=\frac{8}{9}\]
[soluce.]
[/prof 3]
