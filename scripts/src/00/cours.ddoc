chapter:0
title:Préparations

[intro]
Ce chapitre a pour objectif d'effectuer des révisions de bases des années précédentes : ensembles usuels, calculs fractionnaires et de puissances, développement et factorisation, équations et inéquations. 
[/]

[objectifs]

[=num]
* \hyperref[objectif-00-1]{connaitre les ensembles usuels}\dotfill $\Box$
* savoir calculer et simplifier des résultats :
	\begin{itemize}
* \hyperref[objectif-00-2]{en simplifiant des fractions}\dotfill $\Box$
* \hyperref[objectif-00-3]{en développant et en factorisant}\dotfill $\Box$
* \hyperref[objectif-00-4]{en calculant avec des puissances}\dotfill $\Box$
* \hyperref[objectif-00-5]{en simplifiant des racines carrées}\dotfill $\Box$
	\end{itemize}
* \hyperref[objectif-00-6]{savoir résoudre des questions et inéquations}\dotfill $\Box$

[num=]
[NP]

%%% Début du cours %%%

[= Ensembles de nombres]

[.definition Ensembles usuels]
\label{objectif-00-1}
On dispose des cinq ensembles usuels suivants :
[=item]
* L'ensemble $\N$ des \textbf{entiers naturels} \[ \N = \left \{ 0; 1; 2; 3; \hdots \right \}\]
* L'ensemble $\Z$ des \textbf{entiers relatifs} \[ \Z = \left \{\hdots; -4; -3; -2; -1; 0; 1; 2; 3; \hdots \right \}\]
* L'ensemble $\mathbb{D}$ des nombres décimaux, c'est-à-dire les nombres ayant un nombre fini de chiffres après la virgule.
* L'ensemble $\Q$ des nombres rationnels, c'est-à-dire les nombres s'écrivant sous la forme $\dfrac{p}{q}$, où $p$ est un entier relatif, et $q$ un entier naturel.
* L'ensemble $\R$ de l'ensemble des nombres réels.
[item=]
[definition.]

[.exemple]
Par exemple, \[ \frac{1}{3}\in \Q, \quad 0,41 \in \mathbb{D}, \quad \sqrt{2}\in \R\]
[exemple.]

[.propriete]
On dispose des inclusions suivantes :
\[ \N \subset \Z \subset \mathbb{D} \subset \Q \subset \R \]
[.center]
\includegraphics[scale=0.7]{tex/Chap00/pic/ensembles.mps}
[center.]
[propriete.]

[prof]
[.demonstration]
Par définition, $\N \subset \Z$, et $\Z \subset \mathbb{D}$ (car les entiers relatifs ont un nombre fini de chiffres après la virgule, à savoir $0$). $\Q\subset \R$ par définition également. Il reste à voir que $\mathbb{D} \subset \Q$.

Soit $a\in \mathbb{D}$. Alors $a$ possède un nombre fini de chiffres après la virgule; on note $d$ le nombre de chiffres après la virgule. Alors, \[ b=10^da \in \Z \] et donc \[ a = \frac{b}{10^d} \in \Q \]
[demonstration.]
[/prof 10]

[.notation]
On note \[ \R+ = [0;+\infty[ = \left \{ x\in \R,\quad x\geq 0\right \} \]
De même, on note
\[ \R- = ]-\infty;0] = \left \{ x\in \R,\quad x\leq 0 \right \} \]
On note $\R> = ]0;+\infty[$, et de même pour $\R<$. Les notations $+, -$ et $*$ s'étendent à tous les ensembles vus précédemment.
[notation.]

[.exo]
Décrire les ensembles $\Z^*_-, \Z^+$, $\N^-$ et $\Q-$.
[exo.]

[prof]
[.soluce]
Rapidement, $\Z_-^*$ sont les entiers strictement négatifs, $\Z_+=\N$, $\N_-=\{0\}$ et $\Q-$ sont les rationnels négatifs.
[soluce.]
[/prof 5]

[afaire Exercice \lienexo{1}.]

[= Calculs]

[== Calcul fractionnaire]

Les règles de calcul sur les fractions doivent être maitrisées dès le début d'année. Rappelons-les :

[.propriete]
\label{objectif-00-2} Pour tout $a\in \R$ et $b\in \R_*$ :
\[ \frac{0}{b}=0\quad \quad \frac{a}{1}=a\quad\quad \frac{a}{-1}=-a\]
\[ \frac{a}{-b}=\frac{-a}{b}=-\frac{a}{b} \]
\[ \frac{a}{b}=a\frac{1}{b}=\frac{1}{b}a \]
\[ \frac{1}{\frac{a}{b}} = \frac{b}{a}; \]
pour $k\in \R_*$ \[ \frac{a}{b}=\frac{k\times a}{k\times b} \]
et si $c\in \R$ et $d\in \R_*$ :
\[ \frac{ \frac{a}{b}}{\frac{c}{d}} = \frac{a}{b}\times \frac{d}{c}\]
[propriete.]

[.exo]
Calculer $\ds{A=\frac{\frac{3}{7}}{\frac{2}{5}}}$, $\ds{B=\frac{\frac{2}{3}}{6}}$, $C=3\times \dfrac{7}{18} \qeq D=\dfrac{4+17}{11+4}$.
[exo.]

[prof]
En appliquant ce qui précède, on a rapidement : \[ A=\frac{15}{14},\quad B=\frac{1}{9},\quad C=\frac{7}{6} \]
Enfin, attention : on ne simplifie pas comme on le veut dans une fraction (il faut d'abord factoriser). Ici \[ D=\frac{21}{15}=\frac{7}{5} \]
[/prof 8]

[afaire Exercice \lienexo{2}.]

[== Développement et factorisation]

\label{objectif-00-3} 
Développer une expression c'est l'écrire sous la forme d'une somme; factoriser c'est l'écrire sous la forme d'un produit. Par exemple :

\begin{align*}
	&(x+1)(x+2) = x^2+3x+2 \text{ est un développement}\\
	&x^2+2x = x(x+2) \text{ est une factorisation}
\end{align*}

Les identités suivantes sont à connaître :

[.propriete Identités remarquables] 
On dispose des identités suivantes :
[=item]
* Pour tous réels $a$ et $b$ :
	\begin{align*}
		 (a+b)^2&=a^2+2ab+b^2\\
		 (a-b)^2&= a^2-2ab+b^2\\
		 a^2-b^2&=(a-b)(a+b)
	\end{align*}
* Pour tous réels $a$ et $b$ :
	\begin{align*}
		(a+b)^3 &= a^3+3a^2b+3ab^2+b^3\\
		(a-b)^3&= a^3-3a^2b+3ab^2-b^3\\
		a^3-b^3&= (a-b)(a^2+ab+b^2)\\
		a^3+b^3&= (a+b)(a^2-ab+b^2)
	\end{align*}
[item=]
[propriete.]

[.exo]
Développer, pour tout réel $x$, $(x+1)^2, (x+1)^3, (1-x)^2$ et $(1-x)^3$. Factoriser $4-x^2$ et $8+x^3$.
[exo.]

[prof]
[.soluce]
En utilisant les identités remarquables :
\begin{align*}
	(x+1)^2 &= x^2+2x+1\\
	(x+1)^3 &= x^3+3x^2+3x+1\\
	(1-x)^2 &= x^2-2x+1 \\
	(1-x)^3 &= 1-3x+3x^2-x^3\\
	4-x^2 &= (2-x)(2+x) \\
	8+x^3 &= (2+x)(4-2x+x^2)
\end{align*}
[soluce.]
[/prof 8]

[afaire Exercices \lienexo{6} et \lienexo{7}.]

[== Puissances]
\label{objectif-00-4}
Rappelons la définition de la puissance entière d'un nombre réel :

[.definition]
Soit $a$ un réel non nul, et $n\in \N$. On définit $a^n$ de la manière suivante :
[=item]
* $a^0=1$;
* $a^n = \underbrace{a\times a \times \cdots \times a}_{n\text{ fois}}$ si $n\geq 1$.
[item=]
De plus, on note \[ a^{-n}=\frac{1}{a^n} \]
On définit ainsi $a^n$ pour $n\in \Z$.
[definition.]

[.exemple]
On a $2^6 = 2\times 2 \times 2 \times 2 \times 2 \times 2 = 64$, $(-12)^1 = -12$ et $(1409091)^0 = 1$.
[exemple.]

[.propriete]
Soient $a$ et $b$ deux réels non nuls, et $n, m$ deux entiers relatifs.
[=item]
* (puissances différentes) \[a^n\times a^m = a^{n+m};\quad \frac{a^n}{a^m} = a^{n-m}\qeq (a^n)^m = a^{nm};\]
* (puissances identiques) \[ a^n\times b^n=\left(a\times b\right)^n \qeq \frac{a^n}{b^n}=\left(\frac{a}{b}\right)^n.\]
[item=]
[propriete.]

[.exo]
Simplifier \[ A=\frac{5^4\times 3^{2}\times 2^{3}}{15^3} \qeq \frac{21\times 10^{-3}}{3\times 10^2}.\]
[exo.]

[prof]
[.soluce]
En utilisant la propriété précédente :
\begin{align*}
	A&= 5^1\times  3^{-1} \times 2^3 = \frac{40}{3} 	\\
	B&= \frac{21}{3}\times \frac{10^{-3}}{10^2} = 7\times 10^{-5}
\end{align*}
[soluce.]
[/prof 7]

[afaire Exercices \lienexo{3} et \lienexo{4}.]

[== Racine carrée]
\label{objectif-00-5}
Commençons par un rappel de la définition de la racine carrée d'un réel positif :

[.definition Racine carrée]
Soit $a \in \R+$. Il existe un \emph{unique} réel positif, noté $\sqrt{a}$, vérifiant \[ (\sqrt{a})^2 =a.\]
Ce nombre $\sqrt{a}$ est appelé ``racine carrée'' de $a$.
[definition.]

[.remarque]
On dispose des valeurs remarquables suivantes : \[ \sqrt{0}=0,\quad \sqrt{1}=1,\quad \sqrt{4}=2\qeq \sqrt{9}=3 \]
[remarque.]

[.attention]
\danger\quad On ne dispose pas de la racine carrée d'un nombre strictement négatif. Ainsi, la fonction racine carrée n'est définie que sur $\R_+$.
[attention.]

[.propriete]
Soient $a$ et $b$ deux réels positifs. Alors
\[ \sqrt{a\times b} = \sqrt{a}\times \sqrt{b} \]
et si $b\neq 0$, alors \[ \sqrt{\frac{a}{b}} = \frac{\sqrt{a}}{\sqrt{b}}. \]
[propriete.]

[.attention]
\danger\quad En règle général, $\ds{\sqrt{a+b} \neq \sqrt{a}+\sqrt{b}}$ ! Par exemple, $\sqrt{1+1}=\sqrt{2}$ et ce n'est pas égal à $\sqrt{1}+\sqrt{1}=2$.
[attention.]

[.exo]
Simplifier $\sqrt{8}$, $\sqrt{48}$ et $\ds{\sqrt{\frac{9}{32}}}$.
[exo.]

[prof]
[.soluce]
Rapidement \[ \sqrt{8}=\sqrt{4 \times 2}=2\sqrt{2},\quad \sqrt{48}=4\sqrt{3} \qeq \sqrt{\frac{9}{32}}=\frac{3}{4\sqrt{2}}=\frac{3\sqrt{2}}{8}\] 
[soluce.]
[/prof 7]

[afaire Exercice \lienexo{5}.]

[= Equations, inéquations]
\label{objectif-00-6}

[== Equations]

Une équation est une égalité faisant apparaitre une, ou plusieurs inconnue(s). En général, les inconnues sont notées $x, y, z, \hdots$.

[.exemple]
L'équation $x^2+x=3-x$ est une équation faisant intervenir une variable. L'équation $x+y=\frac{1}{x}-\frac{1}{y}$ fait intervenir deux variables, $x$ et $y$.
[exemple.]

[.definition]
Résoudre une équation, c'est déterminer l'ensemble de \emph{toutes} les inconnues vérifiant l'équation; on parle alors d'une solution de l'équation, et on recherche toutes les solutions de l'équation.
[definition.]

[.methode]
La première chose à faire et de trouver les valeurs interdites (division par $0$, par exemple).

Pour résoudre ensuite une équation, il n'y a pas de méthode universelle : on applique toutes propriétés possibles (multiplication, division) en raisonnant si possible par équivalence pour trouver les solutions.
[methode.]

[.exo]
Résoudre l'équation \[ \frac{1}{x+2}+\frac{x+1}{x} = 1 \]
[exo.]

[prof]
[.soluce]
Déjà, il faut éviter $-2$ et $0$. On se place sur $\R \backslash \{-2; 0\}$. Alors, en multipliant par $x(x+2)$ (qui ne s'annule alors pas) :
\begin{align*}
	\frac{1}{x+2}+\frac{x+1}{x}=1  \text{ équivaut à } &x+(x+2)(x+2)=x(x+1)\\
	\text{ soit } &x+x^2+3x+2 = x^2+2x \\
	\text{ ce qui donne } &2x = -2 \\
	\text{ et donc } &x=-1
\end{align*}
Cette solution est autorisée (car différente de $0$ et $-2$). On a procédé par équivalence, et on peut conclure que l'ensemble des solutions est $\{ -1 \}$, ce qu'on note, en général \[ \mathcal{S}=\{-1\} \]
[soluce.]
[/prof 15]

[.remarque]
On aurait pu, au lieu de mettre des mots en français, écrire $\Longleftrightarrow$. En général, on évitera l'enchaînement de symbole équivalent et implication, souvent illisible. On y reviendra dans le chapitre suivant.
[remarque.]

[afaire Exercices \lienexo{8}, \lienexo{9} et \lienexo{10}.]

[== Inéquations]

Une inéquation est une inégalité faisant apparaitre une, ou plusieurs inconnue(s).

Résoudre une inéquation, c'est trouver toutes les solutions de l'inéquation, c'est-à-dire trouver toutes les valeurs de l'inconnue vérifiant l'inégalité.

[.methode]
Pour résoudre une inéquation, on procède comme pour les équations, en utilisant les règles de calculs pour les inégalités.
[methode.]

[.propriete]
Pour tous réels $x$ et $y$
\begin{itemize}
	\item Pour tout réel $k$, $x\leq y$ si et seulement si $x+k \leq y+k$;
	\item pour tout réel $k > 0$, $x\leq y$ si et seulement si $k\times x\leq k\times y$;
	\item pour tout réel $k < 0$, $x\leq y$ si et seulement si $k\times x\geq k\times y$;
\end{itemize}
On retiendra que multiplier par un nombre strictement positif conserve l'ordre, alors que multiplier par un nombre strictement négatif renverse l'ordre.
[propriete.]

[.remarque]
La propriété précédente est valable en remplaçant $\leq$ par $<$, $>$ et $\geq$.
[remarque.]

[.exemple]
Résoudre l'inéquation \[ -2(x-4)\geq 3x-2 \]
[exemple.]

[prof]
[.soluce]
Pour tout réel $x$ :
\begin{align*}
	-2(x-4)\geq 3x-2 \text{ soit } & -2x+8 \geq 3x-2 \\
	\text{ et donc } & -5x \geq -10 \\
	\text{ puis } &x\leq 2
\end{align*}
Ainsi, l'inégalité est vérifiée pour tout $x\leq 2$ : \[ \mathcal{S} = \left \{x\in \R, \quad x\leq 2\right \} = ]-\infty; 2] \]
[soluce.]
[/prof 15]

[afaire Exercices \lienexo{11}, \lienexo{12}, \lienexo{13}, \lienexo{14}, \lienexo{15}, \lienexo{16}, \lienexo{17} et \lienexo{18}.]
