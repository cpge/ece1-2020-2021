[exercices]

[==* Généralités]

%%% Exercice %%%
[=exo 5 1 Une tribu]
\label{exo-td-16-5}
Soit $\Omega = \{ 1,2,3,4\}$. Montrer que $\mathscr{A} = \left\{ \varnothing , \{ 1 \} , \{ 2 \} , \{ 1,2 \} , \{ 3,4 \}, \{ 2,3,4 \} , \{ 1,3,4 \} ,   \Omega  \right\} $ est une tribu sur $\Omega$.

Montrer que $\mathscr{A}$ n'est pas égale à $\mathscr{P}(\Omega)$.      
[exo=]

[=cor]
Les réunions deux à deux d'éléments de $\mathcal{A}$ sont incluses dans $\mathcal{A}$, et $\Omega \in \mathcal{A}$ : $\mathcal{A}$ est bien une tribu. \\
De plus, $\mathcal{A} \neq \partie(\Omega)$, car par exemple $\{3\} \not \in \mathcal{A}$.
[cor=]
%%% Fin exercice %%%

[=exo 10 1 Des évènements]
\label{exo-td-16-6}
On lance une pièce une infinité de fois. Pour tout $k \in \N^*$, on note $A_k$ l'évènement "le $k$-ième lancer donne pile".
[=enum]
* Décrire par une phrase chacun des évènements suivants :
$$E_1 = \bigcap_{k=5}^{+\infty} A_k,\qquad E_2 = \left(\bigcap_{k=1}^4 \overline{A}_k \right) \cap \left( \bigcap_{k=5}^{+\infty} A_k \right),\qquad E_3 = \bigcup_{k=5}^{+\infty} A_k.$$
* Écrire à l'aide des $A_k$ l'évènement $B_n$ "on obtient au moins une fois pile après le $n$-ième lancer".
* Écrire à l'aide des $A_k$ les évènements :  
[=enum]
* $C_n$ : "on n'obtient plus que des piles à partir du $n$-ième lancer".
* $C$ : "on n'obtient plus que des piles à partir d'un certain lancer".
[enum=]
[enum=]
[exo=]

[=cor]
[=enum]
* $E_1$ représente l'évènement : ``n'obtenir que des piles à partir du $5$-ième lancer'', $E_2$ l'évènement : ``obtenir $4$ fois face puis que des piles'', et $E_3$ l'évènement : ``obtenir au moins un pile à partir du $5$-ième lancer''.
* On s'inspire de $E_3$ : \[ B_n=\bigcup_{k=n}^{+\infty} A_k \]
*  \begin{enumerate}
* De la même manière, on a \[ C_n=\bigcap_{k=n}^{+\infty} A_k \]
* On n'impose pas le début des piles : on est ainsi dans l'un des $C_n$. On a donc \[ C = \bigcup_{n=1}^{+\infty} C_n = \bigcup_{n=1}^{+\infty}\left( \bigcap_{k=n}^{+\infty} A_k\right)  \]
[enum=]
[enum=]
[cor=]
%%% Fin exercice %%%

[==* Limite monotone]

%%% Exercice %%%
[=exo 30 1 Systèmes]
\label{exo-td-16-7}
Une araignée se déplace sur les trois sommets d'un triangle $ABC$. A l'instant $0$, elle est en $A$. Puis, à chaque instant instant $n \in \N^*$, elle se déplace :
[=item]
* si elle est en $A$, elle va en $B$;
* si elle est en $B$, elle va en $A$ avec la probabilité $\cfrac{1}{2}$ et en $C$ avec la probabilité $\cfrac{1}{2}$;
* si elle est en $C$, elle y reste.
[item=]
On note $E$ l'évènement "la puce arrive en $C$". On cherche à montrer que $E$ a lieu presque sûrement.
[=enum]
* Représenter la situation avec un graphe.
* Montrer que l'araignée ne peut arriver au point $C$ qu'à des instants pairs.
* Calculer la probabilité de l'évènement $D_n$ que l'araignée arrive en $C$ pour la première fois à l'instant $2n$, $n \in \N^*$.
* Que dire de $D_p$ et $D_q$ si $p\neq q$ ? 
* En déduire que $\PP(E) = 1$.
[enum=]
[exo=]

[=cor]
\begin{enumerate}
	\item On obtient le graphe suivant :
[.center]
\includegraphics{tex/Chap16/pic/graph1.eps}
[center.]
	\item L'araignée ne peut venir en $C$ qu'à partir de $B$, et si elle y est, elle y reste. Au départ, elle est en $A$. Donc l'araignée va faire $(A-B)\hdots(A-B)-C$ avec au moins une fois $A-B$. Elle y arrivera ainsi forcément en un instant paire (puisqu'en $0$, elle est en $A$).
	\item On note $A_n$ l'évènement : ``l'araignée est en $A$ au $n$-ième instant'' (et de même $B_n$ pour $B$ et $C_n$ pour $C$). Alors, d'après ce qui précède :
		\begin{align*}
			\PP(D_n)	&= \PP(A_0\cap B_1 \cap A_2 \cap \hdots \cap B_{2n-1} \cap C_{2n}) \\
			&= \PP(A_0) \PP_{A_0}(B_1) \PP_{A_0\cap B_1}(A_2) \hdots \PP_{A_0\cap B_1\cap \hdots A_{2n-2}}(B_{2n-1)} \PP_{A_0\cap B_1\cap \hdots \cap B_{2n-1}}(C_{2n}) \text{ par les proba composées}\\
			&= \underbrace{1\times \frac{1}{2} \times 1 \times \frac{1}{2}\times \hdots \times 1\times \frac{1}{2}}_{n-1 \text{ fois}} \times \frac{1}{2} \text{ par indépendance}\\
			&= \left(\frac{1}{2}\right)^{n}  
		\end{align*}
	\item Par définition, $D_p \cap C_q=\vide$ si $p\neq q$ (car on s'intéresse à la première fois où l'araignée arrive en $C$).
	\item Mais alors, par définition : \[ E =\bigcup_{n=1}^{+\infty} D_n \]
	On peut appliquer la propriété de la limite monontone : \begin{align*}
 	\PP(E) &= \PP \left ( \bigcup_{n=1}^{+\infty} D_n\right) \\
 	 &= \lim_{p\to +\infty} \PP \left ( \bigcup_{n=1}^{p} D_n\right) \\
 	&= \lim_{p\to +\infty} \sum_{n=1}^{p} \PP(D_n) \\
 	&= \sum_{n=1}^{+\infty} \left(\frac{1}{2}\right)^{n} 	\\
 	&= \frac{\frac{1}{2}}{1-\frac{1}{2}}=1
 \end{align*}
	Ainsi, $E$ est un évènement presque sûr.
\end{enumerate}
[cor=]
%%% Fin exercice %%%

%%% Exercice %%%
[=exo 30 1 Et la formule des probabilités totales]
\label{exo-td-16-8}
Un joueur lance une pièce non truquée, équilibrée, jusqu'à l'obtention d'un premier pile. S'il lui a fallu $n$ ($n$ entier naturel non nul) lancers pour obtenir ce premier pile, on lui fait alors tirer au hasard un billet de loterie parmi $n!$ billets dont un seul gagnant.

On définit, pour tout entier naturel non nul, les évènements :
\begin{itemize}
\item $E_n$ : "le joueur obtient le premier pile au lancer $n$"
\item $P_n$ : "le joueur obtient pile au $n$-ième lancer"
\item $F_n$ : "le joueur obtient face au $n$-ième lancer"
\item $F$ : "le joueur n'obtient jamais pile"
\end{itemize}
\begin{enumerate}
\item Calculer, pour tout entier naturel $n$ non nul, la probabilité $\PP(E_n)$. Exprimer $\overline{F}$ en fonction des $E_n$, puis en déduire $\PP(\overline{F})$ et $\PP(F)$.
\item On définit l'évènement $G$ : "le joueur obtient le billet gagnant". Pour tout entier naturel $n$ non nul, déterminer $\PP_{E_n}(G)$, puis en déduire $\PP(G)$.
\item Le jeu est-il équilibré (on donne $\sqrt{e} \approx 1,65$) ? 
\end{enumerate}
[exo=]

[=cor]
[=enum]
* Par définition, $\ds{E_n=\bigcap_{k=1}^{n-1} F_k \cap P_n}$. Ainsi, d'après la formule des probabilités composées :
	\begin{align*}
	 \PP(E_n) &= \PP(F_1 \cap F_2 \cap \hdots \cap F_{n-1} \cap P_n)\\
	 &= \PP(F_1)\PP_{F_1}(F_2)\hdots \PP_{F_1\cap \hdots \cap F_{n-2}}(F_{n-1}) \PP_{F_1\cap\hdots\cap F_{n-1}}(P_n) \\
	 &= \underbrace{\frac{1}{2}\hdots \frac{1}{2}}_{n \text{ fois}} \text{ par indépendance} \\
	 &= \left(\frac{1}{2}\right)^n 
	 \end{align*}
* Par définition, $\overline{F}$ représente l'évènement : ``obtenir au moins une fois pile'', ce qui peut s'écrire également : ``obtenir au moins une fois un premier pile''. Ainsi \[ \overline{F} = \bigcup_{n=1}^{+\infty} E_n \]
	 D'après la propriété de la limite monotone : 
	 \begin{align*}
	  \PP(\overline{F}) &= \PP\left( \bigcup_{n=1}^{+\infty} E_n \right) 
	  = \lim_{p\to +\infty} \PP\left( \bigcup_{n=1}^{p} E_n\right) \\
	  &= \lim_{p\to  +\infty} \sum_{n=1}^p \PP(E_n) 
	  = \lim_{p\to +\infty} \sum_{n=1}^p \left(\frac{1}{2}\right)^n \\
	  &= \frac{\frac{1}{2}}{1-\frac{1}{2}} = 1 
	\end{align*}
	Ainsi, $\PP(\overline{F})=1$ et $\PP(F) = 0$ : l'évènement $F$ est donc négligeable.
* L'énoncé nous indique que s'il a fallut faire $n$ lancers pour obtenir pile, on tire un billet parmi $n!$ billets, dont un seul est gagnant et tous les billets sont équiprobables. Ainsi, il n'y a qu'un chance sur $n!$ d'obtenir un billet gagnant, et donc \[ \PP_{E_n}(G) = \frac{1}{n!} \]
	La famille $(E_n)$ est un système complet d'évènements : ils sont $2$ à $2$ incompatibles, et leur réunion est égale à l'univers. Ainsi, d'après la formule des probabilités totales :
	\begin{align*}
	\PP(G) &= \sum_{n=1}^{+\infty} \PP(E_n \cap G) \\
	&= \sum_{n=1}^{+\infty} \PP(E_n)\PP_{E_n}(G) \\
	&= \sum_{n=1}^{+\infty} \left(\frac{1}{2}\right)^n	 \frac{1}{n!} \\
	&= \sum_{n=1}^{+\infty} \frac{\left(\frac{1}{2}\right)^n}{n!} = \eu{\frac{1}{2}} - 1
	\end{align*}
	Puisque $\ds{\sum_{n=1}^{+\infty} a_n = \sum_{n=0}^{+\infty} a_n - a_0}$.
	Ainsi, \[ \boxed{\PP(G) = \sqrt{\E}-1 } \]
* On obtient $\PP(G) \approx 0,65$. On peut ainsi dire que le jeu ne semble pas équilibré. 
[enum=]
[cor=]
%%% Fin exercice %%%

[==* Autres exercices]

	
%%% Exercice %%%
[=exo 20 1 Chaîne de Markov]
\label{exo-td-16-1}
On considère une particule se déposant à chaque seconde sur l'un des trois sommets $A,B,C$ d'un triangle selon le procédé suivant :
\begin{itemize}
    \item[$\bullet$] si la particule se trouve en $B$, elle y reste.
    \item[$\bullet$] si la particule se trouve en $A$, elle se trouve à la seconde suivante sur l'un des trois sommets de façon équiprobable.
    \item[$\bullet$] si la particule se trouve en $C$, à la seconde suivante, elle y reste une fois sur trois, et elle va en $B$ sept fois plus souvent qu'en $A$.
    \item[$\bullet$] à la première seconde, elle se pose au hasard sur l'un des trois sommets.
\end{itemize}
Pour tout $n\geq 1$, on note $A_n$ (resp. $B_n, C_n$) l'événement : ``à la $n^{ieme}$ seconde, la particule se trouve en $A$ (resp. $B,C$)''. On note $a_n,b_n,c_n$ les probabilités des événements $A_n, B_n, C_n$.
\begin{enumerate}
    \item Que valent $a_1, b_1, c_1$ ?
    \item Donner une relation de récurrence entre $a_{n+1}, b_{n+1}, c_{n+1}$ et $a_n, b_n, c_n$.
    \item Montrer que pour tout $n\geq 1$,
     $$c_n=\frac{1}{2^n}-\frac{1}{6^n}$$
     (\textit{On pourra d'abord montrer que $(c_n)$ est une suite récurrente linéaire d'ordre $2$}).\\
     En déduire $a_n$ et $b_n$ en fonction de $n$.
     \item Etudier la convergence des suites $(a_n), (b_n), (c_n)$. Intérpreter.
\end{enumerate}
[exo=]

[=cor]
\begin{enumerate}
	\item A la première seconde, la particule se pose au hasard sur l'un des trois sommets, donc $\displaystyle{a_1=b_1=c_1=\frac{1}{3}}$.
	\item Remarquons que, par définition, $(A_n,B_n,C_n)$ est un système complet d'événements. D'après la formule des probabilités totales, et en utilisant l'énoncé :
		$$a_{n+1}=\PP(A_{n+1})=\PP(A_n)\PP_{A_n}(A_{n+1})+\PP(B_n)\PP_{B_n}(A_{n+1})+\PP(C_n)\PP_{C_n}(A_{n+1})=\frac{1}{3}a_n+ 0\times b_n + \frac{1}{12}c_n$$ 
		$$b_{n+1}=\PP(B_{n+1})=\PP(A_n)\PP_{A_n}(B_{n+1})+\PP(B_n)\PP_{B_n}(B_{n+1}) + \PP(C_n)\PP_{C_n}(B_{n+1})= \frac{1}{3}a_n  + 1\times b_n  + \frac{7}{12}c_n$$
		$$c_{n+1}=\PP(C_{n+1})=\PP(A_n)\PP_{A_n}(C_{n+1}) + \PP(B_n)\PP_{B_n}(C_{n+1}) + \PP(C_n)\PP_{C_n}(C_{n+1}) = \frac{1}{3}a_n + 0\times b_n + \frac{1}{3}c_n$$
		La seule difficulté est de déterminer $\PP_{C_n}(A_{n+1})$ et $\PP_{C_n}(B_{n+1})$. Si on note $a=\PP_{C_n}(A_{n+1})$ et $b=\PP_{C_n}(B_{n+1})$, l'énoncé indique que $b=7a$. De plus, puisque $\PP_{C_n}$ est une loi de probabilité, on doit avoir $\PP_{C_n}(A_{n+1})+\PP_{C_n}(B_{n+1}) +\PP_{C_n}(C_{n+1})=1$, soit \[ a+7a+\frac{1}{3}=1 \]
		ce qui donne $a=\frac{1}{12}$ et $b=\frac{7}{12}$. 
	\item Pour tout $n\geq 0$, en utilisant les relations précédentes, on a
		$$c_{n+2}=\frac{1}{3}a_{n+1}+\frac{1}{3}c_{n+1}=\frac{1}{3}(\frac{1}{3}a_n+\frac{1}{12}c_n)+\frac{1}{3}c_{n+1}= \frac{1}{3}\frac{1}{3}a_n + \frac{1}{3}\frac{1}{12}c_n+\frac{1}{3}c_{n+1}$$
		Or $\displaystyle{c_{n+1}=\frac{1}{3}a_n+\frac{1}{3}c_n \Leftrightarrow \frac{1}{3}a_n=c_{n+1}-\frac{1}{3}c_n}$ donc :
		
		$$c_{n+2}=\frac{1}{3}\left(c_{n+1}-\frac{1}{3}c_n\right) + \frac{1}{3}\frac{1}{12}c_n+\frac{1}{3}c_{n+1} = \frac{2}{3}c_{n+1} -\frac{1}{12}c_n$$
		Ainsi, $(c_n)$ est une suite récurrente linéaire d'ordre $2$. En étudiant son équation caractéristique $X^2-\frac{2}{3}X+\frac{1}{12}$, on obtient deux racines $x_1=\frac{1}{6}$ et $x_2=\frac{1}{2}$. Puisque $c_1=\frac{1}{3}$ et $c_2=\frac{2}{9}$, on obtient après résolution du système 
		$$\boxed{\forall~n\geq 1,~~c_n=\frac{1}{2^n}-\frac{1}{6^n}}$$
		En utilisant les relations vues en $2.$, on obtient
		$$a_n=3c_{n+1}-c_n = 3\left(\frac{1}{2^{n+1}}-\frac{1}{6^{n+1}}\right) -\left(\frac{1}{2^n} - \frac{1}{6^n}\right) = \frac{3}{2\times 2^{n}} - \frac{1}{2\times 6^{n}} - \frac{1}{2^n}+\frac{1}{6^n}$$
		ainsi,
		$$\boxed{\forall~n\geq 1,~~a_n=\frac{1}{2^{n+1}}+\frac{1}{2\times 6^n}}$$
		Enfin, $a_n+b_n+c_n=1$ puisque $(A_n,B_n,C_n)$ est un système complet d'événements, donc
		$$b_n=1-a_n-c_n=1-\left(\frac{1}{2^{n+1}}-\frac{1}{2\times 6^n}\right) - \left(\frac{1}{2^n}-\frac{1}{6^n}\right)$$
		soit
		$$\boxed{\forall~n\geq 1,~~b_n=1+\frac{1}{2\times 6^n}-\frac{3}{2^{n+1}}}$$
	\item Puisque $-1<\frac{1}{2}<1$ et $-1< \frac{1}{6}<1$, on obtient par passage à la limite 
		$$\lim_{n\rightarrow +\infty} a_n=\lim_{n\rightarrow +\infty} c_n=0 \textrm{   et   } \lim_{n\rightarrow +\infty} b_n=1$$
		Ainsi, plus on évolue dans le temps, plus la probabilité que la particule soit en $B$ est grande, et en $A$ et $C$ est faible, ce qui est cohérent, vue la définition.
\end{enumerate}
[cor=]
%%% Fin exercice %%%

%%% Exercice %%%
[=exo 20 1 Un jeu]
\label{exo-td-16-2}
On dispose de deux pièces identiques d'apparence, la pièce $A$ donnant Pile avec une probabilité $a$, et la pièce $B$ donnant Pile avec une probabilité $b$. \\Pour le premier lancer du jeu, on choisit une pièce au hasard et pour les coups suivants, on adopte la stratégie suivante : si on obtient Pile, on garde la pièce pour le lancer suivant, sinon on change de pièce pour le lancer suivant.\\On note, pour tout $k\geq 1$, $A_k$ l'événement ``le $k$-ième  lancer se fait avec la pièce $A$'', $B_k=\overline{A_k}$ et $E_k$ l'événement ``le $k$-ième lancer amène Pile''.
\begin{enumerate}
    \item Trouver une relation entre $\PP(E_k)$ et $\PP(A_k)$.
    \item Trouver une relation entre $\PP(A_{k+1})$ et $\PP(A_k)$.
    \item En déduire $\PP(A_k)$ et $\PP(E_k)$.
\end{enumerate}
[exo=]

[=cor]
\begin{enumerate}
	\item Remarquons que $(A_k, B_k)$ forme un système complet d'événements. D'après la formule des probabilités totales,
		$$\PP(E_k)=\PP(A_k\cap E_k)+\PP(B_k\cap E_k)=\PP(A_k)\PP_{A_k}(E_k)+\PP(B_k)\PP_{B_k}(E_k)$$
		or, d'après l'énoncé, on fait Pile avec la pièce $A$ avec une probabilité $a$, et avec la pièce $B$ avec une probabilité $b$. Ainsi,
		$$\PP(E_k)=\PP(A_k)a + \PP(B_k)b = a\PP(A_k)+b(1-\PP(A_k))$$
		et donc
		$$\boxed{\PP(E_k)=(a-b)\PP(A_k)+b}$$
	\item Puisque $(A_k,B_k)$ forme un système complet d'événements, d'après la formule des probabilités totales, on a
		$$\PP(A_{k+1})=\PP(A_k\cap A_{k+1}) + \PP(B_k\cap A_{k+1}) = \PP(A_k)\PP_{A_k}(A_{k+1}) + \PP(B_k)\PP_{B_k}(A_{k+1})$$
		D'après l'énoncé, si on a la pièce $A$ au $k^{ieme}$ lancer, on garde cette pièce si on fait Pile, donc avec une probabilité $a$. Si on a la pièce $B$ au $k^{ieme}$ lancer, on prend la pièce $A$ si on fait Face, donc avec une probabilité $1-b$. Ainsi,
		$$\PP(A_{k+1}) = \PP(A_k)a + \PP(B_k)(1-b) = a\PP(A_k) + (1-\PP(A_k))(1-b)$$
		soit
		$$\boxed{\PP(A_{k+1}) = (a+b-1)\PP(A_k)+1-b}$$
	\item La relation précédente nous indique que la suite $(\PP(A_k))$ est une suite arithmético-géométrique. Après étude classique, on obtient :
		\begin{itemize}
			\item[$\bullet$] que le point fixe est $l= \frac{1-b}{2-a-b}$
			\item[$\bullet$] que la suite $u$ définie pour tout $k\geq 1$ par $u_k=\PP(A_k)-l$ est géométrique, de raison $(a+b-1)$, de premier terme $u_1=\PP(A_1)-l = \frac{1}{2}-l$.
			\item[$\bullet$] enfin, que 
			$$\boxed{\forall~k\geq 1,~~\PP(A_k) = \left(\frac{1}{2}-\frac{1-b}{2-a-b}\right) \left(a+b-1\right)^{k-1} + \frac{1-b}{2-a-b}}$$
			Enfin, puisque $\PP(E_k)=(a-b)\PP(A_k)+b$, on a
			$$\forall~k\geq 1,~~\PP(E_k)=(a-b)\left(\left(\frac{1}{2}-\frac{1-b}{2-a-b}\right) \left(a+b-1\right)^{k-1} + \frac{1-b}{2-a-b}\right) + b$$
		\end{itemize}
\end{enumerate}
\remarqueL{Si $a=b=\frac{1}{2}$, le résultat se simplifie en 
$$\PP(A_k) = \frac{1}{2} \textrm{  et  } \PP(E_k)=\frac{1}{2}$$
ce qui est cohérent (cas d'équiprobabilité)}
[cor=]
%%% Fin exercice %%%

%%% Exercice %%%
[=exo 30 1 Un autre jeu]
\label{exo-td-16-3}
Deux joueurs $A$ et $B$ jouent chacun avec deux dés équilibrés. $A$ gagnera en amenant un total de $7$, et $B$ en amenant un total de $6$. $B$ joue le premier et ensuite (si nécessaire), $A$ et $B$ jouent alternativement. Le jeu s'arrête dès que l'un des deux gagne.
\begin{enumerate}
    \item Déterminer la probabilité des événements $G_A$ (resp $G_B$) : ``le joueur $A$ obtient $7$'' (resp ``le joueur $B$ obtient $6$'')
    \item On introduit les événements $B_n$ (resp. $A_n$) : ``le joueur $B$ (resp $A$) gagne à son $n$-ième lancer''. Déterminer la probabilité de ces événements.
    \item On note $V_A$ (resp. $V_B$) l'événement ``le jouer $A$ (resp. $B$) gagne''. Décrire ces deux événements en fonction des événements précédents. En déduire la probabilité de $V_A$ et $V_B$. Le jeu est-il équilibré ?
\end{enumerate}
[exo=]

[=cor]
\begin{enumerate}
	\item On s'intéresse à la somme de deux dés. Notre univers est donc $\Omega = \llbracket 1,6 \rrbracket ^2$, de cardinal $36$ et toutes les issues sont équiprobables. Pour obtenir $6$, il y a $5$ issues possibles ($1-5,5-1, 2-4, 4-2, 3-3$), et pour obtenir $7$, il y en a $6$ ($1-6, 6-1, 2-5, 5-2, 4-3, 3-4$). Ainsi,
		$$\PP(G_A)=\frac{6}{36}=\frac{1}{6} \textrm{  et  } \PP(G_b)=\frac{5}{36}$$
	\item Pour que $B$ gagne à son $n^{ieme}$ lancer, il faut que $B$ ait perdu à chacun de ses $(n-1)^{ieme}$ lancers, et $A$ aussi. On a alors par la formule des probabilités composées :
		$$\PP(B_n)=\PP(\overline{B_1}\cap \overline{A_1}\cap \cdots \cap \overline{B_{n-1}}\cap \overline{A_{n-1}}\cap \overline{B_n}) = \PP(\overline{B_1}) \PP_{\overline{B_1}}(\overline{A_1})\cdots \PP_{\overline{B_1}\cap \overline{A_1}\cap \cdots \cap \overline{B_{n-1}}\cap \overline{A_{n-1}}}(B_n)$$
		Or les lancers sont indépendants, donc 
		\begin{align*}\PP(B_n)&=\underbrace{\PP(\overline{G_B}) \times \PP(\overline{G_A}) \times \hdots \times \PP(\overline{G_B}) \times \PP(\overline{G_A})}_{n-1\text{ fois}} \times \PP(G_B)\\
		&=\left(1-\frac{5}{36}\right)^{n-1}\left(1-\frac{1}{6}\right)^{n-1}\frac{5}{36}
		\end{align*}
		soit
		$$\forall~n\geq 1,~~\PP(B_n)=\left(\frac{31 \times 5}{216}\right)^{n-1} \frac{5}{36}=\frac{5}{36}\left(\frac{155}{216}\right)^{n-1}$$
		Par le même raisonnement, il faut que $B$ perde ses $n$ lancers, $A$ ses $n-1$ premiers lancers. Ainsi,
		$$\PP(A_n)=\left(1-\frac{5}{36}\right)^n \left(1-\frac{1}{6}\right)^{n-1}\frac{1}{6}$$
		soit
		$$\forall~n\geq1,~~\PP(A_n)=\frac{31}{216} \left( \frac{155}{216}\right)^{n-1}$$
	
		\item Remarquons que, pour tout $n$,
			$$V_A=\bigcup_{n=1}^{+\infty} A_n \textrm{  et  } V_B=\bigcup_{n=1}^{+\infty} B_n$$
			Les événements $(A_n)$ étant deux à deux incompatibles, on a alors, d'après la propriété de la limite monotone : 
			$$\PP(V_A)=\sum_{n=1}^{+\infty} \PP(A_n) \textrm{  et  } \PP(V_B)=\sum_{n=1}^{+\infty} \PP(B_n)$$
		On reconnait dans les deux cas une série géométrique de raison $-1<\frac{155}{216}<1$ qui converge, et on a 
		$$\PP(V_A)=\sum_{n=1}^{+\infty} \frac{31}{216} \left( \frac{155}{216}\right)^{n-1} = \frac{31}{216} \frac{1}{1-\frac{155}{216}}=\frac{31}{61}$$
		$$\PP(V_B)=\sum_{n=1}^{+\infty} \frac{5}{36}\left(\frac{155}{216}\right)^{n-1} = \frac{5}{36} \frac{1}{1-\frac{155}{216}}=\frac{30}{61}$$
		Le jeu n'est donc pas équilibré.\\
		Remarquons également que $\PP(V_A)+\PP(V_B)=1$, ce qui est normal car $(V_A;V_B)$ forme un système complet d'événements.
\end{enumerate}
[cor=]
%%% Fin exercice %%%

%%% Exercice %%%
[=exo 20 2 Des boules]
\label{exo-td-16-4}
Soit $n>0$. On dispose d'une boite $A$ contenant $n$ boules numérotées de $1$ à $n$, et de $n$ boîtes $A_1\cdots A_n$. Pour tout $k\in \llbracket 1,n\rrbracket$, la boite $A_k$ contient $k$ boules numérotées de $1$ à $k$.\\
On tire au hasard une boule de $A$. En désignant $k$ le numéro obtenu, on tire alors au hasard une boule dans la boite $A_k$. Soit $X$ la variable aléatoire égale au numéro de la boule obtenue à l'issue du 2e tirage. Déterminer la loi de $X$.
[exo=]

[=cor]
Notons $E_i$ l'événement ``la boule $i$ est tirée dans l'urne $A$'', et $F^i_j$ l'événement ``la boule $j$ est tirée de l'urne $i$''. Remarquons que, par définition,
	$$\forall~i \in \llbracket 1;n \rrbracket,~\PP(E_i)=\frac{1}{n}$$
	et
	$$\forall~i \in \llbracket 1;n \rrbracket,~~\forall~j \in \llbracket 1;i \rrbracket,~~\PP(F_j^i) = \frac{1}{i} \textrm{  et  } \PP(F_j^i) = 0 \textrm{ si } j > i$$
	La famille $(E_i)$ forme un système complet d'événements. D'après la formule des probabilités totales,
	$$\forall~j \in \llbracket 1;n \rrbracket,~~\PP(X=j) = \sum_{i=1}^n \PP(E_i \cap (X=j)) = \sum_{i=1}^n \PP(E_i)\PP_{E_i}(X=j)$$
	Or, par construction, $\displaystyle{\PP_{E_i}(X=j)=\PP(F^i_j)}$. Ainsi
	$$\forall~j \in \llbracket 1;n \rrbracket,~~\PP(X=j)=\sum_{i=1}^n \PP(E_i)\PP(F^i_j) = \sum_{i=j}^n \frac{1}{n} \frac{1}{i}$$
	et donc
	$$\boxed{\forall~j \in \llbracket 1;n \rrbracket,~~\PP(X=j) = \frac{1}{n} \sum_{i=j}^n \frac{1}{i}}$$
[cor=]

[==* Sur les chaînes de Markov]

%%% Exercice %%%
[=exo 45 2 Présentation des chaînes de Markov]
\label{exo-td-16-9}

\subsubsection*{Calcul matriciel}
\noindent On définit les matrices $A,B$ par $A=\matrice{
4 & 4 & 4 \\ 
3 & 3 & 6 \\ 
3 & 6 & 3}
$ et $B=\frac{1}{12}A.$ 

\begin{enumerate}
\item On pose $P=\matrice{
0 & -8 & 1 \\ 
-1 & 3 & 1 \\ 
1 & 3 & 1
}$.

\begin{enumerate}
\item Vérifier   que $P$ est inversible d'inverse $P^{-1}=\dfrac{1}{22}\matrice{0& -11 & 11 \\ 
-2 & 1 & 1 \\ 
6 & 8 & 8}
$. 
\item Calculer $P^{-1}BP$. En déduire une matrice diagonale $D$ telle que $B=PDP^{-1}$.
\item Montrer par récurrence que $\forall n\geq0,$ $B^{n}=PD^{n}P^{-1}$.
\item Déterminer l'expression de la matrice $B^{n}$ en fonction de $n$.
\end{enumerate}
\end{enumerate}

\subsubsection*{Application à un processus aléatoire}

\noindent Un distributeur de jouets distingue trois catégories de jouets :%
\begin{description}
\item $T$ : les jouets traditionnels tels que poupées, peluches ;
\item $M$ : les jouets liés à la mode inspirés directement d'un livre, un film,
une émission ;
\item $S$: les jouets scientifiques vulgarisant une technique récente.
\end{description}

Il estime que
\begin{enumerate}[label=(\roman*)]
\item Le client qui a acheté un jouet traditionnel une année pour Noel
choisira, l'année suivante, un jouet de l'une des trois catégories avec une équiprobabilité ;
\item Le client qui a acheté un jouet inspiré par la mode optera l'année
suivante pour un jouet $T$ avec la probabilité $\frac{1}{4}$, pour un jouet 
$M$ avec la probabilité $\frac{1}{4}$, pour un jouet $S$ avec la probabilité
$\frac{1}{2}$;
\item Le client qui a acheté un jouet scientifique optera l'année
suivante pour un jouet $T$ avec la probabilité $\frac {1}{4}$, pour un jouet $M$
avec la probabilité $\frac{1}{2}$, pour un jouet $S$ avec la probabilité $%
\frac{1}{4}$.
\end{enumerate}
Le volume des ventes de ce commerçant vient de se composer d'une part $p_{0}=%
\frac{45}{100}$ de jouets de la catégorie $T$, d'une part $q_{0}=\frac{25}{%
100}$ de jouets de la catégorie $M$ et d'une part $r_{0}=\frac{30}{100}$ de
jouets de la catégorie $S$.

On désigne par $p_{n},q_{n},r_{n}$, les probabilités respectives des jouets $%
T$, $M$, $S$ dans les ventes du distributeur le $n$-ième Noël suivant.

\begin{enumerate}
\item Montrer que le triplet $(p_{n+1},q_{n+1},r_{n+1})$ s'exprime en
fonction du triplet $(p_{n},q_{n},r_{n})$.
\item Déterminer une matrice $C$ telle que $\matrice{
p_{n+1} \\ 
q_{n+1} \\ 
r_{n+1}} =C\matrice{
p_{n} \\ 
q_{n} \\ 
r_{n}}$.
\item Montrer que $\forall n\geq0,$ $\matrice{p_{n} \\ q_{n} \\ r_{n}} =C^{n}\matrice{ p_{0} \\ q_{0} \\ r_{0}}$.
\item Exprimer $(p_{n},q_{n},r_{n})$ en fonction de $n$.

\item Quelles parts à long terme les trois catégories de jouets représenteront-elles dans la vente si l'attitude des consommateurs reste
constante?
\end{enumerate}

\subsubsection*{Résumé}

Le processus décrit dans cet exercice est un exemple de chaîne de Markov. Pour simplifier les notations, nous allons coder 
les catégories $T$, $M$ et $S$ par les nombres $0$, $1$ et $2$ et introduire la suite de variables aléatoires $(X_n)$ , 
où $X_n$ vaut  $0$, $1$ ou $2$ selon que le client choisit la $n$-ème année un jouets de la 
catégorie $T$, $M$ et $S$. On a ainsi $p_n=P(X_n=0)\dots$.

La caractéristique d'une chaîne de Markov est que la valeur de $X_n$ (plus exactement la probabilité qu'elle prenne chaque 
valeur) ne dépend que de celle de $X_{n-1}$ et non de ce qui s'est passé avant. C'est bien le cas dans notre exemple.
La matrice $C$ est appelée \textbf{matrice de transition} de la chaîne.
\begin{enumerate}
\item Quelle propriété possède $C$ ?
 \item Représenter les probabilités de passage de $X_n$ d'une valeur à une autre sur un graphe.
 \item Notons $p_{\infty}$, $q_{\infty}$, $r_{\infty}$ les limites de $(p_n)$, $(q_n)$, $(r_n)$ ; et $\pi_{\infty}=\matrice{p_{\infty} \\ q_{\infty} \\ r_{\infty}}$? Que vaut $C\pi_{\infty}$ ?\\
Cette convergence des probabilités vers l'état stable, indépendemment de l'état initial, est une propriété importante
des chaînes de Markov.

Pour des renseignements supplémentaires sur les chaînes de markov :
\lien{http://images.math.cnrs.fr/Markov-et-la-Belle-au-bois-dormant}
\end{enumerate}
[exo=]

[=cor]
\subsubsection*{Calcul matriciel}
\begin{enumerate}
	\item ~\begin{enumerate}
	\item Deux méthodes : on applique la méthode du Pivot pour démontrer que $P$ est inversible, et trouver $P^{-1}$. Ou alors, on calcule $P\times P^{-1}$ et on constate que ça fait $I_3$.
	\item Après calculs, on obtient $P^{-1}BP=\matrice{-\frac{1}{4}&0&0\\0&\frac{1}{12}&0\\0&0&1}$. En posant $D=\matrice{-\frac{1}{4}&0&0\\0&\frac{1}{12}&0\\0&0&1}$, on a alors $B=PDP^{-1}$.
	\item Par récurrence classique, en utilisant, pour l'hérédité : 
	\[ B^{n+1}=B^n B = (PD^nP^{-1})(PDP^{-1}) = PD^n\underbrace{P^{-1}P}_{=I_3}DP^{-1}=PD^{n+1}P^{-1} \]
	\item En utilisant ce qui précède, on calcule $PD^nP^{-1}$, en remarquant que $D$ est diagonale, donc $D^n$ est facile à calculer. On obtient :
	\begin{align*} B^n &= \matrice{
0 & -8 & 1 \\ 
-1 & 3 & 1 \\ 
1 & 3 & 1
} \matrice{\left(-\frac{1}{4}\right)^n&0&0\\0&\left(\frac{1}{12}\right)^n&0\\0&0&1} \dfrac{1}{22}\matrice{0& -11 & 11 \\ 
-2 & 1 & 1 \\ 
6 & 8 & 8} \\
 &= \frac{1}{22}\matrice{
0 & -8 & 1 \\ 
-1 & 3 & 1 \\ 
1 & 3 & 1
}  \matrice{0 & -11\left(-\frac{1}{4}\right)^n & 11\left(-\frac{1}{4}\right)^n \\
 -2\left(\frac{1}{12}\right)^n & \left(\frac{1}{12}\right)^n & \left(\frac{1}{12}\right)^n\\ 6 & 8 & 8}\\
 &= \frac{1}{22}\matrice{16\left(\frac{1}{12}\right)^n + 6 & -8\left(\frac{1}{12}\right)^n+8 & -8\left(\frac{1}{12}\right)^n+8 \\ -6\left(\frac{1}{12}\right)^n+6 & 11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8 & -11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8\\ -6\left(\frac{1}{12}\right)^n+6 & -11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8 & 11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8}
 \end{align*}
	\end{enumerate}
\end{enumerate}
\subsubsection*{Application à un processus aléatoire}


On notera $T_n$ l'évènement ``le client achète un jouet traditionnel l'année $n$'' (et de même pour $M_n$ et $S_n$).
\begin{enumerate}
	\item On applique la formule des probabilités totales, au système complet d'évènements $(T,M,S)$ qui par construction est un système complet d'évènements. Ainsi 
	\begin{align*}
	 p_{n+1}=\PP(T_{n+1}) &= \PP(T_{n+1}\cap T_n) + \PP(T_{n+1} \cap M_n) + \PP(T_{n+1}\cap S_n) \\
	 &= \PP(T_n)\PP_{T_n}(T_{n+1}) +\PP(M_n)\PP_{M_n}(T_{n+1}) + \PP(S_n)\PP_{T_n}(T_{n+1})\\ 
	 &= p_n\frac{1}{3}+q_n\frac{1}{3}+r_n\frac{1}{3}
	\end{align*}
	Par le même raisonnement, on obtient :
	\begin{align*}
	 q_{n+1}=\PP(M_{n+1}) &= \frac{1}{4}p_n+	\frac{1}{4}q_n+\frac{1}{2}r_n \\
	 r_{n+1}=\PP(S_{n+1}) &= \frac{1}{4}p_n+\frac{1}{2}q_n+\frac{1}{4}r_n
	\end{align*}
	\item Ainsi, on obtient \[
	\matrice{p_{n+1}\\q_{n+1}\\r_{n+1}} = \matrice{\frac{1}{3}&\frac{1}{3}&\frac{1}{3}\\\frac{1}{4}&\frac{1}{4}&\frac{1}{2}\\\frac{1}{4}&\frac{1}{2}&\frac{1}{4}} \matrice{p_n\\q_n\\r_n}=B \matrice{p_n\\q_n\\r_n} \]
	\item Par récurrence habituelle (en partant de $C^0=I_3$ et en utilisant la relation vue à la question d'avant).
	\item Ainsi, en utilisant la partie précédente :
	\begin{align*}
	 \matrice{p_n\\q_n\\r_n} &= 	\frac{1}{22}\matrice{16\left(\frac{1}{12}\right)^n + 6 & -8\left(\frac{1}{12}\right)^n+8 & -8\left(\frac{1}{12}\right)^n+8 \\ -6\left(\frac{1}{12}\right)^n+6 & 11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8 & -11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8\\ -6\left(\frac{1}{12}\right)^n+6 & -11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8 & 11\left(-\frac{1}{4}\right)^n+3\left(\frac{1}{12}\right)^n+8} \matrice{\frac{45}{100}\\\frac{25}{100}\\\frac{30}{100}}
	\end{align*}
	dont le calcul est long...
	\item On cherche la limite quand $n$ tend vers l'infini. Puisque $-1<\frac{1}{12}<1$ et $-1<-\frac{1}{4}<1$, la plupart des termes tend vers $0$. On obtient au final la limite : 
	\[ \matrice{p_{\infty}\\q_{\infty}\\r_{\infty}} = \matrice{\frac{71}{220}\\\frac{71}{220}\\\frac{71}{220}}\]
\end{enumerate}

\subsubsection*{Résumé}

\begin{enumerate}
	\item La somme des termes d'une ligne vaut $1$, qui traduit le fait que la valeur de l'état $n+1$ ne dépend que de l'état $n$.
	\item On obtient le graphe suivant : \begin{center}\includegraphics{tex/Chap16/pic/graph2.eps}\end{center}
	\item Puisque $\matrice{p_{n+1}\\q_{n+1}\\r_{n+1}}=C\matrice{p_n\\q_n\\r_n}$, par passage à la limite (qui est légitime par somme et produit) \[ \pi_{\infty} = C\pi_{\infty} \] 
\end{enumerate}
[cor=]
%%% Fin exercice %%%

[/exercices]
