chapter:16
title:Espaces probabilisés infinis

[intro]
L'objectif de ce chapitre est de généraliser la notion d'espace probabilisé, que nous avons vu dans le chapitre 6, à des espaces infinis.
[/]

[objectifs]

[=num]
* \hyperref[objectif-16-1]{Connaître la notion de tribu}\dotfill $\Box$
* \hyperref[objectif-16-2]{Connaître la notion de système complet d'évènements dans le cas général}\dotfill $\Box$
* \hyperref[objectif-16-3]{Connaître les propriétés d'une probabilité}\dotfill $\Box$
* \hyperref[objectif-16-4]{Savoir utiliser la propriété de la limite monotone}\dotfill $\Box$
* \hyperref[objectif-16-5]{Savoir démontrer qu'un évènement est négligeable et presque sûr}\dotfill $\Box$
* \hyperref[objectif-16-6]{Connaître la définition d'une probabilité conditionnelle}\dotfill $\Box$
* \hyperref[objectif-16-7]{Savoir utiliser la formule des probabilités composées}\dotfill $\Box$
* \hyperref[objectif-16-8]{Savoir utiliser la formule des probabilités totales}\dotfill $\Box$
* \hyperref[objectif-16-9]{Connaître la formule de Bayes}\dotfill $\Box$
* \hyperref[objectif-16-10]{Savoir manipuler des évènements mutuellement indépendants}\dotfill $\Box$
* \hyperref[objectif-16-11]{Connaître la définition d'une variable aléatoire}\dotfill $\Box$
* \hyperref[objectif-16-12]{Savoir déterminer le support et la loi d'une variable aléatoire}\dotfill $\Box$
* \hyperref[objectif-16-13]{Connaître la définition et les propriétés de la fonction de répartition}\dotfill $\Box$ 
[num=]
[NP]

%%%% Début du cours %%%%
[= Espace probabilisable]

[== Tribu]
    
Pour un espace probabilisé fini, si on note $\Omega$ l'univers, on utilisait comme ensemble des événements $\mathcal{P}(\Omega)$, l'ensemble des parties de $\Omega$. Pour un ensemble $\Omega$ infini, il est peu judicieux de prendre toutes les parties comme ensemble des événements. On va prendre certains ensembles seulement, en imposant des propriétés essentielles pour pouvoir parler d'événements, puis plus tard, de probabilité.
[.definition]
\label{objectif-16-1}
Soit $\Omega$ un ensemble quelconque. Soit $\mathcal{A}$ un ensemble de sous-ensembles de $\Omega$ ($\mathcal{A} \subset \mathcal{P}(\Omega)$). On dit que $\mathcal{A}$ est une \textbf{tribu} (ou une \textbf{$\sigma$-algèbre}) s'il vérifie les propriétés suivantes :
[=item]
* $\Omega \in \mathcal{A}$;
* $\mathcal{A}$ est stable par passage au complémentaire : $\forall~A \in \mathcal{A}, \overline{A} \in \mathcal{A}$;
* $\mathcal{A}$ est stable par union dénombrable : si pour tout $i\in \N$, $A_i\in \mathcal{A}$, alors
        \[\bigcup_{i=0}^{+\infty} A_i \in \mathcal{A}\]
[item=]
[definition.]

[.exemple]
La tribu $\{ \vide; \Omega\}$ est une tribu, appelée \textbf{tribu grossière}. La tribu $\mathcal{P}(\Omega)$ est une tribu, appelée \textbf{tribu discrète}.
[exemple.]

[.proposition]
Soit $\mathcal{A}$ une tribu sur $\Omega$. 
[=item]
* $\vide \in \mathcal{A}$;
* $\mathcal{A}$ est stable par union et intersection finie;
* $\mathcal{A}$ est stable par intersection dénombrable : si pour tout $i\in \N$, $A_i\in \mathcal{A}$, alors
        \[\bigcap_{i=0}^{+\infty} A_i \in \mathcal{A}\]
[item=]
[proposition.]

[.demonstration]
Si $\Omega \in \mathcal{A}$, alors $\vide=\overline{\Omega}\in \mathcal{A}$. La troisième propriété découle de la stabilité par passage au complémentaire, et les lois de de Morgan.
[demonstration.]

[.remarque]
Lorsque l'ensemble $\Omega$ est fini, on prendra toujours comme tribu $\mathcal{P}(\Omega)$, puisque (en prenant de l'avance) on sait calculer la probabilité d'un ensemble fini.
[remarque.]

[afaire Exercice \lienexo{5}.]
[== Espace probabilisable]
    
[.definition]
Soit $\Omega$ un ensemble, et $\mathcal{A}$ une tribu sur $\Omega$. L'ensemble $(\Omega, \mathcal{A})$ est appelé \textbf{espace probabilisable}.
[definition.]

[.exemple]
L'ensemble $(\Omega, \mathcal{P}(\Omega))$ est un espace probabilisable.
[exemple.]

[.remarque]
Puisqu'on est dans le cadre des probabilités, si $(\Omega, \mathcal{A})$ est un espace probabilisable, tout ensemble $A\in \mathcal{A}$ est appelé \textbf{événement}. Ainsi, la tribu $\mathcal{A}$ contient l'ensemble des événements auxquels on va s'intéresser.
[remarque.]

[== Système complet d'évènements]
    
[.definition]
\label{objectif-16-2}
Soit $(\Omega, \mathcal{A})$ un espace probabilisable. Soit $(A_i)_{i\in \N}$ des événements de $\mathcal{A}$. On dit que $(A_i)$ est un \textbf{système complet d'événements} si
[=item]
* Les $A_i$ sont deux à deux disjoints : 
    \[\forall~i\neq j,~~ A_i \cap A_j=\vide\]
* La réunion des $A_i$ est égale à $\Omega$ :
    \[\bigcup_{i\in \N} A_i=\Omega\]
[item=]
[definition.]

[.remarque]
On dit que deux événements $A_i$ et $A_j$ sont \textbf{incompatibles} lorsqu'ils sont disjoints.
[remarque.]

[.exemple]
Si $\Omega=\N$, et $\mathcal{A}=\mathcal{P}(\N)$, alors les ensembles $A_i=\{i\}$, pour tout entier $i$, forment un système complet d'événements.
[exemple.]

[afaire Exercice \lienexo{6}.]
[= Probabilité et espace probabilisé]

[== Probabilité]
    
[.definition]
\label{objectif-16-3}
Soit $(\Omega, \mathcal{A})$ un espace probabilisable. On appelle \textbf{probabilité} sur $(\Omega, \mathcal{A})$ une application \\$\PP:\mathcal{A} \rightarrow [0;1]$ vérifiant 
[=item]
* (\textit{probabilité de l'événement certain}) ~~$\PP(\Omega)=1$.
* (\textit{$\sigma$-additivité}) ~~Pour toute suite d'événements $(A_i)_{i\in \N}$ \textbf{deux à deux incompatibles}, on a
    \[\PP\left( \bigcup_{i=0}^{+\infty} A_i \right) = \sum_{i=0}^{+\infty} \PP(A_i)\]
[item=]
[definition.]

[.remarque]
On utilisera souvent la $\sigma$-additivité sur des unions finies, plutôt qu'infinies.
[remarque.]

[== Propriétés]
 
[.propriete]
Soit $\PP$ une probabilité sur un espace probabilisable $(\Omega, \mathcal{A})$.
[=item]
* $\PP(\vide)=0$;
* Pour tout événement $A\in \mathcal{A}$, $\PP(\overline{A})=1-P(A)$;
* Si $A,B \in \mathcal{A}^2$ avec $A\subset B$, alors $\PP(A)\leq \PP(B)$.
[item=]
[propriete.]

[prof]
[.demonstration]
Les preuves reposent systématiquement sur l'additivité ou $\sigma$-additivité des probabilités :
[=item]
* $\Omega$ et $\vide$ sont incompatibles; par additivité de $\PP$, on a
    \[\PP(\vide \cup \Omega) = \PP(\vide) + \PP(\Omega) = \PP(\vide)+1\]
    Or $\vide \cup \Omega=\Omega$, donc $\PP(\Omega)=1=\PP(\vide)+1$, c'est-à-dire $\PP(\vide)=0$. 
* $A$ et $\overline{A}$ sont incompatibles par définition, et $A\cup \overline{A}=\Omega$. Donc, par additivité de $\PP$ :
    \[1=\PP(\Omega)=\PP(A\cup \overline{A})=\PP(A)+\PP(\overline{A})\]
* Si $A\subset B$, notons $C=B\backslash A=B\cap \overline{A}\in \mathcal{A}$. Par construction, $A$ et $C$ sont incompatibles, et $A\cup C =B$. Par additivité de $\PP$ :
    \[\PP(B)=\PP(A\cup C) =\PP(A)+\PP(C)\]
    Puisque $\PP(C)\geq 0$, on en déduit le résultat.
[item=]
[demonstration.]
[/prof 10]

[.theoreme]
Soit $\PP$ une probabilité sur un espace probabilisable $(\Omega, \mathcal{A})$. Soient $A$ et $B$ deux événements de $\mathcal{A}$. Alors
\[\PP(A\cup B) = \PP(A)+\PP(B)-\PP(A\cap B)\]
[theoreme.]


[prof]
[.demonstration]
Par construction, $A\cap B$ et $A\backslash B$ sont incompatibles, et\\ $(A\cap B) \cup (A\backslash B)=A$. Par additivité de $\PP$,\[\PP(A) = \PP(A\cap B)+\PP(A\backslash B)\]
Or $\PP(A\backslash B)+\PP(B)=\PP(A\cup B)$ à nouveau par additivité de $\PP$ ($B$ et $A\backslash B$ sont incompatibles).
Donc
\[\PP(A)=\PP(A \cap B)+\PP(A\cup B)-\PP(B)\]
[demonstration.]
[/prof 7]

[==  Espace probabilisé]

[.definition]
Soit $(\Omega, \mathcal{A})$ un espace probabilisable, et $\PP$ une probabilité sur $(\Omega, \mathcal{A})$. L'espace $(\Omega, \mathcal{A}, \PP)$ est appelé \textbf{espace probabilisé}.
[definition.]

[.remarque]
La plupart du temps, on ne cherchera pas l'espace probabilisé, il sera fixé une fois pour toute, sans être forcément explicitement donné.
[remarque.]

[= Propriétés des probabilités]

Dans cette section, on se donne un espace probabilisé $(\Omega, \mathcal{A}, \PP)$.

[== Propriété de la limite monotone]

[.definition] 
Soit $(A_i)_{i\in N}$ une suite d'événements de $\mathcal{A}$.
[=item]
* On dit que la suite $(A_i)$ est \textbf{croissante} si, pour tout entier $i$, \[A_i \subset A_{i+1}\]
* On dit que la suite $(A_i)$ est \textbf{décroissante} si, pour tout entier $i$, \[A_i \supset A_{i+1}\]
[item=]
[definition.]

[.theoreme Propriété de la limite monotone]
\label{objectif-16-4} 
Soit $(A_i)_{i\in \N}$ une suite d'événements de $\mathcal{A}$.
[=item]
* Si la suite $(A_i)$ est croissante, alors
    \[\PP\left( \bigcup_{n=0}^{+\infty} A_n \right) = \lim_{n\rightarrow +\infty} \PP(A_n)\]
* Si la suite $(A_i)$ est décroissante, alors
    \[\PP\left( \bigcap_{n=0}^{+\infty} A_n \right) = \lim_{n\rightarrow +\infty} \PP(A_n)\]
[item=]
[theoreme.]

[.exemple]
On possède une pièce bien équilibrée qu'on lance une infinité de fois. Déterminer la probabilité de n'obtenir jamais pile.
[exemple.]

[prof]
[.soluce]
On note $A_n$ l'événement ``n'obtenir aucun pile jusqu'au $n^\textrm{ième}$ lancer''. Alors, par construction 
$A_{n+1} \subset A_n$ (puisque si on n'a pas obtenu pile jusqu'au $n+1^\textrm{ième}$ lancer, c'est qu'on n'a pas obtenu pile jusqu'au $n^\textrm{ième}$ lancer). Puisque $\PP(A_n)=\left(\frac{1}{2}\right)^n$, on a
\[\PP\left( \bigcap_{n=0}^{+\infty} A_n \right) = \lim_{n\rightarrow +\infty} \PP(A_n)=0\]
Ainsi, la probabilité de n'obtenir jamais pile est nulle.
[soluce.]
[/prof 5]

La propriété de la limite monotone se généralise à des événements quelconques :

[.proposition]
Soit $(A_i)$ une suite d'événements de $\mathcal{A}$.Alors
\[\PP\left( \bigcup_{n=0}^{+\infty} A_n \right) = \lim_{n\rightarrow +\infty} \PP\left( \bigcup_{i=0}^{n} A_i \right)\]
\[\PP\left( \bigcap_{n=0}^{+\infty} A_n \right) = \lim_{n\rightarrow +\infty} \PP\left( \bigcap_{i=0}^{n} A_i \right)\]
[proposition.]

[.demonstration]
Il suffit de constater que $\displaystyle{\left( \bigcup_{i=0}^{n} A_i \right)_{n\in \N}}$ est une suite croissante, et $\displaystyle{\left( \bigcap_{i=0}^{n} A_i \right)_{n\in \N}}$ est une suite décroissante.
[demonstration.]

[afaire Exercices \lienexo{7} et \lienexo{8}.]

[== Evénement vrai presque sûrement]
    
L'exemple précédent nous a donné un événement de probabilité nulle, mais qui n'est pas l'événement impossible : en effet, on peut théoriquement n'obtenir jamais pile, en faisant systématiquement face.

[.definition]
\label{objectif-16-5}
Soit $A$ un événement de $\mathcal{A}$. 
[=item]
* On dit que $A$ est \textbf{négligeable} si $\PP(A)=0$.
* On dit que $A$ est \textbf{presque sûr} (ou \textbf{vrai presque sûrement}) si $\PP(A)=1$.
[item=]
[definition.]

[.exemple]
Dans l'exemple précédent, l'événement ``obtenir au moins une fois pile'' est presque sûr, et l'événement ``n'obtenir jamais pile'' est négligeable. 
[exemple.]

[.remarque]
Ce cas n'arrivait jamais dans un espace probabilisé fini. En effet, dans ce cas, $\PP(A)=0$ si et seulement si $A=\vide$. C'est une particularité des espaces probabilisés infinis, dont il faut se méfier.
[remarque.]

[== Probabilités conditionnelles]
    
La définition de probabilités conditionnelles, vue dans le cas des probabilités finies, se généralise aux espaces probabilisés quelconques.

[.definition]
\label{objectif-16-6}
Soient $A$ et $B$ deux événements, tels que $\PP(A)\neq 0$. On appelle \textbf{probabilité de $B$ sachant $A$}, et on note $\PP_A(B)$ le nombre
\[\PP_A(B)=\frac{\PP(A\cap B)}{\PP(A)}\]
La fonction $\PP_A:\mathcal{A}\rightarrow [0;1]$ est également une probabilité sur $(\Omega, \mathcal{A}, \PP)$.
[definition.]
    
[.theoreme Probabilités composées] 
\label{objectif-16-7}
Pour tous événements $A_1,\cdots,A_n$, on a
\[\PP(A_1\cap\cdots \cap A_n) = \PP(A_1) \PP_{A_1}(A_2) \PP_{A_1\cap A_2}(A_3)\cdots \PP_{A_1\cap \cdots \cap A_{n-1}}(A_n)\]
[theoreme.]    

[.theoreme Formule des probabilités totales] 
\label{objectif-16-8}
Soit  $(\Omega, \mathcal{A},  \PP)$ un espace probabilisé. Soit $(A_i)_{i\in \N}$ un système complet d'événements de $\Omega$, tel que pour tout $i\in \N,~ \PP(A_i)\neq 0$. Alors, pour tout événement $B$, on a
\[\PP(B)=\sum_{k=0}^{+\infty} \PP(B\cap A_k)=\sum_{k=0}^{+\infty} \PP(A_k) \PP_{A_k}(B)\]
En particulier,
\[\sum_{k=0}^{+\infty} \PP(A_k) = 1\]
[theoreme.]

[prof]
[.demonstration]
Par définition, \[ \bigcup_{k=0}^{+\infty} (A_k\cap B) = \left(\bigcup_{k=0}^{+\infty} A_k \right)\cap B = \Omega\cap B = B \]
De plus, puisque les $(A_i)$ sont deux à deux disjoints, les $(A_i\cap B)$ le sont également. Par $\sigma$-additivité, on en déduit alors que 
\[ \PP(B) = \sum_{k=0}^{+\infty} \PP(A_k \cap B) \]
De plus, puisque $\PP(A_k)\neq 0$ pour tout $k$, on peut conclure que \[ \PP(B) = \sum_{k=0}^{+\infty} \PP(A_k)\PP_{A_k}(B) \] 
[demonstration.]
[/prof 6]

[.exemple Exemple fondamental]
Si $A\in \mathcal{A}$ est un évènement, alors $(A, \overline{A})$ forme un système complet d'évènements. Ainsi, pour tout évènement $B$, on aura \[ \PP(B)=\PP(A\cap B)+\PP(\overline{A}\cap B) \]
[exemple.]

[.theoreme Formule de Bayes] 
\label{objectif-16-9}
Soient $A$ et $B$ deux événements de $\mathcal{A}$ de probabilité non nulle. Alors
\[\PP_B(A)=\frac{\PP(A)}{\PP(B)} \PP_A(B)\]
[theoreme.]

[prof]
[.demonstration]
Puisque $A$ et $B$ sont de probabilités non nulles, on peut écrire : 
\begin{align*}
 \PP(A\cap B) &= \PP(A)\PP_A(B) & & \text{et}  & \PP(A\cap B) &= \PP(B)\PP_B(A)
\end{align*}
On a alors 
\begin{align*}
 \PP_B(A) &= \frac{\PP(A\cap B)}{\PP(B)} \\
 &= \frac{\PP(A)\PP_A(B)}{\PP(B)} = \frac{\PP(A)}{\PP(B)}\PP_A(B)
\end{align*}
[demonstration.]
[/prof 5]

[== Indépendance mutuelle d’une suite infinie d’événements]
    
La notion d'indépendance mutuelle se généralise à une suite infinie d'événements :

[.definition]
\label{objectif-16-10}
Soit $(A_i)_{i\in\N}$ une suite infinie d'événements de $\mathcal{A}$. On dit que la suite $(A_i)$ est \textbf{mutuellement indépendante} si pour tout sous ensemble \underline{fini} $J\subset \N$, on a
\[\PP\left( \bigcap_{i\in J} A_i \right) = \prod_{i \in J} \PP(A_i)\]
[definition.]

[afaire Exercices \lienexo{1}, \lienexo{2} et \lienexo{3}.]

[= Variables aléatoires]

Dans cette section, on se donne un espace probabilisé $(\Omega, \mathcal{A}, \PP)$.

[== Définition]
	
[.definition]
\label{objectif-16-11}
Une \textbf{variable aléatoire} $X$ sur $(\Omega, \mathcal{A}, \PP)$ est une application $X:\Omega \rightarrow \R$ telle que, pour tout réel $x$, 
\[\left\{ \omega \in \Omega,~X(\omega) \leq x \right \} \in \mathcal{A}\]
On appelle \textbf{support} de $X$, et on note $X(\Omega)$ l'ensemble des valeurs prises par $X$.
[definition.]

[.remarque]
La condition imposée à $X$ permet simplement de pouvoir calculer la probabilité de plusieurs ensembles, du type $\{w\in \Omega, X(w)=x\}$, ... Sans cette condition, les ensembles qu'on manipule ne sont \textit{a priori} pas dans la tribu $\mathcal{A}$, et on ne peut donc pas \textit{a priori} calculer leur probabilité.
[remarque.]
	
[.exemple]
On lance un dé bien équilibré et on note $X$ le nombre de lancers nécessaires pour obtenir $6$. La variable aléatoire $X$ est à valeur dans $\N$, et $X(\Omega)=\N^*$ (il faut au moins un lancer pour obtenir $6$).
[exemple.]
	
[.remarque]
Si $X$ et $Y$ sont deux variables aléatoires définies sur un même espace probabilisé, alors $X+Y$, $\min(X,Y):\omega \mapsto \min(X(\omega),Y(\omega))$ et $\max(X, Y):\omega \mapsto \max(X(\omega),Y(\omega))$ sont également des variables aléatoires sur cet espace probabilisé.
[remarque.]

[.definition]
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$, et $g:\R \rightarrow \R$ une fonction. Alors la fonction \[g(X) : \begin{array}{l}\Omega \rightarrow \R\\\omega \mapsto g(X(\omega))\end{array}\] est également une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$.
[definition.]

[.exemple]
En prenant pour $g$ la fonction carrée, on peut définir la variable aléatoire $X^2: \omega \mapsto X(\omega)^2$.
[exemple.]

[== Propriétés]

[.definition]
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$, et $x\in \R$. On note
\[[X=x] = \{ \omega \in \Omega, X(\omega)=x \}\]
\[[X<x] = \{ \omega \in \Omega, X(\omega)<x \}\]
\[[X\leq x] = \{ \omega \in \Omega, X(\omega)\leq x \}\]
Si $I$ est une partie de $\R$, on note également 
\[[X\in I] = \{ \omega \in \Omega, X(\omega)\in I \}\]
Si $x$ et $y$ sont deux réels tels que $x<y$ alors on note
\[[x\leq X \leq y] = \{ \omega \in \Omega, x\leq X(\omega) \leq y\}\]
[definition.]

[.remarque]
Ces différents ensembles sont dans la tribu $\mathcal{A}$ par construction de $X$. Ainsi, on pourra calculer leur probabilité, et on notera par exemple
\[\PP(X=x) = \PP( [X=x] ) \textrm{ et } \PP(x\leq X \leq y) = \PP( [x\leq X \leq y])\]
[remarque.]

[.exo]
Dans l'exemple précédent, déterminer $\PP(X=1)$ et $\PP(X\leq 2)$.
[exo.]

[prof]
[.soluce]
$[X=1]$ correspond à l'ensemble des tirages ayant un $6$ au premier lancer; $[X\leq 2]$ correspond à l'ensemble des tirages ayant un $6$ au premier lancer, ou alors un $6$ au deuxième lancer mais pas au premier. Par construction,
$\PP(X=1)=\frac{1}{6}$ et $\PP(X\leq 2) = \frac{1}{6}+\frac{5}{6}\frac{1}{6}=\frac{11}{36}$.
[soluce.]
[/prof 7]

[.theoreme]
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$ et à valeur dans $\N$ ou $\Z$. Alors, la suite des $([X=i])_{i\in \N}$ (ou $([X=i])_{i\in \Z}$) est un système complet d'événement associé à la variable aléatoire $X$.
[theoreme.]

[.demonstration]
Les ensembles $[X=i]$, par construction, sont deux à deux disjoints, et puisque la variable aléatoire est à valeurs dans $\N$ (ou $\Z$), la réunion des $[X=i]$ recouvre bien $\Omega$.
[demonstration.]

[.exemple]
Dans l'exemple précédent, la variable aléatoire étant à valeur dans $\N^*$, la suite $([X=i])_{i\in \N^*}$ représente un système complet d'événements de $\Omega$.
[exemple.]

[== Loi d'une variable aléatoire]

[.definition]
\label{objectif-16-12}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$. On appelle \textbf{loi} de la variable aléatoire $X$, la donnée des $\PP(X=x)$ pour tout réel $x$.
[definition.]

[.remarque]
Contrairement aux variables aléatoires finies, où on donnait la loi de probabilités sous forme de tableau, il faudra ici donner une formule plus générale.
[remarque.]

[.exemple]
Dans le cas de notre lancer de dé, où $X$ désigne le premier lancer où on obtient $6$. Alors $\PP(X=1)=\frac{1}{6}$, $\PP(X=2)=\frac{5}{6}.\frac{1}{6}$, et plus généralement
\[P(X=i)=\left(\frac{5}{6}\right)^{i-1}\frac{1}{6}\]
[exemple.]

[.remarque]
Cette loi s'appelle loi géométrique. Nous y reviendrons plus tard.
[remarque.]

[.theoreme]
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$ et à valeur dans $\N$ ou $\Z$. Alors
\[\sum_{i \in X(\Omega)} \PP(X=i)=1\]
[theoreme.]

[.demonstration]
En effet, dans ce cas, la suite $([X=i])$ forme un système complet d'événements.
[demonstration.]

[.exemple]
Dans notre exemple, en utilisant la série géométrique (avec $-1\leq \frac{5}{6}<1$) : 
\[\sum_{i=1}^{+\infty} \PP(X=i)=\frac{1}{6}\sum_{i=1}^{+\infty} \left(\frac{5}{6}\right)^{i-1}=\frac{1}{6}\frac{1}{1-\frac{5}{6}}=1\]
[exemple.]

[.remarque]
Si $X(\Omega) \subset \Z$, on dit que la variable aléatoire est \textbf{discrète}.
[remarque.]

[afaire Exercice \lienexo{4}.]

[== Fonction de répartition]

[.definition]
\label{objectif-16-13}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$. On appelle \textbf{fonction de répartition} de $X$, et on note $F_X$ la fonction définie sur $\R$ par 
\[F_X : x \mapsto \PP(X\leq x)\]
[definition.]

[.propriete]
La fonction de répartition $F_X$ de $X$ est une fonction croissante, continue à droite en tout point, et telle que 
\[\lim_{x\rightarrow -\infty} F_X(x)=0 \textrm{ et } \lim_{x\rightarrow +\infty} F_X(x)=1\]
[propriete.]

[prof]
[.demonstration]
Si $x\leq y$, alors \[F_X(y)=P(X\leq y) = P(X\leq x) + P(x<X\leq y) \geq P(X\leq x)=F_X(x)\] donc $F_X$ est bien croissante. 
\[\lim_{x\rightarrow -\infty} P(X\leq x) = P(\vide)=0\textrm{ et } \lim_{x\rightarrow +\infty} P(X\leq x) = P(\Omega)=1\]
On admettra la continuité à droite en tout point.
[demonstration.]
[/prof 7]

[.theoreme]
Pour connaitre la loi de probabilité de $X$, il faut et il suffit de connaitre la fonction de répartition de $X$.
[theoreme.]

[.demonstration]
Dans le cas où $X$ est à valeur dans $\N$ ou $\Z$, on constate par exemple que \[\PP(X=i)=\PP(X\leq i) - \PP(X\leq i-1) = F_X(i)-F_X(i-1)\]
Le cas où $X$ est à valeur dans $\R$ quelconque est admis.
[demonstration.]
