chapter:18
title:Espaces vectoriels et applications linéaires

[intro]
Ce chapitre est très important et tombe régulièrement au concours. Il est abstrait, mais pas difficile. Il sera approfondi l'année prochaine. \\Il doit être maitrisé dans son ensemble.
[/]

[objectifs]

[=num]
* Connaître la définition d'espaces vectoriels et de sous-espaces vectoriels :
[=item]
* \hyperref[objectif-18-1]{Savoir démontrer qu'un ensemble est un sous-espace vectoriel d'un espace vectoriel}\dotfill $\Box$
* \hyperref[objectif-18-2]{Savoir montrer qu'un vecteur est une combinaison linéaire d'autres vecteurs}\dotfill $\Box$ 
[item=]
* Maitriser la notion de base :
[=item]
* \hyperref[objectif-18-3]{Savoir montrer qu'une famille est libre}\dotfill $\Box$
* \hyperref[objectif-18-4]{Savoir montrer qu'une famille est génératrice}\dotfill $\Box$
* \hyperref[objectif-18-5]{Savoir montrer qu'une famille est une base d'un espace vectoriel}\dotfill $\Box$
* \hyperref[objectif-18-6]{Connaître les bases canoniques des espaces usuels}\dotfill $\Box$
* \hyperref[objectif-18-7]{Savoir déterminer la dimension d'un sous-espace vectoriel}\dotfill $\Box$
%* \hyperref[objectif-18-8]{Savoir déterminer les coordonnées d'un vecteur dans une base}\dotfill $\Box$
[item=]
* Concernant les applications linéaires :
[=item]
* \hyperref[objectif-18-9]{Savoir montrer qu'une application est linéaire}\dotfill $\Box$
* \hyperref[objectif-18-10]{Savoir déterminer la matrice d'une application linéaire de $\MM_{n,1}(\R)$ dans $\MM_{p,1}(\R)$}\dotfill $\Box$
* \hyperref[objectif-18-11]{Savoir déterminer le noyau d'une application linéaire}\dotfill $\Box$
* \hyperref[objectif-18-12]{Savoir déterminer l'image d'une application linéaire}\dotfill $\Box$
* \hyperref[objectif-18-13]{Savoir démontrer qu'une application linéaire est injective, surjective, bijective}\dotfill $\Box$
[item=]
[num=]
[NP]

%%% Début du cours %%%

[= Espaces vectoriels]

[== Généralités]

[.definition]
Soit $E$ un ensemble non vide.
[=item]
* On dit que la loi $+$ est une \textbf{loi de composition interne} sur $E$ si $\forall~(x,y)\in E^2, x+y \in E$.
* On dit que la loi $\cdot$ est une \textbf{loi de composition externe} sur $E$ si $\forall~x\in E,\forall~\lambda \in \R, \lambda\cdot x \in E$.
[item=]
[definition.]

[.exemple]
L'exemple le plus classique est l'ensemble des matrices $\MM_{n,p}(\R)$. La loi d'addition de matrices est une loi de composition interne, et la multiplication par un réel est une loi de composition externe.
[exemple.]

[.definition]
Soit $E$ un ensemble non vide, muni d'une loi interne, noté $+$, et d'une loi externe, noté $\cdot$. On dit que $E$ est un \textbf{espace vectoriel} sur $\R$ si les lois vérifient les propriétés suivantes :
[=item]
* (commutativité de $+$) : $\forall~(x,y) \in E^2, x+y=y+x$.
* (associativité de $+$) : $\forall~(x,y,z)\in E^3, (x+y)+z=x+(y+z)$
* (neutre pour $+$) : il existe un élément, noté $0_E$, tel que $\forall~x \in E, x+0_E=0_E+x=x$.
*  (inverse pour $+$) : pour tout $x \in E$, il existe un élément $y \in E$, tel que $x+y=y+x=0_{E}$.  Cet élément est appelé \textit{opposé} de $x$, et est noté $-x$.
* (neutre pour $\cdot$) $\forall~x\in E, 1.x = x$
* (distributivité de $\cdot$) $\forall~\lambda\in \R, \forall~(x,y)\in E^2, \lambda.(x+y)=\lambda.x+\lambda.y$
* (distributivité de $\cdot$) $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, (\lambda+\mu).x=\lambda.x+\mu.x$
* $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, \lambda.(\mu.x)=(\lambda\times\mu).x$.
[item=]    
[definition.]

[.remarque]
Si $E$ est un espace vectoriel, les éléments de $E$ sont alors appelés les \textbf{vecteurs}, et les réels sont appelés les \textbf{scalaires}. L'élément $0_{E}$ est appelé vecteur nul.
[remarque.]

[.remarque]
Les quatre premières propriétés font de $(E,+)$ ce qu'on appelle un \textbf{groupe abélien} ou groupe commutatif.
[remarque.]

[.remarque]
Le symbole $\cdot$ de la loi de composition externe est très souvent omis. On notera plus souvent $2x$ plutôt que $2\cdot x$.
[remarque.]

[.proposition]
[Exemple fondamental] Les ensembles $\MM_{n,1}(\R)$ pour $n\geq 1$, munis de l'addition de matrices, et de la multiplication par un réel, sont des espaces vectoriels.
[proposition.]

[prof]
[.demonstration]
En effet, si 
$A=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right) \qeq B=\left( \begin{array}{c} b_1 \\\vdots \\b_n \end{array}\right) $
alors
\[A+B=\left( \begin{array}{c} a_1+b_1 \\\vdots \\a_n+b_n \end{array}\right)=\left( \begin{array}{c} b_1+a_1 \\\vdots \\b_n+a_n \end{array}\right)=B+A\]
\[(A+B)+C=\left( \begin{array}{c} (a_1+b_1)+c_1 \\\vdots \\(a_n+b_n)+c_n \end{array}\right)=
\left( \begin{array}{c} a_1+(b_1+c_1) \\\vdots \\a_n+(b_n+c_n) \end{array}\right)=A+(B+C)\]
\[A+0_{n,1}= \left( \begin{array}{c} a_1+0 \\\vdots \\a_n+0 \end{array}\right)=\left( \begin{array}{c} 0+a_1 \\\vdots \\0+a_n \end{array}\right)=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right)=A\]
\[A+(-A)=\left( \begin{array}{c} a_1+(-a_1) \\\vdots \\a_n+(-a_n) \end{array}\right)=\left( \begin{array}{c} 0 \\\vdots \\0 \end{array}\right)=0_{n,1}\]
\[1.A=\left( \begin{array}{c} 1.a_1 \\\vdots \\1.a_n \end{array}\right)=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right)=A\]
\[\lambda.(A+B)=\lambda.\left( \begin{array}{c} a_1+b_1 \\\vdots \\a_n+b_n \end{array}\right)
    =\left( \begin{array}{c} \lambda (a_1+b_1) \\\vdots \\\lambda (a_n+b_n) \end{array}\right)    =\left( \begin{array}{c} \lambda a_1+\lambda b_1 \\\vdots \\\lambda a_n+\lambda b_n \end{array}\right)=\lambda.\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right) + \lambda.\left( \begin{array}{c} b_1 \\\vdots \\b_n \end{array}\right)=\lambda.A+\lambda.B\]
\[(\lambda+\mu).A=\left( \begin{array}{c} (\lambda+\mu)a_1 \\\vdots \\(\lambda+\mu)a_n \end{array}\right)=\left( \begin{array}{c} \lambda a_1+\mu a_1 \\\vdots \\\lambda a_n+\mu a_n \end{array}\right)=\lambda.A+\mu.A\]
\[\lambda.(\mu.A) = \lambda.\left( \begin{array}{c} \mu a_1 \\\vdots \\\mu a_n \end{array}\right)
=\left( \begin{array}{c} \lambda \mu a_1 \\\vdots \\\lambda \mu a_n \end{array}\right) =(\lambda\times\mu).A\]
En première année, nous n'utiliserons que ces espaces vectoriels, dans le cas où $n=1,2,3$ ou $4$.
[demonstration.]
[/prof 20]

[== Règles de calculs]

On se place ici dans un espace vectoriel $E$.   
 
[.proposition]
Pour tous $\lambda \in \R$ et $x\in E$, on a
[=item]
* $\lambda.0_E=0_E$ et $0.x=0_E$
* $(-\lambda).x=\lambda.(-x)=-(\lambda.x)$. 
* $x+(-y)=x-y$.
[item=]
[proposition.]

[.theoreme]
Soit $\lambda \in \R$, et $x\in E$. Alors
\[\lambda.x=0_E \Leftrightarrow x=0_E\quad \textrm{ou}\quad \lambda=0\]
[theoreme.]

[== Combinaison linéaire]

Soit $E$ un espace vectoriel.

[.definition]
On appelle \textbf{famille de vecteurs} de $E$ une $n$-liste $(e_1,\cdots, e_n)$ d'éléments de $E$.
[definition.]


[.exemple]
Si $A=\left(\begin{array}{c} 1\\2 \end{array}\right)$ et 
$B=\left(\begin{array}{c} 4\\-5 \end{array}\right)$, alors $(A,B)$ désigne une famille de deux vecteurs de $\MM_{2,1}(\R)$.
[exemple.]

[.definition]
Soient $(e_1, \cdots, e_p)$ une famille de $p$ vecteurs de $E$. Soit $x$ un vecteur de $E$. On dit que $x$ est une \textbf{combinaison linéaire} de la famille $(e_1,\cdots, e_p)$ s'il existe des réels $(\lambda_1, \cdots, \lambda_p)$ tels que 
\[x=\sum_{i=1}^p \lambda_i e_i = \lambda_1 e_1+\cdots+ \lambda_p e_p\]
Les réels $\lambda_1,\cdots, \lambda_p$ sont alors les \textbf{coefficients} de la combinaison linéaire.
[definition.]
 
[.remarque]
Il n'y a pas forcément unicité de la combinaison linéaire.
[remarque.]

[.exemple]
Soient $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$. Alors, $2.A-3.B=\left( \begin{array}{c}11\\1\end{array}\right)$ est une combinaison linéaire de $A$ et $B$.
[exemple.]

[.methode]
\label{objectif-18-2}
Pour montrer qu'un vecteur $x$ est combinaison linéaire d'une famille $(e_1,\cdots,e_p)$, on écrit $\ds{x=\sum_{i=1}^p \lambda_i e_i}$ et on résout un système pour déterminer (ou non) les réels $\lambda_1,\cdots,\lambda_p$.
[methode.]

[.exo]
Notons $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$ et $X=\left( \begin{array}{c}-5\\4\end{array}\right)$. Montrer que $X$ est combinaison linéaire de $A$ et $B$.
[exo.]

[prof]
[.soluce]
On écrit $X=\lambda A + \mu B$. On a alors
\[\left( \begin{array}{c}-5\\4\end{array}\right)=\left( \begin{array}{c}\lambda -3\mu\\2\lambda+\mu\end{array}\right)\]
On résout alors le système :
\[\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ 2\lambda&+&\mu&=&4 \end{array}\right. \Leftrightarrow 
\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ &&7\mu&=&14 \end{array}\right.
  \Leftrightarrow 
  \left\{ \begin{array}{ccc} \lambda&=&1 \\ \mu&=&2 \end{array}\right.
\]
Ainsi, $X=A+2B$.
[soluce.]
[/prof 7]

[= Sous-espace vectoriel]
        
[== Définition]
        
[.definition]
Soit $E$ un espace vectoriel. Soit $F$ un sous ensemble de $E$ \underline{non vide}. On dit que $F$ est un \textbf{sous-espace vectoriel} de $E$ si les restrictions des lois $+$ et $\cdot$ à $F$ font de $F$ un espace vectoriel.
[definition.]

[.exemple]
Si $E$ est un espace vectoriel, alors $\{0_E\}$ et $E$  sont des sous-espaces vectoriels de $E$.
[exemple.]

[.propriete]
Soit $F$ un sous-espace vectoriel d'un espace vectoriel $E$. Alors $0_E \in F$ et $F$ est un espace vectoriel.
[propriete.]

[.demonstration]
Par définition, $F$ est un espace vectoriel. Par propriété, si $x\in F$ (puisque $F$ est non vide), alors $0.x \in F$ c'est-à-dire $0_E \in F$.
[demonstration.]


[.proposition]
Soit $F$ un sous ensemble d'un espace vectoriel $E$. Alors, $F$ est un sous-espace vectoriel de $E$ si et seulement si
[=item]
* $F \neq \vide$
* $\forall~(x, y) \in F^2, x+y\in F$ ($F$ est stable par addition)
* $\forall~x\in F, \lambda \in \R, \lambda.x \in F$ ($F$ est stable par multiplication par un scalaire)
[item=]
[proposition.]

[.remarque]
[=item]
* La première propriété se vérifie en général en montrant que le neutre $0_E$ est dans $F$.
* Les deux dernières propriétés peuvent être regroupées en une seule :
    \[\forall~(x, y)\in F^2, \forall~\lambda \in \R, \lambda.x+y\in F\]
[item=]
[remarque.]

[.methode]
\label{objectif-18-1}
On utilise la proposition précédente pour démontrer qu'un ensemble est un sous-espace vectoriel.
[methode.]

[.exo]
Montrer que $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$ est un sous-espace vectoriel de $\MM_{2,1}(\R)$.
[exo.]

[prof]
[.soluce]
En effet,
[=item]
* $O_{2,1} \in F$ : il suffit de prendre $t=0$.
* Si $A=\left(\begin{array}{c} a \\ 0 \end{array}\right), B=\left(\begin{array}{c} b \\ 0 \end{array}\right)$ et $\lambda \in \R$, alors
    \[\lambda.A+B= \left(\begin{array}{c} \lambda a+b \\ 0 \end{array}\right) \in F\]
    en prenant $t=\lambda a+b$.
[item=]
[soluce.]
[/prof 7]

[.theoreme]
Soit $(\mathcal{S})$ un système linéaire homogène de $n$ équations à $n$ inconnues. L'ensemble des solutions de $(\mathcal{S})$ forme un sous-espace vectoriel de $\MM_{n,1}(\R)$
[theoreme.]

[prof]
[.demonstration]
Soit $M\in \MM_{n}(\R)$ la matrice associée au système $\mathcal{S}$. Alors l'ensemble des solutions $F$ du système $(\mathcal{S})$  s'écrit également 
\[F=\left \{ X \in \MM_{n,1}(\R),~MX=0_{n,1} \right \}\]
avec $0_{n,1} = \left( \begin{array}{c} 0\\\vdots \\0 \end{array}\right)$.
[=item]
* $F \subset \MM_{n,1}(\R)$.
* $0_{n,1} \in F$. En effet, $M.0_{n,1} = 0_{n,1}$ par définition de la matrice nulle.
* Soient $X$ et $Y$ dans $F$, et $\lambda \in \R$. Alors
    \[M.(\lambda X + Y) = \lambda M.X + M.Y = 0_{n,1} + 0_{n,1} = 0_{n,1}\]
    puisque $MX=MY=0_{n,1}$. Donc $\lambda X + Y \in F$.
[item=]
$F$ est bien un sous-espace vectoriel de $\MM_{n,1}(\R)$.
[demonstration.]
[/prof 15]

[afaire Exercices \lienexo{1}, \lienexo{2} et \lienexo{3}.]

[== Sous-espaces engendrés]

On se donne un espace vectoriel $E$.

[.definition]
Soient $(e_1, \cdots, e_p)$ une famille de vecteurs de $E$. On appelle \textbf{sous-espace vectoriel engendré par} $(e_1,\cdots,e_p)$, et on note $\Vect(e_1,\cdots, e_p)$, l'ensemble formé de toutes les combinaisons linéaires de $(e_1,\cdots, e_p)$. Ainsi,
\[x \in \Vect(e_1,\cdots,e_p) \Leftrightarrow\exists~(\lambda_1,\cdots, \lambda_p) \in \R^p,~~x=\lambda_1e_1+\cdots+\lambda_pe_p\]
[definition.]
 
[.exemple]
Soit $A=\left( \begin{array}{c} 1 \\ 2 \end{array}\right)$. Alors \[\Vect(A) = \{ \lambda.A,~\lambda\in \R\}= \left\{ \left( \begin{array}{c} \lambda \\ 2\lambda \end{array}\right), \lambda \in \R\right\}\] 
$\Vect(A)$ est appelée \textbf{droite vectorielle}.
[exemple.]

[.theoreme]
Soit $(e_1, \cdots, e_p)$ une famille de vecteurs de $E$. Alors $\Vect(e_1,\cdots,e_p)$ est un sous-espace vectoriel de $E$.
[theoreme.]

[.propriete]    
Soit $(e_1, \cdots, e_p)$  une famille de vecteurs de $E$. Alors
[=item]
* $\forall~k\in \llbracket 1,p \rrbracket, e_k \in \Vect(e_1,\cdots,e_p)$
* $\Vect(e_1,\cdots,e_p)$ est le plus petit sous-espace vectoriel contenant la famille $(e_1,\cdots, e_p)$. Ainsi, si $F$ est un sous-espace vectoriel contenant $e_1,\cdots, e_p$, nécessairement, \\$\Vect(e_1,\cdots,e_p) \subset F$.
* Si $F=\Vect(e_1,\cdots, e_p, e_{p+1})$ et si $e_{p+1}$ est combinaison linéaire des vecteurs $e_1,\cdots, e_p$, alors $F=\Vect(e_1,\cdots,e_p)$.
* Si $(a_1,\cdots, a_p)$ sont des réels tous non nuls, et si $F=\Vect(a_1 e_1,\cdots, a_p e_p)$, alors \\$F=\Vect(e_1,\cdots, e_p)$.
[item=]
[propriete.]

[.exemple]
[=item]
* Soit $A=\left( \begin{array}{c} 2 \\ -4 \end{array}\right)$. Alors $\Vect(A)=\Vect\left(\left( \begin{array}{c} 1 \\ -2 \end{array} \right) \right)$.
* Soient $B=\left( \begin{array}{c} 0 \\ 1 \end{array}\right)$ et $C=\left( \begin{array}{c} 2 \\ -3 \end{array}\right)$. Alors $\Vect(A,B,C)=\Vect(A,B)$ puisque $C=A+B$.
* Soit $F$ un sous-espace vectoriel contenant les vecteurs $A$ et $B$ précédents. Alors, nécessairement, $\Vect(A,B) \subset F$.
[item=]
[exemple.]

[afaire Exercices \lienexo{4} et \lienexo{5}.]

[= Base d'un espace vectoriel]

[== Définition]

[.definition]
Soit $E$ un espace vectoriel. Soit  $\mathcal{B}=(e_1,\cdots, e_p)$ une famille de vecteurs de $E$. \\
On dit que $\mathcal{B}$ est une \textbf{base} de $E$ si, pour tout vecteur $x$ de $E$, il existe une \textit{unique} $n$-liste $(\lambda_1,\cdots, \lambda_p)$ de réels tels que 
\[x=\sum_{i=1}^p \lambda_i e_i\]
Les réels $(\lambda_1,\cdots, \lambda_p)$ sont appelés les \textbf{coordonnées} de $x$ dans la base $\mathcal{B}$
[definition.]

[.exemple]
Si $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$, alors tout élément de $F$ s'écrit de manière unique sous la forme $t.\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$. Ainsi, la famille composée du vecteur $\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$ est une base de $F$.
[exemple.]

[.methode]
\label{objectif-18-5}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est une base d'un espace vectoriel $E$, on prend $x\in E$ et on résout $\lambda_1e_1+\cdots +\lambda_pe_p = x$. S'il existe une unique solution, alors $(e_1,\cdots, e_p)$ est bien une base de $E$.
[methode.]

[.exemple]
Montrer que $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est une base de $\MM_{2,1}(\R)$.
[exemple.]

[prof]
[.soluce]
Soit $\left(\begin{array}{c} x\\y\end{array}\right)$ un élément de $\MM_{2,1}(\R)$. On cherche $a$ et $b$ tels que 
\[a\left(\begin{array}{c}1\\2\end{array}\right)+b \left(\begin{array}{c} 1\\3 \end{array}\right)=\left(\begin{array}{c} x\\y\end{array}\right) \Leftrightarrow \left(\begin{array}{c}a+b\\2a+3b \end{array}\right)=\left(\begin{array}{c}x\\y\end{array}\right)\]
 On résout le système
 \[\left\{ \begin{array}{ccccc}a&+&b&=&x \\2a&+&3b&=&y \end{array}\right.
 \Leftrightarrow \left\{ \begin{array}{ccccc}a&+&b&=&x \\&&b&=&y-2x \end{array}\right.\]
 Le système est de Cramer, donc il possède une unique solution. \\
 \textbf{Bilan} : la famille $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est bien une base de de $\MM_{2,1}(\R)$.
[soluce.]
[/prof 8]

[afaire Exercices \lienexo{6} et \lienexo{8}.]

[.definition]
Soit $E$ un espace vectoriel. Soit  $(e_1,\cdots, e_p)$ une famille de vecteurs de $E$. 
[=item]
* On dit que la famille $(e_1,\cdots, e_p)$ est \textbf{libre} si
    \[\lambda_1 e_1+\cdots +\lambda_p e_p = 0_E \Rightarrow \lambda_1=\cdots=\lambda_p = 0\] Si elle n'est pas libre, on dit qu'elle est \textbf{liée}.
* On dit que la famille $(e_1,\cdots, e_p)$ est \textbf{génératrice} si $\Vect(e_1,\cdots,e_p)=E$.
[item=]
[definition.]

[.methode]
\label{objectif-18-3}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est libre, on écrit $\lambda_1e_1+\cdots \lambda_pe_p=0_E$ et on résout le système. S'il admet comme seule solution $(0,\cdots,0)$ alors elle est libre, sinon elle est liée.
[methode.]

[.exemple]
Montrer que la famille $\left(\begin{array}{c}1\\1\\1\end{array}\right),\left(\begin{array}{c}2\\1\\0\end{array}\right)$ est libre.
[exemple.]

[prof]
[.soluce]
On cherche $a$ et $b$ deux réels tels que 
\[a\left(\begin{array}{c}1\\1\\1\end{array}\right)+b\left(\begin{array}{c}2\\1\\0\end{array}\right)=\left(\begin{array}{c}0\\0\\0\end{array}\right)\]
Ainsi,
\[\left(\begin{array}{c} a+2b\\a+b\\a \end{array}\right) = \left(\begin{array}{c}0\\0\\0\end{array}\right)\]
On obtient alors $a=0$ puis $b=0$. Ainsi, la famille est libre.
[soluce.]
[/prof 6]

[afaire Exercice \lienexo{7}.]

[.methode]
\label{objectif-18-4}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est génératrice, on écrit $\lambda_1e_1+\cdots \lambda_pe_p=x$ avec $x\in E$ et on résout le système. S'il admet au moins une solution alors elle est génératrice, sinon elle ne l'est pas et on exhibe alors un contre-exemple.
[methode.]

[.remarque]
[=item]
* Une famille libre est une famille dans laquelle aucun vecteur ne peut être exprimé comme combinaison linéaire des autres vecteurs. Ainsi, tout vecteur est ``utile'' dans la famille.
* Une famille génératrice est une famille qui permet de récupérer, par combinaison linéaire, tout vecteur de E; en revanche, plusieurs combinaisons linéaires peuvent mener au même vecteur : il n'y a pas forcément unicité de la décomposition. 
[item=]
[remarque.]

[.theoreme]
Soit $E$ un espace vectoriel. Soit  $(e_1,\cdots, e_p)$ une famille de vecteurs de $E$. $(e_1,\cdots, e_p)$ est une base si et seulement si elle est libre et génératrice.
[theoreme.]

[== Base canonique de $\MM_{n,1}(\R)$]

[.remarque]
On s'intéresse à $\MM_{3,1}(\R)$. Toute matrice $A=\left( \begin{array}{c} a_1\\a_2\\a_3 \end{array} \right) \in \MM_{3,1}(\R)$ s'écrit de manière unique sous la forme
\[A=a_1\left(\begin{array}{c} 1 \\ 0 \\ 0 \end{array}\right)   +a_2\left(\begin{array}{c} 0 \\ 1 \\ 0 \end{array}\right)+a_3\left(\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right)\]
Ainsi, les trois matrices $e_1=\left(\begin{array}{c} 1 \\ 0 \\ 0 \end{array}\right), e_2=\left(\begin{array}{c} 0 \\ 1 \\ 0 \end{array}\right), e_3=\left(\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right)$ permettent de décrire toutes les matrices de $\MM_{3,1}(\R)$ :  $(e_1,e_2, e_3)$ représente une base de $\MM_{3,1}(\R)$.
[remarque.] 
    
[.definition]
\label{objectif-18-6}
Soit $n\in \N^*$. On note $e_1=\left(\begin{array}{c} 1 \\ \vdots \\ 0 \end{array}\right)$, $e_2=\left(\begin{array}{c} 0 \\ 1\\\vdots \\ 0 \end{array}\right)$, ..., $e_n=\left(\begin{array}{c} 0 \\ \vdots\\ 0 \\ 1 \end{array}\right)$ des matrices de $\MM_{n,1}(\R)$.  Toute matrice de $\MM_{n,1}(\R)$ peut s'écrire de manière unique sous la forme \[a_1.e_1+\cdots+a_n.e_n\] où $(a_1,\cdots, a_n)\in \R^n$. La famille de vecteurs $(e_1, \cdots, e_n)$ forme une \textbf{base}, appelée \textbf{base canonique} de $\MM_{n,1}(\R)$.
[definition.]

[.exemple]
[Cas $n=2,3,4$] 
[=item]
* $\left(\left(\begin{array}{c} 1 \\ 0 \end{array}\right), \left(\begin{array}{c} 0\\ 1 \end{array}\right) \right)$ forme une base de $\MM_{2,1}(\R)$. 
* $\left(\left(\begin{array}{c} 1 \\ 0\\0 \end{array}\right), \left(\begin{array}{c} 0\\ 1 \\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\1\end{array}\right) \right)$ forme une base de $\MM_{3,1}(\R)$. 
* $\left(\left(\begin{array}{c} 1 \\ 0\\0 \\0\end{array}\right), \left(\begin{array}{c} 0\\ 1 \\0\\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\1\\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\0\\1\end{array}\right) \right)$ forme une base de $\MM_{4,1}(\R)$. 
[item=]
[exemple.]

[.remarque]
Il n'y a pas unicité de la base. Par exemple, dans $\MM_{2,1}(\R)$, les vecteurs $\left(\begin{array}{c} 2 \\ 0\end{array}\right)$ et $\left(\begin{array}{c} 0\\ -1 \end{array}\right)$ forment également une base. 
[remarque.]

[.definition]
Vous verrez l'année prochaine que toute base d'un espace vectoriel possède le même nombre d'éléments : ainsi, dans $\MM_{2,1}(\R)$, les bases possèdent $2$ vecteurs. On appelle ce nombre la \textbf{dimension} de l'espace vectoriel, et on le notera $\dim(E)$. 
[definition.]

[.exemple]
$\MM_{2,1}(\R)$ est de dimension $2$, et plus généralement $\MM_{n,1}(\R)$ est de dimension $n$.
[exemple.]

[.methode]
\label{objectif-18-7}
Pour déterminer la dimension d'un espace vectoriel, on détermine d'abord une base, et on conclut.
[methode.]

[.exemple]
Soit $F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~x+y=0 \right \}$. Déterminer la dimension de $F$.
[exemple.]

[prof]
[.soluce]
Remarquons qu'on peut écrire 
\[F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~y=-x \right \}\]
soit
\[F=\left\{ \left(\begin{array}{c}x\\-x\end{array}\right),~x\in \R \right \}\]
Ainsi,
\[F=\left\{ x\left(\begin{array}{c}1\\-1\end{array}\right),~x\in \R \right \}\]
et donc $\left(\begin{array}{c}1\\-1\end{array}\right)$ est une base de $F$ :
\[F=\Vect\left(\left(\begin{array}{c}1\\-1\end{array}\right)\right)\]
et $\dim(F) = 1$.
[soluce.]	
[/prof 9]

[= Applications linéaires]

[== Définition]

[.definition] 
Soient $E$ et $F$ deux espaces vectoriels. On appelle \textbf{application linéaire} de $E$ dans $F$ toute application $f:E\rightarrow F$ telle que 
[=item]
* $\forall~(x,y)\in E^2, f(x+y)=f(x)+f(y)$
* $\forall~x\in E,\forall~\lambda\in \R, f(\lambda x)=\lambda f(x)$
[item=]
[definition.]

[.remarque] 
On peut réunir les deux propriétés en une seule : $f:E\rightarrow F$ est une application linéaire si et seulement si
\[\forall~(x,y)\in E^2, \forall~\lambda\in \R, f(\lambda x+y)=\lambda f(x)+f(y)\]
[remarque.]

[.methode]
\label{objectif-18-9}
Pour montrer qu'une application $f$ est une application linéaire, on prendra $x$ et $y$ dans $E$, $\lambda \in \R$, et on montre que $f(\lambda x + y) = \lambda f(x) + f(y)$.
[methode.]

[.exo]
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{3,1}(\R)$ définie pour tout $(a, b)\in \R^2$ par 
\[f\left( \left(\begin{array}{c} a\\b\end{array} \right) \right)=   \left(\begin{array}{c} b\\0\\a\end{array} \right)\]
Montrer que $f$ est une application linéaire.
[exo.]

[prof]
[.soluce]
En effet, si $A=\left(\begin{array}{c} a_1\\a_2\end{array} \right)$ et $B=\left(\begin{array}{c} b_1\\b_2\end{array} \right)$, et si $\lambda \in \R$, alors
\[f(\lambda A+B)=f\left( \left( \begin{array}{c} \lambda a_1+b_1\\\lambda a_2+b_2 \end{array}\right) \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
et
\[\lambda f(A)+f(B)=\lambda \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)+\left(\begin{array}{c} b_2\\0\\b_1\end{array} \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
Donc $f(\lambda A + B)=\lambda f(A)+f(B)$ : $f$ est une application linéaire.
[soluce.]
[/prof 12]

[afaire Exercice \lienexo{9}.]

[.notation]
L'ensemble des applications linéaires de $E$ dans $F$ est noté $\mathcal{L}(E,F)$.
[notation.]

[.definition]
[=item]
* Si $F=E$, et si $f:E\rightarrow E$ est une application linéaire, on dit que $f$ est un \textbf{endomorphisme}.
* Si $F=\R$, et si $f\in \mathcal{L}(E,\R)$, on dit que $f$ est une \textbf{forme linéaire}.
[item=]
[definition.]

[.propriete]
Soient $E$ et $F$ deux espaces vectoriels, et $f:E\rightarrow F$ une application linéaire. Alors $f(0_E)=0_F$.
[propriete.]

[.proposition]  
Soient $E$, $F$ et $G$ trois espaces vectoriels, et soit $f\in \mathcal{L}(E,F)$ et $g \in \mathcal{L}(F,G)$. Alors $g~\circ~f~\in~\mathcal{L}(E,G)$.
[proposition.]

[prof]
[.demonstration]
Soient $x, y \in E^2$ et $\lambda \in \R$. Alors, en utilisant respectivement la linéarité de $f$ puis de $g$ :
\[g\circ f (\lambda x + y) = g( f(\lambda x + y)) = g(\lambda f(x)+f(y)) = \lambda g(f(x))+g(f(y))=\lambda g\circ f(x)+g \circ f(y)\]
[demonstration.]
[/prof 3]

[== Cas de $\mathcal{L}(\MM_{n,1}(\R),\MM_{p,1}(\R))$]

[.theoreme]   
Soit $f:\MM_{n,1}(\R)\rightarrow \MM_{p,1}(\R)$ une application linéaire. Alors, il existe une matrice \\$M\in \MM_{p,n}(\R)$ telle que
\[\forall~X \in \MM_{n,1}(\R), f(X)=MX\]
Cette matrice est appelée matrice \textbf{associée} à $f$ dans les bases canoniques respectives et est notée $\mathrm{Mat}_{\mathcal{B}}~f$.
[theoreme.]

[.methode]
\label{objectif-18-10}
Pour déterminer la matrice associée à une application linéaire $f:\MM_{n,1}(\R) \rightarrow \MM_{p,1}(\R)$, on prend la base canonique $(e_1,\cdots, e_n)$ de $\MM_{n,1}(\R)$, et on calcule les images $f(e_i)$ par $f$ qu'on range dans une matrice.
[methode.]

[.exo]
Soit $f$ l'application définie dans l'exemple précédent : 
 \[f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)\]
 Déterminer la matrice associée à $f$ dans les bases canoniques.
[exo.]

[prof]
[.soluce]
Soit $e_1=\left(\begin{array}{c}1\\0\end{array}\right)$ et $e_2=\left(\begin{array}{c}0\\1\end{array}\right)$ la base canonique de $\MM_{2,1}(\R)$. On a
\[f(e_1)=\left(\begin{array}{c}0\\0\\1\end{array}\right) \qeq f(e_2)=\left(\begin{array}{c}1\\0\\0\end{array}\right)\]
Ainsi,
 \[   f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=
 \left( \begin{array}{cc} 0&1\\0&0 \\ 1&0\end{array}\right) \left(\begin{array}{c} a_1\\a_2\end{array} \right) \]
 donc $\forall~X\in \MM_{2,1}(\R),~f(X)=MX$ avec $M=\left( \begin{array}{cc} 0&1\\0&0 \\ 1&0\end{array}\right) $.
[soluce.]
[/prof 8]

[afaire Exercice \lienexo{10}.]
 
[== Noyau d'une application linéaire]

[.definition]
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{noyau} de $f$, et on note $\Ker f$, le sous-ensemble de $E$ défini par 
     \[\Ker f = \left \{ x\in E,~f(x)=0_F \right\}\]
[definition.]

[.theoreme]
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Ker f$ est un sous-espace vectoriel de $E$.
[theoreme.]

[prof]
[.demonstration]
Puisque $f(0_E)=0_F$, on a déjà $0_E \in \Ker f$. Ensuite, si $x$ et $y$ sont deux vecteurs de $\Ker f$, et si $\lambda \in \R$, alors
\[f(\lambda x + y) = \lambda f(x) +f(y) \underbrace{=}_{x\textrm{ et }y \textrm{ dans } \ker f} \lambda 0_F+0_F=0_F\]
donc $\lambda x + y \in \Ker f$ : $\Ker f$ est bien un sous-espace vectoriel de $E$.
[demonstration.]
[/prof 6]

[.methode]
\label{objectif-18-11}
Pour déterminer le noyau d'une application linéaire, on résout l'équation $f(x)=0_F$ d'inconnue $x$. On se ramène en général à un système.
[methode.]

[.exo]
Déterminer le noyau de l'application linéaire précédente.
[exo.]

[prof]
[.soluce]
Si $f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)$, alors $X=\left( \begin{array}{c} x_1\\x_2 \end{array} \right) \in \Ker f$ si et seulement si
\[f\left( X \right)=   \left(\begin{array}{c} x_2\\0\\x_1\end{array} \right)=\left( \begin{array}{c} 0\\0\\0 \end{array} \right)\]
ce qui donne le système 
\[\left \{ \begin{array}{ccc} x_2 &=& 0 \\ 0 &=& 0 \\ x_1 &=& 0 \end{array} \right. \Leftrightarrow \left \{ \begin{array}{ccc} x_2 &=& 0 \\x_1 &=& 0 \end{array} \right. \Leftrightarrow X=0_E\]
Donc, $\Ker f = \{ 0_E \}$.
[soluce.]
[/prof 8]

[.theoreme]
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. $f$ est injective si et seulement si $\Ker f = \{ 0_E \}$.
[theoreme.]

[prof]
[.demonstration]
Supposons $f$ injective. Soit $x$ tel que $f(x)=0_F$ alors $f(x)=f(0_E)$. Par injectivité de $f$, $x=0_E$. Donc $\Ker f = \{ 0_E \}$. \\
Réciproquement, supposons que $\Ker f = \{ 0_E \}$. Soient $x$ et $x'$ tels que $f(x)=f(x')$. Par linéarité de $f$, $f(x-x')=0_F$. Puisque $\Ker f = \{ 0_E \}$, alors $x-x'=0_E$ c'est-à-dire $x=x'$ : $f$ est injective.
[demonstration.]
[/prof 7]

[== Image d'une application linéaire]

[=== Définition]

[.definition]  
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{image} de $f$, et on note $\Img f$, le sous-ensemble de $F$ défini par 
     \[\Img f = \left \{ y \in F, ~\exists~x\in E,~y=f(x) \right \}\]
[definition.]

[.theoreme]
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Img f$ est un sous-espace vectoriel de $F$. Si $\mathcal{B}=(e_1,\cdots, e_n)$ est une base de $E$, alors 
\[\Img f = \Vect(f(e_1),\cdots, f(e_n))\]
[theoreme.]

[prof]
[.demonstration]
Puisque $O_F=f(0_E)$, on a $O_F \in \Img f$. Soient $a$ et $b$ deux éléments de $\Img f$, et $\lambda \in \R$. Puisque $a$ et $b$ sont dans $\Img f$, il existe $x$ et $y$ dans $E$ tels que
\[a=f(x) \qeq  b=f(y)\]
Mais alors
\[\lambda a + b = \lambda f(x) + f(y)  \underbrace{=}_{\textrm{linéarité de }f} f(\lambda x + y)\]
Donc $\lambda a + b$ s'écrit $f(z)$ pour un certain $z\in E$ : $\lambda a + b \in \Img f$. $\Img f$ est donc bien un sous-espace vectoriel de $F$.
[demonstration.]
[/prof 8]

[=== Rang]

Puisque $\Img f$ est un sous-espace vectoriel, on peut s'intéresser à sa dimension :

[.definition]
Soient $E$ et $F$ deux espaces vectoriels de dimension finie. Soit $f\in \mathcal{L}(E,F)$. On appelle \textbf{rang} de $f$, et on note $\rg(f)$, la dimension de $\Img f$.
[definition.]

[.methode]
\label{objectif-18-12}
Pour déterminer l'image d'une application linéaire, on prend une base $\mathcal{B}=(e_1,\cdots,e_n)$ de $E$ et on écrit $\Img f = \Vect(f(e_1),\cdots,f(e_n))$. On essaie ensuite d'en déterminer une base.
[methode.]

[.exo]
Déterminer l'image de l'application linéaire précédente.
[exo.]

[prof]
[.soluce]
Si $f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)$, on prend $\mathcal{B}=(e_1,e_2)$ la base canonique de $\MM_{2,1}(\R)$. On a donc $\Img f = \Vect(f(e_1),f(e_2))$.
\[f(e_1)=f\left( \left( \begin{array}{c}1\\0 \end{array} \right) \right) = \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right) \qeq
f(e_2)=f\left( \left( \begin{array}{c}0\\1 \end{array} \right) \right) = \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\]
La famille $\left( \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right), \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\right)$ est libre car les vecteurs ne sont pas colinéaires.
Donc
\[\Img f = \Vect \left(\left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right),\left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array}  \right) \right)
= \left \{ \left( \begin{array}{c} \lambda \\0\\\mu \end{array} \right) , \lambda \in \R, \mu \in \R \right\}\]
et $\left( \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right), \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\right)$  forme une base de $\Img f$. Ainsi, $\rg(f) = 2$.
[soluce.]
[/prof 16]

[.theoreme]    
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est surjective si et seulement si $\Img f = F$.
[theoreme.]

[prof]
[.demonstration]
Par définition, $f$ est surjective si et seulement si $f(E)=F$. Or, par définition, $f(E)=\Img f$. Donc $f$ est surjective si et seulement si $\Img f = F$.
[demonstration.]
[/prof 3]

[.theoreme]
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est bijective si et seulement si $\Ker f = \{0_E \}$ et $\Img f = F$. 
[theoreme.]

[.methode]    
\label{objectif-18-13}
Pour montrer qu'une application linéaire $f:E\rightarrow F$ est bijective, on montrera que $\Ker f=\{0_E\}$ et que $\Img f = F$.
[methode.]

[.exo]
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{2,1}(\R)$ l'application définie par 
\[f\left( \left(\begin{array}{c}x_1\\x_2	
\end{array}\right) \right) = \left( \begin{array}{c} 2x_2\\x_1 \end{array}\right)\]
est bijective.
[exo.]

[prof]
[.soluce]
$f$ est bien linéaire (c'est un endomorphisme de $\MM_{2,1}(\R)$. Déterminons noyau et image.
\begin{itemize}[label=\textbullet]
	\item Soit $X=\left(\begin{array}{c}x\\y\end{array}\right) \in \Ker f$. Alors
		\[f(X)=\left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow \left(\begin{array}{c}2y\\x\end{array}\right) = \left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow x=y=0\]
		ainsi, $\Ker f =\left\{ \left(\begin{array}{c}0\\0\end{array}\right) \right\}$ et $f$ est injective.
	\item Soit $e_1=\left(\begin{array}{c}1\\0\end{array}\right)$ et $e_2=\left(\begin{array}{c}0\\1\end{array}\right)$ la base canonique de $\MM_{2,1}(\R)$. Alors,
		\[f(e_1)=\left(\begin{array}{c}0\\1\end{array}\right)=e_2 \qeq f(e_2)=\left(\begin{array}{c}2\\0\end{array}\right)=2e_1\]
		La famille $(2e_1,e_2)$ est libre (car les vecteurs ne sont pas colinéaires) donc forme une base de $\Img f$ :
		\[\Img f = \Vect(2e_1,e_2) = \Vect(e_1,e_2) = \MM_{2,1}(\R)\]
		Ainsi, $f$ est surjective.
\end{itemize}
\textbf{Bilan} : $f$ est bien bijective.
[soluce.]
[/prof 15]

[.definition]
Soient $E$ et $F$ deux espaces vectoriels.
[=item]
* Si $f \in \mathcal{L}(E,F)$ est bijective, on dit que $f$ est un \textbf{isomorphisme} de $E$ dans $F$.
* Si $f \in \mathcal{L}(E)$ est bijective, on dit que $f$ est un \textbf{automorphisme}.
* On note $\mathrm{Isom}(E,F)$ l'ensemble des isomorphismes de $E$ dans $F$, et $\mathrm{Aut}(E)$ ou $GL(E)$ l'ensemble des automorphismes de $E$.
[item=]
[definition.]

[.theoreme]
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathrm{Isom}(E,F)$. Alors $f^{-1}$ est également une application linéaire et $f^{-1}\in \mathrm{Isom}(F,E)$.
[theoreme.]

[prof]
[.demonstration]
Soient $x,y$ deux éléments de $F$ et $\lambda$ un réel. Puisque $f$ est bijective, il existe un unique $a\in E$ tel que $f(a)=x$ et un unique $b\in E$ tel que $f(b)=y$. Donc $f(\lambda a + b)=\lambda f(a)+f(b) = \lambda x + y$ donc, par unicité de bijectivité de $f$, $\lambda a + b = f^{-1}(\lambda f(a)+f(b))$, c'est-à-dire
\[\lambda f^{-1}(x)+f^{-1}(y)=f^{-1}(\lambda x + y)\]
$f^{-1}$ est bien linéaire.
[demonstration.]
[/prof 6]

[afaire Exercices \lienexo{11}, \lienexo{12}, \lienexo{13}, \lienexo{14} et \lienexo{15}.]

%%% Fin du cours %%%
