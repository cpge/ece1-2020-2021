REPO = $(shell echo $(CHAP) | sed 's/^0*//')
REP = $(shell printf "%02d" $(REPO) )
TITRE = $(shell head -n 1 "tex/Chap$(CHAP)/Cours.tex" | cut -d "{" -f 2 | cut -d "}" -f 1 )

pdf: titrechap chapprof chapeleve chappubli clean
edit: titrechap edition clean
complet: courscomplet clean

toutpdf:
	num=9; while [ $$num -le $(REP) ]; do \
		eval $(MAKE) pdf CHAP=$$num ;\
		((num = num + 1)) ; \
	done

titre:
	@./scripts/titre
	
titrechap:
	@printf "Traitement du chapitre \033[31;01m%s\033[00m : \033[31;01m%s\033[00m\n" $(REP) "$(TITRE)"

edition:
	@echo "\\\documentclass[maths,noir]{antoine}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof" >> encours.nouveau.tex
	@echo "\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo "\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\titredulivre{Prépa ECE1}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/Chap$(CHAP)/graphe/}{tex/Chap$(CHAP)/td/}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/Chap$(CHAP)/pic/}{tex/Chap$(CHAP)/mp/}}" >> encours.nouveau.tex
	@echo "\\\lstset{inputpath=tex/Chap$(CHAP)/script/}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\setcounter{chapter}{" >> encours.nouveau.tex
	@echo $(shell echo $(CHAP)\-1 | bc) >> encours.nouveau.tex
	@echo "}" >> encours.nouveau.tex
	@echo "\\\begin{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Cours}" >> encours.nouveau.tex
	@echo "\\\ifodd\\\thepage\\\else \\\hbox{}\\\fi" >> encours.nouveau.tex
	@echo "\\\end{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Exo}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
	@printf "Edition du chapitre %s...\n" $(CHAP)
#	@./scripts/edit $(CHAP)
	@open -a Texpad ./encours.nouveau.tex
	@read -a S

chapprof:
	@printf "[  ] Compilation du chapitre %s - prof" $(REP)
	@echo "\\\documentclass[maths]{antoine}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof" >> encours.nouveau.tex
	@echo "\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo "\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\titredulivre{Prépa ECE1}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/Chap$(CHAP)/graphe/}{tex/Chap$(CHAP)/td/}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/Chap$(CHAP)/pic/}{tex/Chap$(CHAP)/mp/}}" >> encours.nouveau.tex
	@echo "\\\lstset{inputpath=tex/Chap$(CHAP)/script/}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\setcounter{chapter}{" >> encours.nouveau.tex
	@echo $(shell echo $(CHAP)\-1 | bc) >> encours.nouveau.tex
	@echo "}" >> encours.nouveau.tex
	@echo "\\\begin{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Cours}" >> encours.nouveau.tex
	@echo "\\\ifodd\\\thepage\\\else \\\hbox{}\\\fi" >> encours.nouveau.tex
	@echo "\\\end{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Exo}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*prof.pdf
	@./scripts/compile
	@./scripts/renomme $(REP) prof "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - prof\n" $(REP)

chapeleve:
	@printf "[  ] Compilation du chapitre %s - eleve" $(REP)
	@echo "\\\documentclass[maths,noir]{antoine}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\csundef{@afterend@exercices@hook}" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof" >> encours.nouveau.tex
	@echo "\\\proffalse" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo "\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\titredulivre{Prépa ECE1}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/Chap$(CHAP)/graphe/}{tex/Chap$(CHAP)/td/}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/Chap$(CHAP)/pic/}{tex/Chap$(CHAP)/mp/}}" >> encours.nouveau.tex
	@echo "\\\lstset{inputpath=tex/Chap$(CHAP)/script/}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\setcounter{chapter}{" >> encours.nouveau.tex
	@echo $(shell echo $(CHAP)\-1 | bc) >> encours.nouveau.tex
	@echo "}" >> encours.nouveau.tex
	@echo "\\\begin{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Cours}" >> encours.nouveau.tex
	@echo "\\\ifodd\\\thepage\\\else \\\hbox{}\\\fi" >> encours.nouveau.tex
	@echo "\\\end{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Exo}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*eleve.pdf
	@./scripts/compile
	@./scripts/renomme $(REP) eleve "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - eleve\n" $(REP)

chappubli:
	@printf "[  ] Compilation du chapitre %s - publi" $(REP)
	@echo "\\\documentclass[maths]{antoine}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\csundef{@afterend@exercices@hook}" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof" >> encours.nouveau.tex
	@echo "\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo "\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\titredulivre{Prépa ECE1}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{\\\href{mailto:antoine.crouzet@normalesup.org}{A. Crouzet}}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{\\\doclicenseIcon}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/Chap$(CHAP)/graphe/}{tex/Chap$(CHAP)/td/}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/Chap$(CHAP)/pic/}{tex/Chap$(CHAP)/mp/}}" >> encours.nouveau.tex
	@echo "\\\lstset{inputpath=tex/Chap$(CHAP)/script/}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\setcounter{chapter}{" >> encours.nouveau.tex
	@echo $(shell echo $(CHAP)\-1 | bc) >> encours.nouveau.tex
	@echo "}" >> encours.nouveau.tex
	@echo "\\\begin{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Cours}" >> encours.nouveau.tex
	@echo "\\\ifodd\\\thepage\\\else \\\hbox{}\\\fi" >> encours.nouveau.tex
	@echo "\\\end{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Exo}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*publi.pdf
	@./scripts/compile 
	@./scripts/renomme $(REP) publi "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - publi\n" $(REP)

courscomplet:
	@cp tex/cours_complet.tex encours.complet.tex
	@./scripts/compilecomplet
	@mv encours.complet.pdf "PDF/Cours complet.pdf"

clean:
	@printf "Nettoyage.\n"
	@${RM} encours.* 
	@find . -name "*.aux" -exec rm {} \;
