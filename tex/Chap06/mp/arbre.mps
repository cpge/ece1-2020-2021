%!PS
%%BoundingBox: -1 -75 220 96 
%%HiResBoundingBox: -0.25 -74.07634 219.87665 95.33617 
%%Creator: MetaPost 2.00
%%CreationDate: 2020.11.03:2103
%%Pages: 1
%*Font: ecrm1000 9.96265 9.96265 52:88
%*Font: cmr7 5.92781 6.97385 30:fa
%*Font: cmmi10 8.4683 9.96265 3d:8
%*Font: cmr10 9.96265 9.96265 31:d
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
113.3858 49.6063 lineto stroke
61.55742 37.4776 moveto
(6) cmr7 5.92781 fshow
63.9921 35.74164 moveto
(=) cmmi10 8.4683 fshow
67.75584 35.74164 moveto
(10) cmr7 5.92781 fshow
116.3858 46.17554 moveto
(R) ecrm1000 9.96265 fshow
newpath 0 0 moveto
113.3858 -49.6063 lineto stroke
61.55742 -22.05054 moveto
(4) cmr7 5.92781 fshow
63.9921 -23.7865 moveto
(=) cmmi10 8.4683 fshow
67.75584 -23.7865 moveto
(10) cmr7 5.92781 fshow
116.3858 -53.03705 moveto
(V) ecrm1000 9.96265 fshow
newpath 126.7176 49.6063 moveto
211.75696 92.12598 lineto stroke
172.95474 82.8319 moveto
(1) cmr7 5.92781 fshow
175.38942 81.09593 moveto
(=) cmmi10 8.4683 fshow
179.15317 81.09593 moveto
(2) cmr7 5.92781 fshow
214.75696 88.91577 moveto
(1) cmr10 9.96265 fshow
newpath 126.7176 49.6063 moveto
211.75696 49.6063 lineto stroke
172.95474 57.31982 moveto
(1) cmr7 5.92781 fshow
175.38942 55.58386 moveto
(=) cmmi10 8.4683 fshow
179.15317 55.58386 moveto
(3) cmr7 5.92781 fshow
214.75696 46.39609 moveto
(2) cmr10 9.96265 fshow
newpath 126.7176 49.6063 moveto
211.75696 7.08661 lineto stroke
172.95474 31.80775 moveto
(1) cmr7 5.92781 fshow
175.38942 30.0718 moveto
(=) cmmi10 8.4683 fshow
179.15317 30.0718 moveto
(6) cmr7 5.92781 fshow
214.75696 3.8764 moveto
(4) cmr10 9.96265 fshow
newpath 126.856 -49.6063 moveto
211.89536 -28.34645 lineto stroke
173.09186 -29.13704 moveto
(1) cmr7 5.92781 fshow
175.52654 -30.873 moveto
(=) cmmi10 8.4683 fshow
179.29028 -30.873 moveto
(2) cmr7 5.92781 fshow
214.89536 -31.55666 moveto
(2) cmr10 9.96265 fshow
newpath 126.856 -49.6063 moveto
211.89536 -70.86613 lineto stroke
173.09186 -54.64848 moveto
(1) cmr7 5.92781 fshow
175.52654 -56.38445 moveto
(=) cmmi10 8.4683 fshow
179.29028 -56.38445 moveto
(2) cmr7 5.92781 fshow
214.89536 -74.07634 moveto
(4) cmr10 9.96265 fshow
showpage
%%EOF
