\chapter{Convergence d'une suite}

\objectifintro{Nous allons généraliser la notion de limite, qui a été vue en classe de Terminale. Nous introduirons des méthodes pour déterminer les limites et des théorèmes permettant de montrer l'existence de limites.}

\begin{extrait}{Donna Tartt (1963-) -- \emph{Le Chardonneret}}
Quand je regardais le tableau, j’éprouvais la même convergence en un seul et unique point : un bref instant touché par le soleil qui existait maintenant et pour toujours. C’est fortuitement que je remarquais ma chaîne à la cheville de l’oiseau, ou que je songeais combien la vie de cette petite créature, battant brièvement des ailes puis toujours forcée, sans espoir, d’atterrir au même endroit, avait dû être cruelle.
\end{extrait}

\begin{objectifs}
\begin{numerote}
	\item Concernant les limites :
		\begin{itemize}[label=\textbullet]
			\item \hyperref[objectif-07-1]{Connaître la définition mathématique des limites}\dotfill $\Box$
			\item \hyperref[objectif-07-2]{Savoir déterminer des limites en utilisant les théorèmes  (somme, produit, quotient)}\dotfill $\Box$
			\item \hyperref[objectif-07-3]{Savoir utiliser le théorème d'encadrement et les théorèmes de comparaison}\dotfill $\Box$
			\item \hyperref[objectif-07-4]{Connaître les croissances comparées}\dotfill $\Box$
			\item \hyperref[objectif-07-5]{Savoir appliquer le théorème de la limite monotone}\dotfill$\Box$
		\end{itemize}
			\item \hyperref[objectif-07-6]{Savoir reconnaître les suites adjacentes}\dotfill $\Box$
			\item \hyperref[objectif-07-7]{Savoir démontrer qu'un suite est négligeable devant une autre}\dotfill$\Box$
			\item \hyperref[objectif-07-8]{Savoir démontrer que deux suites sont équivalentes}\dotfill$\Box$
\end{numerote}
	
\end{objectifs}

\section{Généralités}

	\subsection{Limite finie}


\begin{definition}
\label{objectif-07-1}
Soit $(u_n)$ une suite et $\ell$ un nombre réel. Si tout intervalle ouvert contenant $\ell$ contient tous les termes de la suite à partir d'un certain rang, on dit que la suite $(u_n)$ \textbf{a pour limite $\ell$}, et on note
$$\lim_{n\rightarrow +\infty} u_n=\ell \quad\text{ou}\quad u_n\tendversen{n\to +\infty} \ell$$
Mathématiquement, $(u_n)$ a pour limite $\ell$ si et seulement si
$$\forall~\eps >0,~\exists~n_0\in \N,~\forall~n\in \N,~~n\geq n_0 \Rightarrow |u_n-\ell|<\eps$$
\end{definition}

\begin{center}
\includegraphics{tex/Chap07/mp/limite.mps}
\end{center}

\begin{exemple}
La suite $u$ définie pour tout $n$ par $u_n=\frac{1}{n+1}$ converge vers $0$ : $$\lim_{n\to +\infty} \frac{1}{n+1}=0$$
\end{exemple}

\begin{propriete}
Si une suite $(u_n)$ a une limite finie, celle-ci est \textbf{unique}.
\end{propriete}

\ifprof
\begin{demonstration}
On suppose que $(u_n)$ converge vers $\ell$ et $\ell'$ et que $\ell\neq \ell'$. Donc la distance entre $\ell$ et $\ell'$ est non nulle. On la note $d$, et on s'intéresse aux deux intervalles $]\ell-\frac{d}{4};\ell+\frac{d}{4}[$ et $]\ell'-\frac{d}{4};\ell'+\frac{d}{4}[$. Par définition de la convergence, tous les termes de la suite sont dans ces deux intervalles à partir d'un certain rang. Or :
\begin{center}
\includegraphics{tex/Chap07/mp/unicite.mps}
\end{center}
les deux intervalles sont disjoints, ce qui est absurde.
\end{demonstration}
\else
\lignes{6}
\fi

	\subsection{Limite infinie}

\begin{definition}
Soit $(u_n)$ une suite.
\begin{itemize}
	\item[$\bullet$] Si tout intervalle de la forme $]a;+\infty[$ contient tous les termes de la suite à partir d'un certain rang, on dit que la suite $(u_n)$ \textbf{a pour limite $+\infty$}, et on note $$\lim_{n\rightarrow +\infty} u_n=+\infty \quad\text{ou}\quad u_n\tendversen{n\to+\infty} +\infty$$
	\item[$\bullet$] Si tout intervalle de la forme $]-\infty,a[$ contient tous les termes de la suite à partir d'un certain rang, on dit que la suite $(u_n)$ \textbf{a pour limite $-\infty$}, et on note $$\lim_{n\rightarrow +\infty} u_n=-\infty \quad\text{ou}\quad u_n\tendversen{n\to+\infty} -\infty$$
\end{itemize}
Mathématiquement, $(u_n)$ a pour limite $+\infty$ si et seulement si
$$\forall~A\in \R,~\exists~n_0\in \N,~\forall~n\in \N,~n\geq n_0 \Rightarrow u_n>A$$	
\end{definition}

\begin{center}
\includegraphics{tex/Chap07/mp/limiteinfinie.mps}
\end{center}

\begin{exemple}
La suite $u$ définie pour tout $n$ par $u_n=n$ a pour limite $+\infty$, et la suite $v$ définie pour tout $n$ par $v_n=-n^2$ a pour limite $-\infty$ :
		$$\lim_{n\rightarrow +\infty} n=+\infty~\textrm{ et }~\lim_{n\rightarrow +\infty} -n^2=-\infty$$	
\end{exemple}

\begin{algorithme}
Si une suite \textbf{croissante} $(u_n)$ a pour limite $+\infty$, on peut utiliser l'algorithme suivant pour déterminer le plus petit entier $n$ vérifiant $u_n> A$ (où $A$ est un réel positif quelconque) :

~\\
\begin{algorithm}[H]
\DontPrintSemicolon
\SetAlgoLined
\SetKwInOut{Initialisation}{Initialisation}
\SetKwFor{Tq}{Tant que}{}{FinTantque}
\Entree{Saisir $A$ (nombre positif)}
\Initialisation{\;$n\leftarrow 0$\; $U \leftarrow u_0$}
\Tq{$U \leq A$}{
	$n \leftarrow n+1$\;
	$U \leftarrow u_{n}$\;}
\Sortie{Afficher $n$}
\caption{SEUIL}
\end{algorithm}
\end{algorithme}

En Scilab, pour la suite $u$ définie par
$$\left \{ \begin{array}{l} u_0=1\\ \forall~n,~u_{n+1}=1+u_n^2\end{array}\right.$$
on obtient :

\begin{scilab}[Seuil pour une suite croissante]
{\small \inputscilab{seuil.sce}}
\end{scilab}


	\subsection{Suite convergente}

\begin{definition}
On dit qu'une suite est \textbf{convergente} si elle possède une limite finie. On dit qu'elle est \textbf{divergente} si elle possède une limite infinie.	
\end{definition}


	\subsection{Suite sans limite}

\begin{remarque}
Certaines suites ne possèdent aucune limite, que ce soit finie ou infinie.	
\end{remarque}

\begin{exemple}
La suite $((-1)^n)$ prend la valeur $1$ aux termes pairs, et $-1$ aux termes impairs. Elle ne peut donc ni converger, ni diverger : elle ne possède donc pas de limite.	
\end{exemple}

\section{Théorèmes sur les limites}
%
% 	\subsection{Suites de types $u_n=f(v_n)$}
%
% %TODO On n'a pas les limites de fonctions (!). A repousser ?
% \theoremeL{Soit $v$ une suite. Soit $f$ une fonction définie sur $\R$ et $u$ la suite définie par $u_n=f(v_n)$ pour tout $n$. Soient $a,b$ deux éléments de $\R\cup \{-\infty;+\infty\}$.\\
% Si $\displaystyle{\lim_{n\rightarrow +\infty}v_n = a}$, et si $\displaystyle{\lim_{x\rightarrow a} f(x)=b}$, alors
% $$\lim_{n\rightarrow +\infty}u_n = b$$
% }
%
% \begin{demonstration}
% Admis.
% \end{demonstration}

	\subsection{Théorème de comparaison}

\begin{theoreme}
\label{objectif-07-3}
Soient $(u_n)$ et $(v_n)$ deux suites convergentes, de limites respectives $\ell$ et $\ell'$. Si, pour tout $n\geq n_0$, on a $u_n \leq v_n$, alors $\ell \leq \ell'$.	
\end{theoreme}


\begin{demonstration}
	Supposons au contraire que $\ell>\ell'$. En notant $d$ la distance (non nulle) entre $\ell$ et $\ell'$, cela signifie qu'à partir d'un certain rang, les termes de la suite $(u_n)$ se trouve dans l'intervalle $]\ell-\frac{d}{4};\ell+\frac{d}{4}[$. Or, $v_n\geq u_n$, donc, à partir d'un certain rang $v_n \geq \ell-\frac{d}{4}$. Donc l'intervalle $]\ell'-\frac{d}{4};\ell'+\frac{d}{4}[$ ne contient aucun élément de la suite $(v_n)$ à partir d'un certain rang : absurde.
\end{demonstration}

	\subsection{Théorème d'encadrement}

\begin{theoreme}[Théorème d'encadrement] 
Soient $(u_n)$, $(v_n)$ et $(w_n)$ trois suites. On suppose que, pour tout $n \geq n_0$,
	$u_n \leq v_n \leq w_n$
et que $\displaystyle{\lim_{n\rightarrow+\infty} u_n=\lim_{n\rightarrow+\infty} w_n=\ell}$.
Alors, $(v_n)$ converge et $$\lim_{n\rightarrow+\infty} v_n=\ell$$
\end{theoreme}

\begin{center}
\includegraphics{tex/Chap07/mp/gendarme.mps}
\end{center}

\begin{remarque}
Ce théorème est également appelé théorème des gendarmes.	
\end{remarque}

\ifprof
\begin{demonstration}
Soit $I$ un intervalle ouvert contenant $\ell$. \\Par définition de la limite, il existe un rang $n_1$ tel que pour $n\geq n_1$,  $u_n$ soit dans $I$. De même, il existe un rang $n_2$ tel que, pour $n\geq n_2$, $w_n$ soit dans $I$. \\ Mais alors, pour tout $n$ plus grand que $n_1$ \textbf{et} $n_2$, $u_n\in I$ et $w_n\in I$.
Or, $u_n\leq v_n\leq w_n$, et puisque $I$ est un intervalle, on en déduit que pour tout $n$ plus grand que $n_1$ et $n_2$, $v_n \in I$.\\
Ceci étant vrai pour tout intervalle ouvert $I$ contenant $\ell$, on en déduit bien que $\displaystyle{\lim_{n\rightarrow +\infty} v_n=\ell}$.
\end{demonstration}
\else
\lignes{6}
\fi

\begin{methode}
Pour déterminer la limite d'une suite où $(-1)^n$ apparait, on appliquera (quasi) systématiquement le théorème d'encadrement.	
\end{methode}

\begin{exemple}
Déterminer la limite de la suite $(u_n)$ définie pour tout $n>0$ par $$u_n=\frac{(-1)^n+2}{n}$$	
\end{exemple}

\solution[6]{
Pour tout $n\neq 0$, on a $-1\leq (-1)^n \leq 1$, donc $1 \leq (-1)^n+2 \leq 3$ et puisque $n>0$, on a
$$\frac{1}{n}\leq \frac{(-1)^n+2}{n} \leq \frac{3}{n}$$
Or, $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n}=\lim_{n\rightarrow +\infty} \frac{3}{n}=0}$. Par le théorème d'encadrement, la limite de $(u_n)$ existe et $$\lim_{n\rightarrow +\infty} \frac{(-1)^n+2}{n}=0$$
}

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites, et $\ell$ un réel. On suppose que pour tout $n\geq n_0$, \\$|u_n-\ell|\leq v_n$ et que $\displaystyle{\lim_{n\rightarrow +\infty} v_n=0}$. Alors
$$\lim_{n\rightarrow +\infty} u_n=\ell$$	
\end{theoreme}


\begin{demonstration}
Application du théorème d'encadrement.
\end{demonstration}

\begin{exemple}
On constate que, pour tout $n\geq 0$ :
$$\left | \frac{(-1)^n}{n} \right| = \frac{1}{n}$$
et $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n}=0}$. Ainsi, d'après le théorème précédent, la suite $\left( \frac{(-1)^n}{n}\right)_{n\geq 1}$ converge et a pour limite $0$.	
\end{exemple}

\begin{theoreme}[Théorème de comparaison] 
Soient $(u_n)$ et $(v_n)$ deux suites.
\begin{itemize}
	\item[$\bullet$] Si pour tout $n\geq n_0$, $u_n\geq v_n$ et si $\displaystyle{\lim_{n\rightarrow +\infty} v_n=+\infty}$ alors $\displaystyle{\lim_{n\rightarrow +\infty} u_n=+\infty}$
	\item[$\bullet$] Si pour tout $n\geq n_0$, $u_n\leq v_n$ et si $\displaystyle{\lim_{n\rightarrow +\infty} v_n=-\infty}$ alors $\displaystyle{\lim_{n\rightarrow +\infty} u_n=-\infty}$
\end{itemize}	
\end{theoreme}

\ifprof
\begin{demonstration}
Démontrons le premier point. Soit $A$ un réel strictement positif quelconque. Puisque $\displaystyle{\lim_{n\rightarrow +\infty} v_n=+\infty}$, il existe un rang $n_0$ tel que, pour tout $n\geq n_0$, on a $v_n\in[a;+\infty[$. Or, puisque pour tout $n$, $u_n\geq v_n$, on a également $u_n\in[a;+\infty[$ pour $n\geq n_0$.\\
Par définition de la limite infinie, cela signifie donc que $\displaystyle{\lim_{n\rightarrow +\infty} u_n=+\infty}$
\end{demonstration}
\else
\lignes{5}
\fi

\subsection{Convergence des suites monotones}

\begin{theoreme}[Théorème de la limite monotone] 
\label{objectif-07-5}
Toute suite croissante majorée converge. Toute suite décroissante minorée converge.
\end{theoreme}

\begin{demonstration}
	Théorème admis.
\end{demonstration}

\begin{theoreme}
Toute suite croissante non majorée a pour limite $+\infty$. Toute suite décroissante non minorée a pour limite $-\infty$.	
\end{theoreme}

\ifprof
\begin{demonstration}
Soit $(u_n)$ une suite croissante non majorée. La suite étant non majorée, quel que soit le réel $a$, on peut trouver un terme $u_N$ de la suite strictement supérieur à $a$. On a donc
$u_N > a$.\\ Or, la suite $u$ étant croissante, on a, pour tout $n \geq N$, $u_n \geq u_N > a$.
Tous les termes de la suite $u$ sont donc dans l'intervalle $]a;+\infty[$ a partir d'un certain rang. Ceci étant vrai pour tout $a$, par définition, on en déduit
$$\lim_{n\rightarrow +\infty} u_n=+\infty$$
\end{demonstration}
\else
\lignes{6}
\fi

\begin{exemple}
La suite $(u_n)$ définie par $u_n=n^2$ est croissante, non majorée : sa limite est $+\infty$. \\La suite $v$ définie pour tout $n>0$ par $v_n=1-\frac{1}{n}$ est croissante, majorée (par 1) : elle converge donc.
\end{exemple}

\begin{theoreme}
Soit $(u_n)$ une suite \textbf{croissante} de limite $\ell$. Alors, pour tout entier $n$, on a $u_n\leq \ell$.	
\end{theoreme}

\ifprof
\begin{demonstration}
Supposons qu'il existe un entier $n_0$ tel que $u_{n_0}>\ell$. Notons $r=u_{n_0}-\ell>0$. Par croissance de la suite $u$, on a donc, pour tout $n\geq n_0$, $u_n\geq u_{n_0}$. Mais alors, l'intervalle $]\ell-r;\ell+r[$ ne contient aucun terme de la suite à partir de $n_0$. Cela contredit le fait que la suite $u$ converge vers $\ell$ : ceci est absurde.
\end{demonstration}
\else
\lignes{4}
\fi

\afaire{Exercices \lienexo{5} et \lienexo{6}.} %% OK


\section{Opération sur les limites}
\label{objectif-07-2}

    \subsection{Limites usuelles}

\begin{theoreme}
	On dispose des limites suivantes :
\begin{center}
\begin{tabular}{lll}
$\displaystyle{\lim_{n\rightarrow +\infty} n^p = +\infty ~~(p\in \N^*)}$~~~~ & $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n^p} = 0 ~~(p\in \N^*)}$~~~~ & $\displaystyle{\lim_{n\rightarrow +\infty} \sqrt{n} = +\infty}$\\~&~&~\\
$\displaystyle{\lim_{n\rightarrow +\infty} |n| = +\infty}$ &
$\displaystyle{\lim_{n\rightarrow +\infty} \ln(n) = +\infty}$ &
$\displaystyle{\lim_{n\rightarrow +\infty} \E^{n} = +\infty}$\\~&~&~\\
$\displaystyle{\lim_{n\rightarrow +\infty} n^\alpha = +\infty ~~(\alpha \in \R^*_+)}$&
$\displaystyle{\lim_{n\rightarrow +\infty} n^\alpha = 0 ~~(\alpha \in \R^*_-)}$&
$\displaystyle{\lim_{n\rightarrow +\infty} n!=+\infty}$
\end{tabular}
\end{center}
\end{theoreme}

    \subsection{Limite de $u_n+v_n$}

\begin{center}{\renewcommand\arraystretch{1.4}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim v_n \backslash \lim u_n$ & $\ell$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'$ & $\ell+\ell'$ & $+\infty$ & $-\infty$ \\
  \hline
  $+\infty$ & $+\infty$ & $+\infty$ & IND \\
  \hline
  $-\infty$ & $-\infty$ & IND & $-\infty$\\
  \hline
\end{tabular}}
\end{center}

\begin{exo}
Déterminer la limite $\ds{\lim_{n\rightarrow +\infty} n^2+\sqrt{n}}$.	
\end{exo}

\solution[2]{En effet, $\ds{\lim_{n\to +\infty} n^2=+\infty}$ et $\ds{\lim_{n\to +\infty}\sqrt{n}=+\infty}$. Par somme, $\ds{\lim_{n\to +\infty} n^2+\sqrt{n}=+\infty}$.}


    \subsection{Limite de $u_n \times v_n$}

\begin{center}{\renewcommand\arraystretch{1.4}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim v_n \backslash \lim u_n$ & $\ell\neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'\neq 0$ & $\ell.\ell'$ & signe($\ell'$).$\infty$ & -signe($\ell'$).$\infty$ \\
  \hline
  $+\infty$ & signe($\ell$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $-\infty$ & -signe($\ell$).$\infty$ & $-\infty$ & $+\infty$\\
  \hline
\end{tabular}}
\end{center}

\begin{remarque}
On retiendra qu'on applique la règle des signes pour déterminer le signe du résultat.	
\end{remarque}

\begin{exo}
Déterminer la limite $\ds{\lim_{n\rightarrow +\infty} n\eu{n}}$.	
\end{exo}

\solution[2]{En effet, $\ds{\lim_{n\to +\infty} n=+\infty}$ et $\ds{\lim_{n\to +\infty}\eu{n}=+\infty}$. Par produit, $\ds{\lim_{n\to +\infty} n\eu{n}=+\infty}$.}

    \subsection{Limite de $\frac{u_n}{v_n}$ si la limite de $(v_n)$ n'est pas nulle}

\begin{center}{\renewcommand\arraystretch{1.4}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim v_n \backslash \lim u_n$ & $\ell$ & $+\infty$ & $-\infty$ \\
 \hline
  $\ell'\neq 0$ & $\frac{\ell}{\ell'}$ & signe($\ell'$).$\infty$ & -signe($\ell'$).$\infty$ \\
  \hline
  $+\infty$ & $0$ & IND & IND \\
  \hline
  $-\infty$ & $0$ & IND & IND\\
  \hline
\end{tabular}}
\end{center}

\begin{exo}
Déterminer la limite $\ds{\lim_{n\to +\infty}\frac{2-\frac{1}{n}}{\frac{2}{n}-1}}$.	
\end{exo}

\solution[4]{Par somme, on a les limites suivantes :
\[ \lim_{n\to +\infty}2-\frac{1}{n}=2 \quad\text{et}\quad \lim_{n\to +\infty} \frac{2}{n}-1=-1\]
Par quotient, on en déduit que \[ \lim_{n\to +\infty}\frac{2-\frac{1}{n}}{\frac{2}{n}-1}=-2 \]
}

    \subsection{Limite de $\frac{u_n}{v_n}$ si la limite de $(v_n)$ est nulle}

\begin{center}{\renewcommand\arraystretch{1.4}
\begin{tabular}{|c|c|c|c|c|}
\hline
 $\lim v_n \backslash \lim u_n$ & $0$ & $l \neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $0^+$ & IND & signe($l$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $0^-$ & IND & -signe($l$).$\infty$ & $-\infty$ & $+\infty$ \\
  \hline
\end{tabular}}
\end{center}

    \subsection{Limite de $(q^n)$}

\begin{theoreme}
Soit $q$ un nombre réel. On s'intéresse à la suite $(q^n)$.
\begin{itemize}
	\item[$\bullet$] Si $q > 1$, $\displaystyle{\lim_{n\rightarrow +\infty} q^n=+\infty}$.
	\item[$\bullet$] Si $-1<q<1$, $\displaystyle{\lim_{n\rightarrow +\infty} q^n=0}$.
	\item[$\bullet$] Si $q = 1$, $\displaystyle{\lim_{n\rightarrow +\infty} 1^n=1}$.
	\item[$\bullet$] Si $q\leq -1$, la suite $(q^n)$ ne possède pas de limite.
\end{itemize}	
\end{theoreme}

\ifprof
\begin{demonstration}
Tout part de l'inégalité de Bernoulli, qui se démontre à l'aide d'une récurrence : pour tout $x>0$, et pour tout entier $n$, on a $$(1+x)^n\geq 1+nx$$
\begin{itemize}
	\item[$\bullet$] Si $q>1$, on peut écrire $q=1+x$ avec $x=q-1>0$. D'après l'inégalité de Bernoulli $$q^n\geq 1+nx=1+n(q-1)$$
	Or, puisque $q-1>0$, on a
	$$\lim_{n\rightarrow +\infty} 1+n(q-1) = +\infty$$
	Par théorème d'encadrement, $$\lim_{n\rightarrow +\infty} q^n=+\infty$$
	\item[$\bullet$] Si $q=1$, la suite $(q^n)$ est la suite constante égale à $1$. Elle converge donc vers $1$.
	\item[$\bullet$] Si $-1<q<1$, posons $Q=\frac{1}{|q|}>1$. Alors, d'après ce qui précède
	$$\lim_{n\rightarrow +\infty} Q^n=+\infty$$
	Or, on a $$0\leq |q|^n =\left(\frac{1}{Q}\right)^n=\frac{1}{Q^n}$$
	Par théorème d'encadrement, puisque $\displaystyle{\lim_{n\rightarrow +\infty}Q^n =+\infty}$ on a
	$$\lim_{n\rightarrow +\infty}|q|^n=0\textrm{ et donc } \lim_{n\rightarrow +\infty}q^n=0$$
	\item[$\bullet$] Si $q=-1$, la suite $(-1)^n$ vaut $1$ pour les termes pairs, et $-1$ pour les termes impairs. Elle ne peut donc converger.
	\item[$\bullet$] Si $q<-1$, on a $\displaystyle{\lim_{n\rightarrow +\infty} |q|^n =+\infty}$. Donc la suite $(|q|^n)$ prend des valeurs aussi grandes que l'on veut. Or, la suite $(q^n)$ prend des valeurs positives pour les termes pairs, et négatives pour les termes impairs. Elle ne peut donc pas avoir de limite.
\end{itemize}
\end{demonstration}
\else
\lignes{20}
\fi

\begin{methode}
Pour déterminer la limite d'une suite composée de puissances, on met les plus grandes puissances en facteur, et on utilise le résultat précédent.	
\end{methode}

\begin{exo}
Soit $u$ la suite définie pour tout entier $n$ par $$u_n=\frac{3^n+4^n}{3\times 4^n+2^n}$$
Déterminer la limite de la suite $u$.	
\end{exo}

\solution[5]{Pour tout entier $n$, on a $$u_n=\frac{4^n\left(1+\frac{3^n}{4^n}\right)}{4^n\left(3+\frac{2^n}{4^n}\right)}=\frac{ 1+\left(\frac{3}{4}\right)^n}{3+\left(\frac{2}{4}\right)^n}$$
Puisque $-1<\frac{3}{4}<1$ et $-1<\frac{2}{4}<1$, on a $$\lim_{n\rightarrow +\infty} \left(\frac{3}{4}\right)^n = \lim_{n\rightarrow +\infty} \left(\frac{2}{4}\right)^n=0$$
Par somme et quotient, on en déduit que $$\lim_{n\rightarrow +\infty} u_n=\frac{1}{3}$$
}

\afaire{Exercices \lienexo{11}, \lienexo{12} et \lienexo{13}.}


    \subsection{Croissances comparées}

\begin{theoreme}[Croissances comparées]
\label{objectif-07-4}	
Pour tous réels $a$ et $b$ strictement positifs :
$$\lim_{n\rightarrow +\infty} \frac{\E^n}{n^a} = +\infty$$
$$\lim_{n\rightarrow +\infty} \frac{n^a}{\ln^b(n)} = +\infty$$
et de manière générale, pour $q>1$,
$$\lim_{n\rightarrow +\infty} \frac{q^n}{n^a} = +\infty ~~\textrm{et}~~\lim_{n\rightarrow +\infty} \frac{n!}{q^n} = +\infty$$
\end{theoreme}

\begin{remarque}
On note souvent de la manière suivante (avec $q>1$ et $a>0$) :
$$\ln^b(n) << n^a << q^n << n!$$
On donnera une notation rigoureuse à la fin de ce chapitre.	
\end{remarque}

\begin{demonstration}
Voir chapitre Limite de fonctions.
\end{demonstration}

\begin{exemple}
On a
$$\lim_{n\rightarrow +\infty} \frac{n}{\E^n} = 0 \textrm{ et } \lim_{n\rightarrow +\infty} \frac{\ln(n)}{n}=0$$	
\end{exemple}

\begin{exo}
Déterminer $\displaystyle{\lim_{n\rightarrow +\infty} \frac{n+\ln(n)}{n+1}}$.	
\end{exo}

\solution[5]{On constate que, pour tout $n>0$ :
$$\frac{n+\ln(n)}{n+1} = \frac{n\left(1+\frac{\ln(n)}{n}\right)}{n\left(1+\frac{1}{n}\right)} = \frac{1+\frac{\ln(n)}{n}}{1+\frac{1}{n}}$$
Par croissances comparées, $\displaystyle{\lim_{n\rightarrow +\infty} \frac{\ln(n)}{n}=0}$. Par somme, on a donc $\displaystyle{\lim_{n\rightarrow +\infty} 1+\frac{\ln(n)}{n}=1}$. On a également $\displaystyle{\lim_{n\rightarrow +\infty} 1+\frac{1}{n}=1}$. Par quotient,
$$\lim_{n\rightarrow +\infty} \frac{n+\ln(n)}{n+1}=1$$
}

\afaire{Exercices \lienexo{14} et \lienexo{1}.}


\section{Suites adjacentes}

\begin{definition}
\label{objectif-07-6}
On dit que deux suites $(u_n)$ et $(v_n)$ sont \textbf{adjacentes} si $(u_n)$ est croissante, $(v_n)$ est décroissante et si $\displaystyle{\lim_{n\rightarrow +\infty} v_n-u_n=0}$.
\end{definition}

\begin{exemple}
Les suites $u$ et $v$ définies pour tout $n\geq 1$ par $u_n=-\frac{1}{n}$ et $v_n=\frac{1}{n}$ sont adjacentes. 	
\end{exemple}

\solution[5]{En effet, la suite $u$ est croissante et $v$ est décroissante :
pour tout $n$, 
\begin{align*}
 u_{n+1}-u_n &= -\frac{1}{n+1} - \left(-\frac{1}{n}\right)\\
 &= \frac{-n+(n+1)}{n(n+1)} = \frac{1}{n(n+1)} > 0\\
 v_{n+1}-v_n &= \frac{1}{n+1}-\frac{1}{n} \\
  &= \frac{n-(n+1)}{n(n+1)} = -\frac{1}{n(n+1)} < 0 
\end{align*}
Enfin, pour tout $n$, \[ v_n-u_n=\frac{2}{n} \tendversen{n\to +\infty} 0. \]
}

\begin{theoreme}
Si deux suites sont adjacentes, alors elles sont convergentes, et elles ont la même limite.	
\end{theoreme}

\ifprof
\begin{demonstration}
On montre que la définition des suites adjacentes entraîne que, pour tout $n$, $u_0\leq u_n \leq v_n\leq v_0$ : en effet, la suite $(v_n-u_n)$ est décroissante par construction, et de limite $0$ : cela implique que tous les termes de la suite $(v_n-u_n)$ sont positifs.\\
La suite $(u_n)$ est donc croissante majorée : elle converge, vers une limite que l'on note $\ell$. De même, la suite $(v_n)$ est décroissante minorée : elle converge, vers $\ell'$. Or, par définition, $\displaystyle{\lim_{n\rightarrow +\infty}v_n-u_n=0}$. Par opération sur les limites, cela implique $\ell-\ell'=0$, soit $\ell=\ell'$.
\end{demonstration}
\else
\lignes{15}
\fi

\begin{methode}
Pour montrer que deux suites sont adjacentes, on montre qu'une est croissante, l'autre est décroissante et que la différence des deux tend vers $0$.	
\end{methode}

\begin{exo}
Soient $u$ et $v$ deux suites définies pour tout $n\geq 1$ par $$u_n=\sum_{k=0}^n \frac{1}{k!} ~~\textrm{et}~~v_n=u_n+\frac{1}{n\times n!}$$
Montrer que $u$ et $v$ sont adjacentes.	
\end{exo}

\solution[10]{Pour tout entier $n$, on  a
$$u_{n+1}-u_n=\frac{1}{(n+1)!} > 0$$
donc la suite $(u_n)$ est croissante. De même
$$v_{n+1}-v_n=u_{n+1}+\frac{1}{(n+1).(n+1)!} - \left(u_n+\frac{1}{nn!}\right) = \frac{1}{(n+1)!} +\frac{1}{(n+1).(n+1)!}-\frac{1}{nn!}$$
Après mise au même dénominateur
$$v_{n+1}-v_n=\frac{n(n+1)+n-(n+1)^2}{n(n+1).(n+1)!}=\frac{-1}{n(n+1).(n+1)!} <0$$
donc la suite $(v_n)$ est décroissante. Enfin
$v_n-u_n=\frac{1}{n.n!}$
et $$\lim_{n\rightarrow +\infty} \frac{1}{n.n!}=0 ~~\textrm{ par quotient}$$
\textbf{Bilan} : les suites $u$ et $v$ sont bien adjacentes.
}

\begin{remarque}
Etant adjacentes, elles convergent, et ont la même limite. On peut montrer que leur limite commune est $\E$.	
\end{remarque}

\afaire{Exercices \lienexo{7} et \lienexo{8}.}

\section{Comparaison de suites}

L'idée de cette section est d'introduire des méthodes de comparaison de suites, permettant de déduire certains résultats sur les limites.

	\subsection{Négligeabilité}

\begin{definition}
\label{objectif-07-7}	
Soient $u$ et $v$ deux suites, $v$ ne s'annulant pas à partir d'un certain rang. On dit que $u$ est négligeable par rapport à $v$ au voisinage de $+\infty$ si et seulement si
\[ \frac{u_n}{v_n} \underset{n\rightarrow +\infty}{\longrightarrow} 0 \]
On note alors $u_n=o_{+\infty}(v_n)$, ou plus simplement $u_n=o(v_n)$, et on lit ``$u$ est un petit $o$ de $v$ au voisinage de $+\infty$''.
\end{definition}

\begin{exemple}
On a $n=o(n^2)$.
\end{exemple}

\solution[3]{En effet, $\ds{\frac{n}{n^2}=\frac{1}{n} \underset{n\rightarrow +\infty}{\longrightarrow} 0 }$.}

\begin{propriete}[Opérations sur la négligeabilité] 
Soient $u, v$ et $w$ trois suites non nulles à partir d'un certain rang.
\begin{numerote}
	\item (Multiplication par un réel) Si $u_n=o(v_n)$ alors pour tout réel $k$, $ku_n=o(v_n)$.
	\item (Quotient) Si $u_n=o(v_n)$ alors $u_nw_n=o(v_nw_n)$ et $\ds{\frac{u_n}{w_n} = o\left(\frac{v_n}{w_n}\right)}$.
	\item (Transitivité) Si $u_n=o(v_n)$ et $v_n=o(w_n)$ alors $u_n=o(w_n)$.
	\item (Somme) Si $u_n=o(v_n)$ et $w_n=o(v_n)$ alors $u_n+w_n=o(v_n)$.
\end{numerote}	
\end{propriete}

\begin{remarque}
Attention : pour la somme, il faut que les suites soient négligeables par rapport à une même suite.	
\end{remarque}

\begin{exo}
Montrer que $\eu{-n} = o(n^4)$ et que $\ln(n)-2n^2=o(n^4)$.	
\end{exo}

\solution[3]{Remarquons que
\[ \frac{\eu{-n}}{n^4}=\frac{1}{\E^nn^4} \underset{n\rightarrow +\infty}{\longrightarrow} 0 \quad \text{par quotient.} \]
Enfin, $\ln(n)=o(n^4)$ et $n^2=o(n^4)$ (par croissances comparées). Par somme, $\ln(n)-2n^2=o(n^4)$.
}

\begin{remarque}
Une suite vérifiant $u=o(1)$ est une suite qui tend vers $0$.	
\end{remarque}

\begin{proposition}[Croissances comparées]
On peut écrire les croissances comparées ainsi : si $\alpha>0$ et $\beta>0$ :
\[ n^\alpha = o(\E^n),\quad \quad (\ln n)^\alpha = o(n^\beta),\quad \quad \text{si }\alpha<\beta,\quad n^\alpha = o(n^\beta), \qeq \E^n = o(n!) \]
De manière générale, si $1<q<p$ :
\[ n^\alpha = o(q^n),\quad q^n=o(p^n) \qeq q^n=o(n!) \]
\end{proposition}

	\subsection{Équivalence}

\begin{definition}
\label{objectif-07-8}
Soient $u$ et $v$ deux suites, $v$ ne s'annulant pas à partir d'un certain rang. On dit que $u$ et $v$ sont équivalentes au voisinage de $+\infty$ si
\[ \frac{u_n}{v_n} \underset{n\rightarrow +\infty}{\longrightarrow} 1 \]
On note $u_n \underset{+\infty}{\sim} v_n$, ou plus simplement $u_n \sim v_n$.
\end{definition}

\begin{exo}
Montrer que $n^2+n\sim n^2$.	
\end{exo}

\solution[3]{En effet, pour $n\geq 1$ :
\[ \frac{n^2+n}{n^2}=1+\frac{1}{n} \underset{n\rightarrow+\infty}{\longrightarrow} 1 \]
}

\begin{remarque}
On dispose d'une autre définition : $u$ et $v$ sont équivalentes si et seulement si
\[ u_n = v_n + o_{+\infty}(v_n) \]
En effet,
\[ \frac{u_n-v_n}{v_n} = \frac{u_n}{v_n}-1 \underset{n\rightarrow +\infty}{\longrightarrow} 0 \]	
\end{remarque}


\begin{consequence}
Soient deux suites $u$ et $v$ équivalentes. Alors
\begin{itemize}[label=\textbullet]
	\item Si $u$ converge vers $\ell$, $v$ converge également vers $\ell$.
	\item Si $u$ est de signe constant à partir d'un certain rang, $v$ est également de signe constant et de même signe à partir d'un certain rang.
\end{itemize}
\end{consequence}

\begin{propriete}[Opérations sur les équivalents] 
Soient $u,v,w$ et $x$ quatre suites non nulles à partir d'un certain rang.
\begin{numerote}
	\item (Compatibilité avec la multiplication) Si $u_n \sim v_n$ et $w_n\sim x_n$, alors $u_nw_n \sim v_nx_n$.
	\item (Compatibilité avec le quotient) Si $u_n\sim v_n$ et $w_n\sim x_n$, alors $\ds{\frac{u_n}{w_n}\sim \frac{v_n}{x_n}}$.
	\item (Compatibilité avec les puissances) Si les suites $u$ et $v$ sont strictement positives, et telles que $u_n \sim v_n$, alors pour tout entier $p\in \Z$, $u_n^p \sim v_n^p$.
\end{numerote}
\end{propriete}

\begin{demonstration}
Les preuves se font en utilisant la définition. Par exemple, remarquons que
\[ \frac{u_nw_n}{v_nx_n} = \frac{u_n}{v_n} \times \frac{w_n}{x_n} \underset{n\rightarrow+\infty}{\longrightarrow} 1 \]
\end{demonstration}

\begin{remarque}\logoattention
Attention : en général, on ne peut ni ajouter, ni soustraire des équivalents.
\end{remarque}

\begin{exo}
Déterminer
\[ \lim_{n\rightarrow +\infty} \frac{\ln(n)+n^4}{1-n^4} \]	
\end{exo}

\solution[5]{Puisque $\ln(n)=o(n^4)$, on a $\ln(n)+n^4\sim n^4$. De même, $1-n^4\sim -n^4$. Par quotient
\[ \frac{\ln(n)+n^4}{1-n^4} \sim \frac{n^4}{-n^4}=-1 \]
et donc
\[ \lim_{n\rightarrow +\infty} \frac{\ln(n)+n^4}{1-n^4} = -1 \]
}

\begin{proposition}[Formule de Stirling]
On dispose d'un équivalent de $n!$ :
\[ n! \sim \left (\frac{n}{e}\right)^n \sqrt{2\pi n} \]
\end{proposition}

\begin{remarque}
On retrouve, grâce à ce résultat, que $q^n=o(n!)$.	
\end{remarque}

\afaire{Exercice \lienexo{15}.}

