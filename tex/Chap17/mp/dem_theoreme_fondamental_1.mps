%!PS
%%BoundingBox: -23 -10 157 100 
%%HiResBoundingBox: -22.3926 -9.4204 156.3055 99.6126 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.25:1438
%%Pages: 1
%*Font: cmsy10 9.96265 9.96265 41:a
%*Font: cmr10 9.96265 9.96265 28:c08
%*Font: cmmi10 9.96265 9.96265 61:840001
%*Font: cmmi7 6.97385 6.97385 66:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.7 0.7 0.7 setrgbcolor
newpath 28.34645 0 moveto
28.34645 22.67725 lineto
46.02068 23.60355 60.0421 36.51657 76.53532 42.51968 curveto
76.53532 0 lineto
 closepath fill
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 76.53532 0 moveto
76.53532 42.51968 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 28.34645 0 moveto
28.34645 22.67725 lineto
46.02068 23.60355 60.0421 36.51657 76.53532 42.51968 curveto
101.45114 51.58841 114.8381 81.73712 141.73225 85.03935 curveto
145.51276 85.50354 149.29224 85.97601 153.07066 86.45676 curveto stroke
newpath 0 0 moveto
155.90549 0 lineto stroke
newpath 152.21094 -1.53036 moveto
155.90549 0 lineto
152.21094 1.53036 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 99.21259 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 1.53061 95.51744 moveto
0 99.21259 lineto
-1.53061 95.51744 lineto
 closepath
gsave fill grestore stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
 [1.5 1.5 ] 0 setdash
newpath 76.53532 42.51968 moveto
0 42.51968 lineto stroke
45.99414 14.5174 moveto
(A) cmsy10 9.96265 fshow
53.94904 14.5174 moveto
(\() cmr10 9.96265 fshow
57.82344 14.5174 moveto
(x) cmmi10 9.96265 fshow
63.51733 14.5174 moveto
(\)) cmr10 9.96265 fshow
73.68837 -7.2895 moveto
(x) cmmi10 9.96265 fshow
-2.49065 -9.4204 moveto
(0) cmr10 9.96265 fshow
25.7134 -7.2895 moveto
(a) cmmi10 9.96265 fshow
-22.3926 40.02902 moveto
(f) cmmi10 9.96265 fshow
-16.44269 40.02902 moveto
(\() cmr10 9.96265 fshow
-12.5683 40.02902 moveto
(x) cmmi10 9.96265 fshow
-6.87439 40.02902 moveto
(\)) cmr10 9.96265 fshow
98.47775 70.14732 moveto
(C) cmsy10 9.96265 fshow
103.72336 68.65292 moveto
(f) cmmi7 6.97385 fshow
showpage
%%EOF
