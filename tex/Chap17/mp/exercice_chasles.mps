%!PS
%%BoundingBox: -65 -107 235 150 
%%HiResBoundingBox: -64.17953 -106.69922 234.25827 149.2189 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.26:0946
%%Pages: 1
%*Font: cmr10 9.96265 9.96265 30:c
%*Font: cmsy10 9.96265 9.96265 43:8
%*Font: cmmi7 6.97385 6.97385 66:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 1 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -42.51968 42.51968 moveto
0 127.55905 lineto
85.03937 0 lineto
127.55905 -85.03937 lineto
212.59842 0 lineto stroke
 0.7 0.7 0.7 setrgbcolor
newpath -42.51968 0 moveto
-42.51968 42.51968 lineto
0 127.55905 lineto
0 0 lineto
 closepath fill
newpath 0 127.55905 moveto
0 0 lineto
85.03937 0 lineto
 closepath fill
newpath 85.03937 0 moveto
127.55905 -85.03937 lineto
212.59842 0 lineto
 closepath fill
 0 0 0 setrgbcolor 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath -63.77953 0 moveto
233.85826 0 lineto stroke
newpath 230.16173 -1.53119 moveto
233.85826 0 lineto
230.16173 1.53119 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -106.29921 moveto
0 148.8189 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 1.53143 145.12177 moveto
0 148.8189 lineto
-1.53143 145.12177 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath -42.51968 -2.12611 moveto
-42.51968 2.12611 lineto stroke
newpath 0 -2.12611 moveto
0 2.12611 lineto stroke
newpath 42.51968 -2.12611 moveto
42.51968 2.12611 lineto stroke
newpath 85.03937 -2.12611 moveto
85.03937 2.12611 lineto stroke
newpath 127.55905 -2.12611 moveto
127.55905 2.12611 lineto stroke
newpath 170.07874 -2.12611 moveto
170.07874 2.12611 lineto stroke
newpath 212.59842 -2.12611 moveto
212.59842 2.12611 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath -2.12611 -85.03937 moveto
2.12611 -85.03937 lineto stroke
newpath -2.12611 -42.51968 moveto
2.12611 -42.51968 lineto stroke
newpath -2.12611 0 moveto
2.12611 0 lineto stroke
newpath -2.12611 42.51968 moveto
2.12611 42.51968 lineto stroke
newpath -2.12611 85.03937 moveto
2.12611 85.03937 lineto stroke
newpath -2.12611 127.55905 moveto
2.12611 127.55905 lineto stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 127.55905 0 moveto
127.55905 -85.03937 lineto stroke
-7.08128 -8.52039 moveto
(0) cmr10 9.96265 fshow
40.02904 -9.4204 moveto
(1) cmr10 9.96265 fshow
-7.9813 39.30948 moveto
(1) cmr10 9.96265 fshow
23.35983 102.7458 moveto
(C) cmsy10 9.96265 fshow
28.60544 101.2514 moveto
(f) cmmi7 6.97385 fshow
showpage
%%EOF
