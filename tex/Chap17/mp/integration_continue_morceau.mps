%!PS
%%BoundingBox: -107 -22 192 107 
%%HiResBoundingBox: -106.69922 -21.65985 191.73859 106.69922 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.26:0900
%%Pages: 1
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.8 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -85.03937 85.03937 moveto
-42.51968 42.51968 lineto stroke
newpath -42.51968 0 moveto
42.51968 42.51968 lineto stroke
newpath 42.51968 0 moveto
60.41763 3.15585 66.48294 28.46591 85.03937 29.76364 curveto
115.53847 31.89656 144.90631 42.17526 170.07874 59.5273 curveto stroke
 0.7 0.7 0.7 setrgbcolor
newpath -85.03937 0 moveto
-85.03937 85.03937 lineto
-42.51968 42.51968 lineto
-42.51968 0 lineto
 closepath fill
newpath -42.51968 0 moveto
42.51968 42.51968 lineto
42.51968 0 lineto
 closepath fill
newpath 42.51968 0 moveto
60.41763 3.15585 66.48294 28.46591 85.03937 29.76364 curveto
115.53847 31.89656 144.90631 42.17526 170.07874 59.5273 curveto
170.07874 0 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath -106.29921 0 moveto
191.33858 0 lineto stroke
newpath 187.64204 -1.53119 moveto
191.33858 0 lineto
187.64204 1.53119 lineto
 closepath
gsave fill grestore stroke
 0.8 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -21.25984 moveto
0 106.29921 lineto stroke
 0 0.8 dtransform truncate idtransform setlinewidth pop
newpath 1.5307 102.60385 moveto
0 106.29921 lineto
-1.5307 102.60385 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
