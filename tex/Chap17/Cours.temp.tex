\chapter{Intégration}


\objectifintro{Dans ce chapitre, on rappelle la notion de primitive, ainsi que de l'intégrale sur un segment, que l'on calculera à l'aide de différentes méthodes de calcul pratiques (intégration par parties, changement de variable). On revient également sur la notion de primitive. On introduit la notion de sommes de Riemann pour le calcul intégral, et on généralise, enfin, la notion d'intégrale à un intervalle quelconque.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
        \item Concernant les primitives :
                        \begin{itemize}[label=\textbullet]
                                \item connaître les primitives usuelles\dotfill $\Box$
                                \item savoir déterminer des primitives dans les cas de dérivation classique\dotfill $\Box$
                                \item connaître les opérations sur les primitives\dotfill $\Box$
                        \end{itemize}
        \item Connaître la notion d'intégrale et les différentes propriétés de l'intégrale :
                        \begin{itemize}[label=\textbullet]
                                	\item définition de l'intégrale\dotfill $\Box$
                                \item linéarité et relation de Chasles\dotfill $\Box$
                                \item encadrement et inégalité de la moyenne\dotfill $\Box$
                                \item positivité et croissance de l'intégrale\dotfill $\Box$
                                \item fonction positive et intégrale nulle\dotfill $\Box$
                        \end{itemize}
        \item Concernant les méthodes de calcul d'intégrales :
                        \begin{itemize}[label=\textbullet]
                                \item l'intégration par partie\dotfill $\Box$
                                \item le changement de variable\dotfill $\Box$
                                \item les fonctions définies par une intégrale\dotfill $\Box$
                        \end{itemize}
        \item Savoir utiliser les sommes de Riemann pour calculer des sommes de séries\dotfill $\Box$
       \item Connaître la définition d'intégrale sur un intervalle :
                        \begin{itemize}[label=\textbullet]
                                \item connaître la définition\dotfill $\Box$
                                \item connaître les différentes propriétés usuelles\dotfill $\Box$
                        \end{itemize}
        \item Concernant les théorèmes d'existence :
                        \begin{itemize}[label=\textbullet]
                                \item connaître le théorème de majoration des fonctions positives\dotfill $\Box$
                                \item connaître les intégrales de référence (Riemann, exponentielle)\dotfill $\Box$
                        \end{itemize}
        \item Connaître la notion de convergence absolue :
                        \begin{itemize}[label=\textbullet]
                                \item connaître la définition\dotfill $\Box$
                                \item l'inégalité triangulaire\dotfill $\Box$
                        \end{itemize}

\end{numerote}

\newpage 


\section{Primitives}

	\subsection{Définition}
	
\definitionL{S% oient $f$ et $F$ deux fonctions définies sur un intervalle $I$, avec $F$ dérivable. Si $F'=f$, on dit que $F$ est une \textbf{primitive} de $f$ sur l'intervalle $I$.}
%
% \exempleL{\begin{itemize}
% 	\item[$\bullet$] Soit $f$ la fonction définie sur $\R$ par $f(x)=2x$. Alors $f$ admet comme primitive sur $\R$ la fonction $F:x\mapsto x^2$, mais également les fonctions $G:x\mapsto x^2+1$, $H:x\mapsto x^2+l,~~~l\in \R$.
% 	\item[$\bullet$] La fonction $x\mapsto \E^x$ admet comme primitive sur $\R$ la fonction $x\mapsto \E^x$.
% \end{itemize}}
%
% 	\subsection{Différentes primitives d'une fonction}
%
% \theoremeL{Les seules primitives de la fonction nulle sur un intervalle $I$ sont les fonctions constantes.}
%
% \preuve{En effet, si $F'=0$ sur l'intervalle $I$, nous avons vu dans le chapitre sur la dérivation, que cela implique que $F$ est constante. Réciproquement, les fonctions constantes ont une dérivée nulle.}
%
% \consequenceL{Soit $F$ une primitive d'une fonction $f$ sur l'intervalle $I$. Alors toutes les primitives de $f$ sur $I$ s'écrivent $F+\lambda$, avec $\lambda$ constante réelle.}
%
% \preuve{En effet, en notant $F$ et $G$ deux primitives de $f$ sur $I$, on a $(F-G)'=F'-G'=f-f=0$. D'après le résultat précédent, $F-G$ est une fonction constante sur l'intervalle $I$, c'est à dire $F=G+\lambda$ avec $\lambda$ constante réelle.}
%
% 	\subsection{Fonction continue et primitive}
%
% \theoremeL{Une fonction continue sur un intervalle $I$ admet des primitives sur $I$.}
%
% \ifprof
% \preuve{Démontrons, pour simplifier, le théorème dans le cas où $f$ est une fonction croissante et positive. Soit $f$ une fonction continue croissante sur $[a;b]$, et $\mathcal{C}_f$ sa courbe représentative dans un repère orthonormé.
% Pour tout $x\in [a;b]$, on note $\mathcal{A}(x)$ l'aire de la surface délimitée par l'axe des abscisses, la courbe $\mathcal{C}_f$, entre les abscisses $a$ et $x$.
% \begin{center}
%     \includegraphics{tex/Chap15/mp/dem_theoreme_fondamental_1.mps} %[width=7cm]{tex/Chap15/pic/fonction1}
% \end{center}
%
% Cette fonction est bien définie sur $[a;b]$. On va montrer que $\mathcal{A}$ est dérivable sur $[a;b]$, et que $\mathcal{A}'=f$.\\
% Soit $h>0$ et $x\in[a;b]$. Le nombre $\mathcal{A}(x+h)-\mathcal{A}(x)$ représente l'aire de la surface délimitée par l'axe des abscisses, la courbe de $f$, et les abscisses $x$ et $x+h$.
% \begin{center}
%     \includegraphics{tex/Chap15/mp/dem_theoreme_fondamental_2.mps} %[width=8cm]{tex/Chap15/pic/fonction2}
% \end{center}
% Puisque $f$ est croissante, on peut encadrer cette aire par deux rectangles :
% \[(x+h-x)f(x)\leq \mathcal{A}(x+h)-\mathcal{A}(x) \leq (x+h-x)f(x+h)\]
% donc
% \[f(x) \leq \frac{\mathcal{A}(x+h)-\mathcal{A}(x)}{h} \leq f(x+h)\]
% Par continuité de $f$, $\displaystyle{\lim_{h\rightarrow 0} f(x+h)=f(x)}$. Par encadrement, on a donc
% \[\lim_{h\rightarrow 0^+} \frac{\mathcal{A}(x+h)-\mathcal{A}(x)}{h} = f(x)\]
% Donc $\mathcal{A}$ est bien dérivable à droite en $x$, et $\mathcal{A}'_d(x)=f(x)$.
% \\On montre de même que $\mathcal{A}$ est dérivable à gauche en $x$, et que $\mathcal{A}'_g(x)=f(x)$. Les dérivées à droite et à gauche de $\mathcal{A}$ étant égales, on en déduit que $\mathcal{A}$ est dérivable et que sa dérivée est $f$.
% }
% \else
% \lignes{38}
% \fi
%
% 	\subsection{Fonction primitive et condition initiale}
%
% \theoremeL{Soit $f$ une fonction admettant des primitives sur un intervalle $I$. Soit $x_0$ un réel de l'intervalle $I$, et $y_0$ un réel donné. Alors il existe une, et une seule, primitive $F$ de $f$ sur $I$ telle que $F(x_0)=y_0$.\\
% En particulier, $f$ admet une unique primitive s'annulant en un $x_0$ donné.}
%
% \ifprof
% \preuve{Soit $F$ une primitive de $f$. Alors, la fonction $G$ définie par $G=F-F(x_0)+y_0$ est également une primitive de $f$, et $G(x_0)=y_0$. \\
% Si $G$ et $H$ sont deux primitives de $f$ telles que $G(x_0)=H(x_0)=y_0$, alors, puisqu'on peut écrire $G=H+l$, on a $G(x_0)=H(x_0)+l = G(x_0)+l$, donc $l=0$ et $G=H$.}
% \else
% \lignes{5}
% \fi
%
% \exempleL{La fonction $F$ définie sur $\R$ par $F(x)=x^2-1$ est une primitive de $f:x\mapsto  2x$ vérifiant $F\left(1\right)=0$.}
%
% \section{Recherche de primitives}
%
% Pour rechercher des primitives, on utilise les formules connues pour la dérivation, et les dérivées connues.
%
% 	\subsection{Fonctions usuelles}
%
% \begin{center}\begin{tabular}{|c|c|c|}\hline
% \textbf{Fonction $f$} & \textbf{Primitive $F$} &  \textbf{Intervalle $I$} \\ \hline
%  $x\mapsto a$ (constante non nulle) & $x \mapsto ax$ & $\R$ \\ \hline
%  &&\\ $x\mapsto x^n (n\geq 1)$ & $\displaystyle{x\mapsto \frac{x^{n+1}}{n+1}}$ & $\R$ \\ &&\\ \hline
%  &&\\
%  $\displaystyle{x\mapsto \frac{1}{x^n}}$ ($n>1$) & $ x\mapsto -\frac{1}{(n-1)x^{n-1}}$ &
% $]-\infty;0[ \textrm{ ou} ]0;+\infty[ $\\  &&\\\hline
% && \\$x\mapsto x^\alpha ~~(\alpha \neq -1)$ & $x\mapsto \frac{x^{\alpha+1}}{\alpha +1}$ & $\R^*_+$ \\ && \\\hline
%   &&\\ $\displaystyle{x\mapsto \frac{1}{\sqrt{x}}}$ & $x\mapsto 2\sqrt{x}$ & $\R^*_+$\\  &&\\ \hline
%   &&\\$\displaystyle{x\mapsto \frac{1}{x}}$ & $\ln\left(|x|\right)$ & $\R>$ ou $\R<$ \\ &&\\ \hline
%  $x\mapsto \E^x$ & $x \mapsto \E^x$ & $\R$ \\ \hline
% \end{tabular}\end{center}
%
%
% 	\subsection{Utilisation des formules de dérivation}
%
% On peut utiliser les formules suivantes :
%
% \remarqueL{\[\frac{u'}{\sqrt{u}}=(2\sqrt{u})'~~~~~~~~\frac{u'}{u^2}=\left(-\frac{1}{u}\right)'\]
% \[2u'u=(u^2)'~~~~~~~~u'u^n=\left(\frac{1}{n+1}u^{n+1}\right)'~~~~~~~~u'u^\alpha = \left( \frac{u^{\alpha+1}}{\alpha+1}\right)' (\alpha \neq -1)\]
% \[\frac{u'}{u}=(\ln |u|)'~~~~~~~~u'\E^u=(\E^u)'\]}
%
% \exempleL{Soit $f$ la fonction définie sur $\R^*_+$ par $f(x)=\frac{\ln(x)}{x}$. Déterminer une primitive de $f$.}
%
% \solution[4]{On pose $u(x)=\ln x$. Alors $f(x)=u'(x)u(x)$ et admet donc comme primitive $F:x \mapsto \frac{1}{2}u^2(x)=\frac{1}{2}\ln(x)^2$.}
%
%     \subsection{Opération sur les primitives}
%
% \theoremeL{Soient $F$ et $G$ deux primitives respectives des fonctions $f$ et $g$ sur un meme intervalle $I$, et $\lambda \in \R$.
% \begin{itemize}
%     \item[$\bullet$] $\lambda F$ est une primitive de $\lambda f$ sur $I$.
%     \item[$\bullet$] $F+G$ est une primitive de $f+g$ sur $I$.
% \end{itemize}
% }
%
% \preuve{Assez immédiate : $(\lambda F)'=\lambda F'=\lambda f$ et $(F+G)'=F'+G'=f+g$ en exploitant la linéarité de la dérivation.}
%
% \section{Intégrale sur un segment}
%
%     \subsection{Définition}
%
% \definitionL{Soit $f$ une fonction continue sur $[a;b]$. On appelle \textbf{intégrale} de $a$ à $b$ de la fonction $f$ le nombre réel $F(b)-F(a)$, où $F$ est une primitive quelconque de $f$ sur $[a;b]$. On note $\displaystyle{\int_a^b f(t)dt=F(b)-F(a)}$.}
%
% \remarqueL{Par convention de notation, on note $[F(t)]_a^b = F(b)-F(a)$, de sorte que \[\int_a^b f(t)dt=[F(t)]_a^b = F(b)-F(a)\]}
%
% \exempleL{$\displaystyle{\int_1^2 xdx = \left[ \frac{x^2}{2} \right]_1^2 = 2-\frac{1}{2}=\frac{3}{2}}$. On peut également prendre une autre primitive :
%  \[\int_1^2 xdx = \left[ \frac{x^2}{2}+1 \right]_1^2 = 3-\frac{3}{2}=\frac{3}{2}\]}
%
% \remarqueL{La définition de l'intégrale ne dépend pas de la primitive choisie. En effet, soient $F$ et $G$ deux primitives de $f$. D'après un résultat précédent, il existe un réel $\lambda$ tel que $G=F+\lambda$. Mais alors
% \[G(b)-G(a)=(F(b)+\lambda) - (F(a)+\lambda)= F(b)-F(a)\]}
%
% \remarqueL{Dans l'écriture $\displaystyle{\int_a^b f(t)dt}$, $t$ est une variable muette. Ainsi, \[\int_a^b f(t)dt= \int_a^b f(u)du = \int_a^b f(x)dx\]}
%
%     \subsection{Interprétation graphique}
%
% \remarqueL{Si $a<b$, le nombre $\displaystyle{\int_a^b f(t)dt}$ représente l'aire \textbf{algébrique} (ou aire \textbf{signée}) en unité d'aire du domaine délimité par $\mathcal{C}$, courbe représentative de $f$, l'axe des abscisses, et les droites d'équations $x=a$ et $x=b$.
%
% \begin{center}
% \includegraphics{tex/Chap15/mp/cas_fonction_continue.mps}
% \end{center}}
%
%
% \section{Premières propriétés}
%
% 	\subsection{Premiers résultats}
%
% \definitionL{Soit $f$ une fonction continue sur un intervalle $I$, et soient $a$ et $b$ deux réels de $I$.
% \begin{itemize}
% 	\item[$\bullet$] On a $\displaystyle{\int_a^bf(t)dt=-\int_b^af(t)dt}$.
% 	\item[$\bullet$] Lorsque $a=b$, $\displaystyle{\int_a^af(t)dt=0}$
% \end{itemize}}
%
% \ifprof
% \preuve{Soit $F$ une primitive de $f$ sur $I$. Alors \[\int_a^b f(t)dt=F(b)-F(a) = -(F(a)-F(b)) = - \int_b^a f(t)dt\]
% \[\int_a^a f(t)dt=F(a)-F(a)=0\]
% }
% \else
% \lignes{3}
% \fi
%
% 	\subsection{Relation de Chasles}
%
% \theoremeL{Soit $f$ une fonction continue sur un intervalle $I$. Soient $a,b,c$ trois réels quelconques de $I$. Alors
% \[\int_a^b f(t)dt+\int_b^c f(t)dt=\int_a^c f(t)dt\]}
%
% \ifprof
% \preuve{Soit $F$ une primitive de $f$ sur $I$. Alors
% \[\int_a^b f(t)dt+\int_b^c f(t)dt = (F(b)-F(a)) + (F(c)-F(b)) = F(c)-F(a) = \int_a^c f(t)dt\]
% }
% \else
% \lignes{3}
% \fi
%
% \exempleL{[Calcul de l'intégrale d'une fonction affine par morceaux] Soit $f$ la fonction donnée ci-dessous. Déterminer $I(f)=\int_{-1}^5 f(t)dt$.
%
% \begin{center}
% \includegraphics{tex/Chap15/mp/exercice_chasles.mps}
% \end{center}
% }
%
% \solution[3]{
% D'après la relation de Chasles :
% \[\int_{-1}^5f(t)dt=\int_{-1}^0f(t)dt+\int_0^2 f(t)dt+\int_2^3 f(t)dt+\int_3^5f(t)dt=2+3-1-2=2\]
% }
%
% \consequenceL{~
% \begin{itemize}
% 	\item[$\bullet$] Si $f$ est paire sur $[-a;a]$, alors \[\int_{-a}^a f(t)dt=2\int_0^a f(t)dt\]
% 	\item[$\bullet$] Si $f$ est impaire sur $[-a;a]$, alors \[\int_{-a}^a f(t)dt=0\]
% \end{itemize}}
%
% 	\subsection{Linéarité}
%
% \theoremeL{Soient $f$ et $g$ deux fonctions continues sur un intervalle $I$, soit $\lambda$ un réel quelconque, et soient $a,b$ deux réels quelconques de l'intervalle $I$. Alors
% \[\int_a^b \lambda f(t)dt=\lambda \int_a^b f(t)dt\]
% \[\int_a^b (f+g)(t)dt=\int_a^b f(t)dt+\int_a^b g(t)dt\]}
%
% \preuve{En effet, si $F$ et $G$ sont des primitives sur $I$ respectivement de $f$ et de $g$, alors $\lambda F$ est une primitive sur $I$ de $f$, et $F+G$ est une primitive de $f+g$ sur $I$. Le résultat s'en déduit.}
%
%     \subsection{Primitive nulle en un point}
%
% \theoremeL{[Théorème fondamental de l'intégration] Soit $f$ une fonction continue sur un intervalle $I$, $x_0\in I$. La fonction \[F:x\mapsto \int_{x_0}^x f(t)dt\] est l'unique primitive de $f$ sur $I$ qui s'annule en $x_0$. Ainsi, $F$ est de classe $\mathcal{C}^1$ sur $I$, et $F'=f$.}
%
% \ifprof
% \preuve{Soit $G$ une primitive de $f$ sur $I$ (qui existe parce qu'elle est continue). Alors, pour tout $x$ de $I$, \[F(x)=\int_{x_0}^x f(t)dt=G(x)-G(x_0)\]
% Donc $F(x_0)=0$, $F$ est dérivable sur $I$ et on a, pour tout $x$ de $I$, $F'(x)=G'(x)=f(x)$. Donc $F$ est une primitive de $f$, et puisque $F(x_0)=0$, c'est la primitive de $f$ nulle en $0$.}
% \else
% \lignes{5}
% \fi
%
% \exempleL{Soit $f$ la fonction définie sur $\R$ par \[f(x)=\int_0^x \E^{-t^2}dt\] Déterminer $f'$.}
%
% \solution[3]{$f$ est la primitive de la fonction $x\mapsto \eu{-x^2}$ (fonction continue sur $\R$) nulle en $0$. Ainsi, $f$ est de classe $\CC^1$ sur $\R$ et
% \[\forall~x,~f'(x)=\eu{-x^2}\]}
%
% \section{Propriétés d'encadrement et valeur moyenne}
%
% 	\subsection{Encadrement}
%
% \theoremeL{Soient $f$ et $g$ deux fonctions continues sur un intervalle $I$, et soient $a,b$ deux réels quelconques de $I$.
% \begin{itemize}
% 	\item[$\bullet$] \textbf{Positivité de l'intégrale}. Si $a\leq b$ et si, pour tout réel $t$ de $[a;b]$, $f(t)\geq 0$, alors \[\int_a^b f(t)dt\geq 0\]
% 	\item[$\bullet$] \textbf{Croissance de l'intégrale.} Si $a\leq b$ et si, pout tout réel $t$ de $[a;b]$ $f(t)\leq g(t)$ alors \[\int_a^b f(t)dt\leq \int_a^b g(t)dt\]
% \end{itemize}}
%
% \remarqueL{\danger ~Il faut absolument que $a\leq b$ !}
%
% \ifprof
% \preuve{Soit $F$ une primitive de $f$ sur $I$, et $G$ une primitive de $g$ sur $I$.
% \begin{itemize}
% 	\item[$\bullet$] Si $f$ est positive sur $I$, alors $F$ est croissante sur $I$ (puisque $F'=f$). Mais alors, si $a\leq b$, \[\int_a^b f(t)dt=F(b)-F(a)\geq 0 \textrm{ par croissance de } F\]
% 	\item[$\bullet$] Si $f\leq g$ sur $I$, alors $g-f\geq 0$ sur $I$. D'après le résultat précédent, $\displaystyle{\int_a^b (g(t)-f(t))dt \geq 0}$. Par linéarité, on obtient bien $\displaystyle{\int_a^b f(t)dt \leq \int_a^b g(t)dt}$.
%
% \end{itemize}}
% \else
% \lignes{7}
% \fi
%
%     \subsection{Inégalité de la moyenne}
%
% \theoremeL{[Inégalité de la moyenne] Soit $f$ une fonction continue sur un intervalle $I$. Soient $m$ et $M$ deux réels, et $a,b$ deux réels de l'intervalle $I$ tels que $a\leq b$.\\
% Si $m\leq f\leq M$ sur $[a;b]$ alors
% \[m(b-a)\leq \int_a^b f(t)dt\leq M(b-a)\]
% Et si $a\neq b$ :
% \[m \leq \frac{1}{b-a}\int_a^b f(t)dt\leq M\]}
%
% \ifprof
% \preuve{Pour tout $t$ dans $[a;b]$, on a $m\leq f(t)\leq M$. D'après le théorème précédent, puisque $a\leq b$, on a alors
% \[\int_a^b mdt\leq \int_a^b f(t)dt \leq \int_a^b Mdt\]
% Or $\int_a^b mdt=m(b-a)$ et $\int_a^b Mdt=M(b-a)$ (car les fonction $t\mapsto M$ et $t\mapsto m$ sont constantes sur $[a;b]$), ce qui donne le résultat.}
% \else
% \lignes{5}
% \fi
%
% \exercice{On admet que la fonction $\ds{f:x\mapsto \frac{1}{\E^x+\eu{-x}}}$ est décroissante sur $[0;+\infty[$. Pour tout entier naturel $n$, on pose \[I_n=\int_n^{n+1}f(x)dx\]
% Prouver que pour tout entier naturel $n$, $f(n+1)\leq I_n \leq f(n)$, puis en déduire que la suite $(I_n)$ est convergente.}
%
% \solution[10]{
% Soit $n$ un entier. Puisque $f$ est décroissante sur $\R^+$, elle l'est sur $[n;n+1]$. Ainsi, pour tout réel $t\in [n;n+1]$, on a
% 	\[f(n+1)\leq f(t) \leq f(n)\]
% 	D'après l'inégalité de la moyenne, on a alors
% 	\[f(n+1)(n+1-n) \leq \int_n^{n+1} f(t)dt \leq f(n)(n+1-n)\]
% 	soit
% 	\[f(n+1) \leq I_n \leq f(n)\]
% 	Constatons enfin que \[\lim_{n\rightarrow +\infty} f(n)=\lim_{n\rightarrow +\infty} f(n+1)=\lim_{x\rightarrow +\infty} f(x)=0 \textrm{  par quotient.}\]
% 	D'après le théorème d'encadrement, on en déduit que la suite $(I_n)$ converge, et que sa limite vaut $0$.
% }
%
%
% \theoremeL{[Inégalité triangulaire] Soient $f$ une fonction continue sur $[a;b]$. Alors
% \[\left| \int_a^b f(t)dt \right| \leq \int_a^b |f(t)|dt\]}
%
% \preuve{Théorème admis.}
%
% 	\subsection{Valeur moyenne d'une fonction}
%
% \definitionL{Soit $f$ une fonction continue sur un intervalle $I$, et soient $a,b$ deux réels distincts de $I$. Le nombre réel \[\frac{1}{b-a}\int_a^b f(t)dt\] est appelé \textbf{valeur moyenne} de $f$ entre $a$ et $b$.}
%
% \theoremeL{Dans les conditions précédentes, il existe un réel $c$ situé entre $a$ et $b$, tel que
% \[\frac{1}{b-a}\int_a^b f(t)dt=f(c)\]}
%
% \ifprof
% \preuve{Soit $f$ une fonction continue sur le segment $[a;b]$. Puisqu'elle est continue, l'image du segment $[a;b]$ est un segment $[m;M]$. Mais alors, pour tout $x$ de $[a;b]$ :
% \[m\leq f(x)\leq M \Leftrightarrow \int_a^b mdt\leq \int_a^b f(t)dt \leq \int_a^b Mdt\]
% Soit
% \[m\leq \frac{1}{b-a}\int_a^b f(t)dt \leq M\]
% Le réel $\frac{1}{b-a}\int_a^b f(t)dt$ est donc compris entre $m$ et $M$. D'après le théorème des valeurs intermédiaires, cette valeur est atteinte en un réel $c\in[a;b]$.}
% \else
% \lignes{7}
% \fi
%
%     \subsection{Fonctions positive et intégrale nulle}
%
%
% \propositionL{Soit $f$ une fonction \underline{continue} sur $[a;b]$. Si
% \[\forall~t \in [a;b], f(t)\geq 0 \textrm{ et } \int_a^b f(t)dt=0\]
% alors
% $f(t)=0$ pour tout $t\in [a;b]$.}
%
% \ifprof
% \preuve{Supposons par l'absurde qu'il existe $\alpha \in ]a;b[$ tel que $f(\alpha)>0$. Par continuité de $f$, il existe un intervalle $J=]\alpha-\eps; \alpha+\eps[$ tel que, pour tout $x\in J$, $f(x)\geq \frac{f(\alpha)}{2}$.
% Mais alors,
% \[\int_a^b f(t)dt \geq \int_J f(t)dt \geq \frac{f(\alpha)}{2}\times 2\eps > 0\]
% ce qui est absurde.}
% \else
% \lignes{7}
% \fi
%
%     \subsection{Intégration d'une fonction continue par morceaux}
%
% \remarqueL{Rappel : une fonction $f$ est dite continue par morceaux sur le segment $[a,b]$ s’il existe une subdivision $a_0=a<a_1 <\cdots<a_n=b$ telle que les restrictions de $f$ à chaque intervalle ouvert $]a_i , a_{i+1} [$ admettent un prolongement continu à l’intervalle fermé $[a_i, a_{i+1}]$.    }
%
% \definitionL{Pour tout $i\in\llbracket 0;n-1\rrbracket;$, notons $\widehat{f_i}$ le prolongement par continuité de $f_i$ sur l'intervalle $[a_i;a_{i+1}]$. On appelle alors intégrale de $f$ sur $[a;b]$ le nombre
% \[\int_a^b f(x)dx= \sum_{i=0}^{n-1} \int_{a_i}^{a_{i+1}} \widehat{f_i}(x)dx\]
% }
%
% \begin{center}\includegraphics{tex/Chap15/mp/integration_continue_morceau.mps}\end{center}
%
% \remarqueL{Ainsi, pour calculer l'intégrale d'une fonction continue par morceaux, on calcule l'intégrale sur chacun des intervalles $[a_i;a_{î+1}]$ de la subdivision, puis on additionne les différentes valeurs.}
%
% \section{Méthode de calcul d'intégrales}
%
% 	\subsection{Intégration par partie}
%
% \theoremeL{Soient $u$ et $v$ deux fonctions de classe $\mathcal{C}^1$ sur $I$. Alors, pour tous réels $a$ et $b$ de $I$ :
% \[\int_a^bu(t)v'(t)dt=[u(t)v(t)]_a^b-\int_a^b u'(t)v(t)dt\]}
%
% \ifprof
% \preuve{La fonction $uv$ est dérivable sur $I$ et on a $(uv)'=u'v+uv'$. Donc $uv'=uv-u'v$, et toutes ces fonctions sont continues sur $I$. On en déduit donc :
% \[\int_a^b (uv')(t)dt=\int_a^b [(uv)'(t)- (u'v)(t)]dt\]
% Par linéarité de l'intégrale, et puisque $uv$ est une primitive de $(uv)'$, on a
% \[\int_a^b u(t)v'(t)dt=[u(t)v(t)]_a^b-\int_a^b u'(t)v(t)dt\]}
% \else
% \lignes{5}
% \fi
%
%
% \methodeL{Pour calculer une intégrale par intégration par partie, on détermine les fonctions $u$ et $v$ qui interviennent et on vérifie qu'elles sont de classe $\mathcal{C}^1$ sur l'intervalle considéré.}
%
% \exempleL{Calculer $\displaystyle{\int_0^1 t\E^tdt}$.}
%
% \solution[5]{Pour tout $t \in [0;1]$, posons $u(t)=t$ et $v'(t)=\E^t$. $u$ et $v$ sont de classe $\CC^1$ sur $[0;1]$. Alors, $u'(t)=1$ et $v(t)=e^t$. On a donc
% \[\int_0^1 t\E^tdt = [t\E^t]_0^1 - \int_0^1 1\E^tdt = \E - [\E^t]_0^1=1\]}
%
% \exempleL{Calculer $\displaystyle{\int_1^e \ln(x)dx}$.}
%
% \solution[6]{ Puisque $\ln(t)=1\times \ln(t)$, pour tout $t \in [1;\E]$, on pose $u(t)=\ln(t)$ et $v'(t)=1$. Les fonctions $u$ et $v$ sont bien de classe $\mathcal{C}^1$ sur $[1;\E]$. On a donc $u'(t)=\frac{1}{t}$ et $v(t)=t$. Alors
% \[\int_1^e \ln(t)dt= [t\ln(t)]_1^\E - \int_1^\E t\frac{1}{t}dt\]
% et donc
% \[\int_1^\E \ln(t)dt = \E - [t]_1^\E = 1\]
%   }
%
% \remarqueL{Ainsi, une primitive de la fonction $\ln$ sur $]0;+\infty[$ est $x\mapsto x\ln(x)-x$.}
%
% % Changement de variable
%     \subsection{Changement de variable}
%
% L'idée du changement de variable est de se ramener à une intégrale que l'on sait calculer.
%
% \theoremeL{Soit $f$ une fonction continue sur $[a;b]$, et $u$ une fonction $\mathcal{C}^1$ sur $[\alpha; \beta]$, telle que $u([\alpha; \beta])\subset [a;b]$. Alors
% \[\int_\alpha^\beta f(u(t))u'(t)dt = \int_{u(\alpha)}^{u(\beta)} f(x)dx\]}
%
% \remarqueL{Il faut donc reconnaitre une forme $f(u)u'$ pour pouvoir effectuer un changement de variable. On n'oubliera pas de remplacer également les bornes d'intégration.}
%
% \ifprof
% \preuve{Soit $F$ une primitive de $f$, et  $g=F\circ u$. $g$ est $C^1$ sur $[\alpha; \beta]$ (puisque $F$ et $u$ sont $C_1$) et on a $g'= F'(u) \times u'=f(u)u'$. Donc $g$ est une primitive de $f(u)u'$. Donc
% \[\int_\alpha^\beta f(u(t))u'(t)dt=[g(t)]_\alpha^\beta = F(u(\beta))-F(u(\alpha))=[F(t)]_{u(\alpha)}^{u(\beta)} =\int_{u(\alpha)}^{u(\beta)} f(x)dx\]
% }
% \else
% \lignes{5}
% \fi
%
% \exempleL{Calculer $\displaystyle{I=\int_0^1 t\sqrt{1-t^2}dt}$ en posant $u=1-t^2$.}
%
% \solution[5]{Pour tout $t\in [0;1]$, $u(t)=1-t^2$. $u$ est de classe $\CC^1$ sur $[0;1]$. Alors
% \[I=\int_0^1 -\frac{1}{2}u'(t)\sqrt{u(t)}dt = -\frac{1}{2} \int_{u(0)}^{u(1)} \sqrt{x}dx = \frac{1}{2}\int_0^1 \sqrt{t}dt = \frac{1}{2}\left[\frac{2x\sqrt{x}}{3}\right]_0^1=\frac{1}{3}\]}
%
% \methodeL{Soit $I=\int_a^b f(t)dt$. Pour effectuer un changement de variable :
% \begin{itemize}
%     \item[$\bullet$] On pose $x=u(t)$ où $u$ est une fonction de classe $\mathcal{C}^1$ sur $[a;b]$. On écrit alors $t=u^{-1}(x)$ puis $dt=(u^{-1})'(x)dx$.
%     \item[$\bullet$] On s'occupe des bornes : lorsque $t=a$, $x=u(a)$ et lorsque $t=b$, $x=u(b)$.
%     \item[$\bullet$] Enfin, on exprime $f(t)$ et $dt$ uniquement avec $x$ et $dx$.
% \end{itemize}
% On a alors $\int_a^b f(t)dt = \int_{u(a)}^{u(b)} g(x)dx$, cette intégrale étant a priori plus simple à calculer.}
%
% \exempleL{Calculer $\displaystyle{I=\int_0^1 \frac{\ln(1+\E^t)}{1+\eu{-t}} dt}$ en effectuant le changement de variable $x=1+\E^t$.}
%
% \solution[12]{~\begin{itemize}
%     \item[$\bullet$] La fonction $u:t\mapsto 1+\E^t$ est de classe $\mathcal{C}^1$ sur $[0;1]$.
%     \item[$\bullet$] $u(0)=2$ et $u(1)=1+e$.
%     \item[$\bullet$] Puisque $x=1+\E^t$, alors $t=\ln(x-1)$. La fonction $x\mapsto \ln(x-1)$ est dérivable sur $[2;1+e]$, et donc
%     \[dt=  \frac{1}{x-1} dx\]
% \end{itemize}
% Ainsi,
% \[I=\int_2^{e+1} \frac{\ln(u)}{1-\eu{-\ln(u-1)}} \frac{du}{u-1}=\int_2^{e+1} \frac{\ln u }{u}du\]
% et donc
% \[I=\left[ \frac{1}{2} (\ln u)^2\right]_2^{e+1} = \frac{1}{2}((\ln(e+1))^2-\ln(2)^2\]}
%
%
%     \subsection{Fonctions définies par une intégrale}
%
% On peut être amené à étudier une fonction du type $\displaystyle{F: x\mapsto \int_{x_0}^x f(t)dt}$
% ou $\displaystyle{G:x\mapsto \int_{u(x)}^{v(x)} f(t)dt}$.
%
% \methodeL{Pour étudier ce genre de fonctions :\begin{itemize}
%     \item[$\bullet$] Dans le premier cas, on reconnait, après avoir étudié la continuité de $f$, la primitive de $f$ nulle en $x_0$, que l'on étudiera en tant que telle.
%     \item[$\bullet$] Dans le deuxième cas, on introduira systématiquement une primitive $H$ de $f$ si $f$ est continue. Dans ce cas, $G(x)=H(v(x))-H(u(x))$ et on étudiera ensuite (dérivation par exemple, si $u$ et $v$ sont dérivables).
% \end{itemize}}
%
% \exempleL{Soit $\displaystyle{g:x\mapsto \int_x^{2x} f(t)dt}$ où $f$ est une fonction continue sur $\R$. Déterminer $g'$.}
%
% \solution[5]{En notant $F$ une primitive de $f$ sur $\R$, on a pour tout réel $x$, $g(x)=F(2x)-F(x)$ qui est de classe $\mathcal{C}^1$ puisque $f$ est continue. On a donc
% \[\forall~x\in \R,~g'(x)=2F'(2x)-F'(x)=2f(2x)-f(x)\]}
%
% \section{Sommes de Riemann}
%
%     \subsection{Définition}
%
% \definitionL{Soit $f$ une fonction continue sur un intervalle $[a;b]$. Soit $n$ un entier strictement positif. On note,
% \[S_n(f)=\frac{b-a}{n} \sum_{k=0}^{n-1} f\left(a+k\frac{b-a}{n}\right) \textrm{ et }
% S_n'(f)=\frac{b-a}{n} \sum_{k=1}^{n} f\left(a+k\frac{b-a}{n}\right)\]}
%
% \remarqueL{Les sommes $S_n$ et $S_n'$ représentent l'aire des réctangles associés à la fonction $f$ lorsqu'on effectue un découpage régulier de l'intervalle $[a;b]$ :
% \begin{center}
% \includegraphics[width=10cm]{tex/Chap15/pic/riemann}
% \end{center}
% }
%
% \remarqueL{Dans le cas d'une fonction continue sur $[0;1]$, les sommes de Riemann s'écrivent
% \[S_n(f)=\frac{1}{n} \sum_{k=0}^{n-1} f\left(\frac{k}{n}\right) \textrm{ et }
% S_n'(f)=\frac{1}{n} \sum_{k=1}^{n} f\left(\frac{k}{n}\right)\]
% }
%
%     \subsection{Sommes de Riemann et intégrale}
%
% Les sommes de Riemann d'une fonction continue ont une propriété très importante : elles convergent vers l'intégrale de $f$ sur $[a;b]$ et en sont donc une très bonne approximation.
%
% \theoremeL{Soit $f$ une fonction continue sur $[a;b]$. Alors les suites $(S_n(f))_n$ et $(S_n'(f))_n$ convergent et
% \[\lim_{n\rightarrow +\infty} S_n(f) = \lim_{n\rightarrow +\infty} S_n'(f)=\int_a^b f(t)dt\]}
%
% \ifprof
% \preuve{Pour simplifier les calculs, supposons que $a=0$ et $b=1$, et supposons $f$ continue croissante sur $[0;1]$.
% ~\\Soit $n\in \N^*$ fixé. Pour tout $k$ entre $0$ et $n-1$, on a
% \[\forall~t \in \left[\frac{k}{n}; \frac{k+1}{n}\right], f\left(\frac{k}{n}\right)\leq f(t) \leq f\left(\frac{k+1}{n}\right)\]
% en utilisant la croissance de $f$. D'après l'inégalité de la moyenne, on a donc
% \[\frac{1}{n}f\left(\frac{k}{n}\right)\leq \int_{k/n}^{(k+1)/n} f(t)dt \leq \frac{1}{n}f\left(\frac{k+1}{n}\right)\]
% En additionnant ces inégalités pour $k$ entre $0$ et $n-1$ :
% \[\sum_{k=0}^{n-1}
% \frac{1}{n}f\left(\frac{k}{n}\right)\leq \sum_{k=0}^{n-1}\int_{k/n}^{(k+1)/n} f(t)dt \leq \sum_{k=0}^{n-1}\frac{1}{n}f\left(\frac{k+1}{n}\right)\]
% soit, par la relation de Chasles
% \[\sum_{k=0}^{n-1} \frac{1}{n}f(\frac{k}{n})\leq \int_0^1 f(t)dt \leq \sum_{k=0}^{n-1}\frac{1}{n}f(\frac{k+1}{n})\]
% En effectuant le changement de variable $j=k+1$ dans la deuxième somme, on déduit donc
% \[S_n(f) \leq \int_0^1 f(t)dt \leq \sum_{k=1}^n \frac{1}{n}f(\frac{k}{n}) = S_n(f)-\frac{f(0)}{n}+\frac{f(1)}{n}\]
% Cette inégalité s'écrit également
% \[\int_0^1 f(t)dt +\frac{f(0)}{n}-\frac{f(1)}{n} \leq S_n(f) \leq \int_0^1 f(t)dt\]
% En passant à la limite, par encadrement, on déduit donc bien
% \[\lim_{n\rightarrow +\infty} S_n(f)=\int_0^1 f(t)dt\]
% }
% \else
% \lignes{25}
% \fi
%
%     \subsection{Application : limite de certaines suites}
%
% \methodeL{Les sommes de Riemann peuvent nous permettre de calculer la limite de certaines suites. }
%
% \exempleL{Soit $u$ la suite définie pour tout $n\geq 1$ par
% \[u_n=\frac{1^3+2^3+\cdots +n^3}{n^4}\]    Déterminer la limite de $u$.}
%
% \solution[8]{Pour tout $n\geq1$, \[u_n= \sum_{k=0}^n \frac{k^3}{n^4} =\frac{1}{n}\sum_{k=0}^n \left(\frac{k}{n}\right)^3\]
%
% On reconnait une somme de Riemann de la fonction $f:x\mapsto x^3$ sur le segment $[0;1]$, qui est continue sur $[0;1]$. Alors, $(u_n)$ converge, et
% \[\lim_{n\rightarrow +\infty} u_n=\int_0^1 x^3dx=\left[\frac{x^4}{4}\right]_0^1 = \frac{1}{4}\]}
%
%     \subsection{Application : valeur approchée d'intégrale}
%
% Pour déterminer une valeur approchée d'une intégrale sur un segment, on peut utiliser la méthode des rectangles, qui consiste à calculer l'une des sommes de Riemann dans le cas d'une fonction continue et monotone.
%
%
% \methodeL{[Méthode des rectangles]
% Lorsqu'une fonction est croissante ou décroissante sur un segment $I=[a,b]$, on décompose $I$ en subdivision de longueur $\frac{b-a}{n}$ : \[\left[ a, a+\frac{b-a}{n}\right ], \left[ a+\frac{b-a}{n}, a+2\frac{b-a}{n}\right]\hdots,\left[ a+k\frac{b-a}{n}, a+(k+1)\frac{b-a}{n}\right],\hdots \left[ a+(n-1)\frac{b-a}{n}, b\right]\]
% On calcule alors la somme des aires des rectangles ``inférieurs'' et ``supérieurs'' : pour l'intervalle \[\left[ a+k\frac{b-a}{n}, a+(k+1)\frac{b-a}{n}\right],\] on prend le rectangle de hauteur $f\left(a+k\frac{b-a}{n}\right)$ (rectangle inférieur), ou $f\left(a+(k+1)\frac{b-a}{n}\right)$.
% \begin{center}\includegraphics{tex/Chap15/pic/rectangle}\end{center}
% Lorsque $n$ tend vers $+\infty$, l'aire obtenue, si la fonction est continue, tend vers l'intégrale de la fonction sur le segment.
% }
%
% \begin{scilab}[Méthode des rectangles]
% {\small \inputscilab{tex/Chap15/script/rectangle.sce}}
% \end{scilab}
%
% \section{Intégrale sur un intervalle quelconque}
%
% L'idée est d'étendre la notion d'intégrale, mais sur un intervalle infini, du type $[a;+\infty[$, $]-\infty; a[$ voire $]-\infty;+\infty[$.
%
%     \subsection{Définition}
%
% \definitionL{~\begin{itemize}
%     \item[$\bullet$] Soit $f$ une fonction continue sur $[a;+\infty[$. On dit que l'intégrale $\displaystyle{\int_a^{+\infty} f(t)dt}$ est \textbf{convergente} si et seulement si $\displaystyle{\lim_{x\rightarrow +\infty} \int_a^x f(t)dt}$ existe et est finie. Dans ce cas, on note
% \[\int_{[a;+\infty[} f(t)dt=\int_a^{+\infty} f(t)dt=\lim_{x\rightarrow +\infty} \int_a^x f(t)dt\]
%     On définit de même $\displaystyle{\int_{]-\infty;a]}f(t)dt=\int_{-\infty}^a f(t)dt}$.
%     \item[$\bullet$] Soit $f$ une fonction continue sur $\R$. On dit que l'intégrale $\displaystyle{\int_{-\infty}^{+\infty} f(t)dt}$ est \textbf{convergente} si et seulement si $\displaystyle{\lim_{x\rightarrow +\infty} \int_a^x f(t)dt}$ et $\displaystyle{\lim_{x\rightarrow -\infty} \int_{x}^a f(t)dt}$ existent et sont finies. Dans ce cas, on note
% \[\int_{\R} f(t)dt=\int_{-\infty}^{+\infty} f(t)dt=\lim_{x\rightarrow -\infty} \int_x^a f(t)dt+\lim_{x\rightarrow +\infty} \int_a^x f(t)dt\]
% \end{itemize}
% }
%
% \remarqueL{On dit que l'intégrale $\displaystyle{\int_a^{+\infty} f(t)dt}$ est une intégrale \textbf{impropre} (puisque, rigoureusement, l'intégrale n'est définie que sur un segment).}
%
%
% \exempleL{Soit $f:t\mapsto \eu{-t}$ sur $[0;+\infty[$. Calculer $\displaystyle{\int_0^{+\infty} \eu{-t}dt}$.}
%
% \solution[5]{Soit $x>0$. Alors
% \[\int_0^x f(t)dt = [-\eu{-t}]_0^x = 1-\eu{-x}\]
% et $\displaystyle{\lim_{x\rightarrow +\infty} 1-\eu{-x} = 0}$. Donc $\displaystyle{\int_0^{+\infty} f(t)dt}$ existe, et \[\int_0^{+\infty} \eu{-t} = 1\]
% }
%
%     \subsection{Propriétés}
%
% Toutes les propriétés de base se généralisent aux intégrales sur un intervalle quelconque :
%
% \proprieteL{Soient $f$ et $g$ deux fonctions définies sur $[a;+\infty[$, et $\lambda \in \R$.
% \begin{itemize}
%     \item[$\bullet$] Si $\displaystyle{\int_a^{+\infty} f(t)dt}$ converge, alors $\displaystyle{\int_a^{+\infty} \lambda f(t)dt}$ converge, et
%     \[\int_a^{+\infty} \lambda f(t)dt=\lambda\int_a^{+\infty} f(t)dt\]
%     \item[$\bullet$] Si $\displaystyle{\int_a^{+\infty} f(t)dt}$ et $\displaystyle{\int_a^{+\infty} g(t)dt}$ convergent, alors $\displaystyle{\int_a^{+\infty} (f(t)+g(t))dt}$ converge également, et
%     \[\int_a^{+\infty} (f(t)+g(t))dt=\int_a^{+\infty} f(t)dt+\int_a^{+\infty} g(t)dt\]
% \end{itemize}
% }
%
% \remarqueL{Attention : l'intégrale $\displaystyle{\int_a^{+\infty} (f(t)+g(t))dt}$ peut exister, sans pour autant que $\displaystyle{\int_a^{+\infty} f(t)dt}$ et $\displaystyle{\int_a^{+\infty} g(t)dt}$ n'existent.\\Par exemple, $\displaystyle{\int_1^{+\infty} \frac{1}{t}dt}$  et $\displaystyle{\int_1^{+\infty} \frac{1}{t^2}-\frac{1}{t}dt}$ ne convergent pas, et pourtant la somme $\displaystyle{\int_1^{+\infty} \frac{1}{t^2}dt}$ converge. }
%
% \proprieteL{[Relation de Chasles] Soient $-\infty \leq a < c < b \leq +\infty$ et $f$ une fonction continue sur $]a;b[$. Alors $\int_a^b f(t)dt$ converge si et seulement si $\int_a^c f(t)dt$ et $\int_c^b f(t)dt$ convergent. Dans ce cas
% \[\int_a^b f(t)dt=\int_a^c f(t)dt+\int_c^b f(t)dt\]}
%
% \theoremeL{Soient $f$ et $g$ deux fonctions définies sur l'intervalle $[a;+\infty[$. On suppose que \[\forall~x\in [a;+\infty[, 0\leq f(x)   \leq g(x)\]
% ~\\Alors, si $\displaystyle{\int_a^{+\infty} g(t)dt}$ converge, $\displaystyle{\int_a^{+\infty} f(t)dt}$ est également convergente, et on a
% \[0\leq \int_a^{+\infty} f(t)dt \leq \int_a^{+\infty} g(t)dt\]}
%
% \remarqueL{Attention : il faut absolument que $f$ soit positive.}
%
% \exempleL{Montrer que $\displaystyle{\int_1^{+\infty} \frac{1}{t+t^2}dt}$ converge.}
%
%
% \solution[5]{Remarquons que pour tout $t\geq 1$ on a $t+t^2\geq t^2$, soit
% \[0\leq \frac{1}{t+t^2} \leq \frac{1}{t^2}\]
% Or, pour $x>1$, on a \[\int_1^x \frac{1}{t^2}= \left[-\frac{1}{t}\right]_1^x = 1-\frac{1}{x}\]
% qui possède une limite finie quand $x$ tend vers $+\infty$. Donc l'intégrale $\displaystyle{\int_1^{+\infty}\frac{1}{t^2}dt}$ converge. Par comparaison, l'intégrale $\displaystyle{\int_1^{+\infty} \frac{1}{t+t^2}dt}$ converge.
% }
%
%     \subsection{Intégrales de référence}
%
% \theoremeL{[Intégrale de Riemann] La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $[1;+\infty[$ si et seulement si $\alpha >1$. Dans ce cas
% \[\int_1^{+\infty} \frac{1}{t^\alpha} = \frac{1}{\alpha -1}\]}
%
% \ifprof
% \preuve{Dans le cas où $\alpha \neq 1$, on a
% \[\int_1^x \frac{1}{t^\alpha}dt = \left[ \frac{1}{1-\alpha}\frac{1}{t^{\alpha-1}}\right]_1^x = \frac{1}{(1-\alpha)x^{\alpha-1}}-\frac{1}{1-\alpha}\]
% \begin{itemize}[label=\textbullet]
% 	\item Si $\alpha>1$ : quand $x$ tend vers $+\infty$, cette intégrale converge vers $\displaystyle{\frac{1}{\alpha-1}}$
% 	\item Si $\alpha < 1$ : \[\lim_{x\rightarrow +\infty} \frac{1}{x^{\alpha - 1 }} = +\infty\]
% 	donc l'intégrale diverge.
% 	\item Si $\alpha=1$, on a \[\int_1^x \frac{1}{t}dt=\ln(x) \textrm{ et } \lim_{x\rightarrow +\infty} \ln(x)=+\infty\] L'intégrale diverge donc.
% \end{itemize}
% }
% \else
% \lignes{10}
% \fi
%
% \theoremeL{[Intégrale de Riemann - 2] La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $[0;1]$ si et seulement si $\alpha<1$. Dans ce cas,
% \[ \int_0^1 \frac{1}{t^\alpha} = \frac{1}{1-\alpha} \]
% }
%
% \ifprof
% \preuve{Pour $\alpha \neq 1$, on a, pour tout $u\in]0;1[$,
% \[ \int_u^1 \frac{1}{t^\alpha}dt = \left[ \frac{1}{1-\alpha} \frac{1}{t^{\alpha-1}} \right]_u^1 = \frac{1}{1-\alpha}- \frac{1}{1-\alpha} \frac{1}{u^{\alpha-1}}\]
% Si $\alpha>1$, $\displaystyle{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = +\infty}$. L'intégrale diverge donc. Si $\alpha<1$, $\displaystyle{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = 0}$ donc l'intégrale converge et $\displaystyle{\int_0^1 \frac{1}{t^\alpha}dt=\frac{1}{1-\alpha}}$.\\
% Si $\alpha=1$, pour $u\in ]0;1[$, \[ \int_u^1 \frac{1}{t}dt=\ln(1)-\ln(u)=-\ln(u) \stackrel{u\rightarrow 0}{\longrightarrow} +\infty\]
% Donc l'intégrale diverge également.
% }
% \else
% \lignes{10}
% \fi
%
% \theoremeL{La fonction $f:t\mapsto \eu{-\alpha t}$ est intégrable sur $[0;+\infty[$ si et seulement si $\alpha >0$. Dans ce cas,
% \[\int_0^{+\infty} \eu{-\alpha t}dt =\frac{1}{\alpha}\]}
%
% \ifprof
% \preuve{Si $\alpha=0$, $\eu{-\alpha t} = 1$ et la fonction constante égale à $1$ n'est pas intégrable sur $[0;+\infty[$. Sinon,
% \[\int_0^x \eu{-\alpha t}dt = \left[ \frac{\eu{-\alpha t}}{-\alpha} \right]_0^x = -\frac{\eu{-\alpha x}}{\alpha} +\frac{1}{\alpha}\]
% Cette intégrale converge si et seulement si $\alpha > 0$, et dans ce cas
% \[\lim_{x \rightarrow +\infty} \int_0^x \eu{-\alpha t}dt = \frac{1}{\alpha}\]}
% \else
% \lignes{9}
% \fi
%
% \theoremeL{La fonction $f:t\mapsto \ln(t)$ est intégrable sur $]0;1]$, et on a \[ \int_0^1 \ln(t)dt=-1 \] }
%
% \ifprof
% \preuve{
% Soit $a\in ]0;1[$. Par intégration par parties, ou en utilisant une primitive de $\ln$, on a
% \[ \int_a^1 \ln(t)dt = \left[ t\ln(t)-t \right]_a^1 = -1-a\ln(a)+a \]
% Or, on a $\displaystyle{\lim_{a\rightarrow 0} a\ln(a) = 0}$ (croissance comparée). Par somme, $\displaystyle{\lim_{a\rightarrow 0} \int_a^1 \ln(t)dt=1}$. Ainsi, l'intégrale converge, et \[ \int_0^1 \ln(t)dt=-1 \]
% }
% \else
% \lignes{9}
% \fi
%
% \theoremeL{La fonction $f:x\mapsto \eu{-x^2}$ est intégrable sur $\R$ et
% \[\int_{-\infty}^{+\infty} \eu{-t^2}dt =\sqrt{\pi}\]}
%
% \preuve{Théorème admis.}
%
%     \subsection{Convergence absolue}
%
%  \definitionL{Soit $f$ une fonction définie sur $[a;+\infty[$.
%
%  On dit que l'intégrale $\displaystyle{\int_a^{+\infty} f(t)dt}$ \textbf{converge absolument} sur $[a;+\infty[$ si $\displaystyle{\int_a^{+\infty} |f(t)|dt}$ converge.}
%
%  \remarqueL{Rigoureusement, on dit que $f$ est \textbf{intégrable} sur $[a;+\infty[$ si $\displaystyle{\int_a^{+\infty} |f(t)|dt}$ converge.}
%
%    \theoremeL{Soit $f$ une fonction définie sur $[a;+\infty[$. Si $\displaystyle{\int_a^{+\infty} f(t)dt}$ est absolument convergente, alors elle est convergente, et on a
%    \[\left|\int_a^{+\infty} f(t)dt\right| \leq \int_a^{+\infty} |f(t)|dt\]}
%
% \preuve{Admis.}

\remarqueL{\danger{La réciproque n'est pas vraie. Une intégrale peut être convergente, sans êtres absolument convergente. On dit alors que l'intégrale est \textbf{semi-convergente}.}} 
