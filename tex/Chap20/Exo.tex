%%%%%%%%%%%%%%%%%
%%% EXERCICES %%%
%%%%%%%%%%%%%%%%%
\begin{exercices}

\begin{ExosApp}

\section*{Répartition et densité}


\begin{exoApp}{15}{1}[Fonctions de répartition]
Parmi les fonctions suivantes, déterminer celles qui définissent la fonction de répartition d'une variable aléatoire à densité. Le cas échéant, déterminer une densité associée.

$$F_1(x)=\left\{ \begin{array}{ll} \eu{x} & \textrm{si } x<0 \\ 1 &\textrm{sinon}\end{array}\right.~~~~
F_2(x)=\left \{ \begin{array}{ll} 0 & \textrm{si }x\leq 1 \\ \frac{x-1}{x+1} &\textrm{si }x>1\end{array}\right.$$
\end{exoApp}

\begin{correction}
$F_1$ est continue, et de classe $\CC^1$ sur $]-\infty;0[$ et sur $]0;+\infty[$ comme fonctions usuelles. De plus, \[\ds{\lim_{x\rightarrow 0^-} F_1(x)=\E^0=1=\lim_{x\rightarrow 0^+} F_1(x)}.\] De plus, $F_1$ est croissante sur $\R$, de limite $0$ en $-\infty$, et $1$ en $+\infty$. Comme $F_1$ est continue sur $\R$, $\CC^1$ sur $\R$ sauf éventuellement en $0$, c'est donc la fonction de répartition d'une variable aléatoire à densité. Une densité est donnée par la dérivée de $F_1$ sur $]-\infty;0[$ et sur $]0;+\infty[$, et par une valeur arbitraire positive en $0$. Par exemple, une densité est
\[ f_1 : x \mapsto \left \{ \begin{array}{ll} \E^x & \textrm{si }x<0\\ 0 & \textrm{si } x\geq 0\end{array}\right. \]
	De même, $F_2$ est croissante, de limite $0$ en $-\infty$ et $1$ en $+\infty$, de classe $\CC^1$ sur $]-\infty; 1[$ et sur $]1;+\infty[$, et on a
	\[ \lim_{x\rightarrow 1^+} F_2(x) = 0 = \lim_{x\rightarrow 1^-} F_2(x) \]
	Ainsi, $F_2$ est continue sur $\R$, de classe $\CC^1$ sur $\R$ sauf éventuellement en $1$. C'est donc la fonction de répartition d'une variable aléatoire à densité, dont une densité est donnée par (en donnant une valeur arbitraire en $1$) :
		\[ f_2 : x \mapsto \left \{ \begin{array}{ll} 0 & \text{si }x\leq 1\\ \frac{2}{(1+x)^2} & \text{sinon } \end{array}\right. \]
\end{correction}


\begin{exoApp}{15}{1}[Densités]
Pour chacune des fonctions suivantes, déterminer celles qui définissent une densité d'une variable aléatoire à densité. Le cas échéant, déterminer la fonction de répartition associée.
$$f_1(x)=\frac{1}{1+|x|}~~~~~~~
f_2(x)=\frac{\E^x}{(\E^x+1)^2}~~~~~~~
f_3(x)=\left\{ \begin{array}{ll} 1+x & \textrm{si } x\in [-1;0]\\ 1-x&\textrm{si } x\in ]0;1]\\0 & \textrm{sinon}\end{array}\right.$$
\end{exoApp}

\begin{correction}
Remarquons que les trois fonctions sont positives sur $\R$, continue sur $\R$ (sauf éventuellement en $-1, 0$ et $1$ pour $f_3$). Il reste à calculer leur intégrale sur $\R$.
\begin{itemize}[label=\textbullet]
	\item On utilise la relation de Chasles pour ôter la valeur absolue. Soient $A<0<B$ deux réels.
		\begin{eqnarray*}
			\int_A^B f_1(x)\dd x &=& \int_A^0 f_1(x)\dd x + \int_0^B f_1(x)\dd x \\
				&=& \int_A^0 \frac{1}{1-x}\dd x + \int_0^B \frac{1}{1+x}\dd x \\
				&=& \left[ -\ln(|1-x|)\right]_A^0 + \left [\ln(|1+x|)\right]_0^B \\
				&=& \ln(1-A)+\ln(1+B) \underset{A\rightarrow -\infty, B\rightarrow +\infty}{\longrightarrow} +\infty
		\end{eqnarray*}
		Donc $f_1$ n'est pas une densité de probabilité.
	\item De même, soient $A<0<B$. Alors
		\begin{eqnarray*}
			\int_A^B f_2(x)\dd x &=& \int_A^B \frac{\E^x}{(\E^x+1)^2}\dd x \\
				&=& \left[ -\frac{1}{1+\E^x}\right]_A^B \\
				&=& \underbrace{-\frac{1}{1+\E^B}}_{\underset{A\rightarrow-\infty}{\longrightarrow} 0}+\underbrace{\frac{1}{1+\E^A}}_{\underset{B\rightarrow+\infty}{\longrightarrow 1}} \longrightarrow 1
		\end{eqnarray*}
		Ainsi, $\ds{\int_{-\infty}^{+\infty} f_2(x)\dd x = 1}$ : $f_2$ est une densité de probabilité.
	\item Constatons, pour $f_3$, que $\ds{\int_{-\infty}^{-1} f_3(x)\dd x = 0}$ et $\ds{\int_1^{+\infty} f_3(x)\dd x=0}$. Ainsi
		\begin{eqnarray*}
			\int_{-\infty}^{+\infty} f_3(x)\dd x &=& \int_{-1}^1 f_3(x)\dd x \\
				&=& \int_{-1}^0 f_3(x)\dd x + \int_0^1 f_3(x)\dd x \\
				&=& \int_{-1}^0 1+x \dd x + \int_0^1 1-x \dd x \\
				&=& \left [ x+\frac{x^2}{2}\right]_{-1}^0 + \left [ x-\frac{x^2}{2}\right]_0^1 \\
				&=& -\left(-1+\frac{1}{2}\right) + 1-\frac{1}{2} = 1
		\end{eqnarray*}
		$f_3$ est donc une densité de probabilité (et on peut remarquer qu'elle est continue sur $\R$).
\end{itemize}
Il nous reste à déterminer les fonctions de répartition associées aux densité $f_2$ et $f_3$.
\begin{itemize}
	\item Pour $f_2$, l'expression de $f_2$ étant valable sur $\R$, on calcule rapidement :
	\begin{align*}
	F_2(x) &= \PP(X\leq x) = \int_{-\infty}^x f_2(t)\dd t \\
	    &= \lim_{A\to -\infty} \int_A^x \frac{\eu{t}}{(1+\eu{t})^2}\dd t \\
		%&= \int_{-\infty}^x \frac{\eu{t}}{(1+\eu{t})^2} \dd t \\
		&=  \lim_{A\to -\infty} \left[ -\frac{1}{1+\eu{t}} \right]_{A}^x \\
		&= \lim_{A\to -\infty} -\frac{1}{1+\eu{x}} - \left(-\frac{1}{1+\eu{A}}\right) = -\frac{1}{1+\eu{x}}+1
	\end{align*}
	\textbf{Bilan} : la fonction de répartition associée à $f_2$ est donc \[\boxed{ \forall~x\in \R, \quad F_2(x) = \frac{\eu{x}}{1+\eu{x}} }\]
	\item Pour $f_3$, l'expression de la densité impose de traiter $4$ cas : un cas par définition de $f_3$.
	\begin{itemize}[label=$\star$]
		\item Si $x\in ]-\infty, -1]$ :
		\begin{align*}
		   F_3(x) &= \PP(X\leq x) = \int_{-\infty}^x f_3(t)\dd t\\
		   &= \int_{-\infty}^x 0 \dd t = 0 	
		\end{align*}
		\item Si $x\in ]-1, 0]$ : 
		\begin{align*}
			F_3(x) &= \int_{-\infty}^x f_3(t)\dd t\\
			&= \int_{-\infty}^{-1} f_3(t)\dd t + \int_{-1}^x f_3(t)\dd t\\
			&= \int_{-\infty}^{-1} 0\dd t + \int_{-1}^x (1+t)\dd t\\
			&= 0 + \left[ t+\frac{t^2}{2}\right]_{-1}^x \\
			&= x+\frac{x^2}{2}+\frac{1}{2}	
		\end{align*}
		\item Si $x\in ]0, 1[$ : 
		\begin{align*}
		 F_3(x) &= \int_{-\infty}^x f_3(t)\dd t\\
		 &= \int_{-\infty}^{-1} 0 \dd t + \int_{-1}^0 1+t\dd t + \int_0^x 1-t \dd t \\
		 &= 0 + \left[ t+\frac{t^2}{2}\right]_{-1}^0 + \left[t-\frac{t^2}{2}\right]_0^x \\
		 &= 0 + \frac{1}{2} + x-\frac{x^2}{2}  	
		\end{align*}
		\item Enfin, si $x\geq 1$ :
		\begin{align*}
		F_3(x) &= \int_{-\infty}^x f_3(t)\dd t\\
		&= \int_{-\infty}^{-1} 0 \dd t +\int_{-1}^0 1+t\dd t+\int_0^1 1-t\dd t +\int_1^x 0 \dd t \\
		&= 0 + \frac{1}{2} +\frac{1}{2} + 0 = 1	
		\end{align*}
	\end{itemize}
On peut donc conclure que :
\[ \boxed{\forall~x\in \R,\quad F_3(x) = \left \{ \begin{array}{rr}0 & \text{si }x\leq -1\\
 \frac{1}{2}+x+\frac{x^2}{2}&\text{si } -1\leq x \leq 0 \\
 \frac{1}{2}+x-\frac{x^2}{2}& \text{si } 0\leq x\leq 1 \\
 1 & \text{si } x\geq 1	
 \end{array}
 \right.}\]
 dont on peut tracer la courbe représentative :
 \begin{center}
 \includegraphics{fonction_repartition_ex2_3.mps}	
 \end{center}

\end{itemize}
\end{correction}

\section*{Espérance}


\begin{exoApp}{20}{1}[L'espérance est esclave]
Déterminer l'existence, et le cas échéant la valeur de l'espérance des variables aléatoires dont la fonction de répartition, ou la densité est donnée dans l'exercice 1 ou 2.
\end{exoApp}

\iffalse
\begin{correction}
A faire.
\end{correction}
\fi 

\section*{Variables aléatoires à densité}


\begin{exoApp}{15}{1}[Une variable aléatoire sans espoir]
Soit $f$ la fonction définie sur $\R$ par
$$f(x)=\left\{ \begin{array}{ll}\frac{a}{x\sqrt{x}} & \textrm{si } x\geq 1 \\ 0 & \textrm{sinon}\end{array}\right.$$
\begin{enumerate}
  \item Déterminer le réel $a$ pour que $f$ soit une densité de probabilité d'une certaine variable aléatoire $X$.
  \item Déterminer la fonction de répartition associée à $X$.
  \item Montrer que $X$ n'admet pas d'espérance.
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item $f$ est continue sur $\R$ sauf éventuellement en $1$. Elle est positive si $a\geq 0$. Déterminons son intégrale sur $\R$. Soit $A>1$. Alors
		\begin{eqnarray*}
			\int_{-\infty}^{A} f(x)\dd x &=& 0 + \int_{1}^A \frac{a}{x^{3/2}}\dd x  \\
				&=& \left [ a \frac{x^{-1/2}}{-1/2} \right]_1^A \\
				&=&  2a -\frac{2a}{\sqrt{A}} \underset{A\rightarrow +\infty}{\longrightarrow}  2a
		\end{eqnarray*}
		Ainsi, $\ds{\int_{-\infty}^{+\infty} f(x)\dd x=1}$ si et seulement si $2a=1$, c'est-à-dire $a=\frac{1}{2}$, qui est bien positif.
		\\\textbf{Bilan} : $f$ est une densité de probabilité si et seulement si $a=\frac{1}{2}$.
  \item Par définition, la fonction de répartition $F$ est donnée par $\ds{F:x\mapsto \int_{-\infty}^x f(t)\dd t}$. Ainsi
\begin{itemize}
  \item Si $x\leq 1$ : $\ds{F(x)=\int_{-\infty}^x 0 \dd t = 0}$.
  \item Si $x>1$ : \[ F(x)=\int_{-\infty}^1 0\dd t +\int_1^x \frac{1}{2t\sqrt{t}} \dd t = \left [ \frac{1}{2} \frac{t^{-1/2}}{-1/2} \right]_1^x = 1-\frac{1}{\sqrt{x}} \]
\end{itemize}
		Ainsi \[ F:x\mapsto \left \{ \begin{array}{ll} 0 & \text{ si } x\leq 1\\ 1-\frac{1}{\sqrt{x}} & \text{ si } x > 1 \end{array} \right. \]
  \item $X$ admet une espérance si et seulement si l'intégrale $\ds{\int_{-\infty}^{+\infty} xf(x)\dd x}$ est absolument convergente, c'est-à-dire si et seulement si les intégrales $\ds{\int_{-\infty}^0 xf(x)\dd x}$ et $\ds{\int_0^{+\infty} xf(x)\dd x}$ sont convergentes car la densité $f$ est positive.
\begin{itemize}
  \item $\ds{\int_{-\infty}^0 xf(x)\dd x = 0}$.
  \item Soit $A>1$. Alors
				\begin{eqnarray*}
					\int_0^A xf(x)\dd x &=& \int_0^1 0\dd x + \int_1^A \frac{1}{2\sqrt{x}} \dd x \\
					&=& \left [ \sqrt{x} \right]_1^A \\
					&=& \sqrt{A}-1 \underset{A\rightarrow +\infty}{\longrightarrow} +\infty
				\end{eqnarray*}
				L'intégrale ne converge donc pas.
\end{itemize}
		Ainsi, $X$ n'admet pas d'espérance.
\end{enumerate}
\end{correction}

\begin{exoApp}{30}{1}[Des variables aléatoires]
Soit $f$ la fonction définie sur $\R$ par
$$\left\{ \begin{array}{ll} \eu{x} & \textrm{si } x<0 \\ 0 &\textrm{sinon}\end{array}\right.$$
\begin{enumerate}
  \item Montrer que $f$ est une densité de probabilité d'une certaine variable aléatoire que l'on notera $X$.
  \item Déterminer la fonction de répartition associée.
  \item Montrer que $X$ admet une espéarnce, et la calculer.
  \item On pose $Y=X^2$ et on admet que $Y$ est une variable aléatoire.
\begin{enumerate}
  \item Déterminer la fonction de répartition de $G$ de $Y$.
  \item Montrer que $Y$ est une variable à densité et déterminer une densité $g$ de $Y$.
  \item Répondre aux mêmes questions avec $Z=2X+1$.
\end{enumerate}
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item $f$ est positive sur $\R$, continue comme fonctions usuelles sur $\R$ sauf éventuellement en $0$. De plus, $\ds{\int_0^{+\infty} f(x)\dd x = 0}$. Soit $A<0$. Alors
		\[ \int_{A}^0 f(x)dx=\int_A^0 \E^x\dd x = [\E^x]_A^0 = 1-\E^A \underset{A\rightarrow -\infty}{\longrightarrow} 1 \]
		Ainsi, $f$ est bien une densité de probabilité.
  \item Notons $F$ la fonction de répartition. Si $x<0$, alors \[ F(x)= \int_{-\infty}^x f(t)\dd t = \lim_{A\rightarrow -\infty} [\E^t]_A^x = \E^x \]
	 et si $x\geq 0$, alors \[ F(x)=\int_{-\infty}^x f(t)\dd t=\int_{-\infty}^0 \E^t \dd t = 1 \]
	 Ainsi, \[ F:x\mapsto \left \{ \begin{array}{ll} \E^x & \text{si} x<0\\ 1 & \text{sinon} \end{array} \right. \]
  \item  $X$ admet une espérance si et seulement si l'intégrale $\ds{\int_{-\infty}^{+\infty} xf(x)\dd x}$ est absolument convergente, c'est-à-dire si et seulement si les intégrales $\ds{\int_{-\infty}^0 xf(x)\dd x}$ et $\ds{\int_0^{+\infty} xf(x)\dd x}$ sont convergentes car la densité $f$ est positive. Ici,
		\[ \int_0^{+\infty} tf(t)\dd t = 0 \]
		Soit $A<0$. Alors
		\begin{eqnarray*}
			\int_A^0 tf(t)dt &=& \int_A^0 t\E^t dt \\
			&\underset{I.P.P}{=}& \left[ t\E^t \right]_A^0 - \int_A^0 \E^tdt \quad\quad\text{(fonctions de classe $\CC^1$)}\\
			&=& -A\E^A - (1-\E^A) \underset{A\rightarrow -\infty}{\longrightarrow} -1 \text{ par croissances comparées}
		\end{eqnarray*}
		Ainsi, $X$ admet une espérance, et $\esperance[X]=-1$.
  \item ~
\begin{enumerate}
  \item Soit $x \in \R$. Alors
				\begin{eqnarray*}
					G(x) &=& \PP(Y\leq x) \\
						&=& \PP(X^2 \leq x)
				\end{eqnarray*}
				Si $x<0$, alors $(X^2\leq x) = \vide$ et $\PP(X^2\leq x) =0$.\\Si $x\geq 0$, alors
				\begin{eqnarray*}
					G(x)&=& \PP( |X| \leq \sqrt{x})\\
					&=& \PP(-\sqrt{x}\leq X \leq \sqrt{x}) \\
					&=& \PP(X\leq \sqrt{x}) - \PP(X\leq -\sqrt{x})\\
					&=& F(\sqrt{x})-F(-\sqrt{x}) \\
					&=& 1- \eu{-\sqrt{x}}
				\end{eqnarray*}
			Ainsi
			\[ G:x\mapsto \left \{ \begin{array}{ll}
				0 & \textrm{si } x < 0 \\
				1-\eu{-\sqrt{x}} & \textrm{sinon}
			\end{array}\right. \]
  \item $F$ est de classe $\CC^1$ sur $\R$ sauf éventuellement en $0$. De plus,
				\[ \lim_{x\rightarrow 0^+} G(x)=0=\lim_{x\rightarrow 0^-} G(x)\]
				Ainsi, $G$ est continue sur $\R$, et $Y$ est donc une variable aléatoire à densité. Une densité est donnée par (en prenant une valeur arbitraire en $0$) :
				\[ g:x\mapsto \left \{ \begin{array}{ll} 0 & \text{si} x\leq 0\\ \frac{\eu{-\sqrt{x}}}{2\sqrt{x}} & \text{sinon} \end{array} \right. \]
  \item Notons $H$ la fonction de répartition de $Z$. Alors, pour tout réel $x$,
				\begin{eqnarray*}
					H(x) &=& \PP(Z\leq x) \\
					&=& \PP(2X+1 \leq x) \\
					&=& \PP\left(X\leq \frac{x-1}{2}\right)\\
					&=& F\left(\frac{x-1}{2}\right)
				\end{eqnarray*}
				Ainsi, $F$ étant continue sur $\R$, $\CC^1$ sur $\R$ sauf en $0$, et $x\mapsto \frac{x-1}{2}$ étant de classe $\CC^1$ sur $\R$, par composée, $H$ est continue sur $\R$, $\CC^1$ sur $\R$ sauf en $1$. $Z$ est donc une variable aléatoire à densité, dont une densité est donnée par
				\[ h:x\mapsto \left \{ \begin{array}{ll}
 							\frac{1}{2}\eu{\frac{x-1}{2}} & \text{si} x\leq 1 \\
 							0 & \text{sinon}
 						\end{array}\right. \]
\end{enumerate}
\end{enumerate}
\end{correction}

\begin{exoApp}{30}{1}[EML 96]
Soit $\displaystyle{f(x)=\left\{ \begin{array}{ll} \eu{-|x|}& \textrm{si }-\ln 2 \leq x \leq \ln 2 \\ 0 & \textrm{sinon} \end{array}\right.}$.
\begin{enumerate}
  \item Montrer que $f$ est une densité de probabilité d'une certaine variable aléatoire, que l'on notera $X$.
  \item Déterminer la fonction de répartition associée.
  \item Montrer que $X$ admet une espérance et la calculer.
  \item On pose $Y=|X|$ et on admet que $Y$ est une variable aléatoire.
\begin{enumerate}
  \item Déterminer la fonction de répartition $G$ de $Y$.
  \item Montrer alors que $Y$ est une variable à densité, et déterminer une densité $g$ de $Y$.
\end{enumerate}
\end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
	\item La fonction $f$ est positive sur $\R$, continue sur $\R$ sauf éventuellement en $-\ln 2$ et en $\ln 2$. Il reste à calculer son intégrale sur $\R$, c'est-à-dire ici sur $[-\ln 2; \ln 2]$ puisqu'elle est nulle en dehors. En utilisant la relation de Chasles (obligatoire à cause de la valeur absolue) :
		\begin{eqnarray*}
			\int_{-\ln 2}^{\ln 2} f(x)=dx &=& \int_{-\ln 2}^0 \eu{-(-x)} dx + \int_0^{\ln 2} \eu{-x} dx \\
				&=& [ \E^x]_{-\ln 2}^0 + [-\eu{-x}]_0^{\ln 2} \\
				&=& 1-\eu{-\ln 2} + (-\eu{-\ln 2} + 1) \\
				&=& 1-\eu{\ln(1/2)} - \eu{\ln(1/2)} + 1 \\
				&=& 1-\frac{1}{2}-\frac{1}{2}+1 = 1
		\end{eqnarray*}
		Ainsi, $f$ est bien une densité de probabilité.
	\item Par définition de la densité, on doit distinguer selon la position de $x$ par rapport à $-\ln 2$, $\ln 2$ mais également $0$ pour le calcul.
		\begin{itemize}[label=\textbullet]
			\item Si $x<-\ln 2$, $\ds{F(x) = \int_{-\infty}^x 0dt = 0}$
			\item Si $-\ln 2 \leq x \leq 0$, alors
				\[ F(x)=\int_{-\infty}^x f(t)dt=\int_{-\ln 2}^x \eu{-(-t)}dt = [\E^t]_{-\ln 2}^x = \E^x-\frac{1}{2} \]
			\item Si $0\leq x \leq \ln 2$, alors
				\[ F(x)=\int_{-\infty}^x f(t)dt = \int_{-\ln 2}^0 \E^t dt + \int_0^x \eu{-t}dt = [\E^t]_{-\ln 2}^0 + [-\eu{-t}]_0^x = \frac{1}{2} -\eu{-x}+1=\frac{3}{2}-\eu{-x} \]
			\item Enfin, si $x\geq \ln 2$, alors \[ F(x)=\int_{-\infty}^x f(t)dt = \int_{-\ln 2}^{\ln 2} f(t)dt = 1 \]
		\end{itemize}
		\textbf{Bilan} : on obtient la fonction de répartition suivante :
		\[ F:x\mapsto \left \{ \begin{array}{ll} 0 & \text{si } x<\ln 2\\
 			\E^x-\frac{1}{2}& \text{si } -\ln 2 \leq x \leq 0 \\
 			\frac{3}{2}-\eu{-x} & \text{si } 0\leq x \leq \ln 2 \\
 			1 & \text{si } x\geq \ln 2
 \end{array}\right. \]
 et on peut vérifier qu'elle est bien continue en $-\ln 2$, $0$ et $\ln 2$.
 	\item $X$ étant à support fini (puisque $f$ est nulle en dehors de $[-\ln 2; \ln 2]$, $X$ admet une espérance. En utilisant la relation de Chasles, puis une intégration par parties (les fonctions étant de classe $\CC^1$ sur les intervalles considérés) :
 		\begin{eqnarray*}
 			\int_{-\infty}^{+\infty} xf(x)dx &=& \int_{-\ln 2}^{\ln 2} xf(x)dx \\
 				&=& \int_{-\ln 2}^0 x\eu{x}dx + \int_0^{\ln 2} x\eu{-x} dx \\
 				&=& \left([x\E^x]_{-\ln 2}^0 - \int_{-\ln 2}^0 \E^xdx\right) +
 				\left( [-x\eu{-x}]_0^{\ln 2} - \int_0^{\ln 2} -\eu{-x}dx \right)\\
 				&=& \frac{\ln 2}{2} - [\E^x]_{-\ln 2}^0  -\frac{\ln 2}{2} - [\eu{-x}]_0^{\ln 2} \\
 				&=& -1+\frac{1}{2} - (\frac{1}{2}-1) = 0
 		\end{eqnarray*}
		Ainsi, $X$ admet une espérance, et $\esperance[X]=0$.\\
\begin{remarque}
La densité $f$ étant paire, $x\mapsto xf(x)$ est impaire, et donc son intégrale sur un intervalle centré en $0$ est nulle.
\end{remarque}
	\item ~\begin{enumerate}
			\item Soit $x$ un réel. Ainsi, $G(x)=\PP(Y\leq x) = \PP(|X|\leq x)$.
				\begin{itemize}[label=\textbullet]
					\item Si $x<0$, $(|X|\leq x)=\emptyset$ et $\PP(|X|\leq x) = 0$.
					\item Si $x\geq 0$, alors
						\begin{eqnarray*}
							G(x) &=& \PP(|X| \leq x) \\
								&=& \PP(-x \leq X \leq x) \\
								&=& \PP(X\leq x) - \PP(X\leq -x) \\
								&=& F(x)-F(-x)
						\end{eqnarray*}
					\begin{itemize}
						\item Si $x>\ln 2$, alors
						\[ G(x)=F(x)-F(-x)=1-0 = 1 \]
						\item Si $0\leq x \leq \ln 2$, alors $F(x)=\frac{3}{2}-\eu{-x}$ et $F(-x)=\E^x-\frac{1}{2}$, et donc
						\[ G(x)=\frac{3}{2}-\eu{-x}-(\E^x-\frac{1}{2}) = 2-\eu{-x}-\E^x \]
					\end{itemize}
				\end{itemize}
				Ainsi, \[ G:x\mapsto \left \{ \begin{array}{ll} 0 & \text{si } x< 0 \\
 					2-\eu{-x}-\E^x & \text{si } 0\leq x \leq \ln 2 \\
 					1 & \text{si } x\geq \ln 2
 \end{array}\right. \]
  				\item $G$ est de classe $\CC^1$ sur chacun des intervalles $]-\infty; 0[$, $]0; \ln 2[$ et $]\ln 2; +\infty[$ comme sommes de fonctions usuelles. De plus, elle est continue en $0$ et en $\ln 2$ (calcul rapide des limites). Donc $G$ est continue sur $\R$, de classe $\CC^1$ sur $\R$ sauf $0$ et $\ln 2$ : $Y$ est donc une variable aléatoire à densité, dont une densité est la dérivée de $G$ là où elle est dérivable, et en prenant des valeurs arbitraires pour $0$ et $\ln 2$. Ainsi, une densité est
  					\[ g : \mapsto  \left \{ \begin{array}{ll} 0 & \text{si } x\leq 0\\ \eu{-x}-\E^x & \text{si } 0<x<\ln 2\\
 						0 & \text{si } x\geq \ln 2
 \end{array}\right. \]

		\end{enumerate}

\end{enumerate}
\end{correction}

\end{ExosApp}
\end{exercices}

