\chapter{Variables aléatoires à densité}

\objectifintro{
L'objectif de ce chapitre est de s'intéresser aux variable aléatoires dont le support est un intervalle, et non plus fini ou dénombrable. C'est l'occasion d'introduire la notion de densité, et d'étendre la notions d'espérance. On s'intéresse alors à des lois usuelles : uniforme, exponentielle et normale.
}

\begin{extrait}{Andrée Chedid (1929 -- 2011). \emph{Textes pour un poème}}
La poésie n'est pas refus ou survol de la vie ; mais plutôt une manière de la multiplier, de rendre compte de sa largesse. Elle témoigne aussi d'une soif qui nous hante, d'un sens impénétrable qui nous tient en haleine, d'une densité que le quotidien dilapide trop souvent.
\end{extrait}

\begin{objectifs}


%\begin{numerote}
%  \item mener un raisonnement par récurrence\dotfill $\Box$
%\end{numerote}
\end{objectifs}

%%% Début du cours %%%

\section{Variables aléatoires à densité}

On s'intéresse dans ce chapitre aux variables aléatoires $X$ dont l'univers image $X(\Omega)$ est un intervalle, ou une union d'intervalles.\\
Dans l'ensemble de ce chapitre, on se donne un espace probabilisé $(\Omega, \mathcal{A}, \PP)$.

\subsection{Notion de densité}

\begin{definition}
Soit $X$ une variable aléatoire définie sur $(\Omega, \mathcal{A}, \PP)$. On note $F_X$ sa fonction de répartition : \\$\forall~x\in \R, F_X(x)=\PP(X\leq x)$. On dit
 que $X$ est une \textbf{variable à densité} lorsque $F_X$ est continue sur $\R$, et de classe $\mathcal{C}^1$ sur $\R$ \emph{sauf éventuellement un nombre fini de points}.
\end{definition}

\begin{methode}
Pour démontrer qu'une variable aléatoire est à densité, il suffit de montrer que $F_X$ est croissante sur $\R$, de limite $0$ en $-\infty$ et $1$ en $+\infty$ (ça fait de $F_X$ une fonction de répartition), continue sur $\R$, et de classe $\mathcal{C}^1$ sur $\R$ sauf en un nombre fini de point.
\end{methode}

\begin{exo}
Soit $X$ une variable aléatoire dont la fonction de répartition est donnée par
$$F_X : x \mapsto \frac{\E^x}{1+\E^x}$$ Montrer que $X$ est une variable à densité.
\end{exo}

\ifprof
\begin{soluce}
$F_X$ est continue sur $\R$ comme quotient de fonctions continues, et est même $\mathcal{C}^1$ sur $\R$ comme quotient de fonctions de classe $\mathcal{C}^1$ dont le dénominateur ne s'annule pas. La dérivée de $F$ est $F':x \mapsto \frac{\E^x}{(1+\E^x)^2}$ qui est strictement positive sur $\R$, donc $F$ est également croissante. Enfin, $F$ est de limite $0$ en $-\infty$ (par quotient) et $1$ en $+\infty$ (après factorisation par $\E^x$). Donc $X$ est une variable à densité.
\end{soluce}
\else
\lignes{14}
\fi

\begin{remarque}
$F_X$ étant de classe $\mathcal{C}^1$ sur $\R$ sauf éventuellement en un nombre fini de point, on peut définir $F_X'$ sauf éventuellement en un nombre fini de point. Cette fonction $F_X'$ sera alors continue sur $\R$ sauf éventuellement en un nombre fini de point.
\end{remarque}

\begin{definition}
Soit $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$, et $F_X$ sa fonction de répartition. On appelle \textbf{densité} de $X$ toute fonction $f_X$, positive sur $\R$, telle que pour tout $x \in \R$ où $F_X$ est de classe $\mathcal{C}^1$, $f_X(x)=F_X'(x)$.
\end{definition}

\begin{remarque}
La valeur prise par la densité $f_X$ aux points où $F_X$ n'est pas $\mathcal{C}^1$ n'a pas d'importance. On peut même modifier un nombre fini de valeurs de $f_X$ sans que cela change la densité :
\end{remarque}

\begin{theoreme}
Soit $f$ une fonction, à valeurs positives, qui ne diffère de $F_X'$ qu'en un nombre fini de points. Alors $f$ est également une densité de $X$.
\end{theoreme}

\subsection{Propriétés}

\begin{proposition}
Soient $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$, $F_X$ sa fonction de répartition, et $f_X$ une densité de $X$.
\begin{itemize}
  \item $\displaystyle{\int_{-\infty}^{+\infty} f_X(t)\dd t=1}$.
  \item La fonction $f_X$ est continue sauf éventuellement en un nombre fini de points.
\end{itemize}
\end{proposition}

\ifprof
\begin{demonstration}
\begin{itemize}
  \item Puisque $\displaystyle{\lim_{x\rightarrow +\infty} F_X(x)=1}$ et que $\displaystyle{\lim_{x\rightarrow -\infty} F_X(x)=0}$, on a
    $$\int_{-\infty}^{+\infty} f_X(t)\dd t = \lim_{x\rightarrow +\infty} \int_{-x}^x f_X(t)\dd t = \lim_{x\rightarrow +\infty} F_X(x)-F_X(-x) = 1$$
  \item Puisque $F_X$ est de classe  $\mathcal{C}^1$ sur $\R$ sauf éventuellement en un nombre fini de points, par définition, $f_X$ est continue sur $\R$, sauf en un nombre fini de point.
\end{itemize}
\end{demonstration}
\else
\lignes{7}
\fi

\begin{theoreme}
Soit $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$, soit $F_X$ sa fonction de répartition et $f_X$ une densité de $X$. Alors
$$\forall~x\in \R, F_X(x)=\PP(X\leq x)=\int_{-\infty}^x f_X(t)\dd t$$
$$\textrm{et }\quad
\PP(X>x) = 1-F_X(x)=\int_{x}^{+\infty} f_X(t)\dd t $$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, puisque $f_X=F_X'$ sauf éventuellement en un nombre fini de points, par propriété de l'intégration
$$\int_{-\infty}^x f_X(t)\dd t = \lim_{a \rightarrow -\infty} [F_X(t)]_{a}^x = \lim_{a\rightarrow -\infty} F_X(x)-F_X(a) = F_X(x)-0=F_X(x)$$
Enfin, par la relation de Chasles,
$$1-F_X(x)=\int_{-\infty}^{+\infty} f_X(t)\dd t- \int_{-\infty}^{x} f_X(t)\dd t=
    \int_{-\infty}^{+\infty} f_X(t)\dd t+ \int_{x}^{-\infty} f_X(t)\dd t=\int_{x}^{+\infty} f_X(t)\dd t$$
\end{demonstration}
\else
\lignes{17}
\fi

\begin{remarque}
Puisque la fonction de répartition définit la loi de $X$, d'après ce qui précède, la connaissance d'une densité $f_X$ permet de connaitre complètement $F_X$, et donc la loi de $X$.
\end{remarque}

\begin{propriete}
Soit $X$ une variable aléatoire à densité, $F_X$ sa fonction de répartition, et $f_X$ une densité. Alors
\begin{itemize}
  \item Pour tout $x\in \R$, $\PP(X=x)=0$
  \item Pour tous $a$ et $b$ dans $\R$ tels que $a<b$,
    $$\PP(a\leq X \leq b) = \PP(a<X\leq b) = \PP(a\leq X < b) = \PP(a<X<b) = F_X(b)-F_X(a)=\int_a^b f_X(t)\dd t$$
\end{itemize}
\end{propriete}

\ifprof
\begin{demonstration}
\begin{itemize}
  \item On pose, pour tout $n\in \N^*$, $A_n=\left[x-\frac{1}{n}< X \leq x+\frac{1}{n}\right]$. La suite d'évènements $(A_n)$ est décroissante, et $$\lim_{n\rightarrow +\infty} \bigcap_{k=1}^n A_k=[X=x]$$ D'après la propriété de limite monotone :
    $$\PP(X=x) = \lim_{n\rightarrow +\infty} \PP(A_n)$$
    et
    $$\lim_{n\rightarrow +\infty} \PP(A_n)=\lim_{n\rightarrow +\infty}  \PP\left(X\leq x+\frac{1}{n}\right)-\PP\left(X\leq x-\frac{1}{n}\right) = \lim_{n\rightarrow +\infty} F_X(x+\frac{1}{n})-F_X(x-\frac{1}{n})=0$$
    par continuité de $F_X$.
  \item D'après ce qui précède,
    $$\PP(a\leq X \leq b) = \PP(X=a)+\PP(a<X\leq b) = 0+\PP(a<X\leq b)$$
    Les autres se font de la même manière. Enfin,
    $$\PP(a< X \leq b) = \PP(X\leq b) - \PP(X\leq a) = F_X(b)-F_X(a)=\int_{-\infty}^b f_X(t)\dd t-\int_{-\infty}^a f_X(t)\dd t= \int_a^b f_X(t)\dd t$$
    par la relation de Chasles.
\end{itemize}
\end{demonstration}
\else
\lignes{16}
\fi

\subsection{Reconnaitre une densité}

\begin{theoreme}
Soit $f$ une fonction positive, continue sur $\R$ sauf éventuellement en un nombre fini de points, telle que
$$\int_{-\infty}^{+\infty} f(t)\dd t=1$$
Alors $f$ est la densité d'une variable aléatoire.
\end{theoreme}

\begin{methode}
Pour démontrer qu'une fonction est une densité, il faut donc montrer trois points :
\begin{itemize}
  \item que $f$ est positive sur $\R$,
  \item que $f$ est continue sur $\R$ sauf éventuellement en un nombre fini de points,
  \item et que $\displaystyle{\int_{-\infty}^{+\infty} f(t)\dd t=1}$.
\end{itemize}
\end{methode}

\begin{exo}
Soit $f$ la fonction définie sur $\R$ par  $f(x)=\eu{-x}$ si $x\geq 0$, et $0$ sinon. Montrer que $f$ est une densité sur $\R$.
\end{exo}

\ifprof
\begin{soluce}
$f$ est bien positive sur $\R$, continue sur $\R^*_-$ et $\R^{*}_+$ (donc sur $\R$ sauf en un point), et on a
$$\int_{-\infty}^{+\infty} f(t)\dd t = \lim_{a\rightarrow +\infty} \int_{-a}^a  f(t)\dd t$$
Pour $a>0$, on a
$$ \int_{-a}^a f(t)\dd t=\int_{-a}^0 0\dd t + \int_0^{a} \eu{-t}\dd t =  0 + [-\eu{-x}]_0^{a} = 1-\eu{-a}$$
or $\displaystyle{\lim_{a\rightarrow +\infty} 1-\eu{-a}=1}$. Ainsi, $$\int_{-\infty}^{+\infty} f(t)\dd t = 1$$
$f$ est donc bien une densité.
\end{soluce}
\else
\lignes{15}
\fi

\subsection{Transformation affine}

\begin{theoreme}
Soit $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$, de fonction de répartition $F_X$ et de densité $f_X$. Soient $a$ et $b$ deux réels tels que $a\neq 0$. Alors $Y=aX+b$ est également une variable à densité, dont une densité est
$$ f_Y:x \mapsto \frac{1}{|a|} f_X\left(\frac{x-b}{a}\right) $$
\end{theoreme}

\ifprof
\begin{demonstration}
On suppose que $a>0$. Déterminons la fonction de répartition de la variable aléatoire $Y=aX+b$ :
$$F_Y(x)=\PP(Y\leq x) = \PP(aX+b    < x) = \PP\left(X < \frac{x-b}{a}\right)=F_X\left(\frac{x-b}{a}\right)$$
La fonction $x\mapsto F_X\left( \frac{x-b}{a} \right)$ est une fonction continue (par composée de fonctions continues), et de classe $\mathcal{C}^1$ sauf en un éventuellement nombre fini de points (la fonction $x\mapsto \frac{x-b}{a}$ est $\mathcal{C}^1$ sur $\R$ et $F_X$ est $\mathcal{C}^1$ sauf éventuellement en un nombre fini de points). Donc la variable aléatoire $Y$ est à densité.\\
Par définition,
$$\forall~x \in \R, \quad F_Y(x)=F_X\left( \frac{x-b}{a} \right)$$
De plus, en tout réel $x$ où la fonction $F_Y$ est de classe $\mathcal{C}^1$, on a
$$F_Y'(x)=\frac{1}{a}F_X'\left(\frac{x-b}{a}\right) = \frac{1}{a}f_X\left(\frac{x-b}{a}\right)$$
Par construction, la fonction $f_Y:x \mapsto \frac{1}{a}f_X\left(\frac{x-b}{a}\right)$ est positive, continue sauf en un nombre fini de points. D'après la relation précédente, $f_Y$ est une densité de $Y$.\\
La cas $a<0$ se traite de la même manière, en n'oubliant pas qu'en divisant par $a$, on changera le sens des inégalités.
\end{demonstration}
\else
\lignes{25}
\fi

\begin{remarque}
Il  est inutile d'apprendre ce résultat. En effet, il est souvent plus simple de le retrouver, en partant de $F_Y(x)=\PP(Y\leq x)$ et en revenant à $X$.
\end{remarque}

\section{Espérance d'une variable aléatoire à densité}

\subsection{Définition}

\begin{definition}
Soit $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$, dont $f_X$ est une densité.
On dit que $X$ admet \textbf{une espérance} si l'intégrale
$\displaystyle{\int_{-\infty}^{+\infty} tf_X(t)\dd t}$
est absolument convergente.\\
Dans ce cas, on appelle \textbf{espérance} de $X$, et on note $E(X)$ le nombre
$$E(X)=\int_{-\infty}^{+\infty} tf_X(t)\dd t$$
\end{definition}

\begin{remarque}
\begin{itemize}
  \item Il faut que l'intégrale soit \textbf{absolument} convergente, et non simplement convergente.
  \item Une densité étant positive, on constate que :
    $$\forall~t\geq 0, |tf_X(t)|=tf_X(t)\textrm{ et } \forall~t\leq 0, |tf_X(t)|=-tf_X(t)$$
    Ainsi, l'intégrale $\displaystyle{\int_{-\infty}^{+\infty} tf_X(t)\dd t}$ est absolument convergente si, et seulement si, les intégrales $\displaystyle{\int_{-\infty}^{0} tf_X(t)\dd t}$ et $\displaystyle{\int_{0}^{+\infty} tf_X(t)\dd t}$ sont convergentes.
\end{itemize}
\end{remarque}

\begin{methode}
Pour montrer qu'une variable aléatoire à densité admet une espérance, on procède en deux temps :
\begin{itemize}
  \item On montre que l'intégrale est absolument convergente : pour cela, on calcule $\displaystyle{\int_a^0 tf_X(t)\dd t}$ et $\displaystyle{\int_0^b tf_X(t)\dd t}$ et on montre que les intégrales admettent une limite lorsque $a$ tend vers $-\infty$ et $b$ tend vers $+\infty$. Dans ce cas, d'après la remarque précédente,  $\displaystyle{\int_{-\infty}^{+\infty} tf_X(t)\dd t}$  est absolument convergente, et la variable aléatoire admet donc une espérance.
  \item Une fois cela fait, on peut conclure que $X$ admet une espérance, qui vaut alors
\[\int_{-\infty}^{0} tf_X(t)\dd t+\int_{0}^{+\infty} tf_X(t)\dd t,\] valeurs qu'on aura calculées précédemment.
\end{itemize}
\end{methode}

\begin{exo}
Soit $X$ une variable aléatoire à densité dont une densité est la fonction $f$ définie sur $\R$ par $f(x)=0$ si $x<0$, et $f(x)=\eu{-x}$ sinon. Montrer que $X$ admet une espérance, et calculer $E(X)$.
\end{exo}

\ifprof
\begin{soluce}
On a déjà vu que $f$ est bien une densité.\\
On constate que $\displaystyle{\int_{-\infty}^0 tf(t)\dd t=0}$. De plus, pour $b > 0$ :
$$\int_{0}^{b} tf(t)\dd t = \int_{0}^{b} t\eu{-t}\dd t = 1-b\eu{-b}-\eu{-b}$$
(résultat qu'on peut obtenir par intégration par partie) qui admet une limite lorsque  $b$ tend vers $+\infty$ (limites classiques et croissances comparées). \\
L'intégrale est absolument convergente, donc $X$ admet une espérance, et
$$E(X)=\int_{-\infty}^{+\infty} tf(t)\dd t=\int_0^{+\infty} t\eu{-t}\dd t=1$$
\end{soluce}
\else
\lignes{20}
\fi

\subsection{Propriétés}

Tout comme dans le cas discret, l'espérance est linéaire \textbf{sous réserve d'existence}.

\begin{theoreme}
Soit $X$ une variable aléatoire à densité sur $(\Omega,\mathcal{A},\PP)$ \textbf{admettant une espérance}, et soit $a$ un réel non nul et $b$ un réel quelconque. Alors la variable aléatoire $aX+b$ admet également une espérance, et on a
$$E(aX+b)=aE(X)+b$$
\end{theoreme}

\ifprof
\begin{demonstration}
On note $f_X$ une densité de $X$, et $Y=aX+b$. Une densité de $Y$ est $f_Y:x\mapsto \frac{1}{|a|} f_X\left(\frac{x-b}{a}\right)$. Pour simplifier, on va supposer que $a>0$ et on admet que l'intégrale $\displaystyle{\int_{-\infty}^{+\infty} tf_Y(t)\dd t}$ est absolument convergente. Alors, par changement de variable $u=\frac{t-b}{a}$, on a
$$\int_{-\infty}^{+\infty} tf_Y(t)\dd t= \int_{-\infty}^{+\infty} (au+b)\frac{1}{a}f_X(u)a\dd u=\int_{-\infty}^{+\infty} (au+b)f_X(u)\dd u$$
Or, puisque $f_X$ est une densité, et que $X$ admet une espérance, on a
$$\int_{-\infty}^{+\infty} bf_X(u)\dd u=b \textrm{ et }
\int_{-\infty}^{+\infty} auf_X(u)\dd u = aE(X)$$
Donc
$E(Y)=aE(X)+b$.
\end{demonstration}
\else
\lignes{15}
\fi

\subsection{Variable aléatoire centrée}

\begin{definition}
Soit $X$ une variable aléatoire à densité sur $(\Omega, \mathcal{A}, \PP)$ admettant une espérance.
\begin{itemize}
  \item On dit que $X$ est une variable aléatoire \textbf{centrée} si $E(X)=0$.
  \item Si $X$ n'est pas centrée, la variable aléatoire $Y=X-E(X)$ est une variable aléatoire à densité centrée.
\end{itemize}
\end{definition}

\section{Lois à densité usuelles}

\subsection{Loi uniforme}

\subsubsection{Définition}

\begin{definition}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A},\PP)$. On dit que $X$ suit la \textbf{loi uniforme} sur l'intervalle $[a;b]$, et on note $X \hookrightarrow \mathcal{U}([a;b])$, si $X$ est une variable aléatoire à densité, de densité
$$f(x)=\left\{ \begin{array}{ll} \frac{1}{b-a} & \textrm{si } x\in [a;b] \\ 0 & \textrm{si } x\not \in [a;b] \end{array} \right.$$
La loi uniforme sur $[a;b]$ est la même que sur $[a;b[$, $]a;b]$ ou $]a;b[$.
\end{definition}

\begin{remarque}
La fonction $f$ est bien une densité. En effet, $f$ est continue sur $\R \backslash \{a;b\}$, positive sur $\R$, et $$\int_{-\infty}^{+\infty} f(t)\dd t = \int_a^b \frac{1}{b-a}\dd t = \left[ \frac{t}{b-a}\right]_a^b = \frac{b}{b-a}-\frac{a}{b-a}=1$$
\end{remarque}

\begin{remarque}
Si $X \hookrightarrow \mathcal{U}([0;1])$, alors une densité de $X$ est la fonction $f$ définie par $f(x)=1$ si $x\in [0;1]$, $0$ sinon.
\end{remarque}

\subsubsection{Propriétés}

\begin{theoreme}
Soit $X$ une variable aléatoire suivant la loi uniforme sur $[a;b]$.
\begin{itemize}
  \item La fonction de répartition de $X$ est la fonction $F_X$ définie sur $\R$ par
    $$F_X(x)=\left\{ \begin{array}{ll} 0 & \textrm{si } x<a \\
                                      \frac{x-a}{b-a} & \textrm{si } a\leq x \leq b\\
                                       1 & \textrm{si } x >b\end{array}\right.$$
  \item $X$ admet une espérance, et $E(X)=\frac{a+b}{2}$.
\end{itemize}
\end{theoreme}

\ifprof
\begin{demonstration}
\begin{itemize}
  \item Pour $x<a$, on a $$\int_{-\infty}^x f_X(t)dt = \int_{-\infty}^x 0 dt = 0$$
		Pour $a\leq x \leq b$, on a
		$$\int_{-\infty}^x f_X(t)dt = \int_a^x \frac{1}{b-a}dt = \left[ \frac{1}{b-a} t \right]_a^x = \frac{x}{b-a}-\frac{a}{b-a}=\frac{x-a}{b-a}$$
		Enfin, pour $x>a$, on a
		$$\int_{-\infty}^x f_X(t)dt = \int_a^b f_X(t)dt = 1$$
  \item Pour le deuxième point, calculons
$$\int_{-\infty}^{+\infty} |tf(t)|dt = \int_a^b |t|\frac{1}{b-a}dt$$
Cette intégrale converge, puisqu'il s'agit d'une intégrale sur un segment, et que la fonction $t\mapsto |t|$ est continue. Donc $X$ admet une espérance, qui vaut
$$E(X)=\int_{-\infty}^{+\infty} tf(t)dt=\int_a^b t\frac{1}{b-a}dt = \frac{1}{b-a} \left[ \frac{t^2}{2} \right]_a^b = \frac{b^2-a^2}{2(b-a)}=\frac{(b-a)(b+a)}{2(b-a)}=\frac{a+b}{2}$$
\end{itemize}
\end{demonstration}
\else
\lignes{20}
\fi

\begin{proposition}
Soient $a$ et $b$ deux réels, tels que $a<b$. Alors
$$X\hookrightarrow \mathcal{U}([0;1]) \Leftrightarrow Y=a+(b-a)X \hookrightarrow \mathcal{U}([a;b])$$
\end{proposition}

\ifprof
\begin{demonstration}
En effet, si $Y = a+(b-a)X$ alors, pour tout réel $x$
$$F_Y(x)=\PP(a+(b-a)X\leq x) = \PP\left(X\leq \frac{x-a}{b-a}\right)=F_X\left(\frac{x-a}{b-a}\right) \textrm{  et donc  } F_X(x)=F_Y(a+(b-a)x)$$
\begin{itemize}
  \item Si $Y\suit \mathcal{U}[a;b]$, alors \begin{itemize}
			\item si $x<0$, $a+(b-a)x<a$ donc $F_X(x)=F_Y(a+(b-a)x) = 0$
			\item si $0\leq x \leq 1$, alors $a\leq a+(b-a)x\leq b$ donc $F_X(x)=F_Y(a+(b-a)x)=\frac{(a+(b-a)x)-a}{b-a}=\frac{(b-a)x}{b-a}=x$
			\item si $x>1$, alors $a+(b-a)x>b$ donc $F_X(x)=F_Y(a+(b-a)x)=1$.\end{itemize}
			On reconnait la fonction de répartition d'une loi $\mathcal{U}[0;1]$ : $X\suit \mathcal{U}[0;1]$
  \item Si $X\suit \mathcal{U}[0;1]$, alors \begin{itemize}
			\item si $x<a$ alors $\frac{x-a}{b-a}<0$ donc $F_Y(x)=  F_X\left(\frac{x-a}{b-a}\right) = 0$
			\item si $a\leq x \leq b$ alors $0\leq \frac{x-a}{b-a}\leq 1$ donc $F_Y(x)=F_X\left(\frac{x-a}{b-a}\right)=\frac{x-a}{b-a}$
			\item si $x>b$ alors $\frac{x-a}{b-a}>1$ donc $F_Y(x)=F_X\left(\frac{x-a}{b-a}\right)=1$\end{itemize}
			 On reconnait la fonction de répartition d'une loi $\mathcal{U}[a
			 ;b]$ : $X\suit \mathcal{U}[a;b]$
\end{itemize}
\end{demonstration}
\else
\lignes{15}
\fi

\subsection{Loi exponentielle}

\subsubsection{ Définition}

\begin{definition}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A},\PP)$ et $\lambda>0$. On dit que $X$ suit la \textbf{loi exponentielle} de paramètre $\lambda$, et on note $X \hookrightarrow \mathcal{E}(\lambda)$, si $X$ est une variable aléatoire à densité, de densité
$$f(x)=\left\{ \begin{array}{ll} \lambda e^{-\lambda x} & \textrm{si } x\geq 0 \\ 0 & \textrm{si } x<0 \end{array} \right.$$
La loi exponentielle est à support sur $\R^+$ mais peut être également définie à support sur $\R^+_*$.
\end{definition}

\begin{remarque}
La fonction $f$ est bien une densité. En effet, $f$ est continue sur $\R^*$, et positive sur $\R$. De plus, $$\int_{-\infty}^{+\infty} f(t)dt = \lim_{a\rightarrow +\infty} \int_{-a}^a f(t)dt$$
Soit $a>0$, alors
$$\int_{-a}^a f(t)dt = \int_{-a}^0 0dt + \int_0^a \lambda e^{-\lambda t}dt = \left[ -e^{-\lambda t} \right]_0^{a} = 1-e^{-\lambda a}$$
et $\displaystyle{\lim_{a\rightarrow +\infty} 1-e^{-\lambda a} = 1}$. Donc
$$\int_{-\infty}^{+\infty} f(t)dt=1$$ et $f$ est bien une densité.
\end{remarque}

\begin{remarque}
On peut simuler une loi exponentielle avec \textit{grand}.
\end{remarque}

\begin{scilab}
[Simulation d'une loi exponentielle]
\inputscilab{exponentielle.sci}
\end{scilab}

\begin{center}
\includegraphics[width=16cm]{exponentielle}
\end{center}

\subsubsection{Propriétés}

\begin{theoreme}
Soit $X$ une variable aléatoire suivant la loi exponentielle de paramètre $\lambda >0$.
\begin{itemize}
  \item La fonction de répartition de $X$ est la fonction $F_X$ définie sur $\R$ par
    $$F_X(x)=\left\{ \begin{array}{ll} 0 & \textrm{si } x<0 \\
                                      1-e^{-\lambda x} & \textrm{si } x\geq 0\end{array}\right.$$
  \item $X$ admet une espérance, et $E(X)=\frac{1}{\lambda}$.
\end{itemize}
\end{theoreme}

\ifprof
\begin{demonstration}
Notons $f_X$ la densité vue précédemment.
\begin{itemize}
  \item Pour $x<0$, on a
			$$F_X(x)=\int_{-\infty}^x f(t)dt=\int_{-\infty}^x 0dt = 0$$
		  Pour $x>0$, on a
		    $$F_X(x)=\int_{-\infty}^x f(t)dt=\int_{-\infty}^x 0dt+\int_0^x \lambda e^{-\lambda t}dt = \left[ -e^{-\lambda t}\right]_0^x = 1-e^{-\lambda x}$$
  \item Pour le deuxième point, remarquons que
$$\int_{-\infty}^{+\infty} |tf(t)|dt = \int_0^{+\infty} |t|\lambda e^{-\lambda t}dt=\int_0^{+\infty} t \lambda e^{-\lambda t}dt$$
Donc l'intégrale est absolument convergente si et seulement si l'intégrale $\displaystyle{\int_0^{+\infty} t \lambda e^{-\lambda t}dt}$ existe et est finie. \\Soit $a>0$. Par intégration par partie, on obtient
$$\int_0^{a} t \lambda e^{-\lambda t}dt= \int_{0}^{a} e^{-\lambda t}dt = \left[ \frac{-e^{-\lambda t}}{\lambda} \right]_0^{a} = \frac{e^{-\lambda a}}{\lambda}+\frac{1}{\lambda}$$
Or $\displaystyle{\lim_{a\rightarrow +\infty} \frac{e^{-\lambda a}}{\lambda}+\frac{1}{\lambda}=\frac{1}{\lambda}}$. Ainsi, cette intégrale converge, donc $X$ admet une espérance, qui vaut
$$E(X)=\int_{-\infty}^{+\infty} tf(t)dt=\int_0^{+\infty} t \lambda e^{-\lambda t}dt = \frac{1}{\lambda}$$
\end{itemize}
\end{demonstration}
\else
\lignes{30}
\fi

\begin{proposition}
Soit $\lambda > 0$. Alors $$X \hookrightarrow \mathcal{E}(1) \Leftrightarrow Y=\frac{1}{\lambda} X \hookrightarrow \mathcal{E}(\lambda)$$
\end{proposition}

\ifprof
\begin{demonstration}
Pour tout réel $x$, on a
$$F_X(x)=\PP(X\leq x)= \PP(\lambda Y\leq x) = \PP\left(Y\leq \frac{x}{\lambda}\right)=F_Y\left(\frac{x}{\lambda}\right) \textrm{  et donc  } F_Y(x)=F_X(\lambda x)$$
\begin{itemize}
  \item Si $Y\suit \mathcal{E}(\lambda)$, alors
$$\forall~x<0,~F_X(x)=F_Y\left(\frac{x}{\lambda}\right)=0 \textrm{ et } \forall~x\geq 0,~F_X(x)=F_Y\left(\frac{x}{\lambda}\right)=1-e^{-\lambda\frac{x}{\lambda}} = 1-e^{-x}$$
On reconnait la fonction de répartition d'une loi $\mathcal{E}(1)$ : $X\suit \mathcal{E}(1)$.
  \item Si $X\suit \mathcal{E}(1)$, alors
$$\forall~x<0,~F_Y(x)=F_X\left(\lambda x\right)=0 \textrm{ et } \forall~x\geq 0,~F_Y(x)=F_X\left(\lambda x\right)=1-e^{-(\lambda x)} = 1-e^{-\lambda x}$$
On reconnait la fonction de répartition d'une loi $\mathcal{E}(\lambda)$ : $Y\suit \mathcal{E}(\lambda)$.
\end{itemize}
\end{demonstration}
\else
\lignes{24}
\fi

\subsubsection{Loi sans vieillissement}

\begin{definition}
Soit $X$ une variable aléatoire à valeurs positives, et telle que pour tout $x\geq 0$, $\PP(X>x)>0$. On dit que la loi de $X$ est \textbf{sans vieillissement} si
$$\forall~(x,y)\in (\R^+)^2,~\PP_{(X>y)}(X>y+x)=\PP(X>x)$$
\end{definition}

\begin{remarque}
Par définition des probabilités conditionnelles, la loi de $X$ est sans vieillissement si et seulement si, $$\forall~(x,y)\in (\R^+)^2,~\PP(X>y+x)=\PP(X>x)\PP(X>y)$$
\end{remarque}

\begin{exemple}
La loi géométrique $\mathcal{G}(p)$ est la seule loi discrète sans vieillissement.
\end{exemple}

\begin{theoreme}
Soit $X$ une variable à densité, à valeurs positives, et telle que $\forall~x>0, \PP(X>x)>0$. Alors
$X$ est une variable sans mémoire si et seulement si $X$ suit une loi exponentielle.
\end{theoreme}

\ifprof
\begin{demonstration}
\begin{itemize}
  \item Montrons que si $X\hookrightarrow \mathcal{E}(\lambda)$, alors $X$ est sans mémoire. Soient $x,y$ deux réels positifs. Puisque $F_X(x)=1-e^{-\lambda x}$, alors $P(X>x)=1-\PP(X\leq x)=1-F_X(x)=e^{-\lambda x}$. Donc
    $$\PP_{(X>y)}(X>y+x)=\frac{\PP((X>y)\cap (X>y+x))}{\PP(X>y)}$$
    Or $(X>y+x)\cap (X>y) = (X>y+x)$. Donc
    $$\PP_{(X>y)}(X>y+x)=\frac{\PP(X>y+x)}{\PP(X>y)}=\frac{e^{-\lambda (x+y)}}{e^{-\lambda y}} = e^{-\lambda x} = \PP(X>x)$$
  \item Réciproquement, soit $X$ une variable aléatoire à densité, à valeurs positives, et sans mémoire. Notons $F_X$ sa fonction répartition, et notons $G=1-F_X$. $G$ est continue, et puisque $X$ est sans mémoire, par définition, on a donc
    $$\forall~(x, y) \in (\R^+)^2,~G(x+y)=G(x)G(y)$$
    On peut montrer (d'abord sur $\N$, puis sur $\Q$ et enfin par continuité sur $\R$) qu'alors, pour tout $x>0$,
    $$G(x)=e^{x \ln (G(1))}$$
    Donc, pour tout réel $x>0$, $F_X(x)=1-G(x)=1-e^{-\lambda x}$ avec $\lambda = -\ln(G(1))>0$. Donc $X$ suit la loi exponentielle de paramètre $\lambda$.
\end{itemize}
\end{demonstration}
\else
\lignes{20}
\fi

\subsection{Lois normales}

\subsubsection{Théorème de Moivre-Laplace}

On se fixe un réel $p \in ]0;1[$. Pour tout $n\geq 1$, soit $Z_n$ une variable aléatoire suivant une loi binomiale de paramètre $p$ et $n$, que l'on centre et réduit.

On a représenté ci-dessous $Z_{10}$, $Z_{50}$ et $Z_{100}$ pour $p=0,50$, en utilisant le programme Scilab ci-dessous.

\begin{scilab}
\inputscilab{tcl.sci}
\end{scilab}

\begin{center}
\includegraphics[width=17cm]{tcl}
\end{center}

On constate que les suites de variables aléatoires $Z_n$ s'approche d'une loi de probabilité dont la densité semble être une courbe ``en cloche''.

\begin{theoreme}
[Moivre-Laplace] Soit $p \in ]0;1[$. On suppose que, pour tout entier naturel $n$ non nul, la variable aléatoire $X_n$ suit une loi binomiale de paramètres $n$ et $p$.\\
Pour tout $n\geq 1$, on pose $$Z_n=\frac{X_n-np}{\sqrt{np(1-p)}}$$
Alors, pour tous réels $a$ et $b$ tels que $a<b$, on a
$$\lim_{n\rightarrow +\infty} \PP(a\leq Z_n \leq b)=\int_a^b \frac{1}{\sqrt{2\pi}} e^{-\frac{x^2}{2}}dx$$
\end{theoreme}

\begin{remarque}
\begin{itemize}
  \item On dit que la suite de variables aléatoires $(Z_n)$ \textbf{converge en loi} vers la loi dont la densité est donnée par le théorème de Moivre-Laplace.
  \item Le théorème de Moivre-Laplace est une cas particulier d'un théorème essentiel de la théorie de la probabilité~: le théorème central limite.
\end{itemize}
\end{remarque}

\begin{experiencehistorique}
Le théorème de Moivre-Laplace a été d'abord démontré par \textbf{Abraham de Moivre} pour $p=0,5$ en 1733. Le cas général a été démontré par \textbf{Pierre Simon de Laplace} en 1812.
\end{experiencehistorique}

\subsubsection{Loi normale centrée réduite}

\begin{definition}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A},\PP)$. On dit que $X$ suit la loi \textbf{normale centrée réduite}, et on note $X \hookrightarrow \mathcal{N}(0,1)$, si $X$ est une variable aléatoire à densité, de densité
$$f(x)=\frac{1}{\sqrt{2\pi}} e^{-\frac{x^2}{2}}$$
\end{definition}

\begin{remarque}
On admet que $f$ définit bien une densité : on peut en tout cas observer qu'elle est continue, positive.
\end{remarque}

\begin{remarque}
On peut simuler une loi normale centrée réduite avec \textit{grand}.
\end{remarque}

\begin{scilab}
[Simulation d'une loi normale centrée réduite]
\inputscilab{normale.sci}
\end{scilab}

\begin{center}
\includegraphics[width=16cm]{normale}
\end{center}

\subsubsection{Propriétés}

\begin{theoreme}
Soit $X$ une variable aléatoire à densité suivant la loi normale centrée réduite.
\begin{itemize}
  \item Sa fonction de répartition est notée $\Phi$ :
    $$\Phi(x)= \int_{-\infty}^x \frac{1}{\sqrt{2\pi}}e^{-\frac{t^2}{2}}dt$$
    On ne peut pas simplifier cette expression, car on ne connait pas de primitive de $f$.
  \item Pour tout réel $x$, $\Phi(-x)=1-\Phi(x)$, et ~$\Phi(0)=\frac{1}{2}$.
  \item Pour tout réel $x$, $\PP(-x\leq X \leq x)=2\Phi(x)-1$.
  \item $X$ admet une espérance, et $E(X)=0$.
\end{itemize}
\end{theoreme}

\ifprof
\begin{demonstration}
\begin{itemize}
  \item Le premier point est la définition de la fonction de répartition.
  \item Par définition, $$\Phi(-x)=\int_{-\infty}^{-x} f(t)dt$$
	     Posons $u=-t$ (on admet ici qu'on peut le faire sur l'intégrale impropre). Alors
	     $$\Phi(-x) = \int_{+\infty}^{x} f(-u)(-du) = \int_{x}^{+\infty} f(u)du \textrm{  par parité de } f$$
	     Ainsi
	     $$\Phi(x)+\Phi(-x)=\int_{-\infty}^x f(t)dt +\int_x^{+\infty} f(t)dt =\int_{-\infty}^{+\infty} f(t)dt=1$$
  \item On a $$\PP(-x\leq X \leq x) = \Phi(x)-\Phi(-x)$$
		D'après le résultat précédent, $$\PP(-x\leq X \leq x) = \Phi(x)-(1-\Phi(x)=2\Phi(x)-1$$
  \item On admet la convergence absolue. Calculons	 $$E(X)=\int_{-\infty}^{+\infty} tf(t)dt = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} te^{-\frac{t^2}{2}}$$
	 Posons $a>0$ et calculons
	 $$\int_{-a}^a te^{-\frac{t^2}{2}} = \left[ -e^{\frac{-t^2}{2}}\right]_{-a}^a = e^{\frac{(-a)^2}{2}} - e^{\frac{a^2}{2}} = 0$$
	 ainsi, $\displaystyle{\lim_{a\rightarrow +\infty} \int_{-a}^a te^{-\frac{t^2}{2}}=0}$.
	 et donc $E(X) =0$.
\end{itemize}
\end{demonstration}
\else
\lignes{25}
\fi

\begin{remarque}
Vous verrez l'année prochaine que $X$ admet une variance, qui vaut $1$. D'où la notation $\mathcal{N}(0,1)$.
\end{remarque}

\subsubsection{Loi normale $\mathcal{N}(\mu,\sigma^2)$}

\begin{definition}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A},\PP)$, $\mu \in \R$, $\sigma\in \R^+_*$. On dit que $X$ suit la \textbf{loi normale} de paramètre $(\mu, \sigma^2)$, et on note $X \hookrightarrow \mathcal{N}(\mu, \sigma^2)$, si $X$ est une variable aléatoire à densité, de densité
$$f(x)=\frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}}$$
\end{definition}

\begin{remarque}
On admet également que $f$ est bien une densité.
\end{remarque}

\begin{theoreme}
Soit $X$ une variable aléatoire suivant la loi normale de paramètres $(\mu,\sigma^2)$. Alors $X$ admet une espérance, et $E(X)=\mu$.
\end{theoreme}

\begin{remarque}
Vous verrez l'année prochaine que $X$ admet une variance, qui vaut $\sigma^2$. Ainsi, $\sigma$ représente l'écart-type de la loi normale.
\end{remarque}

\begin{proposition}
Soit $\mu \in \R$ et $\sigma > 0$. Alors
$$X\hookrightarrow \mathcal{N}(\mu, \sigma^2) \Leftrightarrow X^* = \frac{X-\mu}{\sigma} \hookrightarrow \mathcal{N}(0,1)$$
\end{proposition}

%%% Fin du cours %%%
