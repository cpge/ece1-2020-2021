// Auteur : Crouzet
// Date : 13/07/2015
// Résumé : fonction simulant une loi normale de paramètre mu=0 et sigma=1 

clf; clc; // On efface et réinitialise la fenêtre graphique

N=10000;

mu=0
sigma=1

titre='Loi normale centrée réduite'
    
x=-5:0.1:5
y=1/sqrt(2*%pi)*exp(-x**2/2) // Densité de la loi exponentielle
t=grand(N,1,"nor",mu,sigma) // nor : loi normale

histplot(x, t, style=5); // On représente par classe d'écart 0.1 pour représentation
plot2d(x,y,rect=[-5,0,5,0.5],style=1) // On représente la densité

xtitle("", "Classes", "Effectifs par classe normalisés")
legend(titre) 
a=get("current_axes"); 
a.font_size=1; 
a.x_location="bottom";
