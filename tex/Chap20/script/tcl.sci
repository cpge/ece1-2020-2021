// Auteur : Crouzet
// Date : 13/07/2015
// Résumé : représentation du Théorème Central Limite 

clf; clc; // On efface et réinitialise la fenêtre graphique

// On va simuler des lois binomiales de paramètres n=10, 50 et 100 avec p=0.5
N=100000; 
n=[10,50,100];
p=0.5

x = [-5 : 0.1 : 5];  // Fenêtre de représentation
y=exp(- (x.^2) / 2)  ./ sqrt( 2 * %pi );  // Densité de la loi normale centrée réduite

// Pour chacun des trois paramètres, on effectue une simulation
for i=1:3
	z = (grand(1,N,"bin", n(i), p)-n(i)*p)/sqrt(n(i)*p*(1-p));   // On simule une loi binomiale, qu'on centre et réduit
    L=([-1:n(i)]-n(i)*p+1/2)/sqrt(n(i)*p*(1-p));                 // On découpe pour améliorer la représentation graphique
    subplot(1,3,i);												 
    plot2d(x, y,style=1)										 // On affiche la densité de la loi binomiale
    histplot(L, z, rect=[-5,0,5,0.45],style=5)                   // On affiche Zn
    leg='n='+string(n(i))+' et p='+string(p)					 // On affiche la légende
    legend(leg)
    if i==2; xtitle("Illustration du Théorème Central Limite"); end
    a=get("current_axes"); a.font_size=1; a.x_location="bottom";
end