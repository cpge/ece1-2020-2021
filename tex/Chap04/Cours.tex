\chapter{Polynômes}

\objectifintro{Dans ce chapitre, on introduit un objet qui servira tant en première qu'en deuxième année, et qui a déjà été vu, en partie, au lycée.}

\begin{extrait}{Victor Hugo (1802 -- 1885). \emph{Proses philosophiques, L’âme}}
Rien n’est solitaire, tout est solidaire. L’homme est solidaire avec la planète, la planète est solidaire avec le soleil, le soleil est solidaire avec l’étoile, l’étoile est solidaire avec la nébuleuse, la nébuleuse, groupe stellaire, est solidaire avec l’infini. Ôtez un terme de cette formule, le polynôme se désorganise, l’équation chancelle, la création n’a plus de sens dans le cosmos et la démocratie n’a plus de sens sur la terre. Donc, solidarité de tout avec tout, et de chacun avec chaque chose. La solidarité des hommes est le corollaire invincible de la solidarité des univers. Le lien démocratique est de même nature que le rayon solaire.
\end{extrait}

\begin{objectifs}
\begin{numerote}
	\item \hyperref[objectif-03-1]{Savoir manipuler la notion de degré (définition, opérations)}\dotfill $\Box$
    \item \hyperref[objectif-03-2]{Savoir manipuler la notion de dérivation de polynôme}\dotfill $\Box$
	\item \hyperref[objectif-03-3]{Connaître le principe de la division euclidienne}\dotfill $\Box$
	\item \hyperref[objectif-03-4]{Connaître la notion de racine}\dotfill $\Box$
	\item \hyperref[objectif-03-5]{Savoir factoriser un polynôme dans $\R$}\dotfill $\Box$
	\item \hyperref[objectif-03-6]{Savoir résoudre des équations se ramenant à un polynôme}\dotfill $\Box$
\end{numerote}
\end{objectifs}


\section{Définitions}

    \subsection{Notion de polynômes}

\begin{notation}
Pour tout $i \in \N^*$, on introduit les fonctions $\begin{array}{rrcl} X^i:&\R &\mapsto& \R\\ &x &\mapsto& x^i\end{array}$ et la fonction  $\begin{array}{rrcl}X^0: &\R &\mapsto& \R\\ &x &\mapsto& 1\end{array}$

\end{notation}

\begin{definitions}
On appelle \textbf{polynôme à coefficients réels} toute somme finie d'applications précédentes, c'est-à-dire toute fonction s'écrivant sous la forme
		\[P(X)=a_nX^n+a_{n-1}X^{n-1}+\cdots +a_1X+a_0=\sum_{k=0}^n a_kX^k\]
avec $n \in \N$ et $\forall~k \in \ll 0,n \rr, a_k\in \R$.
\begin{itemize}[label=\textbullet]
    \item Les nombres $a_k$ sont appelés les \textbf{coefficients} du polynôme $P$.
    \item Si $a_n\neq 0$, on dit que $P$ est de \textbf{degré} $n$ et on note $\deg(P)=n$. Dans ce cas, $a_n$ est appelé \textbf{coefficient dominant} du polynôme $P$.
    \item La \textbf{fonction polynôme associée} au polynôme $P$ est la fonction, encore notée $P$, définie sur $\R$ par \\$P:x\mapsto a_nx^n+\cdots +a_1x+a_0$.
\end{itemize}
\end{definitions}

\begin{exemple}
Le polynôme $P(X)=X^4-2X^2+1$ est un polynôme de degré $4$ et de coefficient dominant $1$. Sa fonction polynôme associée est la fonction $P:x \mapsto x^4-2x^2+1$. Ainsi, $P(1)=1^4-2\times 1^2+1=0$.
\end{exemple}


\begin{remarque}
Attention : la notation $P(X)$ représente une fonction (c'est une représentation \textbf{formelle}). On peut ainsi parler du polynôme $P(X)=X^2-1$ plutôt que de parler de la fonction polynôme $P$ définie pour tout $x\in \R$ par $P(x)=x^2-1$.
\end{remarque}

\begin{notation}
On note $\R[0]$ l'ensemble des polynômes à coefficients réels, et $\R[-n]$ l'ensemble des polynômes à coefficients réels de degré inférieur ou égal à $n$. \\Ainsi, $\R[-1]=\{aX+b, (a,b)\in \R^2\}$ et $\R[-2]=\{aX^2+bX+c, (a,b,c)\in \R^3\}$.
\end{notation}

    \subsection{Degré}

\begin{propriete}
\label{objectif-03-1}
Soient $P$ et $Q$ deux polynômes.
\begin{itemize}[label=\textbullet]
    \item Le degré d'un polynôme constant non nul est $0$.
    \item $\deg(P+Q)\leq \max(\deg(P),\deg(Q))$
    \item $\deg(PQ)=\deg(P)+\deg(Q)$.
\end{itemize}
\end{propriete}

\ifprof
\begin{demonstration}
On note $n$ le degré de $P$ et $m$ le degré de $Q$. Ainsi, $P(X)=a_nX^n+\cdots +a_0$ et $Q(X)=b_mX^m+\cdots+b_0$ avec $a_n\neq 0$ et $b_m\neq 0$.
\begin{itemize}[label=\textbullet]
    \item On a $(P+Q)(X)=a_nX^n+\cdots+a_0+b_mX^m+\cdots+b_0$.  Par construction, le degré de $P+Q$ est égale au plus grand des deux degrés, sauf si $n=m$ et $a_n=-b_m$, et dans ce cas, le degré est strictement inférieur à $n$. Dans tous les cas $\deg(P+Q)\leq \max(\deg(P),\deg(Q))$.
    \item Quand on développe $P(X)Q(X)$, le terme de plus haut degré est le terme $a_nX^nb_mX^m=a_nb_mX^{n+m}$ (avec $a_n\neq 0$ et $b_m\neq 0$). Donc $\deg(PQ)=\deg(P) + \deg(Q)$.
\end{itemize}
\end{demonstration}
\else
\lignes{8}
\fi

\begin{remarque}
Le polynôme nul est le polynôme dont tous les coefficients sont nuls. Par convention, on dit qu'il est degré $-\infty$.
\end{remarque}

    \subsection{Dérivée d'un polynôme}

\begin{definition}
\label{objectif-03-2}
	Soit $P(X)=a_nX^n+\cdots +a_1X+a_0 \in \R[0]$ un polynôme. On appelle \textbf{polynôme dérivé}, et on note $P'$, le polynôme définie par
\[P'(X)=na_nX^{n-1}+(n-1)a_{n-1}X^{n-2}+\cdots+2a_2X+a_1\]
\end{definition}

\begin{remarque}
On peut dérivée à nouveau $P'$ et on note $P''$ ou $P^{(2)}$ la dérivée seconde de $P$. Plus généralement, on note $P^{(k)}$ la dérivée $k$-ième de $P$.
\end{remarque}

\begin{exemple}
Soit $P(X)=3X^3+2X^2-1$. Alors $P'(X)=9X^2+4X$, $P^{(2)}(X)=18X+4$, $P^{(3)}(X)=18$ et $P^{(4)}(X)=0$.
\end{exemple}

\begin{remarque}
	\begin{itemize}[label=\textbullet]
    \item Si $\deg(P)=n\geq 1$, alors $\deg(P')=n-1$.
    \item Si $\deg(P)=n$, alors $\forall~k>n,\quad P^{(k)}=0$.
\end{itemize}
\end{remarque}

\afaire{Exercices \lienexo{1}, \lienexo{2} et \lienexo{3}.}

\section{Egalité de polynômes et identification}

       \subsection{Egalité de polynômes}

\begin{theoreme}
  Soient $\ds{P(X)=\sum_{k=0}^n a_kX^k}$ et $\ds{Q(X)=\sum_{k=0}^m b_kX^k}$ deux polynômes. Alors $P=Q$ si et seulement si
	\[n=m \qeq \forall~k \in \ll 0;n \rr, \quad a_k=b_k\]
Ainsi, deux polynômes sont égaux si et seulement s'ils ont même degré, et mêmes coefficients.
\end{theoreme}

\begin{exemple}
Soient $P(X)=2X^2+3X-1$ et $Q(X)=2X^2+aX-b$. Alors $P=Q$ si et seulement si $a=3$ et $b=1$.
\end{exemple}

        \subsection{Application à l'identification}

\begin{exemple}
Soit $P(X)=X^3-X^2-3X+2$. Déterminer trois réels $a,b,c$ tels que
\[\forall~x\in\R, \quad P(x)=(x-2)(ax^2+bx+c)\]
\end{exemple}

\solution[10]{
Soit $Q(X)=(X-2)(aX^2+bX+c)$. En développant,
\[Q(X)=aX^3+bX^2+cX-2aX^2-2bX-2c=aX^3+(b-2a)X^2+(c-2b)X-2c\]
Pour que $P=Q$, par unicité de l'écriture d'un polynôme, on identifie les coefficients :
\[\left\{\begin{array}{rcl}
 a &=& 1 \\
 b-2a &=& -1 \\
 c-2b &=& -3 \\
 -2c &=& 2
\end{array}\right. \Longleftrightarrow
\left\{\begin{array}{rcl}
 a &=& 1 \\
 b &=& 1 \\
 c &=& -1
\end{array}\right.\]
Ainsi, $P(X)=(X-2)(X^2+X-1)$.
}

\exercice{Trouver deux réels $a$ et $b$ tels que, \[\forall~x\in\R\backslash\{-1;1\}, \quad \frac{1}{x^2-1}=\frac{a}{x-1}+\frac{b}{x+1}\]}

\solution[10]{En mettant au même dénominateur, on cherche $a$ et $b$ tels que, pour tout $x \in \R \backslash \{-1;1\}$, on a
\[\frac{1}{x^2-1} = \frac{a(x+1)+b(x-1)}{x^2-1}=\frac{(a+b)x+a-b}{x^2-1}\]
Par identification des coefficients, on a donc
\[\left \{ \begin{array}{ccc} a+b &=& 0 \\ a-b&=&1 \end{array}\right. \Longleftrightarrow \left \{ \begin{array}{ccc} a&=&\frac{1}{2}\\b&=&-\frac{1}{2} \end{array}\right.\]
Ains, pour tout réel $x$ différent de $1$ et $-1$, on a
\[\frac{1}{x^2-1}=\frac{1}{2(x-1)}-\frac{1}{2(x+1)}\]
}

\afaire{Exercice \lienexo{4}.}

\section{Racines d'un polynôme}

    \subsection{Définition}

\begin{definition}
\label{objectif-03-4}
Soit $P\in \R[0]$ et $a$ un réel. On dit que $a$ est une \textbf{racine} de $P$ si $P(a)=0$.
\end{definition}

\begin{remarque}Chercher les racines de $P$, c'est donc résoudre l'équation $P(X)=0$.
\end{remarque}

\begin{exemple}
$1$ est une racine du polynôme $P(X)=X^2-1$. En effet, $P(1)=0$.
\end{exemple}

\begin{definition}
	Soit $P \in \R[0]$ et $a$ un réel. On dit que le polynôme $P$ se \textbf{factorise} par $X-a$ (ou que $X-a$ \textbf{divise} $P$) s'il existe un polynôme $Q\in \R[0]$ tel que $P(X)=(X-a)Q(X)$.
\end{definition}

\begin{exemple}
	Le polynôme $P(X)=X^3-X^2+X-1$ se factorise par $X-1$. En effet, on a $P(X)=(X-1)(X^2+1)$.
\end{exemple}

\begin{theoreme}
  Soit $P \in \R[0]$ et $a$ un réel. Alors
\begin{center}
$a$ est une racine de $P$ si et seulement s'il existe un polynôme $Q(X)$ tel que $P(X)=(X-a)Q(X)$.
\end{center}
Dans ce cas, on a alors $\deg(P)=\deg(X-a) +\deg(Q)$ c'est-à-dire $\deg(Q)=\deg (P)-1$.
\end{theoreme}

\begin{exemple}
	Soit $P$ le polynôme défini par $P(X)=X^3 - 2X +1$. Montrer que $1$ est racine, puis factoriser $P$ par $(X-1)$.
\end{exemple}

\solution[7]{On constate que $P(1)=0$. Ainsi, $1$ est racine. Par théorème, on peut donc factoriser $P$ par $X-1$ : il existe $Q$ un polynôme tel que $P(X)=(X-1)Q(X)$ avec
$\deg(Q)=\deg(P)-1=2$. On cherche donc $a, b$ et $c$ tel que
\[X^3-2X+1=(X-1)(aX^2+bX+c)\]
Après développement, on obtient
\[X^3-2X+1=aX^3+(b-a)X^2+(c-b)X-c\]
Par identification, on obtient $a=1,~b=1$ et $c=-1$. Ainsi
\[X^3-2X+1=(X-1)(X^2+X-1)\]
}

    \subsection{Nombre de racines d'un polynôme}

\begin{theoreme}
  Soit $P\in \R[0]$ un polynôme de degré $n\geq 1$. Alors $P$ a \textbf{au plus} $n$ racines.
\end{theoreme}

\begin{demonstration}
Théorème admis.
\end{demonstration}

\begin{remarque}Ainsi, un polynôme de degré $3$ a au plus $3$ racines, c'est-à-dire qu'il peut en avoir aucune, une, deux ou trois.
\end{remarque}

\begin{consequence}
Soit $P\in \R_n[X]$. Si $P$ a $n+1$ racines, alors $P$ est le polynôme nul.
\end{consequence}

\ifprof
\begin{demonstration}
	En effet, $P$ est au plus de degré $n$. Il ne peut pas admettre plus de $n$ racines, sauf s'il est nul.
\end{demonstration}
\else
\lignes{2}
\fi

    \subsection{Cas des polynômes de degré $2$.}

\begin{theoreme}
  Soit $P(X)=aX^2+bX+c$ un polynôme de degré $2$ ($a\neq 0$). Soit $\Delta=b^2-4ac$ son discriminant.
\begin{itemize}[label=\textbullet]
    \item Si $\Delta>0$, le polynôme $P$ possède deux racines réelles distinctes
    \[x_1=\frac{-b-\sqrt{\Delta}}{2a} \qeq x_2=\frac{-b+\sqrt{\Delta}}{2a}\]
    On a alors $P(X)=a(X-x_1)(X-x_2)$.
    \item Si $\Delta=0$, le polynôme $P$ possède une unique racine réelle dite double
    \[x_0=-\frac{b}{2a}\]
    On a alors $P(X)=a(X-x_0)^2$.
    \item Si $\Delta<0$, le polynôme $P$ ne possède pas de racines réelles, et ne se factorise donc pas dans $\R[0]$.
\end{itemize}
\end{theoreme}

\begin{exemple}
Factoriser $P(X)=2X^2+2X-4$.
\end{exemple}

\solution[5]{On a $\Delta=36>0$ donc le polynôme $P$ possède deux racines réelles
\[x_1=\frac{-2-\sqrt{36}}{2\times 2}=-2 \qeq x_2=1\]
Donc $P(X)=2(X-1)(X+2)$.}

\begin{remarque}
Attention : on n'oubliera pas, lors de la factorisation, de mettre en facteur le coefficient de plus haut degré (dans l'exemple précédent, le $2$).
\end{remarque}

    \subsection{Cas général}

\begin{methode}
\label{objectif-03-5}
  Lorsqu'on doit factoriser un polynôme $P$ (ou déterminer ses racines) :
\begin{itemize}[label=\textbullet]
    \item On cherche des racines évidentes ($-2,-1,0,1,2$) pour se ramener à un (ou  des) polynôme(s) de degré $2$.
    \item On factorise le polynôme $P$ par $X-a$ où $a$ désigne une racine évidente.
    \item Une fois ramené à un (ou des) polynômes de degré $2$, on utilise la méthode classique du discriminant.
\end{itemize}
\end{methode}

\begin{exemple}
	Soit $P(X)=X^3-2X^2-X+2$. Déterminer les racines de $P$.
\end{exemple}

\solution[20]{
\begin{itemize}[label=\textbullet]
    \item Premier étape : on cherche une ``racine évidente'' : $-2, -1, 0, 1, 2$. Ici, on constate que $P(1)=1-2-1+2=0$ donc $1$ est racine évidente.
    \item Deuxième étape : on factorise $P$ par $X-1$ : on écrit $P(X)=(X-1)Q(X)$ avec $\deg(Q)=\deg(P)-1=2$. Donc $Q$ peut s'écrire $aX^2+bX+c$.\\
    On a alors
    \[P(X)=(X-1)(aX^2+bX+c)=aX^3+(b-a)X^2+(c-b)X-c\]
    Par unicité de l'écriture d'un polynôme, on identifie les coefficients :
    \[\left\{\begin{array}{rcl}
 a &=& 1 \\
 b-a &=& -2 \\
 c-b &=& -1 \\
 -c &=& 2
\end{array}\right. \Longleftrightarrow
\left\{\begin{array}{rcl}
 a &=& 1 \\
 b &=& -1 \\
 c &=& -2
\end{array}\right.\]
Donc $P(X)=(X-1)(X^2-X-2)$.
\item Troisième étape : on détermine les racines du polynôme $Q$. Ici, $Q$ est du second degré, dont le discriminant vaut $\Delta=9$. Donc $Q$ possède deux racines :
		\[x_1=\frac{1-\sqrt{9}}{2}=-1 \qeq x_2=2\]
Donc $Q(X)=(X-2)(X+1)$
    \item Quatrième étape : on conclut. On a donc
    \[P(X)=(X-1)(X-2)(X+1)\]
    et $P$ possède trois racines réelles : $1, 2$ et $-1$.
\end{itemize}
}


    \subsection{Division euclidienne}

\begin{remarque}
\label{objectif-03-3}
	Pour factoriser, plutôt que de procéder par identification, on peut également utiliser la division euclidienne.\\En reprenant l'exemple précédent :

\ifprof
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrr|rrr}
 X^3&-2X^2 & -X  &+2&X  &-1 &  \\
\cline{5-7}
-X^3&+X^2 &   &  &X^2&-X&-2\\
\cline{1-2}
    &-1X^2 & -X  &  &   &   &  \\
    &X^2&-X&  &   &   &  \\
    \cline{2-3}
    &     &-2X & +2 &   &   &  \\
    &     &2X&-2&   &   &  \\
              \cline{3-4}
    &     &   &0&   &   &  \\
  \end{array}
\end{equation*}
\else
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrr|rrr}
 X^3&-2X^2 & -X  &+2&X  &-1 &  \\
\cline{5-7}
%-X^3&+X^2 &   &  &X^2&-X&-2
&&&&&&~\\
%\cline{1-2}
 %   &-1X^2 & -X  &  &   &   &
&&&&&& \\
 %   &X^2&-X&  &   &   &
&&&&&& \\
% +   \cline{2-3}
%  &     &-2X & +2 &   &   &
&&&&&&\\
%    &     &2X&-2&   &   &
&&&&&&\\
%              \cline{3-4}
%    &     &   &0&   &   &
&&&&&&\\
  \end{array}
\end{equation*}
\fi
\end{remarque}

\afaire{Exercices \lienexo{5} et \lienexo{7}.}

	\subsection{\'Equations se ramenant à un polynôme}
	
Il est possible de résoudre des équations particulières en se ramenant à une équation polynomiale.

\begin{exemple}
Résoudre l'équation $\ln(x)^2-3\ln(x)+2=0$.	
\end{exemple}

\begin{methode}
\label{objectif-03-6}
Pour résoudre une équation liée à un polynôme, on effectue un changement de variable : on posera, ici,  $u=\ln(x)$ et on se ramènera à un polynôme.
\end{methode}

\solution[10]{L'équation n'a de sens que sur $\interoo{0 +\infty}$. On pose alors $u=\ln(x)$. L'équation s'écrit alors \[ u^2-3u+2=0 \]
Le discriminant de ce polynôme vaut $\Delta=(-3)^2-4\times 2 = 1$. Les racines sont alors \[ u_1=1 \qeq u_2=2 \]
On revient alors à la variable de départ :
\begin{align*}
 u_1=1 &\Leftrightarrow \ln(x_1)=1 \\
 &\Leftrightarrow x_1=\eu{1}=\E \\
 \text{et }u_2=2 &\Leftrightarrow \ln(x_2)=2 \\
 &\Leftrightarrow x_2=\eu{2}
\end{align*}
Les solutions $x_1$ et $x_2$ sont bien dans $\interoo{0 +\infty}$. On peut alors conclure que l'ensemble des solutions de l'équation $\ln(x)^2-3\ln(x)+2=0$ est $\mathcal{S} = \left \{ \E, \eu{2} \right \}$.
}

\begin{exo}
Résoudre l'équation $\eu{2x}-2\eu{x}-3=0$.	
\end{exo}

\solution[10]{L'équation est valable sur $\R$. On pose $u=\eu{x}$. En constatant que $\eu{2x}=\left(\eu{x}\right)^2$, l'équation devient alors $u^2-2u-3=0$.

Les racines sont $u_1=-1$ et $u_2=3$. On revient alors à la variable de départ. $u_1=-1$ devient $\eu{x_1}=-1$ ce qui est impossible. La deuxième devient $\eu{x_2}=3$ soit $x_2=\ln(3)$.

Ainsi, l'équation $\eu{2x}-2\eu{x}-3=0$ admet une unique solution : $\mathcal{S}=\left \{ \ln(3)\right \}$.
}

\afaire{Exercices \lienexo{6} et \lienexo{8}.}
