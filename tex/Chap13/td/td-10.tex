\begin{exoApp}{30}{1}[Sujet de concours - I]\label{\formatexo{10}}
On considère l'application $\varphi$ définie sur $\R^+$ par 
\[\varphi(x)=\left \{ \begin{array}{cc} 1-x^2 \ln(x) & \textrm{ si } x>0 \\ 1 & \textrm{ si } x=0 \end{array} \right.\]
\begin{enumerate}
    \item Déterminer la limite de $\varphi(x)$ en $+\infty$ ainsi que celle de $\frac{\varphi(x)}{x}$ en $+\infty$. Interpréter.
    \item Montrer que $\varphi$ est continue sur $\R^+$.
    \item Justifier la dérivabilité de $\varphi$ sur $\R^{*}_+$ et calculer sa fonction dérivée.
    \item Montrer que $\varphi$ est dérivable en $0$. Donner l'allure de la représentation graphique de $\varphi$ au voisinage du point d'abscisse $0$.
    \item Dresser le tableau de variation de $\varphi$.
    \item On rappelle que $\ln(2)\approx 0,7$. Montrer l'existence d'un unique réel $\alpha$ tel que $\varphi(\alpha)=0$, et justifier que : $\sqrt{2}<\alpha<2$.
\end{enumerate}
\end{exoApp}

\begin{correction}
~\begin{enumerate}
	\item Par produit, $\displaystyle{\lim_{x\rightarrow +\infty} x^2\ln(x)=+\infty}$ donc par somme, \[\lim_{x\rightarrow +\infty} \varphi(x)=-\infty\]
	On obtient alors, pour tout $x>0$, $\displaystyle{\frac{\varphi(x)}{x}=\frac{1}{x}-x\ln(x)}$, et par produit et somme
	\[\lim_{x\rightarrow +\infty} \varphi(x)=-\infty\]
	Ainsi, la courbe de $\varphi$ admet une branche parabolique de direction l'axe des ordonnées en $+\infty$.
	\item $\varphi$ est continue sur $\R^{*}_+$ comme somme et produit de fonctions continues ($\ln$ et fonction carrée). De plus, par croissance comparée, $\displaystyle{\lim_{x\rightarrow 0} x\ln(x)=0}$ donc 
		\[\lim_{x\rightarrow 0^+} \varphi(x)=1=\varphi(0)\]
		La fonction $\varphi$ est donc continue en $0$.\\\textbf{Bilan} : la fonction $\varphi$ est continue sur $\R^+$.
	\item $\varphi$ est dérivable sur $\R^*_+$ comme somme et produit de fonctions dérivables ($\ln$ et fonction carrée). Pour tout réel $x>0$, on a
		\[\varphi'(x)=-2x\ln(x)-x^2\times \frac{1}{x} = -x(2\ln(x)+1)\]
	\item Déterminons le taux d'accroissement en $0$ :
		\[\frac{\varphi(x)-\varphi(0)}{x-0}= \frac{1-x^2\ln(x)-1}{x}=-x\ln(x)\]
		Par croissance comparée,
		\[\lim_{x\rightarrow 0} \frac{\varphi(x)-\varphi(0)}{x-0}=0\]
		\textbf{Bilan} : la fonction $\varphi$ est dérivable en $0$, et $\varphi'(0)=0$. La courbe de la fonction $\varphi$ admet donc une (demi) tangente horizontale au point d'abscisse $0$.
	\item On a démontré que pour tout $x>0$, $\varphi'(x)=-x(2\ln(x)+1)$. Pour tout $x>0$, $-x<0$ et 
		\[2\ln(x)+1>0 \Leftrightarrow \ln(x)>-\frac{1}{2} \Leftrightarrow x>\eu{-\frac{1}{2}} \textrm{ car la fonction $\exp$ est strictement croissante sur $\R$.}\]
		On obtient alors le tableau de signes et de variations suivant :

\begin{center}
\begin{tikzpicture}[yscale=0.7]
	\tkzTabSetup[doubledistance = 1pt]
        \tkzTabInit{$x$ / 1 ,  $-x$ / 1, $2\ln(x)+1$ / 1, $\phi'(x)$ / 1, $\phi(x)$ / 2}{$0$, $\eu{-\frac{1}{2}}$, $+\infty$}
        \tkzTabLine{,-,,-,}
        \tkzTabLine{d,-,z,+,}
        \tkzTabLine{d,+,z,-,}
        \tkzTabVar{D-/$1$, +/$\phi\left(\eu{-\frac{1}{2}}\right)$, - / $-\infty$}
\end{tikzpicture}
\end{center}

avec \[\varphi\left(\eu{-\frac{1}{2}}\right)=1-\left(\eu{-\frac{1}{2}}\right)^2\ln\left(\eu{-\frac{1}{2}}\right)=1+\frac{1}{2\E}\]
	\item Sur $\left[0;\eu{-\frac{1}{2}}\right]$, $\varphi$ admet un minimum local qui vaut $1>0$. L'équation $\varphi(x)=0$ n'admet donc pas de solution sur $\left[0;\eu{-\frac{1}{2}}\right]$.\\
		Sur $\left[\eu{-\frac{1}{2}};+\infty\right[$, $\varphi$ est continue, strictement décroissante,et $\varphi\left(\left[\eu{-\frac{1}{2}};+\infty\right[\right)=\left]-\infty; 1+\frac{1}{2\E}\right]$ et $0 \in  \left]-\infty; 1+\frac{1}{2\E}\right]$ car $1+\frac{1}{2\E}>0$. D'après le corollaire du théorème des valeurs intermédiaires, l'équation $\varphi(x)=0$ admet une unique solution sur $\left[\eu{-\frac{1}{2}};+\infty\right[$.\\
		\textbf{Bilan} : l'équation $\varphi(x)=0$ admet une unique solution sur $\R$. De plus,
		\[\varphi\left(\sqrt{2}\right)=1-(\sqrt{2})^2\ln(\sqrt{2})=1-2\times\frac{1}{2}\ln(2)=1-\ln(2) \approx 0,3\]
		et 
		\[\varphi(2)=1-2^2\ln(2)=1-4\ln(2)\approx -1,8\]
		On a donc $\varphi(\sqrt{2})>\varphi(\alpha)>\varphi(2)$. Par stricte décroissance de $\varphi$, on en déduit que $\sqrt{2}<\alpha<2$.
\end{enumerate}

\end{correction}
