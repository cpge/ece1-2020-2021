\begin{exoApp}{20}{1}[\'Etude de fonctions - III]\label{\formatexo{9}}
Soit $f$ la fonction définie par 
\[f(x)=\left \{ 
\begin{array}{cc} x\eu{-\frac{3}{|x|}} & \textrm{si } x\neq 0 \\
 0 & \textrm{si } x=0
 \end{array}\right.\]

 \begin{enumerate}
     \item Montrer que $f$ est une fonction impaire sur $\R$.
     \item Montrer que $f$ est continue sur $\R$.
     \item Montrer que $f$ est de classe $\CC^1$ sur $\R^*$, puis qu'elle est de classe $\CC^1$ sur $\R$.
     \item Dresser le tableau de variations de $f$.
     \item Déterminer les branches asymptotiques.
 \end{enumerate}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item $\R$ est symétrique par rapport à $0$, et pour tout $x\neq 0$ :
 	\[f(-x)=(-x)\eu{-\frac{3}{|-x|}} = -x\eu{-\frac{3}{|x|}}=-f(x)\]
 	en utilisant la parité de la fonction valeur absolue. 
 	\\\textbf{Bilan}: la fonction $f$ est impaire.
 \item $f$ est continue sur $\R^*$ comme composée de fonctions continues (exponentielle, inverse et valeur absolue). Constatons enfin que $\displaystyle{\lim_{x\rightarrow 0} -\frac{3}{|x|} =-\infty}$, donc par composée, $\displaystyle{\lim_{x\rightarrow 0} \eu{-\frac{3}{|x|}} = 0}$. Par produit,
 	\[\lim_{x\rightarrow 0} f(x) = 0 = f(0)\]
 	La fonction $f$ est donc également continue en $0$.\\\textbf{Bilan}: $f$ est continue sur $\R$.
 \item \danger~-- Attention à la valeur absolue ! On décomposera toujours $\R>$ et $\R<$.\\
  Pour tout $x>0$, $\displaystyle{f(x)=x\eu{-\frac{3}{x}}}$ et pour tout $x<0$, $\displaystyle{f(x)=x\eu{-\frac{3}{-x}}=x\eu{\frac{3}{x}}}$.
  \begin{itemize}
  	\item[$\bullet$]  $f$ est dérivable sur $\R^*_+$ comme composée de fonctions dérivables, et pour tout $x>0$, \[f'(x)=\eu{-\frac{3}{x}}+x\left(\frac{3}{x^2}\right) \eu{-\frac{3}{x}}=\eu{-\frac{3}{x}}\left(1+\frac{3}{x}\right)\]
  	 \item[$\bullet$] $f$ est dérivable sur $\R^*_-$ comme composée de fonctions dérivables, et pour tout $x>0$, \[f'(x)=\eu{\frac{3}{x}}+x\left(-\frac{3}{x^2}\right) \eu{\frac{3}{x}}=\eu{\frac{3}{x}}\left(1-\frac{3}{x}\right)\]
  \end{itemize}
	Déterminons la limite de $f'$ quand $x$ tend vers $0$ :
	\begin{itemize}
		\item[$\bullet$] Pour $x>0$, posons $X=-\frac{3}{x}$. Alors,
		\[\lim_{x\rightarrow 0^+} X = -\infty \qeq \lim_{X\rightarrow -\infty} \eu{X}=0 \textrm{ ainsi que } \lim_{X\rightarrow -\infty} X\E^x=0 \textrm{ (croissances comparées)}\]
		Par composée, \[\lim_{x\rightarrow 0^+} \eu{-\frac{3}{x}}=0 \qeq \lim_{x\rightarrow 0^+} -\frac{3}{x}\eu{-\frac{3}{x}}=0\]
		Par somme, $\displaystyle{\lim_{x\rightarrow 0^+} f'(x)=0}$.
		\item[$\bullet$] De même, pour $x<0$, posons $X=\frac{3}{x}$. Alors,
		\[\lim_{x\rightarrow 0^-} X = -\infty \qeq \lim_{X\rightarrow -\infty} \eu{X}=0 \textrm{ ainsi que } \lim_{X\rightarrow -\infty} X\E^x=0 \textrm{ (croissances comparées)}\]
		Par composée, \[\lim_{x\rightarrow 0^-} \eu{\frac{3}{x}}=0 \qeq \lim_{x\rightarrow 0^-} \frac{3}{x}\eu{\frac{3}{x}}=0\]
		Par somme, $\displaystyle{\lim_{x\rightarrow 0^-} f'(x)=0}$.
	\end{itemize}
	Ainsi, les limites à droite et à gauche étant égales, on en déduit que $\displaystyle{\lim_{x\rightarrow 0} f'(x)=0}$.\\
	Donc, $f$ est continue sur $\R$, dérivable sur $\R^*$ et sa dérivée admet une limite finie  en $0$. D'après le théorème de prolongement $\CC^1$, $f$ est de classe $\CC^1$ sur $\R$ et $f'(0)=0$.
	\item On a vu précédemment que, pour tout $x>0$, $\displaystyle{f'(x)=\left(1+\frac{3}{x}\right)\eu{-\frac{x}{3}}}$ qui est strictement positif sur $\R^*_+$. De même, pour tout $x<0$,  $\displaystyle{f'(x)=\left(1-\frac{3}{x}\right)\eu{\frac{x}{3}}}$ qui est également strictement positif sur $\R^*_-$. Ainsi, pour tout $x\neq 0$, $f'(x)>0$ et $f'(0)=0$. $f$ est donc strictement croissante sur $\R$. De plus, en posant $X=-\frac{3}{|x|}$
	\[\lim_{x\rightarrow +\infty} X = \lim_{x\rightarrow -\infty} X = 0 \textrm{  et  } \lim_{X\rightarrow 0}\E^x=1\]
	donc par composée, $\displaystyle{\lim_{x\rightarrow +\infty} \eu{-\frac{3}{|x|}}=\lim_{x\rightarrow -\infty} \eu{-\frac{3}{|x|}}=1}$. Par produit, on en déduit donc que 
	\[\lim_{x\rightarrow -\infty} f(x)=-\infty \textrm{  et  } \lim_{x\rightarrow +\infty} f(x)=+\infty\]
		On obtient ainsi le tableau de variations suivant :
\begin{center}
\begin{tikzpicture}[yscale=0.7]
        \tkzTabInit{$x$ / 1 ,  $f'(x)$ / 1, $f(x)$ / 2}{$-\infty$, $+\infty$}
        \tkzTabLine{,+,}
        \tkzTabVar{ -/$-\infty$, + / $+\infty$}
\end{tikzpicture}
\end{center}
	\item Puisque les limites en l'infini sont infinies, il faut étudier le comportement asymptotique. Or, pour tout $x\neq 0$,
	$\displaystyle{\frac{f(x)}{x}= \eu{-\frac{3}{|x|}}}$. D'après les limites étudiées précédemment, on obtient donc
	\[\lim_{x\rightarrow +\infty} \frac{f(x)}{x}=\lim_{x\rightarrow -\infty} \frac{f(x)}{x}=1\]
	On s'intéresse donc à $\displaystyle{f(x)-x=x\left(\eu{-\frac{3}{|x|}}-1\right)}$.
	\begin{itemize}
		\item[$\bullet$] Pour $x>0$, on peut écrire
		\[f(x)-x=x(\eu{-\frac{3}{x}} - 1) = \frac{\eu{-\frac{3}{x}}-1}{-\frac{3}{x}} \times (-3)\]
		En posant $X=-\frac{3}{x}$, on constate que $\displaystyle{\lim_{x\rightarrow +\infty} X = 0}$ et $\displaystyle{\lim_{X\rightarrow 0} \frac{\E^x-1}{X}=1}$ (limite du cours). Par composée,
		\[\lim_{x\rightarrow +\infty}  \frac{\eu{-\frac{3}{x}}-1}{-\frac{3}{x}} = 1\] et par produit,
		\[\lim_{x\rightarrow +\infty} f(x)-x=-3\]
		Ainsi, la droite d'équation $y=x-3$ est asymptote oblique à la courbe de $f$ au voisinage de $+\infty$.
		\item[$\bullet$] De la même manière, pour $x<0$, on peut écrire
		\[f(x)-x=x\left(\eu{\frac{3}{x}}-1\right) = \frac{\eu{\frac{3}{x}}-1}{\frac{3}{x}} \times 3\]
		et par un même raisonnement
		\[\lim_{x\rightarrow -\infty} f(x)-x=3\]
		Ainsi, la droite d'équation $y=x+3$ est asymptote oblique à la courbe  de $f$ au voisinage de $-\infty$.
	\end{itemize}
    On obtient ainsi la courbe représentative suivante : \begin{center}
    	\includegraphics{tex/Chap13/mp/exo9.mps}
    \end{center}

\end{enumerate}

\end{correction}
