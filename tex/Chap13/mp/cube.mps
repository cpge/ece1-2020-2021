%!PS
%%BoundingBox: -79 -96 79 96 
%%HiResBoundingBox: -78.95319 -95.32346 78.96184 95.3559 
%%Creator: MetaPost 2.00
%%CreationDate: 2021.01.21:0957
%%Pages: 1
%*Font: cmr10 6.97382 9.96265 2d:98000000010008a041
%%BeginProlog
%%EndProlog
%%Page: 1 1
 1 0 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -77.95319 -94.32346 moveto
-75.82188 -86.4364 -73.4585 -78.61389 -70.86613 -70.86613 curveto
-68.70021 -64.39287 -66.37477 -57.97292 -63.77908 -51.65953 curveto
-61.63184 -46.43687 -59.30083 -41.29022 -56.69205 -36.2819 curveto
-54.54724 -32.1643 -52.21648 -28.14404 -49.605 -24.30508 curveto
-47.45436 -21.14355 -45.11667 -18.11044 -42.51794 -15.30516 curveto
-40.34317 -12.95753 -37.99179 -10.77678 -35.43091 -8.8572 curveto
-33.21144 -7.19353 -30.84476 -5.73407 -28.34386 -4.53403 curveto
-26.06819 -3.44206 -23.69356 -2.57088 -21.2568 -1.91287 curveto
-18.93225 -1.28517 -16.56023 -0.85384 -14.16977 -0.56662 curveto
-11.81729 -0.28397 -9.45093 -0.14128 -7.08272 -0.07028 curveto
-4.72115 0.00052 -2.35828 0.00006 0.00432 0 curveto
2.36691 -0.00006 4.7298 0.0003 7.09137 0.07137 curveto
9.45958 0.14262 11.82594 0.285 14.17842 0.5677 curveto
16.56895 0.85498 18.941 1.28676 21.26546 1.91504 curveto
23.70229 2.57368 26.07689 3.44571 28.35251 4.53835 curveto
30.85344 5.73917 33.22018 7.19928 35.43956 8.86368 curveto
38.00058 10.7843 40.35185 12.96625 42.5266 15.3149 curveto
45.12527 18.12138 47.46303 21.1555 49.61365 24.31807 curveto
52.22522 28.15846 54.5559 32.1802 56.7007 36.29921 curveto
59.30937 41.30905 61.64043 46.45712 63.78773 51.68115 curveto
66.3834 57.996 68.7089 64.41739 70.87479 70.89209 curveto
73.46727 78.642 75.83064 86.46666 77.96184 94.3559 curveto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath -70.86613 -2.1259 moveto
-70.86613 2.1259 lineto stroke
-73.77191 -11.03775 moveto
(-1) cmr10 6.97382 fshow
newpath 70.86613 -2.1259 moveto
70.86613 2.1259 lineto stroke
69.12267 -11.03775 moveto
(1) cmr10 6.97382 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.1259 -70.86613 moveto
2.1259 -70.86613 lineto stroke
2.09999 -77.46036 moveto
(-1) cmr10 6.97382 fshow
newpath -2.1259 70.86613 moveto
2.1259 70.86613 lineto stroke
-6.48692 68.61902 moveto
(1) cmr10 6.97382 fshow
-5.58691 -10.13774 moveto
(0) cmr10 6.97382 fshow
newpath -77.95319 0 moveto
77.95319 0 lineto stroke
newpath 74.25859 -1.5304 moveto
77.95319 0 lineto
74.25859 1.5304 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -94.32346 moveto
0 94.32346 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53119 90.62692 moveto
0 94.32346 lineto
-1.53119 90.62692 lineto
 closepath
gsave fill grestore stroke
 0 0 1 setrgbcolor 0 1.3 dtransform truncate idtransform setlinewidth pop
newpath -49.60608 0 moveto
49.60608 0 lineto stroke
2.09999 -6.86543 moveto
(Tangente) cmr10 6.97382 fshow
showpage
%%EOF
