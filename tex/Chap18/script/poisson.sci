// Résumé : fonction simulant une loi de Poisson de paramètre lambda=5

clf; clc; // On efface et réinitialise la fenêtre graphique

// On génère 10000 simulations suivant la loi de Poisson de paramètre lambda=5
lambda=5;
N=10000;

U =grand(1,N,"poi",lambda);  // poi : Poisson
X = tabul(U,"i");

// On représente graphiquement la simulation
bar(X(:,1), X(:,2)/N,width=0.2) // histogramme
titre='Loi de Poisson de paramètre lambda='+string(lambda)
xtitle(titre) // on change le titre de l'interface graphique
a=get("current_axes") ; // on change les axes pour les rendre plus lisibles
a.font_size=1;
a.x_location="bottom"; 
a.y_location="left";
