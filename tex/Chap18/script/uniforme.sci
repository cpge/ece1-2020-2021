// Résumé : fonction simulant une loi uniforme sur [1;8]

clf; clc; // On efface et réinitialise la fenêtre graphique

// On génère 10000 simulations suivant la loi uniforme sur [1;8] 
n1=1; n2=8;
N=10000;

U =grand(1,N,"uin",n1,n2);  // uin : uniforme
X = tabul(U,"i");  // tabul renvoie la liste U triée en ordre croissant/effectif 
				   //  X(:,1) renvoie les éléments en ordre croissant
				   //  X(:,2) renvoie les effectifs

// On représente graphiquement la simulation
bar(X(:,1), X(:,2)/N,width=0.2) // histogramme
titre='Loi uniforme discrète sur l''intervalle d''entiers ['+string(n1)+','+string(n2)+'].. - N='+string(N)
xtitle(titre) // on change le titre de l'interface graphique
a=get("current_axes") ; // on change les axes pour les rendre plus lisibles
a.font_size=1;
a.x_location="bottom"; 
a.y_location="left";
