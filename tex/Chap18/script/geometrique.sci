// Résumé : fonction simulant une loi géométrique de paramètre p=0.3

clf; clc; // On efface et réinitialise la fenêtre graphique

// On génère 10000 simulations suivant la loi géométrique de paramètre p=0.3
p=0.3;
N=10000;

U =grand(1,N,"geom",p);  // geom : géométrique
X = tabul(U,"i");

// On représente graphiquement la simulation
bar(X(:,1), X(:,2)/N,width=0.2) // histogramme
titre='Loi géométrique de paramètre p='+string(p)
xtitle(titre) // on change le titre de l'interface graphique
a=get("current_axes") ; // on change les axes pour les rendre plus lisibles
a.font_size=1;
a.x_location="bottom"; 
a.y_location="left";
