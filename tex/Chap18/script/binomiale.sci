// Résumé : fonction simulant une loi binomiale de paramètre n=10 et p=0.3

clf; clc; // On efface et réinitialise la fenêtre graphique

// On génère 10000 simulations suivant la loi binomiale de paramètre n=10 et p=0.3
n=10; 
p=0.3;
N=10000;

U =grand(1,N,"bin",n,p);  // bin : binomiale
X = tabul(U,"i");  

// On représente graphiquement la simulation
bar(X(:,1), X(:,2)/N,width=0.2) // histogramme

titre='Loi binomiale de paramètre n='+string(n)+' et p='+string(p)

xtitle(titre) // on change le titre de l'interface graphique

a=get("current_axes") ; // on change les axes pour les rendre plus lisibles
a.font_size=1;
a.x_location="bottom"; 
a.y_location="left";
