\chapter{Variables aléatoires discrètes}

\objectifintro{
Dans ce chapitre, on fait des rappels sur les paramètres concernant les variables aléatoires discrètes. On s'intéresse ensuite aux lois de référence : uniforme, Bernoulli, binomiale, géométrique et de Poisson.
}

\begin{extrait}{Nicolas Grimaldi (1933 -- ). \emph{Le Travail. Communion et excommunication}}
Tout l'ordre social, pour aléatoire et injuste qu'il soit, si absurde et même si scandaleux qu'il puisse être, n'est fondé que sur une ordinaire, diffuse et commune persuasion.
\end{extrait}

\begin{objectifs}
	\begin{numerote}
  \item Concernant les paramètres d'une variable aléatoire :
\begin{itemize}
  \item \lienobj{1}{connaître la définition de l'espérance}
  \item \lienobj{2}{savoir utiliser la formule de transfert}
  \item \lienobj{3}{connaître la définition de la variance}
  \item \lienobj{4}{savoir utiliser la formule de Koenig-Huygens}
\end{itemize}
  \item Concernant les lois usuelles, il faut connaître
\begin{itemize}
  \item \lienobj{5}{la définition et les paramètres de la loi certaine}
  \item \lienobj{6}{la définition et les paramètres de la loi uniforme}
  \item \lienobj{7}{la définition et les paramètres de la loi de Bernoulli}
  \item \lienobj{8}{la définition et les paramètres de la loi binomiale}
  \item \lienobj{9}{la définition et les paramètres de la loi géométrique}
  \item \lienobj{10}{la définition et les paramètres de la loi de Poisson}
\end{itemize}
  \item \lienobj{11}{Savoir générer avec \textsc{scilab} les différentes lois}
\end{numerote}
\end{objectifs}

%%% Début du cours %%%

Dans l'ensemble de ce chapitre, on se donne un espace probabilisé $(\Omega, \mathcal{A}, \PP)$.

\section{Compléments sur les variables aléatoires discrètes}

Dans l'ensemble de cette section, les variables aléatoires seront \textbf{discrètes}, c'est-à-dire que $X(\Omega)$ est inclus dans $\N$ ou $\Z$.

\subsection{Espérance mathématiques}

\begin{definition}
\labelobj{1}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$. On écrit $X(\omega)=\{x_i, i\in I\}$. On dit que $X$ admet une \textbf{espérance} lorsque la série
$\displaystyle{\sum_{i\in I} x_i\PP(X=x_i)}$ est absolument convergente. Dans ce cas, on appelle \textbf{espérance} (ou \textbf{moyenne}) de $X$ le nombre
$$\esperance(X)=\sum_{i\in I} x_i \PP(X=x_i) = \sum_{x \in X(\Omega)} x\PP(X=x)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(X)=\sum_{k=0}^{+\infty} k\PP(X=k)$$
\end{definition}

\begin{remarque}
Si $X(\Omega)$ est fini, $X$ admet une espérance.
\end{remarque}

\begin{exemple}
On dispose d'un dé non truqué qu'on lance une fois. On note $X$ la variable aléatoire qui vaut $5$ si on tombe sur $6$, $1$ si on tombe sur $5$ ou $1$, et $-3$ sinon. Démontrer que $X$ admet une espérance, et déterminer $\esperance(X)$.
\end{exemple}

\ifprof
\begin{soluce}
Alors, $X(\Omega)=\{-3,1,5\}$ est fini, donc possède une espérance, et on a
$$\esperance(X)=-3.\frac{3}{6}+1.\frac{2}{6}+5.\frac{1}{6}=-\frac{1}{3}$$
\end{soluce}
\else
\lignes{3}
\fi

\begin{propriete}
Si $X(\Omega) \subset [p,q]$ et si $X$ admet une espérance, alors $p\leq \esperance(X)\leq q$.\\ En particulier, si $X$ est positive, $\esperance(X)\geq 0$.
\end{propriete}

\ifprof
\begin{demonstration}
Si $X(\Omega)\subset [p,q]$ et si $X(\Omega)=\{x_i, i\in I\}$, alors pour tout $i\in I$, $$p\leq x_i \leq q \Rightarrow p\PP(X=x_i)\leq x_i\PP(X=x_i)\leq q\PP(X=x_i)$$
les probabilités étant positives. En additionnant,
$$\sum_{i\in I} p\PP(X=x_i)\leq \esperance(X) \leq \sum_{i \in I} q\PP(X=x_i)$$
et on peut conclure car $\sum_{i\in I} \PP(X=x_i)=1$.
\end{demonstration}
\else
\lignes{10}
\fi

\begin{propriete}
[Linéarité de l'espérance] Soient $X$ et $Y$ deux variables aléatoires discrètes sur $(\Omega, \mathcal{A}, \PP)$ admettant une espérance. Soient $a$ et $b$ deux réels.
\begin{itemize}
  \item la variable aléatoire $aX+b$ admet une espérance, et $\esperance(aX+b)=a\esperance(X)+b$.
  \item la variable aléatoire $X+Y$ admet une espérance, et $\esperance(X+Y)=\esperance(X)+\esperance(Y)$.
\end{itemize}
\end{propriete}

\ifprof
\begin{demonstration}
Repose sur la linéarité des séries, dans le cas où les deux séries sont absolument convergentes.
\end{demonstration}
\else
\lignes{10}
\fi

\begin{definition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$.
\begin{itemize}
    \item[$\bullet$] On dit que $X$ est \textbf{centrée} si $\esperance(X)=0$.
    \item[$\bullet$] Si elle n'est pas centrée, la variable aléatoire $Y=X-\esperance(X)$ est centrée, et est appelée \textbf{variable aléatoire centrée associée} à $X$.
\end{itemize}
\end{definition}

\ifprof
\begin{demonstration}
En effet, si $Y=X-\esperance(X)$, alors $\esperance(Y)=\esperance(X)-\esperance(X)=0$ en utilisant la linéarité de l'espérance ($a=1$ et $b=-\esperance(X)\in \R$).
\end{demonstration}
\else
\lignes{3}
\fi

\subsection{Formule de transfert}

\begin{theoreme}[Formule de transfert]
\labelobj{2}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$. On écrit $X(\Omega)=\{x_i, i\in I\}$. Soit $g$ une fonction définie sur $\R$. Si $\displaystyle{\sum_{i\in I} g(x_i)\PP(X=x_i)}$ est absolument convergente, alors la variable aléatoire $g(X)$ admet une espérance, et
$$\esperance(g(X))=\sum_{i\in I} g(x_i)\PP(X=x_i)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(g(X)) = \sum_{n=0}^{+\infty} g(n)\PP(X=n)$$
\end{theoreme}

\begin{exemple}
[Exemple fondamental] Si on prend la fonction $g:x\mapsto x^2$, si la série $\displaystyle{\sum_{i\in I} x_i^2\PP(X=x_i)}$ est absolument convergente, alors $X^2$ admet une espérance, et
$$\esperance(X^2)=\sum_{i\in I} x_i^2 \PP(X=x_i)$$
Si $X(\Omega)=\N$, on a ainsi
$$\esperance(X^2)=\sum_{n=0}^{+\infty} n^2\PP(X=n)$$
\end{exemple}

\subsection{Moment d'ordre $r$}

\begin{definition}
Soit $X$ une variable aléatoire sur $(\Omega, \mathcal{A}, \PP)$. Soit $r\in \N^*$. \textbf{Sous réserve d'existence}, on appelle \textbf{moment d'ordre $r$} de $X$ le nombre
$$m_r(X)=\esperance(X^r)$$
\end{definition}

\begin{remarque}
\begin{itemize}
  \item Les variables aléatoires finies possèdent des moments à tout ordre.
  \item Si $X$ admet un moment d'ordre $r$, alors elle admet des moments d'ordre $p$ avec $p\leq r$.
\end{itemize}
\end{remarque}

\subsection{Variance}

\begin{definition}
\labelobj{3}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$. \textbf{Sous réserve d'existence}, on appelle
\begin{itemize}
    \item[$\bullet$] \textbf{moment d'ordre} $2$ de $X$ le nombre $$m_2(X)=\esperance(X^2)$$
    \item[$\bullet$] \textbf{variance} de $X$ le nombre mesurant l'écart entre $X$ et son espérance :
    $$\Var(X)=\esperance[ (X-\esperance(X))^2 ]$$
    \item[$\bullet$] \textbf{écart-type} de $X$ le nombre
    $$\sigma(X)=\sqrt{\Var(X)}$$
\end{itemize}
\end{definition}

\begin{remarque}
Une variable aléatoire discrète peut admettre une espérance sans admettre un moment d'ordre $2$.
\end{remarque}

\begin{proposition}
Si $X$ admet un moment d'ordre $2$, alors $X$ admet une espérance.
\end{proposition}

\ifprof
\begin{demonstration}
Montrons le dans le cas où $X(\Omega)=\N$. Pour tout entier $k$, on a $k\leq k^2$ et donc $$0\leq k\PP(X=k) \leq k^2\PP(X=k)$$
Si $X$ admet un moment d'ordre $2$, la série de terme générale $(k^2\PP(X=k))$ converge. Par comparaison, la série de terme général $(k\PP(X=k))$ converge également, et $X$ admet une espérance.
\end{demonstration}
\else
\lignes{5}
\fi

\begin{theoreme}[Formule de Koenig-Huygens]
\labelobj{4}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$.  $X$ admet un moment d'ordre $2$ si et seulement si $X$ admet une variance, et dans ce cas
$$\Var(X)=\esperance(X^2)-(\esperance(X))^2$$
\end{theoreme}

\ifprof
\begin{demonstration}
On suppose que $X$ admet une espérance. Constatons que $$(X-\esperance(X))^2 = X^2-2X\esperance(X)+(\esperance(X))^2$$ Par linéarité de l'espérance, on remarque, sous réserve d'existence, que $$\esperance[ X^2-2X\esperance(X)+(\esperance(X))^2]=\esperance(X^2)-2\esperance(X)\esperance(X)+(\esperance(X))^2=\esperance(X^2)-\esperance(X)^2$$
Ainsi
$\esperance[(X-\esperance(X))^2]$ existe si et seulement si $\esperance(X^2)$ existe, et dans ce cas, toujours par linéarité de l'espérance,
$$\Var(X)=\esperance[(X-\esperance(X))^2]=\esperance(X^2)-(\esperance(X))^2$$
\end{demonstration}
\else
\lignes{12}
\fi

\begin{experiencehistorique}
\begin{tabular}{p{1cm}p{12cm}p{1cm}}
\lien{https://bit.ly/3kEIVue}
& Cette formule est due à \textbf{Johann Samuel König} (1712--1757), mathématicien allemand, et \textbf{Christian Huygens} (1629--1695), mathématicien néerlandais. & 
\lien{https://fr.wikipedia.org/wiki/Christian_Huygens}
\end{tabular}
\end{experiencehistorique}

\begin{propriete}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$ admettant une variance. Alors, pour tous réels $a$ et $b$, $aX+b$ admet une variance, et
$$\Var(aX+b)=a^2\Var(X)$$
\end{propriete}

\ifprof
\begin{demonstration}
On utilise la formule de Koenig-Huygens. Constatons que $(aX+b)^2=a^2X^2+2abX+b^2$ et $\esperance(aX+b)=a\esperance(X)+b$. Donc $aX+b$ admet une variance si $X$ en admet une, et
$$\Var(aX+b)=\esperance[(aX+b)^2]-(\esperance(aX+b))^2=a^2\esperance[X^2]+2ab\esperance[X]+b^2-a^2\esperance(X)^2-2ab\esperance(X)-b^2$$
Donc
$$\Var(aX+b)=a^2(\esperance(X^2)-\esperance(X)^2)=a^2\Var(X)$$
\end{demonstration}
\else
\lignes{7}
\fi

\begin{definition}
Soit $X$ une variable aléatoire discrète sur $(\Omega, \mathcal{A}, \PP)$.
 \begin{itemize}
     \item[$\bullet$] On dit que $X$ est \textbf{réduite} si $\sigma(X)=\Var(X)=1$.
     \item[$\bullet$] Si $X$ admet une variance, et que $\sigma(X)\neq 0$, la variable aléatoire
     $$Y=\frac{X-\esperance(X)}{\sigma(X)}$$ est centrée et réduite, et est appelée \textbf{variable centrée réduite associée} à $X$. On la note en général $X^*$.
 \end{itemize}
\end{definition}

\begin{remarque}
La variance est toujours positive. Si $\Var(X)=0$ alors l'événement $[X=\esperance(X)]$ est presque sûr : on dit que $X$ suit la \textbf{loi certaine}.
\end{remarque}

 \afaire{Exercices \lienexo{1}, \lienexo{2}, \lienexo{3} et \lienexo{4}.}

\section{Lois usuelles finies}

\subsection{Loi certaine}
\labelobj{5}

\begin{definition}
On dit que la variable aléatoire $X$ suit la \textbf{loi certaine} si et seulement s'il existe un réel $a$ tel que l'événement $(X=a)$ soit presque sûr. Ainsi
$$X(\Omega)=\{a\} \textrm{  et  } \PP(X=A)=1$$
On dit également que $X$ est une variable aléatoire certaine.
\end{definition}

\begin{exemple}
Une urne contient $n$ boules, indiscernables au toucher, de couleurs différentes et portant toutes le numéro $2$ et on tire une boule. On note $X$ le numéro tiré. Alors $X$ est une variable aléatoire suivant la loi certaine, et $(X=2)$ est un événement certain.
\end{exemple}

\begin{theoreme}
Soit $a \in \R$ et $X$ une variable aléatoire suivant la loi certaine égale à $a$. Alors
$$\esperance(X)=a \textrm{ et } \Var(X) = 0$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition de $X$, puisqu'elle ne prend qu'une seule valeur, on a $$\esperance(X)=a.\PP(X=a) = a \textrm{ et } \Var(X) = (a-a)^2.\PP(X=a) = 0$$
\end{demonstration}
\else
\lignes{3}
\fi

\begin{remarque}
Nous avons une réciproque au théorème précédent : si $X$ est une variable aléatoire discrète telle que $\Var(X)=0$ alors $X$ suit une loi certaine.
\end{remarque}

\subsection{Loi uniforme}
\labelobj{6}

\begin{definition}
Soit $n \in \N^*$. On dit que la variable aléatoire $X$ suit la \textbf{loi uniforme} sur $\llbracket 1,n \rrbracket$ si
$$X(\Omega) = \llbracket 1,n \rrbracket \qeq \forall~k \in \llbracket 1,n \rrbracket,~\PP(X=k)=\frac{1}{n}$$
On note alors $X \suit \mathcal{U}(\llbracket 1,n\rrbracket)$.
\end{definition}

\begin{exemple}
On dispose d'un dé à six faces bien équilibré qu'on lance une fois. On note $X$ le chiffre obtenu après le lancer. Alors $X$ suit la loi uniforme sur $\llbracket 1,6 \rrbracket$ : $X \suit \mathcal{U}(\llbracket 1,6 \rrbracket)$.
\end{exemple}

\begin{theoreme}
Soit $n \in \N^*$ et $X$ une variable aléatoire suivant la loi uniforme sur $\llbracket 1,n \rrbracket$. Alors
$$\esperance(X) = \frac{n+1}{2} \qeq \Var(X) = \frac{n^2-1}{12}$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition,
\begin{align*}
\esperance(X)&=\sum_{k=1}^n kP(X=k) \\&= \sum_{k=1}^n \frac{k}{n} = \frac{1}{n} \sum_{k=1}^n k \\&= \frac{1}{n}\frac{n(n+1)}{2}=\frac{n+1}{2}
\end{align*}
Enfin,
\begin{align*}
\Var(X)&=\esperance(X^2)-\esperance(X)^2\\&=\sum_{k=1}^n k^2\PP(X=k) - \frac{(n+1)^2}{4}\\
&= \frac{1}{n}\frac{n(n+1)(2n+1)}{6}-\frac{(n+1)^2}{4}\\&= \frac{2(n+1)(2n+1)-3(n+1)^2}{12}=\frac{n^2-1}{12}
\end{align*}
\end{demonstration}
\else
\lignes{15}
\fi

\begin{remarque}
On peut également définir la loi uniforme sur $\llbracket a,b \rrbracket$ : pour tout $k \in \llbracket a,b \rrbracket$, $$\PP(X=k)= \frac{1}{b-a+1}$$
On note $X \suit \mathcal{U}(\llbracket a,b \rrbracket)$. Dans ce cas, on peut montrer que
$$\esperance(X)=\frac{a+b}{2} \textrm{  et  } \Var(X)=\frac{(b-a)(b-a+2)}{12}$$
Ces formules étant à démontrer systématiquement, en remarquant que si $X\suit \mathcal{U}(\llbracket a;b\rrbracket])$ alors $$X-a+1 \suit \mathcal{U}(\llbracket 1; b-a+1 \rrbracket)$$
\end{remarque}

\begin{remarque}
On peut simuler une loi uniforme sur $\llbracket 1;n \rrbracket$ avec \textsc{grand}.
\end{remarque}

\begin{scilab}[Simulation d'une loi uniforme discrète]
\inputscilab{uniforme.sci}
\end{scilab}

Ce qui donne, après exécution sur Scilab, le graphique suivant. On constate que l'aléatoire fait que les barres de l'histogramme ne sont pas toutes rigoureusement de même taille.

\begin{center}
\includegraphics[width=15cm]{uniforme}
\end{center}

\subsection{Loi de Bernoulli}
\labelobj{7}

\begin{definition}
Soit $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi de Bernoulli} de paramètre $p$ si
$$X(\Omega)=\{0,1\} \textrm{  et  } \PP(X=1)=p ~~~~\PP(X=0)=q=1-p$$
On note alors $X \suit \mathcal{B}(p)$.
\end{definition}

\begin{exemple}
On dispose d'une pièce truquée, dont la probabilité d'obtenir Pile est de $\frac{1}{3}$. Soit $X$ la variable aléatoire valant $1$ si on fait Pile, et $0$ sinon. Alors $X\suit \mathcal{B}\left(\frac{1}{3}\right)$.
\end{exemple}

\begin{theoreme}
Soit $p\in]0,1[$ et $X$ une variable aléatoire suivant la loi de Bernoulli de paramètre $p$. Alors
$$\esperance(X)=p \textrm{  et  } \Var(X)=pq=p(1-p)$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition,
$$\esperance(X)=1.\PP(X=1)+0.\PP(X=0)=1.p=p$$
et
$$\Var(X)=(1-p)^2.\PP(X=1)+(0-p)^2.\PP(X=0)=(1-p)^2p+p^2(1-p)=p(1-p)((1-p)+p)=(1-p)p$$
\end{demonstration}
\else
\lignes{6}
\fi

\subsection{Loi binomiale}
\labelobj{8}

\begin{definition}
Soient $n\in \N^*$ et $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi binomiale} de paramètres $(n,p)$ si
$$X(\Omega)=\llbracket 0,n \rrbracket \textrm{  et  } \forall~k\in \llbracket 0,n \rrbracket,~\PP(X=k)=\binom{n}{k}p^k(1-p)^{n-k}$$
On note alors $X\suit \mathcal{B}(n,p)$.
\end{definition}

\begin{exemple}
[Exemple fondamental] On répète $n$ fois de manière indépendante une expérience ayant deux issues possibles : le succès de probabilité $p$, et l'échec de probabilité $1-p$. On dit alors qu'on dispose d'un schéma de Bernoulli. La variable aléatoire $X$ comptant le nombre de succès obtenus suit une loi binomiale de paramètre $(n,p)$.
\end{exemple}

\begin{remarque}
\begin{itemize}
  \item Il s'agit bien d'une loi de probabilité. En effet, en utilisant la formule du binôme de Newton,
$$\sum_{k=0}^n \PP(X=k) = \sum_{k=0}^n \binom{n}{k} p^k (1-p)^{n-k} = (p+(1-p))^n = 1$$
  \item Dans le cas $n=1$, on retrouve la loi de Bernoulli de paramètre $p$. Ainsi, $\mathcal{B}(1,p)$ représente également la loi de Bernoulli de paramètre $p$.
\end{itemize}
\end{remarque}

\begin{exemple}
\begin{itemize}
  \item On lance $n$ dés équilibrés, et on note $X$ le nombre de $6$ obtenus. Alors $X \suit \mathcal{B}(n,\frac{1}{6})$.
  \item Un avion possède deux moteurs identiques. Ces moteurs tombent en panne avec probabilité $p$. On note $X$ la variable aléatoire qui compte le nombre de moteurs tombés en panne. Alors $X \suit \mathcal{B}(2,p)$.
  \item Une urne contient $a$ boules rouges et $b$ boules noires indiscernables au toucher. On tire successivement et avec remise $n$  boules de l'urne. On note $X$ le nombre de boules noires obtenues. Alors $X \suit \mathcal{B}(n, \frac{b}{a+b})$.
\end{itemize}
\end{exemple}

\begin{theoreme}
Soient $n\in \N^*$ et $p\in]0,1[$. Soit $X$ une variable aléatoire suivant la loi binomiale de paramètre $(n,p)$. Alors
$$\esperance(X)=np \textrm{  et  } \Var(X)=np(1-p)$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition,
\begin{align*}
\esperance(X)=\sum_{k=0}^n k\binom{n}{k}p^k(1-p)^{n-k} &=
\sum_{k=0}^n k\frac{n!}{k!(n-k)!} p^k(1-p)^{n-k}\\
&=\sum_{k=1}^n \frac{n!}{(k-1)!(n-k)!} p^k (1-p)^{n-k}
\end{align*}
Or $$\frac{n!}{(k-1)!(n-k)!} = n \frac{(n-1)!}{(k-1)(n-1-(k-1))!} = n\binom{n-1}{k-1}$$
Ainsi,
\begin{align*}
\esperance(X)&=\sum_{k=1}^n n\binom{n-1}{k-1} p p^{k-1}(1-p)^{n-1-(k-1)}
\\&= np\sum_{k=1}^n \binom{n-1}{k-1} p^{k-1} (1-p)^{n-1-(k-1)}\\
&\underbrace{=}_{i=k-1} np \sum_{i=0}^{n-1} \binom{n-1}{i} p^i (1-p)^{n-1-i} \\
&= np (p+(1-p))^{n-1}=np \text{ en reconnaissant la formule du binôme}
\end{align*}
La variance est admise.
\end{demonstration}
\else
\lignes{20}
\fi

\begin{remarque}
On peut simuler une loi binomiale de paramètre $n$ et $p$ avec \textsc{grand}.
\end{remarque}

\begin{scilab}[Simulation d'une loi binomiale]
\inputscilab{binomiale.sci}
\end{scilab}

Ce qui donne, après exécution sur Scilab, le graphique suivant.

\begin{center}
\includegraphics[width=16cm]{binomiale}
\end{center}

 \afaire{Exercices \lienexo{6} et \lienexo{7}.}

\section{Lois usuelles infinies}

\subsection{Loi géométrique}
\labelobj{9}

\begin{definition}
Soit $p\in ]0,1[$. On dit que la variable aléatoire $X$ suit la \textbf{loi géométrique} de paramètre $p$ si
$$X(\Omega) = \N^* \textrm{  et  } \forall~n\in \N^*,~\PP(X=n)=p(1-p)^{n-1}$$
On note alors $X\suit \mathcal{G}(p)$.
\end{definition}

\begin{remarque}
Il s'agit bien d'une loi de probabilité. En effet, pour tout $n\geq 1$, $\PP(X=n)\geq 0$ et on a
$$\sum_{k=1}^n p(1-p)^{k-1}=p\sum_{k=0}^{n-1} (1-p)^k$$
Ainsi, en reconnaissant une série géométrique de raison $1-p$ avec $-1<1-p<1$ :
$$\lim_{n\rightarrow +\infty} p\sum_{k=0}^{n-1} (1-p)^k = p \frac{1}{1-(1-p)} = 1$$
\end{remarque}

\begin{exemple}
On dispose d'une pièce truquée, dont la probabilité d'obtenir Face est $p$. On effectue des lancers indépendants, jusqu'à l'obtention d'un Face. On note $X$ le nombre de lancers effectués avant d'obtenir Face : on dit que $X$ est le \textbf{temps d'attente} du premier Face. Alors $X \suit \mathcal{G}(p)$.
\end{exemple}

\begin{theoreme}
Soit $p\in ]0,1[$ et $X$ une variable aléatoire suivant la loi géométrique de paramètre $p$. Alors
$$\esperance(X)=\frac{1}{p} \textrm{  et  } \Var(X)=\frac{1-p}{p^2}$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, et en utilisant les séries géométriques et géométriques dérivées ($|1-p|<1$) :
\begin{align*}
\esperance(X)&=\sum_{k=1}^{+\infty} k\PP(X=k)\\
&=\sum_{k=1}^{+\infty} kp(1-p)^{k-1} \\
&=p\sum_{k=1}^{+\infty} k(1-p)^{k-1} \\
&= p \frac{1}{(1-(1-p))^2} = \frac{1}{p}  \text{ en reconnaissant la série géométrique dérivée}
\end{align*}
et
$$\Var(X)=\esperance(X^2)-\esperance(X)^2= \sum_{k=1}^{+\infty} k^2p(1-p)^{k-1}-\frac{1}{p^2}$$
Or, en utilisant les méthodes classiques sur les séries géométriques :
\begin{align*}
\sum_{k=1}^{+\infty} k^2p(1-p)^{k-1}&=p\sum_{k=1}^{+\infty} (k(k-1)+k)(1-p)^{k-1}\\
&=\sum_{k=1}^{+\infty} k(k-1)p(1-p)^{k-1}+\sum_{k=1}^{+\infty} kp(1-p)^{k-1}\\&=p\frac{2(1-p)}{(1-(1-p))^3}+p\frac{1}{(1-(1-p))^2}=\frac{2-p}{p^2}
\end{align*}
et donc
$$\Var(X)=\frac{2-p}{p^2}-\frac{1}{p^2}=\frac{1-p}{p^2}$$
\end{demonstration}
\else
\lignes{22}
\fi

\begin{remarque}
On peut simuler une loi géométrique de paramètre $p$ avec \textsc{grand}.
\end{remarque}

\begin{scilab}[Simulation d'une loi géométrique]
\inputscilab{geometrique.sci}
\end{scilab}

Ce qui donne, après exécution sur Scilab, le graphique suivant.

\begin{center}
\includegraphics[width=13cm]{geometrique}
\end{center}

\subsection{Loi de Poisson}
\labelobj{10}

\begin{definition}
Soit $\lambda>0$. On dit que la variable aléatoire $X$ suit la \textbf{loi de Poisson} de paramètre $\lambda$, si
$$X(\Omega)=\N \textrm{  et  } \forall~n\in \N,~\PP(X=n)=\frac{\lambda^n}{n!} \eu{-\lambda}$$
On note alors $X \suit \mathcal{P}(\lambda)$.
\end{definition}

\begin{remarque}
Il s'agit bien d'une loi de probabilité. En effet, pour tout $n$, $P(X=n)\geq 0$ et on a
$$\sum_{n=0}^{+\infty} \frac{\lambda^n}{n!}\eu{-\lambda} = \eu{-\lambda}\sum_{n=0}^{+\infty} \frac{\lambda^n}{n!}=\eu{-\lambda}\eu{\lambda}=1$$
\end{remarque}

\begin{exemple}
Une usine produit des bouteilles d'eau. Parmi celles-ci, $3\%$ sont défectueuses. On tire au hasard $100$ bouteilles, et on note $X$ le nombre de bouteilles défectueuses. On admet que $X$ suit une loi de Poisson. Alors $X\suit \mathcal{P}(0,03*100=3)$.
\end{exemple}

\begin{remarque}
[Lien avec la loi binomiale] Dans l'exemple précédent, on aurait tout aussi bien pu utiliser une loi binomiale de paramètres $(100, 0,03)$.
\end{remarque}

\begin{theoreme}
Par approximation classique, si $X$ suit une loi binomiale de paramètre $(n,p)$, et si on a $$n\geq 50, p\leq 0,1,~\lambda=np\leq 10$$ alors $X$ peut être approchée par la loi de Poisson de paramètre $\lambda=np$.
\end{theoreme}

\begin{remarque}
On dira, l'année prochaine, que si, pour tout $n\geq 1$, $X_n\suit \mathcal{B}(n, \frac{\lambda}{n})$ , alors quand $n$ tend vers l'infini, la suite $(X_n)$ \textbf{converge en loi} vers une loi de Poisson de paramètre $\lambda$.
\end{remarque}

\begin{theoreme}
Soit $\lambda>0$ et $X$ une variable aléatoire suivant la loi de Poisson de paramètre $\lambda$. Alors
$$\esperance(X)=\lambda \textrm{  et  } \Var(X)=\lambda$$
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, et en reconnaissant la série de l'exponentielle,
$$\esperance(X)=\sum_{n=0}^{+\infty} n\frac{\lambda^n}{n!}\eu{-\lambda}=\eu{-\lambda} \sum_{n=1}^{+\infty} \frac{\lambda^n}{(n-1)!}=\eu{-\lambda}\lambda \sum_{n=1}^{+\infty} \frac{\lambda^{n-1}}{(n-1)!}=\eu{-\lambda} (\lambda \eu{\lambda})=\lambda$$
La variance se fait selon la même idée, en utilisant la série dérivée de l'exponentielle.
\end{demonstration}
\else
\lignes{25}
\fi

\begin{remarque}
On peut simuler une loi de Poisson de paramètre $\lambda$ avec \textit{grand}.
\end{remarque}

\begin{scilab}[Simulation d'une loi de Poisson]
\inputscilab{poisson.sci}
\end{scilab}

Ce qui donne, après exécution sur Scilab, le graphique suivant.

\begin{center}
\includegraphics[width=13cm]{poisson}
\end{center}

 \afaire{Exercices \lienexo{7}, \lienexo{8}, \lienexo{9} et \lienexo{13}}
