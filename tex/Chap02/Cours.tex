\chapter{Utilisation des symboles $\Sigma$ et $\Pi$}

\objectifintro{Le but de ce (petit) chapitre est d'introduire les symboles $\Sigma$ et $\Pi$ que l'on utilisera régulièrement au cours de l'année.}

\begin{objectifs}
	\begin{numerote}
        \item maitriser les symboles $\Sigma$ et $\prod$ :
                        \begin{itemize}[label=\textbullet]
                                \item \hyperref[objectif-02-1]{le changement de variable dans une somme}\dotfill $\Box$
                                \item \hyperref[objectif-02-2]{la simplification d'une somme télescopique}\dotfill $\Box$
                        \end{itemize}
		\item \hyperref[objectif-02-3]{connaître la définition de la factorielle}\dotfill $\Box$
		\item \hyperref[objectif-02-4]{connaître les propriétés de la factorielle}\dotfill $\Box$
\end{numerote}
\end{objectifs}

\section{Définitions et propriétés}

    \subsection{Définition}

\begin{definition}
Soient $n$ et $p$ deux entiers tels que $p<n$. On note $\interent{p n}  = \{p;p+1;\cdots;n\}$.
\end{definition}

\begin{exemple}
Par exemple, $\interent{2 5} = \accol{2,3,4,5}$.	
\end{exemple}


\begin{remarque}
Il y a $n$ entiers dans $\interent{1 n}$, et $n+1$ entiers dans $\interent{0 n}$. Il y a $n-p+1$ entiers dans $\interent{p n}$.
\end{remarque}


\begin{exemple}
Il y a $7$ entiers dans $\interent{2 8}$.
\end{exemple}

\begin{definition}
\begin{itemize}
    \item[$\bullet$] Soient $a_0,a_1,\cdots, a_n$ des réels. On note \[\sum_{k=0}^n a_k=a_0+a_1+\cdots a_n\]
et on lit ``somme de $k=0$ à $n$ des $a_k$''.
    \item[$\bullet$] Soient $a_p, \cdots, a_n$ des réels ($p\leq n$). On note
    \[\sum_{k=p}^n a_k = a_p+\cdots +a_n\]
et on lit ``somme de $k=p$ à $n$ des $a_k$''.
\end{itemize}
\end{definition}

\begin{exemple}
Par exemple, $\displaystyle{\ln(2)+\ln(3)+\cdots + \ln(n) = \sum_{k=2}^n \ln(k)}$.	
\end{exemple}

\begin{exo}
Ecrire la notation en extension de $\ds{\sum_{k=2}^{30} k}$ et $\ds{\sum_{n=1}^p \sqrt{n}}$.\\Ecrire à l'aide du symbole $\sum$ l'expression $\ds{1+\frac{1}{2}+\cdots +\frac{1}{n}}$   et $\ds{2+4+6+\cdots + 18}$.	
\end{exo}

\solution[4]{On a, rapidement :
\begin{itemize}
	\item $\ds{\sum_{k=2}^{30} k = 2 + 3 + \cdots  + 30}$,
	\item $\ds{\sum_{n=1}^p \sqrt{n} = 1 + \sqrt{2}+\cdots + \sqrt{p}}$,
	\item $\ds{1+\frac{1}{2}+\cdots +\frac{1}{n} = \sum_{k=1}^n \frac{1}{k}}$,
	\item $\ds{2+4+\cdots + 18 = \sum_{k=1}^9 2k}$.
\end{itemize}
}

\begin{remarque}
L'ordre de la sommation n'a pas d'importance. Ainsi
$\displaystyle{\sum_{k=1}^n k^2}$ représente la même somme que $\displaystyle{\sum_{k=n}^1 k^2}$.
\end{remarque}

\begin{remarque}
On définit de la même manière $\displaystyle{a_p\times a_{p+1}\times \cdots \times a_n=\prod_{k=p}^n a_k}$.
\end{remarque}


\begin{definition}[Factorielle]
\label{objectif-02-3}
Soit $n$ un entier non nul. On appelle \textbf{factorielle} de $n$, et on note $n!$, le nombre $\displaystyle{n! = \prod_{k=1}^n k}$. \\Par convention, $0!=1$.
\end{definition}


\begin{remarque}
On a ainsi $1!=1$, $2!=1\times 2 = 2$ et $3!=1\times 2 \times 3 = 6$.
\end{remarque}


\begin{proposition}
\label{objectif-02-4}
  Pour tout entier $n\geq 1$, on a \[ (n+1)! = (n+1) \times n! \]
\end{proposition}

\ifprof
\begin{demonstration}
En effet, par définition, \[ (n+1)! = \underbrace{1 \times 2 \times \cdots \times n}_{=n!} \times (n+1) = n! \times (n+1) \]	
\end{demonstration}
\else
\lignes{2}
\fi

\begin{exo}
Pour $n\pgq 1$, simplifier $\ds{\frac{(n+2)!}{n!}}$.	
\end{exo}

\solution[3]{On a
\[ \frac{(n+2)!}{n!} = \frac{(n+2)(n+1)n!}{n!}=(n+2)(n+1) \]}

    \subsection{Première propriétés}

Soient deux entiers $p$ et $n$ tels que $p\leq n$, soient $a_p,\cdots, a_n, b_p,\cdots,b_{n}$ des réels.
   \begin{itemize}
        \item[$\bullet$] Linéarité : $$\sum_{k=p}^n (a_k+b_k) = \sum_{k=p}^n a_k+\sum_{k=p}^n b_k$$
        \[\forall~\lambda\in \R,~\sum_{k=p}^n \lambda.a_k=\lambda\sum_{k=p}^n a_k\]
        \item[$\bullet$] Relation de Chasles : pour tout entier $m> n$,
        \[\sum_{k=p}^m a_k=\sum_{k=p}^n a_k + \sum_{k=n+1}^m a_k\]
        \item[$\bullet$] $\displaystyle{\prod_{k=p}^n (a_k\times b_k) = \prod_{k=p}^n a_k \times \prod_{k=p}^n b_k}$
        \item[$\bullet$] $\displaystyle{\forall~\lambda,~\prod_{k=p}^n (\lambda a_k) = \lambda^{n-p+1}\prod_{k=p}^n a_k}$
    \end{itemize}


    \subsection{$\ln$ et $\exp$}

\begin{propriete}
    \begin{itemize}
        \item[$\bullet$] Pour tous réels $a_p,\cdots, a_n$, on a
        \[\exp\left( \sum_{k=p}^n a_k \right) = \prod_{k=p}^n e^{a_k}\]
        \item[$\bullet$] Pour tous réels $a_p>0, \cdots, a_n>0$, on a
        \[\ln \left( \prod_{k=p}^n a_k \right) = \sum_{k=p}^n \ln(a_k)\]
    \end{itemize}
	
\end{propriete}

	\subsection{Sommes usuelles}

\begin{proposition}\logoparcoeur 
  On dispose des résultats suivants :
\[ \sum_{k=1}^n k = \frac{n(n+1)}{2}  \]
\[ \sum_{k=1}^n k^2 = \frac{n(n+1)(2n+1)}{6} \]
\[ \sum_{k=0}^n q^k = \frac{1-q^{n+1}}{1-q} (\textrm{si } q\neq 1)\]
\end{proposition}


\begin{demonstration}
Elles ont été vues dans le chapitre précédent.	
\end{demonstration}


\section{Changement de variables} 

    \subsection{Variables muettes}

\begin{remarque}
Lorsqu'on écrit $\displaystyle{\sum_{k=p}^n a_k}$, la variable $k$ est appelée \textbf{variable muette} : on peut la remplacer par n'importe quelle autre lettre non utilisée :
\[\sum_{k=p}^n a_k = \sum_{i=p}^n a_i = \sum_{z=p}^n a_z\]
\end{remarque}

    \subsection{Changement de variable}

\label{objectif-02-1}
Puisque la variable d'une somme est muette, on peut faire un changement de variable, qui consiste à ré-écrire la somme différemment.

\begin{proposition}
  Soient $p\leq n$ deux entiers, $l$ un entier, et $a_{p+l},\cdots a_{n+l}$ des réels. Alors
$$\sum_{k=p+l}^{n+l} a_k = \sum_{j=p}^n a_{j+l}$$
On a effectué le changement de variable $j=k-l$ : ainsi, si $k=p+l$, alors $j=p$. De même, $k=n+l$ amène $j=n$.
\end{proposition}

\begin{methode}
  Pour faire un changement de variable $j=f(k)$, on procède en remplaçant toutes les occurrences de $k$ par son expression en fonction de $j$, mais on n'oublie pas de changer les bornes en conséquence !
\end{methode}

\begin{exemple}
Calculer $\displaystyle{S=\sum_{k=0}^n (n-k)}$ en posant $j=n-k$.	
\end{exemple}


\solution[4]{Posons $j=n-k$. Alors
\[S=\sum_{j=n}^0 j = \sum_{j=0}^n j\]
car l'ordre de la somme des termes n'importe pas. On a donc
\[S=\frac{n(n+1)}{2}\]}

	\subsection{Sommes doubles}

\begin{remarque}
On peut envisager de calculer des sommes de sommes. Dans ce cas, on fera attention à utiliser deux variables différentes.
\end{remarque}

\begin{exemple}
La somme suivante est une somme double : \[ \sum_{i=1}^n \sum_{j=1}^n ij \] 	
\end{exemple}

\begin{remarque}
On peut calculer la somme précédente. En effet, lorsqu'on somme sur $j$, la variable $i$ est une variable indépendante. Ainsi
\[ \sum_{i=1}^n \sum_{j=1}^n ij = \sum_{i=1}^n i \sum_{j=1}^n j = \frac{n(n+1)}{2} \frac{n(n+1)}{2} = \left(\frac{n(n+1)}{2}\right)^2 \]

Dans certains cas, on ne peut pas séparer les variables, quand une somme dépend d'une des variables. Par exemple,
\[ \sum_{i=1}^n \sum_{j=1}^i j \]
Dans ce cas, on peut calculer la somme, mais en étant rigoureux :
\[ \sum_{i=1}^n \sum_{j=1}^i j = \sum_{i=1}^n \frac{i(i+1)}{2}=\frac{1}{2}\sum_{i=1}^n i^2+i = \frac{1}{2}\left( \frac{n(n+1)(2n+1)}{6} + \frac{n(n+1)}{2} \right) \]
\end{remarque}

\section{Sommes télescopiques}

    \subsection{Définition}

\begin{definition}
Soient $a_0,\cdots, a_{n+1}$ des réels. On appelle \textbf{somme télescopique} une somme de la forme
\[\sum_{k=0}^n a_{k+1}-a_k\]
\end{definition}

\begin{exemple}
La somme $\displaystyle{\sum_{k=1}^n \frac{1}{k+1}-\frac{1}{k}}$ est une somme télescopique.	
\end{exemple}

    \subsection{Simplification}

\begin{proposition}
\label{objectif-02-2}
  Soit $\displaystyle{S_n=\sum_{k=0}^n a_{k+1}-a_k}$. Alors
\[S_n=a_{n+1}-a_0\]
\end{proposition}

\begin{demonstration}
En effet,
\[S_n=(a_1-a_0)+(a_2-a_1)+(a_3-a_2)+\cdots + (a_n-a_{n-1})+(a_{n+1}-a_n) = -a_0+a_{n+1}\]	
\end{demonstration}

\begin{exemple}
La somme $S_n=\displaystyle{\sum_{k=1}^n \frac{1}{k+1}-\frac{1}{k}}$ se simplifie en
\[S_n=\frac{1}{n+1}-1\]
\end{exemple}

\exercice{Soit $S_n=\displaystyle{\sum_{k=1}^n \ln\left( \frac{k+1}{k} \right)}$. Simplifier $S_n$.}

\solution[4]{On constate en effet, en utilisant les propriétés du logarithme, que
\[S_n=\sum_{k=1}^n \ln(k+1)-\ln(k)\]
La somme $S_n$ est donc télescopique. On a donc
\[S_n=\ln(n+1)-\ln(1)=\ln(n+1)\]}

	\subsection{Produits télescopiques}

On peut définir également les produits télescopiques, avec un résultat assez similaire à celui des sommes télescopiques.

\begin{definition}
Soient $a_0,\cdots a_{n+1}$ des réels tous non nuls. On appelle \textbf{produit télescopique} un produit de la forme
\[\prod_{k=0}^n \frac{a_{k+1}}{a_k}\]
\end{definition}

\begin{exemple}
Le produit
\[\frac{2}{1}\times\frac{3}{2}\cdots \frac{n+1}{n} = \prod_{k=1}^{n} \frac{k+1}{k}\] est un produit télescopique.
\end{exemple}

\begin{proposition}
  Soit $\displaystyle{P_n=\prod_{k=0}^n \frac{a_{k+1}}{a_k}}$ avec $a_0,\cdots a_{n+1}$ tous non nuls. Alors
$\displaystyle{P_n=\frac{a_{n+1}}{a_0}}$
\end{proposition}
