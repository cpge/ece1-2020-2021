
\section{Limites à l'infini}

\subsection{Limites nulles}

\definition{
Si, pour tout $\eps > 0$ (aussi petit qu'on veut), la fonction $f$ est comprise entre $-\eps$ et $+\eps$ (soit $|f(x)| \leq \eps$) lorsque $x$ est suffisamment grand , on dit que $f$ \textbf{a pour limite $0$} quand $x$ tend vers $+\infty$. On note
 $$\lim_{x \rightarrow +\infty} f(x)=0~~~~\textrm{ ou }~~~~\lim_{+\infty} f =0$$}
 
\begin{center}
    \includegraphics[width=8cm]{Chap10/limitenulle}
\end{center}
 
 
\remarque{En langage mathématique, on écrit donc 
$$\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)|<\eps$$}  
 
\remarque{On définit de même $\displaystyle{\lim_{x \rightarrow -\infty}f(x) = 0}$}

\exemple{$\displaystyle{\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$}

\subsection{Limites finies : $l \in \R$}

\definition{Soit $l \in \R$. On dit qu'une fonction $f$ \textbf{a pour limite $l$} quand $x$ tend vers $+\infty$ (ou $-\infty$) si $f(x)-l$ tend vers $0$ quand $x$ tend vers $+\infty$ (ou $-\infty$).}

\remarque{En langage mathématique, on écrit donc 
$$\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)-l|<\eps$$}  

\exemple{$\displaystyle{\lim_{x\rightarrow +\infty} 2+\frac{1}{x}=2}$}

\definition{\textit{(Asymptote horizontale)}
Soit $f$ une fonction telle que $\displaystyle{\lim_{x \rightarrow +\infty} f(x) = l}$ (où $l \in \R$). Alors, la droite d'équation $y=l$ est appelée \textbf{asymptote horizontale} à la courbe de $f$ en $+\infty$ (même chose en $-\infty$).}

\remarque{
Pour étudier la position de la courbe de $f$ par rapport à l'asymptote $y=l$, on étudie le signe de $f(x)-l$. 
\begin{itemize}
	\item[$\bullet$] Si $f(x)-l \geq 0$, la courbe est au dessus de son asymptote.
	\item[$\bullet$] Si $f(x)-l \leq 0$, la courbe est au dessus de son asymptote.
\end{itemize}}

\subsection{Limites infinies}

\definition{
 Si, pour tout nombre $A$ (aussi grand qu'on veut), la fonction $f$ est supérieure ou égale à $A$ dès que $x$ est suffisamment grand, on dit que \textbf{$f$ a pour limite $+\infty$} quand $x$ tend vers $+\infty$. On note 
 $$\lim_{x \rightarrow +\infty} f(x)=+\infty~~~~\textrm{ ou }~~~~\lim_{+\infty} f =+\infty$$}

\begin{center}
    \includegraphics[width=8cm]{Chap10/limiteinfinie}
\end{center}

\remarque{En langage mathématique, on écrit donc 
$$\forall~A>0,~\exists~M,~\forall~x, x>M\Rightarrow f(x)>A$$}  

\remarque{On définit de même $\displaystyle{\lim_{x \rightarrow -\infty} f(x)=+\infty}$, $\displaystyle{\lim_{x \rightarrow +\infty} f(x)=-\infty}$ et $\displaystyle{\lim_{x \rightarrow -\infty} f(x)=-\infty}$}


\section{Limites en $a \in \R$}

Ici, on s'intéresse au comportement d'une fonction $f$ quand $x$ tend vers $a \in \R$. 

    \subsection{Limite réelle en un point}
    
 \definition{Soient $x_0$ et $l$ deux réels.\\
Si $f(x)$ est aussi proche que l'on veut de $l$ dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $l$} quand $x$ tend vers $x_0$. On note
 $$\lim_{x \rightarrow x_0} f(x)=l~~~~\textrm{ ou }~~~~\lim_{x_0} f =l$$}
  

\begin{center}
    \includegraphics[width=8cm]{Chap10/limiteenreel}
\end{center}

\remarque{En langage mathématique, on écrit donc 
$$\forall~\eps>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow |f(x)-l|<\eps$$} 

\remarque{Il y a unicité de la limité. On peut donc bien parler de la limite en $x_0$.}

\exemple{Soit $f$ la fonction définie sur $\R$ par $f(x)=2x+1$. Soit $x_0=2$. Montrer que, quand $x$ tend vers $x_0$, $f(x)$ va tendre vers $5=f(2)$.}

\begin{prof}
\solution{
 On peut le démontrer rigoureusement :
$$|f(x)-5|=|2x+1-5|=|2x-4|=2|x-2|$$
Soit $\eps>0$ fixé. Alors $|f(x)-5|<\eps \Leftrightarrow 2|x-2|<\eps \Leftrightarrow |x-2|<\frac{\eps}{2}$. On pose alors $\alpha=\frac{\eps}{2}$.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

    \subsection{Limite infinie en un point}
    
\definition{Soient $x_0$ un réel.\\
Si $f(x)$ est aussi grand que l'on veut dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $+\infty$} quand $x$ tend vers $x_0$. On note
 $$\lim_{x \rightarrow x_0} f(x)=+\infty~~~~\textrm{ ou }~~~~\lim_{x_0} f =+\infty$$}
 
\begin{center}
    \includegraphics[width=8cm]{Chap10/limitereelinfinie}
\end{center}

\remarque{En langage mathématique, on écrit donc 
$$\forall~A>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow f(x)>A$$} 

\remarque{Il y a unicité de la limité. On peut donc bien parler de la limite en $x_0$. On peut également définir $\displaystyle{\lim_{x\rightarrow x_0} f(x)=-\infty}$.}

    \subsection{Limite à gauche et à droite}

\definition{Soit $x_0$ un réel.
\begin{itemize}
    \item[$\bullet$] Si on s'intéresse à la limite en $x_0$, en imposant $x<x_0$, on parle de la \textbf{limite à gauche} en $x_0$, et on note $\displaystyle{\lim_{x\rightarrow x_0^-}}$ ou $\displaystyle{\lim_{x\rightarrow x_0, x<x_0}}$.
    \item[$\bullet$] Si on s'intéresse à la limite en $x_0$, en imposant $x>x_0$, on parle de la \textbf{limite à droite} en $x_0$, et on note $\displaystyle{\lim_{x\rightarrow x_0^+}}$ ou $\displaystyle{\lim_{x\rightarrow x_0, x>x_0}}$.
\end{itemize}
}

\remarque{Une fonction peut avoir des limites à gauche et à droite en $x_0$ différentes ! Par exemple, $f:x\mapsto\frac{1}{x}$ en $0$.}

\proposition{Soit $f$ une fonction et $x_0$ un réel. Alors $f$ admet une limite en $x_0$ si et seulement si $f$ admet des limites à gauche et à droite en $x_0$ et que ces limites sont les mêmes.\\Dans ce cas, $$\lim_{x\rightarrow x_0} f(x)=\lim_{x\rightarrow x_0^-} f(x) = \lim_{x\rightarrow x_0^+}$$}

\exemple{Ainsi, la fonction inverse n'admet pas de limite en $0$. En effet, 
$$\lim_{x\rightarrow 0^-} \frac{1}{x}=-\infty \textrm{  et  } \lim_{x\rightarrow 0^+} \frac{1}{x}=+\infty$$}

\definition{\textit{(Asymptote verticale)}\\
 Si $\displaystyle{\lim_{x \rightarrow a^+ \textrm{(ou $a^-$)}} f(x) = +\infty \textrm{ ( ou $-\infty$)} }$, on dit que la droite d'équation $y=a$ est une \textbf{asymptote verticale} à la courbe de $f$.}

\section{Théorèmes d'existence et de comparaison}

        \subsection{Fonctions monotones}

Les fonctions monotones possèdent des propriétés intéressantes concernant les limites :

\theoreme{Soit $f$ une fonction monotone sur un segment $[a;b]$. Alors $f$ admet des limites à gauche et à droite en tout point.}

\theoreme{Soit $f$ une fonction monotone sur $I=]a;b[$.
\begin{itemize}
    \item[$\bullet$] Si $f$ est croissante et majorée sur $I$, alors $f$ admet une limite en $b^-$. 
    \item[$\bullet$] Si $f$ est croissante et minorée sur $I$, alors $f$ admet une limite en $a^+$.
    \item[$\bullet$] Si $f$ est décroissante et minorée sur $I$, alors $f$ admet une limite en $b^-$.    
    \item[$\bullet$] Si $f$ est décroissante et majorée sur $I$, alors $f$ admet une limite en $a^+$.
\end{itemize}
}

        \subsection{Limites et inégalités}
        
\theoreme{Soit $I$ un intervalle et $x_0\in I$. Soient $f$ et $g$ deux fonctions définies sur un $I$, sauf éventuellement en $x_0$, mais possédant une limites en $x_0$. Alors, si pour tout $x$ de $I\backslash\{x_0\}$, $f(x)\geq g(x)$ alors $$\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)$$
En particulier, si $f(x)\geq 0$ pour tout $x\neq x_0$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x)\geq 0}$.}

\remarque{Attention : le passage à la limite ne conserve pas les inégalités strictes. Si $f(x)> g(x)$ pour tout $x \neq x_0$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)}$. Par exemple, pour tout $x$, $1+\frac{1}{x}>1$ mais $\displaystyle{\lim_{x\rightarrow +\infty} 1+\frac{1}{x} \geq 1}$.}

	\subsection{Théorème d'encadrement}
	
\theoreme{Soit $I$ un intervalle et $x_0\in I$. Soient $f,g,h$ trois fonctions définies sur $I$ sauf éventuellement en $x_0$. Si, pour tout $x$ de $I \backslash \{x_0\}$, on a $f(x) \leq g(x) \leq h(x)$ et si $f$ et $h$ ont la même limite $l$ en $x_0$, alors $$\lim_{x\rightarrow x_0} g(x)=l$$}

\preuve{Admis. Idée de démonstration dans le chapitre sur les suites.}

\exemple{Soit $g$ la fonction définie sur $]0;+\infty[$ par $g(x)=\frac{[x]}{x}$. Montrer que $\displaystyle{\lim_{x\rightarrow+\infty} f(x)=0}$.}

\begin{prof}
\solution{
 On a, pour tout $x$ de $]0;+\infty[$, $0 \leq g(x) \leq \frac{1}{x}$. Puisque $\displaystyle{\lim_{x\rightarrow +\infty} \frac{1}{x} ==0}$, on en déduit donc 
$$\lim_{x\rightarrow +\infty} \frac{[x]}{x}=0$$
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Comparaison à l'infini}
	
\theoreme{Soient $f$ et $g$ deux fonctions définies sur $I=]a;+\infty[$. Si pour tout $x$ de $I$ :
\begin{itemize}
	\item[$\bullet$] $f(x)\geq g(x)$ \textbf{et si} $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=+\infty}$ alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.
	\item[$\bullet$] $f(x)\leq g(x)$ \textbf{et si} $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=-\infty}$ alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=-\infty}$.
\end{itemize}
}

\begin{prof}
\preuve{Soit $M$ un réel. Par définition, à partir d'un certain réel $b$, on a $g(x)>M$. Or $f(x)\geq g(x)$, donc $f(x) > M$ pour tout $x \geq b$ : par définition, $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.}
\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

% Mettre exemple

       \subsection{Asymptote oblique}
       
\definition{\textit{(Asymptote oblique)} Soit $\mathcal{C}_f$ la courbe représentative d'une fonction $f$ dans un repère donné. Soit $(d)$ une droite d'équation $y=ax+b ~(a\neq 0)$. On dit que la droite $(d)$ est une \textbf{asymptote oblique} à $\mathcal{C}_f$ au voisinage de $+\infty$ si
$$\lim_{x\rightarrow+\infty} [f(x)-(ax+b)] =0$$
}

\exemple{Soit $f$ la fonction définie sur $\R^*$ par $f(x)=\frac{x^2+1}{x}$. Montrer que la droite d'équation $y=x$ est asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et de $-\infty$.}

\begin{prof}
\solution{En effet, pour tout réel $x\in \R^*$, 
$$f(x)=x=\frac{x^2+1}{x}-x=\frac{1}{x}$$
Or $\displaystyle{\lim_{x\rightarrow -\infty} \frac{1}{x}=\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$.
Ainsi la droite d'équation $y=x$ est bien asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et $-\infty$}
\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

\section{Opérations sur les limites et limites usuelles}

        \subsection{Opérations sur les limites}

On suppose connues les limites de deux fonctions $f$ et $g$.

            \subsubsection{Limite de $f+g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'$ & $l+l'$ & $+\infty$ & $-\infty$ \\
  \hline
  $+\infty$ & $+\infty$ & $+\infty$ & IND \\
  \hline
  $-\infty$ & $-\infty$ & IND & $-\infty$\\
  \hline
\end{tabular}
\end{center}

\exemple{
 $$\lim_{x \rightarrow +\infty} (x+\sqrt{x}) = +\infty$$}

\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par somme, $\displaystyle{\lim_{x\rightarrow +\infty} (x+\sqrt{x})=+\infty}$.}
\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}



            \subsubsection{Limite de $f\times g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l\neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'\neq 0$ & $l.l'$ & signe($l'$).$\infty$ & -signe($l'$).$\infty$ \\
  \hline
  $+\infty$ & signe($l$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $-\infty$ & -signe($l$).$\infty$ & $-\infty$ & $+\infty$\\
  \hline
\end{tabular}
\end{center}

\remarque{
Si $l=0$ (et/ou $l'=0$), seul le résultat $\lim(fg)=l.l'=0$ est déterminé. Tout le reste ("$0\times \infty$") est \textbf{indéterminé}. }

\exemple{
$$\lim_{x\rightarrow +\infty} (x\sqrt{x})=+\infty$$
$$\lim_{x\rightarrow 0}  3xe^x =0$$}

\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par produit, $\displaystyle{\lim_{x\rightarrow +\infty} (x+\sqrt{x})=+\infty}$.\\
De même, $\displaystyle{\lim_{x\rightarrow 0} 3x=0}$ et $\displaystyle{\lim_{x\rightarrow 0} e^x = e^0=1}$. Par produit, $\displaystyle{\lim_{x\rightarrow 0} 3xe^x = 0}$.
}
\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

            \subsubsection{Limite de $\frac{f}{g}$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'\neq 0$ & $\frac{l}{l'}$ & signe($l'$).$\infty$ & -signe($l'$).$\infty$ \\
  \hline
  $+\infty$ & $0$ & IND & IND \\
  \hline
  $-\infty$ & 0 & IND & IND\\
  \hline
\end{tabular}
\end{center}

Si $\lim g = 0$, il faut tout d'abord préciser si $\lim g = 0^+$ ($g$ tend vers $0$ en restant positif) ou si $\lim g=0^-$, et on applique :

\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $0$ & $l \neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $0^+$ & IND & signe($l$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $0^-$ & IND & -signe($l$).$\infty$ & $-\infty$ & $+\infty$ \\
  \hline
\end{tabular}
\end{center}

\exemple{
$$\lim_{x \rightarrow +\infty} \frac{1+\frac{1}{x}}{\sqrt{x}}=0$$
$$\lim_{x \rightarrow 0^+} \frac{x^2+1}{x}=+\infty$$
}

%\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par somme, $\displaystyle{\lim_{x\rightarrow +\infty} (x+\sqrt{x})=+\infty}$.}
%\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}


\subsection{Limite d'une fonction composée}

\theoreme{Soient $f,g,h$ trois fonctions telles que $f(x)=g(h(x))$ sur un intervalle $I$. Soient $a,b,c$ des éléments de $\R\cup \{+\infty;-\infty\}$.\\
Si $\displaystyle{\lim_{x\rightarrow a} h(x)=b}$ et $\displaystyle{\lim_{x\rightarrow b} g(x)=c}$, alors
$$\lim_{x\rightarrow x} f(x)=c$$
}

\preuve{Admis.}

\methode{Pour déterminer la limite d'une fonction composée $f(x)=g(h(x))$ en $x_0$ :
\begin{itemize}
    \item[$\bullet$] On pose $X=h(x)$.
    \item[$\bullet$] On détermine la limite $b$ de $X$ en $x_0$.
    \item[$\bullet$] On détermine la limite $c$ de $g$ en $b$, et on conclut : la limite de $f$ en $x_0$ vaut $c$.
\end{itemize}}

\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow \textcolor{blue}{+\infty}} \sqrt{x^2-x+1}}$.}

%\begin{prof}
\solution{
\begin{itemize}
    \item[$\bullet$] On pose $X=x^2-x+1$. On a 
    $$\lim_{x\rightarrow \textcolor{blue}{+\infty}} X = \textcolor{magenta}{+\infty}$$
    \item[$\bullet$] On a $$\lim_{X\rightarrow \textcolor{magenta}{+\infty}} \sqrt{X} = \textcolor{red}{+\infty}$$
\end{itemize}
Par composée, on a donc 
$$\lim_{x\rightarrow \textcolor{blue}{+\infty}} \sqrt{x^2-x+1} = \textcolor{red}{+\infty}$$
}
%\end{prof}
 
% \begin{eleve}
% \begin{solu_eleve}
% \notes[1.8ex]{5}{\textwidth}
% \end{solu_eleve}
% \end{eleve}

\exercice{Montrer que $\displaystyle{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty}$.}

%\begin{prof}
%\solution{Posons $X=3x-1$. Alors :
%\begin{itemize}
%    \item[$\bullet$] On a $\displaystyle{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} 3x-1=0^+}$.
%    \item[$\bullet$] De plus, $\displaystyle{\lim_{X\rightarrow 0^+} \frac{1}{\sqrt{X}} = +\infty}$.
%\end{itemize}
%Par composée,
%$$\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty$$
%}
%\end{prof}


% \begin{eleve}
% \begin{solu_eleve}
% \notes[1.8ex]{5}{\textwidth}
% \end{solu_eleve}
% \end{eleve}


    
        \subsection{Limites usuelles}
        
            \subsubsection{Limites classiques}
            
A retenir : ($n \in \N$)

$$\lim_{x\rightarrow +\infty} x^n = +\infty$$
$$\lim_{x\rightarrow -\infty} x^n = +\infty \textrm{ si $n$ est pair, } -\infty \textrm{ sinon.}$$
$$\lim_{x\rightarrow +\infty} \frac{1}{x^n} = \lim_{x\rightarrow -\infty} \frac{1}{x^n} =0$$
$$\lim_{x\rightarrow +\infty} \sqrt{x} = \lim_{x\rightarrow +\infty} \ln(x)=\lim_{x\rightarrow +\infty} e^x = +\infty$$
$$\lim_{x\rightarrow 0} \ln(x)=-\infty,~~~~\lim_{x\rightarrow -\infty} e^x=0$$
$$\forall~\alpha>0,~\lim_{x\rightarrow +\infty} x^\alpha=+\infty \textrm{ et } \lim_{x\rightarrow 0} x^\alpha=0$$

            \subsubsection{Croissances comparées}
         
\theoreme{Pour tout $\alpha>0$, et $q>0$ $$\lim_{x\rightarrow +\infty} \frac{x^\alpha}{e^x}=0 \textrm{ et } \lim_{x\rightarrow +\infty} \frac{\ln^q(x)}{x^\alpha} = 0$$}

\consequence{Par passage à l'inverse,
$$\lim_{x\rightarrow +\infty} \frac{e^x}{x^\alpha}=+\infty \textrm{ et } \lim_{x\rightarrow +\infty} \frac{x^\alpha}{\ln(x)} = +\infty$$}

\theoreme{Pour tout $\alpha>0$, $$\lim_{x\rightarrow 0^+} x^\alpha \ln(x) =0$$}

\preuve{Posons $X=\frac{1}{x}$. Alors $\displaystyle{\lim_{x\rightarrow 0^+} X =+\infty}$ et $\displaystyle{\lim_{x\rightarrow 0^+} x^\alpha \ln(x)=\lim_{X\rightarrow +\infty} -\frac{\ln(X)}{X^\alpha} = 0}$ d'après ce qui précède.}

\theoreme{Pour tout $n\in \N$, on a $$\lim_{x\rightarrow -\infty} x^ne^x=0$$}

\preuve{Se démontre de la même manière que précédemment (en posant $X=-x$).}


Pour utiliser les croissances comparées, il faut souvent faire un changement de variable pour s'y ramener. 

\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} \frac{e^{2x}}{x^3}}$. On pose $X=2x$. Alors $$\lim_{x\rightarrow +\infty} X = +\infty \textrm{ et } 
\lim_{x\rightarrow +\infty} \frac{e^{2x}}{x^3}=\lim_{X\rightarrow +\infty} \frac{e^{X}}{(X/2)^3}=\lim_{X\rightarrow +\infty} 2^3\frac{e^{X}}{X^3}=+\infty
$$}

       \subsection{Quelques indéterminations classiques}

        \subsubsection{Polynômes et fractions rationnelles}
        
\methode{En $+\infty$ ou en $-\infty$, il y a une méthode classique dite du terme du plus haut degré.
 \begin{itemize}
    \item[$\bullet$] Si $f :x \mapsto a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0$ est un polynôme, alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} a_nx^n}$.
    \item[$\bullet$] Si $g: x \mapsto \frac{a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0}{b_kx^k+b_{k-1}x^{k-1}\cdots + b_0}$ est une fraction rationnelle, alors $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=\lim_{x\rightarrow +\infty} \frac{a_nx^n}{b_kx^k}}$.
\end{itemize}  
Par exemple, si $f:x\mapsto 2x^3-3x^2+6x-1$ alors $$\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} 2x^3=+\infty$$
Si $g:x\mapsto\frac{x^2+1}{2x^2-3x+1}$ alors $$\lim_{x\rightarrow -\infty} g(x)=\lim_{x\rightarrow -\infty} \frac{x^2}{2x^2}=\frac{1}{2}$$}


        \subsubsection{Racines}
       
\methode{Lorsqu'une fonction contient des radicaux, on utilise la quantité conjugué.~\\Par exemple, si $f:x\mapsto \sqrt{x^2+1}-x$ alors la limite en $+\infty$ de $f$ est indéterminée. Alors,
 $$f(x)=(\sqrt{x^2+1}-x)\frac{\sqrt{x^2+1}+x}{\sqrt{x^2+1}+x}=\frac{1}{\sqrt{x^2+1}+x}$$ et donc 
 $$\lim_{x\rightarrow +\infty} f(x)=0$$ 
 }

        \subsubsection{$\frac{\infty}{\infty}$ ou $\frac{0}{0}$}
     
 Dans les cas $\frac{\infty}{\infty}$ ou $\frac{0}{0}$, on commence par mettre au numérateur et au dénominateur le terme prépondérant (en se souvenant des croissances comparées).
 
 \exemple{Soit $f:x\mapsto \frac{x+1}{e^x-1}$. Alors $$\frac{x+1}{e^{x}-1}=\frac{x}{e^x} \frac{1+\frac{1}{x}}{1-e^{-x}}$$
Or $$\lim_{x\rightarrow +\infty} \frac{x}{e^x}=0 \textrm{ et } \lim_{x\rightarrow +\infty} \frac{1+\frac{1}{x}}{1-e^{-x}}=1$$
Par produit $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=0}$.}

\section{Etude des limites d'une fonction}

    \subsection{1ère étape : limites}
    
Lorsqu'on se donne une fonction, on commencera toujours par déterminer ses limites au borne de l'intervalle de définition :
\begin{itemize}
    \item[$\bullet$] Si $f$ est définie sur $\mathbb{R}$, on déterminera $\displaystyle{\lim_{x \rightarrow +\infty} f}$ et $\displaystyle{\lim_{x\rightarrow-\infty} f}$.
    \item[$\bullet$] Si $f$ est définie sur $]-\infty;a[\cup]a;+\infty[$, il faut déterminer $4$ limites : en $+\infty$, $-\infty$, $a^+$ et $a^-$.
\end{itemize}

On notera directement les asymptotes horizontales (limite finie en $+\infty$ ou $-\infty$) et les asymptotes verticales (limite infinie en $a^+$ ou $a^-$).

    \subsection{2ème étape : branches infinies}
    
Si les limites en $+\infty$ et/ou $-\infty$ sont infinies, on cherche une éventuelle asymptote oblique. Pour cela on détermine
$$\lim_{x\rightarrow +\infty} \frac{f(x)}{x}$$
\begin{itemize}
    \item[$\bullet$] Si cette limite est nulle, on dit que la courbe de $f$ possède une branche parabolique de direction l'axe des abscisses en $+\infty$.
    \item[$\bullet$] Si cette limite est infinie, on dit que la courbe de $f$ possède une branche parabolique de direction l'axe des ordonnées en $-\infty$.
    \item[$\bullet$] Si cette limite est un nombre réel $a$, on dit que la courbe de $f$ admet une direction asymptotique d'équation $y=ax$. Il reste alors à calculer $\displaystyle{\lim_{x\rightarrow +\infty} (f(x)-ax)}$ .
        \begin{itemize}
                \item Si cette limite est un réel $b$, la droite d'équation $y=ax+b$ est asymptote oblique à la courbe de $f$ en $+\infty$.
                \item Si cette limite est infinie, on dit que la courbe de $f$ possède une branche parabolique de direction la droite d'équation $y=ax$.
        \end{itemize}
\end{itemize}
(bien sûr, tout ceci est valable en $-\infty$.)


