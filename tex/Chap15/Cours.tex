\chapter{Etude élémentaire des séries}

\objectifintro{Dans ce chapitre, on introduit la notion de série, qui est à rapprocher de l'intégrale généralisée, mais dans le cas d'une suite. On y verra des méthodes de calculs, mais aussi des théorèmes pour montrer des convergences ou divergences de séries, sans pouvoir nécessairement calculer la somme de la série.}

\begin{extrait}{Georg Wilhelm Friedrich Hegel (1770--1831). \emph{Encyclopédie des sciences philosophiques}}
L'homme n'est rien d'autre que la série de ses actes.
\end{extrait}


\begin{objectifs}
	
\begin{numerote}
	\item Connaître la notion de série :
			\begin{itemize}[label=\textbullet]
				\item \lienobj{1}{connaître la définition d'une série, d'une somme et du reste d'une série}
				\item \lienobj{2}{connaître les différentes opérations usuelles}
				\item \lienobj{3}{connaître le lien entre suite et série}
				\item \lienobj{4}{connaître la condition nécessaire de convergence}
				\item \lienobj{5}{connaître les séries de référence}
			\end{itemize}
	\item Concernant les théorèmes de convergence :
			\begin{itemize}[label=\textbullet]
				\item \lienobj{6}{connaître le théorème de comparaison}
				\item \lienobj{7}{connaître le théorème d'équivalence et de négligeabilité}
			\end{itemize}
	\item Concernant l'absolue convergence :
			\begin{itemize}[label=\textbullet]
				\item \lienobj{8}{connaître la définition}
				\item \lienobj{9}{connaître le lien entre absolue convergence et convergence}
			\end{itemize}
\end{numerote}
\end{objectifs}

\section{Définition}

    \subsection{Séries}

\begin{definition}
\labelobj{1}
Soit $(u_n)$ une suite réelle. On appelle \textbf{série de terme général} $u_n$, et on note $\displaystyle{\sum_{n\geq 0} u_n}$ ou plus simplement $\sum u_n$, la suite des sommes partielles $(S_n)$ définie par $$S_n=\sum_{k=0}^n u_k$$ 
\end{definition}

\begin{remarque}
Si la suite $(u_n)$ n'est définie qu'à partir d'un certain rang $n_0$, la série de terme général $u_n$ n'est également définie qu'à partir de $n_0$, ce que l'on note $\displaystyle{\sum_{n\geq n_0} u_n}$. La suite des sommes partielles est $(S_n)_{n\geq n_0}$, avec $\displaystyle{S_n=\sum_{k={n_0}}^n u_k}$.
\end{remarque}

\begin{exemple}
Soit $u$ la suite définie pour tout $n\geq 1$ par $u_n=\frac{1}{n}$. La série de terme général $u_n$ est notée $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$ est appelée la \textbf{série harmonique}.
\end{exemple}

\begin{exo}
Déterminer les premiers termes de la suite des sommes partielles de la série harmonique.
\end{exo}

\solution[2]{On note $(S_n)_{n\geq 1}$ la suite des sommes partielles associées à la série harmonique, c'est-à-dire $\ds{\forall~n\geq 1,\,S_n=\sum_{k=1}^n \frac1k}$. Alors on a
\[ S_1=1,\quad S_2=1+\frac12=\frac32,\quad S_3=1+\frac12+\frac13=\frac{11}{6}, \quad S_4=1+\frac12+\frac13+\frac14=\frac{25}{12} \]
}

      
    \subsection{Convergence}

La série $\sum u_n$ étant une suite, on peut s'intéresser à sa convergence.

\begin{definition}
Soit $(u_n)$ une suite réelle. On dit que la série $\sum u_n$ \textbf{converge} si la suite des sommes partielles $(S_n)$ converge. Dans ce cas :
\begin{itemize}[label=\textbullet]
	\item la limite de la suite $(S_n)$ est alors appelée \textbf{somme} de la série, et est notée $\ds{\sum_{k=0}^{+\infty} u_k}$. On a ainsi
$$\sum_{k=0}^{+\infty} u_k = \lim_{n\rightarrow +\infty} \sum_{k=0}^n u_k$$
	\item on appelle \textbf{reste} de la série la suite $(R_n)$ définie par \[R_n=\sum_{k=n+1}^{+\infty} u_k = \sum_{k=0}^{+\infty} u_k-S_{n} \]
\end{itemize} 
\end{definition}


\begin{attention}
L'écriture $\displaystyle{\sum_{k=0}^{+\infty} u_k}$ n'a de sens que si la série converge, alors que l'écriture $\sum u_n$ a bien un sens, puisqu'elle désigne une suite.
\end{attention}

\begin{remarque}
Les sommes infinies ne se manipulent pas comme les sommes finies (puisqu'en réalité, ce sont des limites, et il faut donc toujours s'assurer de la convergence). C'est pourquoi on calculera (presque) toujours les sommes partielles, qui sont des sommes finies, avant de passer à la limite.
\end{remarque}

    \subsection{Premiers exemples}

\begin{exemple}
Soit $(u_n)$ la suite définie pour tout $n$ par $\ds{u_n=\left(\frac{1}{2}\right)^n}$. Etudier la série $\displaystyle{\sum u_n}$.
\end{exemple}

\solution[10]{
Notons $\displaystyle{S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k}$. Alors 

$$\forall~n,~S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k=\frac{1-\left(\frac{1}{2}\right)^{n+1}}{1-\frac{1}{2}} = 2\left(1-\left(\frac{1}{2}\right)^{n+1}\right)$$

Puisque $-1< \frac{1}{2} < 1$, on a $\displaystyle{\lim_{n\rightarrow +\infty} \left(\frac{1}{2}\right)^{n+1}=0}$. Par somme et produit, on en déduit donc que la série $\displaystyle{\sum_{n\geq 0} \left(\frac{1}{2}\right)^n}$ converge, et on a $$\lim_{n\rightarrow +\infty} S_n=\sum_{k=0}^{+\infty} \left(\frac{1}{2}\right)^k=2$$}


\begin{exemple}
Montrer que la série harmonique, de terme général $\frac{1}{n}$, $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$, est divergente.
\end{exemple}

\solution[10]{Pour tout $n\geq 1$, notons $H_n=\displaystyle{\sum_{k=1}^n \frac{1}{n}}$. 
\\Nous avons vu dans le chapitre sur le calcul différentiel que l'on a, pour tout $k\geq 1$, $$\ln(k+1)-\ln(k)\leq \frac{1}{k}$$
En additionnant ces inégalités, on obtient alors
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) \leq \sum_{k=1}^n \frac{1}{k}=H_n$$
Or, on a
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) =  \ln(n+1)-\ln(1)=\ln(n+1) \textrm{, les termes se téléscopant.}$$
Puisque $\displaystyle{\lim_{n\rightarrow +\infty} \ln(n+1)=+\infty}$, par comparaison, on en déduit que
$$\lim_{n\rightarrow +\infty} \sum_{k=1}^n \frac{1}{k} = +\infty$$
}	

\afaire{Exercice \lienexo{1}}

\section{Propriétés}

    \subsection{Opérations sur les séries}

Les opérations sur les sommes finies se transposent, dans certains cas, aux séries :
\begin{theoreme}[Linéarité]
\labelobj{2}
Soient $(u_n)$ et $(v_n)$ deux suites réelles, et $\lambda$ un réel non nul.
\begin{itemize}
    \item[$\bullet$] Les séries $\sum u_n$ et $\sum \lambda u_n$ sont de même nature (c'est-à-dire qu'elles sont soit toutes les deux convergentes, soit toutes les deux divergentes). Si elles sont convergentes, on a alors
    \[\sum_{k=0}^{+\infty} \lambda u_k= \lambda \sum_{k=0}^{+\infty} u_k\]
    \item[$\bullet$] Si les séries $\sum u_n$ et $\sum v_n$ sont toutes les deux convergentes, alors la série $\sum (u_n+v_n)$ est également convergente, et on a
    \[\sum_{k=0}^{+\infty} (u_k+v_k)=\sum_{k=0}^{+\infty} u_k+\sum_{k=0}^{+\infty} v_k\]
\end{itemize}
\end{theoreme}

\begin{attention}
La réciproque du deuxième point n'est pas vraie. Par exemple, si pour tout $n\geq 1$, $u_n=\frac{1}{n}$ et $v_n=-\frac{1}{n}$, alors la série $\sum u_n+v_n$ converge (vers $0$) alors que ni $\sum u_n$ ni $\sum v_n$ ne convergent.

Il faudra donc toujours s'assurer que les séries convergent avant de séparer les sommes.
\end{attention}

    \subsection{Suite et série}

\begin{theoreme}[Théorème suite série]
\labelobj{3}
Soit $(u_n)$ une suite. Alors $(u_n)$ converge si, et seulement si, la série $\sum (u_{n+1}-u_n)$ converge. Dans ce cas, en notant $\ell$ la limite de $(u_n)$, on a $$\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0$$
\end{theoreme}

\preuve[4]{Notons $\displaystyle{S_n=\sum_{k=0}^n (u_{k+1}-u_k)}$. On constate que $S_n=u_{n+1}-u_0$  par telescopage. Ainsi, $(S_n)$ converge si et seulement si $(u_{n})$ converge. Si $(u_n)$ converge vers $\ell$, par passage à la limite, on obtient bien $\displaystyle{\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0}$.}


\section{Conditions de convergence}

    \subsection{Limite de la suite et convergence}
    
\begin{theoreme}
\labelobj{4}
Soit $(u_n)$ une suite réelle. Si la série $\sum u_n$ est convergente, alors $\displaystyle{\lim_{n\rightarrow +\infty} u_n=0}$.
\end{theoreme}

\preuve[5]{Pour tout $n$, notons $\displaystyle{S_n=\sum_{k=0}^n u_k}$. Alors, pour tout $n\geq 1$, on a $u_n=S_n-S_{n-1}$. Si la série $\sum u_n$ converge, alors la suite $(S_n)$ admet, par définition, une limite que l'on note $\ell$. Mais alors
$$\lim_{n\rightarrow +\infty} S_n=\lim_{n\rightarrow +\infty} S_{n-1}=\ell$$
et donc
$$\lim_{n\rightarrow +\infty} u_n = \ell-\ell = 0$$}    

\begin{remarque}
La contraposée du théorème est intéressante : si la suite $(u_n)$ ne converge pas vers $0$, alors la série $\sum u_n$ n'est pas convergente. Par exemple, la série $\displaystyle{\sum \frac{2n}{n+1}}$ diverge, car son terme général ne tend pas vers $0$.
\end{remarque}

\begin{attention}
Cette condition est nécessaire, mais pas suffisante : en effet, on a $\displaystyle{\lim_{n\rightarrow +\infty} \frac{1}{n}=0}$ et pourtant la série harmonique $\displaystyle{\sum_{n\geq 1} \frac{1}{n}}$ diverge.
\end{attention}
   
\begin{exemple}
Soit $q$ un réel. On s'intéresse à la série $\sum q^n$. Alors, pour que la série $\sum q^n$ converge, il faut que $\displaystyle{\lim_{n\rightarrow +\infty} q^n =0}$, c'est à dire $|q|<1$. On verra plus tard que la réciproque, dans ce cas, est vraie.
\end{exemple}

    \subsection{Séries à termes positifs}

Le cas des séries à termes positifs est plus simple à étudier.

\begin{theoreme}
Soit $(u_n)$ une suite à termes positifs. Alors la série $\sum u_n$ est convergente, si et seulement si, la suite des sommes partielles $(S_n)$ est majorée.
\end{theoreme}

\preuve[3]{Notons $\displaystyle{S_n=\sum_{k=0}^n u_k}$. On a alors $S_{n+1}-S_n=u_{n+1}\geq 0$ : la suite $(S_n)$ est donc croissante. D'après les théorèmes sur les suites monotones, $(S_n)$ converge si et seulement si la suite $(S_n)$ est majorée.}

\section{Séries de référence}
\labelobj{5}

    \subsection{Séries géométriques et dérivées}
    
\begin{definition}[Série géométrique]
Pour tout entier $p$, la série $\ds{\sum_{n\geq p} q^n}$ s'appelle \textbf{série géométrique} de raison $q$.
\end{definition}

\begin{theoreme}
La série $\sum q^n$ est convergente si et seulement si $|q|<1$. Dans ce cas, 
$$\sum_{n=0}^{+\infty} q^n=\frac{1}{1-q}$$
\end{theoreme}

\begin{remarque}
Plus généralement, la série $\displaystyle{\sum_{n\geq p} q^n}$ est convergente si et seulement si $|q|<1$, et dans ce cas, $\displaystyle{\sum_{n=p}^{+\infty} q^n=\frac{q^p}{1-q}}$
\end{remarque}

\preuve[5]{La suite $(q^n)$ converge vers $0$ si et seulement si $|q|<1$. Par condition nécessaire de convergence, la série $\sum q^n$ ne peut pas converger si $|q|\geq 1$.\\
Suppsons alors que $|q|<1$. Notons $\displaystyle{S_n=\sum_{k=0}^n q^k}$. On a $\displaystyle{S_n=\frac{1-q^{n+1}}{1-q}}$. Or, $\displaystyle{\lim_{n\rightarrow +\infty} q^{n+1}=0}$ car $|q|<1$. Donc la suite $(S_n)$ converge vers $\frac{1}{1-q}$ : la série converge, et sa somme vaut $\frac{1}{1-q}$.}

On dispose également de deux séries, appelées séries géométriques dérivées première et deuxième :
\begin{theoreme}[Série géométrique dérivée]
    \begin{itemize}
        \item[$\bullet$] \textbf{Série géométrique dérivée première}\\ La série $\displaystyle{\sum_{n\geq 1} nq^{n-1}}$ converge si et seulement si $|q|<1$. Dans ce cas, $$\sum_{n=1}^{+\infty} nq^{n-1} = \frac{1}{(1-q)^2}$$
        \item[$\bullet$] \textbf{Série géométrique dérivée seconde}\\ La série $\displaystyle{\sum_{n\geq 2} n(n-1)q^{n-2}}$ converge si et seulement si $|q|<1$. Dans ce cas, $$\sum_{n=2}^{+\infty} n(n-1)q^{n-2} = \frac{2}{(1-q)^3}$$
        
    \end{itemize}
\end{theoreme}


\preuve[15]{Démontrons le premier résultat. Si $|q|\geq 1$, la suite $(nq^{n-1})$ ne converge pas vers $0$, donc la série $\displaystyle{\sum_{n\geq 1} nq^{n-1}}$ ne peut converger.\\
Pour tout $x \in ]-1;1[$, notons $\displaystyle{T_n(x)=\sum_{k=0}^{n} x^{k}}$. $T_n$ est une fonction dérivable sur $]-1;1[$, et on a
$$T'_n(x)=\sum_{k=1}^{n} kx^{k-1}$$
D'autre part, $\displaystyle{T_n(x)=\frac{1-x^{n+1}}{1-x}}$. On a donc également
$$T'_n(x)=\frac{-(n+1)x^{n}(1-x)-(1-x^{n+1})(-1)}{(1-x)^2}=\frac{1-(n+1)x^{n}+nx^{n+1}}{(1-x)^2}$$
Puisque $\displaystyle{\lim_{n\rightarrow +\infty} (n+1)x^{n}=\lim_{n\rightarrow +\infty} nx^{n+1}=0}$ (car $|x|<1$ et par croissances comparées), on en déduit que $\displaystyle{\lim_{n\rightarrow +\infty} T'_n(x)=\frac{1}{(1-x)^2}}$.\\
Ainsi, la série $\displaystyle{\sum_{n\geq 1} nx^{n-1}}$ converge, et on a bien $$\sum_{n=1}^{+\infty} nx^{n-1} = \frac{1}{(1-x)^2}$$
}

\begin{remarque}
On remarque que $nq^n = q nq^{n-1}$ et que $n(n-1)q^n=q^2 n(n-1)q^{n-2}$. Ainsi, si $|q|<1$, les séries $\sum nq^n$ et $\sum n(n-1)q^n$ convergent également, et 
$$\sum_{n=1}^{+\infty} nq^n = \frac{q}{(1-q)^2} \textrm{ ~~~ et ~~~  } \sum_{n=2}^{+\infty} n(n-1)q^n = \frac{2q^2}{(1-q)^3}$$
\end{remarque}
    
    \subsection{Séries de Riemann}
    
\begin{definition}[Série de Riemann]
La série de terme général $\frac{1}{n^\alpha}$ ($\alpha \in \R$) est appelée \textbf{série de Riemann}.     
\end{definition}

\begin{theoreme}
La série de Riemann $\displaystyle{\sum_{n\geq 1} \frac{1}{n^\alpha}}$ converge si et seulement si $\alpha >1$.
\end{theoreme}

\preuve{Théorème admis.}

\begin{remarque}
Même si la série converge, on ne connait pas explicitement la valeur de la somme $\displaystyle{\sum_{n\geq 1} \frac{1}{n^\alpha}}$, sauf dans certains rares cas.
\end{remarque}

\afaire{Exercice \lienexo{8}}
    
    \subsection{Série exponentielle}    
    
\begin{definition}[Série exponentielle]
La série de terme général $\frac{x^n}{n!}$ ($x\in \R$) est appelée série exponentielle.
\end{definition}

\begin{theoreme}
Pour tout réel $x$, la série exponentielle \[\sum \frac{x^n}{n!}\] converge, et on a $$\sum_{n= 0}^{+\infty} \frac{x^n}{n!}=\E^x$$
\end{theoreme}

\preuve{Théorème admis.}     

\begin{methode}
Pour déterminer si une série converge ou non, et éventuellement calculer sa limite, on essaiera si possible de se ramener à une des séries usuelles (géométriques, Riemann ou exponentielle).	
\end{methode}

\begin{exemple}
Déterminer la nature de la série de terme générale $u_n=\frac{(-3)^{n+1}}{n!}$.
\end{exemple}

\solution[6]{Remarquons tout d'abord que $$u_n=\frac{(-3)^n(-3)}{n!}=-3\frac{(-3)^n}{n!}$$
La série $\displaystyle{\sum_{n\geq 0} \frac{(-3)^n}{n!}}$ converge puisqu'il s'agit de la série exponentielle, et sa somme vaut $\eu{-3}$. Par produit, la série $\displaystyle{\sum_{n\geq 0} u_n}$ converge, et 
$$\sum_{n=0}^{+\infty} \frac{(-3)^{n+1}}{n!} = -3\eu{-3}$$
}	

\begin{remarque}
Par décalage d'indice, on a également, pour tout réel $x$, $\displaystyle{\sum_{n\geq 1} \frac{x^{n-1}}{(n-1)!}}$ converge également, et $$\sum_{n=1}^{+\infty} \frac{x^{n-1}}{(n-1)!} = \E^x$$
et de manière plus générale
$$\sum_{n=p}^{+\infty} \frac{x^{n-p}}{(n-p)!} = \E^x$$
\end{remarque}

\afaire{Exercice \lienexo{4}}


\section{Théorèmes de convergence}

\subsection{Théorème de comparaison}

Pour majorer $(S_n)$, on commence en général par majorer $(u_n)$. On somme alors ces majorants pour en déduire un majorant de $(S_n)$. On dispose ainsi du théorème suivant :

\begin{theoreme}[Théorème de comparaison]
\labelobj{6}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que pour tout $n$,
$$0 \leq u_n \leq v_n$$
Alors, si la série $\sum v_n$ est convergente, la série $\sum u_n$ est également convergente. Dans ce cas, 
$$\sum_{k=0}^{+\infty} u_k \leq \sum_{k=0}^{+\infty} v_k$$
\end{theoreme}

\preuve[8]{Si on note $\displaystyle{S_n=\sum_{k=0}^n u_k}$ et $\displaystyle{T_n=\sum_{k=0}^n v_k}$, on a, pour tout $n$, $S_n \leq T_n$ (addition des inégalités). De plus, la suite $(T_n)$ est également croissante, de limite $T$. Donc pour tout $n$, $T_n\leq T$. Donc
$$\forall~n, S_n \leq T_n \leq T$$
La suite $(S_n)$ est donc majorée, et d'après le théorème précédent, la série $\sum u_n$ converge. L'inégalité précédente donne alors 
$$\sum_{n=0}^{+\infty} u_n=\lim_{n\rightarrow +\infty} S_n \leq T=\lim_{n\rightarrow +\infty} T_n=\sum_{n=0}^{+\infty} v_n$$}

\begin{exemple}
Soit $(u_n)$ une suite à termes positifs vérifiant, pour tout $n$, $\displaystyle{u_n\leq \frac{1}{2^n}}$. Puisque la série $\displaystyle{\sum \left(\frac{1}{2}\right)^n}$ est une série convergente, la série $\sum u_n$ est donc convergente, et on a $\displaystyle{\sum_{n=0}^{+\infty} u_n\leq 2}$.
\end{exemple}
    
On dispose également d'un critère de divergence :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites à \textbf{termes positifs}. On suppose que pour tout $n$, $0\leq u_n \leq vn$. Alors, si la série $\sum u_n$ diverge vers $+\infty$, alors la série $\sum v_n$ diverge également vers $+\infty$.
\end{theoreme}

\begin{exemple}
Soit $(u_n)$ une suite à termes positifs vérifiant pour tout entier $n\geq 1$, $u_n\geq \frac{1}{n}$. Alors, puisque la série $\sum \frac{1}{n}$ est divergente, la série $\sum u_n$ est également divergente.
\end{exemple}

\afaire{Exercices \lienexo{2} et \lienexo{3}}


		\subsection{Equivalence et négligeabilité}
\labelobj{7} 		
On peut enfin utiliser les équivalents :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n \equi_{+\infty} v_n}$. 
Alors, la série $\sum v_n$ est convergente si et seulement si la série $\sum u_n$ est également convergente.	
\end{theoreme}

\begin{exemple}
Montrer que $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.
\end{exemple}

\solution[4]{Remarquons que \[ \frac{1}{2^n+3^n} \sim \frac{1}{3^n}\]
Puisque les deux suites sont à termes positifs, et que la série $\ds{\sum \frac{1}{3^n}}$ converge (série géométrique), on en déduit que par équivalent, la série $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.
} 	

On dispose également d'un critère en cas de négligeabilité :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n =\mathrm{o}_{+\infty}(v_n)}$. 
Si la série $\sum v_n$ est convergente, alors la série $\sum u_n$ est également convergente.	
\end{theoreme}


\begin{exemple}
Montrer que $\ds{\sum_{n\geq 0} \eu{-n^2}}$ est une série convergente.
\end{exemple}


\solution[5]{Remarquons que $\ds{\eu{-n^2}=\petito{\frac{1}{n^2}}}$. En effet, \[ \mylim[n]{+\infty}{\frac{\eu{-n^2}}{1/n^2}}=\mylim[n]{+\infty}{n^2\eu{-n^2}} = 0 \text{ par croissances comparées}\]	
Les deux suites $(\eu{-n^2})$ et $\left(\frac{1}{n^2}\right)$ sont positives, et la série $\sum \frac{1}{n^2}$ est convergente (Riemann). Par comparaison de série à termes positifs, on en déduit que la série $\sum \eu{-n^2}$ converge.}

    
\section{Convergence absolue}

    \subsection{Définition}
    
\begin{definition}
\labelobj{8}
Soit $(u_n)$ une suite réelle. On dit que la série $\sum u_n$ est \textbf{absolument convergente} si la série $\sum |u_n|$ est convergente.
\end{definition}

\begin{exemple}
La série $\displaystyle{\sum_{n\geq 1} \frac{(-1)^n}{n^2}}$ est absolument convergente : en effet, la série $\displaystyle{\sum_{n\geq 1} \left|\frac{(-1)^n}{n^2}\right| = \sum_{n\geq 1} \frac{1}{n^2}}$ est convergente (série de Riemann).
\end{exemple}
    
    \subsection{Absolue convergence et convergence}
    
\begin{theoreme}
\labelobj{9}
Soit $(u_n)$ une suite réelle. Si la série $\sum u_n$ est absolument convergente, alors elle est convergente.
\end{theoreme}

\begin{remarque}
Pour démontrer qu'une série de signe quelconque est convergente, il peut ainsi être judicieux de montrer qu'elle est absolument convergente, et se ramener donc à une série à termes positifs.
\end{remarque}
    
    \subsection{Convergence et absolue convergence}

\begin{remarque}
La réciproque n'est pas vraie : une série peut être convergente sans être absolument convergente (on dit que la série est \textbf{semi-convergente}). ~\\Par exemple, la série $\displaystyle{\sum_{n\geq 1} \frac{(-1)^n}{n}}$ est convergente, mais n'est pas absolument convergente, puisque la série $$\displaystyle{\sum_{n\geq 1} \left| \frac{(-1)^n}{n}\right| = \sum_{n\geq 1} \frac{1}{n}}$$ n'est pas convergente.
\end{remarque}

    \subsection{Etudier une série}

\begin{methode}
	Pour étudier une série $\sum u_n$, on suit différentes étapes :
\begin{enumerate}
    \item On vérifie si la suite $(u_n)$ tend vers $0$. Si non, la série est divergente.
    \item On s'intéresse à la suite $(u_n)$ et on regarde si elle est équivalente, négligeable devant, ou majorée par une suite dont on sait que la série converge (par exemple, parce que c'est une série de référence). Dans ce cas, on peut conclure quant à la convergence (mais pas sur la somme de la série)
    \item Si on demande de déterminer la somme, on pose la suite des sommes partielles $(S_n)$ et on vérifie si on peut la calculer. Si oui, on peut conclure quant à la convergence, et la valeur de la somme le cas échéant.
    \item Si tout ce qui précède n'a pas abouti, on essaie de majorer (ou minorer) les sommes partielles $(S_n)$ si la série est à termes positifs, ou alors on s'intéresse à l'absolue convergence sinon. 
\end{enumerate}
\end{methode}

\begin{remarque}
On ne peut pas forcément calculer la somme de la série, même si on arrive à prouver que la série converge.
\end{remarque}

\begin{exemple}
Soit $u$ la suite définie pour $n\geq 1$ par $\displaystyle{u_n=\frac{1}{n^2(n^2+1)}}$. Etudier la nature de la série $\displaystyle{\sum_{n\geq 1} u_n}$
\end{exemple}

\solution[10]{Plusieurs méthodes pour cette série. 
\begin{enumerate}
	\item \textbf{Théorème de comparaison} : on constate que \[ u_n\sim \frac{1}{n^4} \]
	La suite $(u_n)$ et la suite $\left(\frac{1}{n^4}\right)$ sont des suites à termes positifs, et la série $\ds{\sum \frac{1}{n^4}}$ converge (série de Riemann avec $4>1$). Par équivalence de suite à termes positifs, la série $\ds{\sum_{n\geq 1} u_n}$ converge.
	\item \textbf{Majoration} : on constate que \[ n^2(n^2+1)=n^4+n^2\geq n^2 \text{ donc } 0\leq \frac{1}{n^2(n^2+1)} \leq \frac{1}{n^2} \]
		La suite $(u_n)$ est à termes positifs et majorée par la suite $\left(\frac{1}{n^2}\right)$ dont la série converge (série de Riemann avec $2>1$). Par majoration, la série $\ds{\sum_{n\geq 1} u_n}$ converge.
	\item \textbf{Calcul des sommes partielles} : 
	Remarquons tout d'abord que la suite $(u_n)$ converge vers $0$. On peut écrire $u_n$ sous la forme 
$$u_n=\frac{1}{n^2}-\frac{1}{n^2+1}=v_n-w_n$$
Or, la série $\displaystyle{\sum_{n\geq 1} v_n}$ converge (série de Riemann). De plus, pour tout $n\geq 1$,
$$0<w_n\leq \frac{1}{n^2}=v_n$$
Par comparaison, $(w_n)$ étant à termes positifs et la série $\displaystyle{\sum_{n\geq 1} v_n}$ étant convergente, on en déduit que la série $\displaystyle{\sum_{n\geq 1} w_n}$ converge également.\\
Par somme, la série $\displaystyle{\sum_{n\geq 1} u_n}$ converge également, et 
$$\sum_{n\geq 1} \frac{1}{n^2(n^2+1)} = \sum_{n\geq 1} \frac{1}{n^2} - \sum_{n\geq 1} \frac{1}{n^2+1}$$
\end{enumerate}
}

\afaire{Exercices \lienexo{5}, \lienexo{6} et \lienexo{7}}

