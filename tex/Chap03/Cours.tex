\chapter{Généralités sur les fonctions}

%%%
%%% Courbes metapost à faire
%%% * fonction croissante et décroissante
%%% * majorant/minnorant
%%% * maximum/minimum
%%% * valeur absolue 
%%% * fonction inverse : metapost
%%% * Fonction racine carrée
%%% * Fonction ln (+ tangente en 1)
%%% * Fonctoin exp (+ tangente en 0)
%%% * Comparaison ln/exp / y=x
%%% * FOnctions puissances §alpha >1 / alpha dans 0,1 et alpha <0
%%% * fonction partie entiere : metapost
%%% * max/min de deux fonctions

 
\objectifintro{Dans ce chapitre, on rappelle certaines généralités sur les fonctions, et on ajoute certains compléments permettant d'étudier les fonctions (parité, périodicité) ainsi que de nouvelles fonctions (valeur absolue, partie entière)}

\begin{extrait}{Paolo Giordano (1982 -- ). \emph{Contagions}}
La nature préfère les croissances vertigineuses, ou résolument plus douces, les exposants et les logarithmes. La nature est par nature non linéaire.	
\end{extrait}

\begin{objectifs}

\begin{numerote}
        \item savoir déterminer certaines caractéristiques d'une fonction :

                        \begin{itemize}[label=\textbullet]
                                \item \hyperref[objectif-04-1]{parité et périodicité}\dotfill $\Box$
                                \item \hyperref[objectif-04-2]{les extrema d'une fonction}\dotfill $\Box$
                        \end{itemize}
        \item connaître les fonctions usuelles (variations, dérivées, représentation graphique) :
                        \begin{itemize}[label=\textbullet]
                                \item \hyperref[objectif-04-3]{fonctions affines et trinômes du second degré}\dotfill $\Box$
                                \item \hyperref[objectif-04-4]{fonctions valeur absolue, racine carrée et inverse}\dotfill $\Box$
                                \item \hyperref[objectif-04-5]{fonction partie entière}\dotfill $\Box$
                                \item \hyperref[objectif-04-6]{fonctions $\ln$, $\exp$ et puissances}\dotfill $\Box$
                        \end{itemize}
        % \item savoir étudier complètement une fonction\dotfill $\Box$
\end{numerote}
\end{objectifs}

\section{Généralités sur les fonctions}

 \subsection{Notion de fonction}

\begin{definition}
 On appelle \textbf{fonction} $f$, définie sur un domaine $I$, un objet qui, à partir d'un nombre $x$ de $I$ donné, associe \textbf{une unique image} noté $f(x)$.

 On note  $x \mapsto f(x)$ pour dire ``$x$, on associe le nombre $f(x)$''.
\end{definition}

\begin{exemple}
 \begin{itemize}[label=\textbullet]
 \item Fonction définie graphiquement :
\begin{center}
\includegraphics{tex/Chap03/mp/fonction_graph.mps}
\end{center}
\item Fonction définie par une formule : $g(x) = 3x^2+1$
\item Fonction définie par un tableau :

\[\begin{array}{|c|c|c|c|c|c|}
\hline
x & -2 & -1 & 0 & 1 & 2 \\
\hline
h(x) & 1 & -3 & 2 & \frac{1}{2} & 0 \\
\hline
\end{array}\]

\end{itemize}
\end{exemple}

\begin{definition}
 On appelle \textbf{ensemble de définition} d'une fonction $f$, noté $\DD_f$ en général, l'ensemble de tous les nombres $x$ où on peut calculer $f(x)$ (c'est à dire, où $f(x)$ est définie).
\end{definition}

\begin{exemple}
 Dans les exemples précédents :
 \begin{itemize}[label=\textbullet]
  \item $f$ est définie graphiquement sur $[-3;1]$ : en effet, on ne peut calculer $f(x)$ que si $-3 \leq x \leq 1$.
  \item $g$ est définie sur $\R$ : en effet, pour tout nombre réel $x$, on peut calculer $3x^2+1$, et donc $g(x)$.
  \item $h$ est définie sur $\{-2;-1;0;1;2\}$.
 \end{itemize}
\end{exemple}

\subsection{Courbe représentative}

\begin{definition}
 Soit $f$ une fonction définie sur $\DD_f$. Soit $(O;I;J)$ un repère (en général orthonormé) du plan. La \textbf{courbe représentative} (ou représentation graphique) de $f$, notée $\CC_f$, est l'ensemble des points $(x;f(x))$  où $x$ décrit l'ensemble de définition $\DD_f$ de $f$.

 \begin{center}
 \includegraphics{tex/Chap03/mp/courbe_fonction.mps}
\end{center}
\end{definition}

     \subsection{Parité et imparité}

\label{objectif-04-1}
 Soit $f$ une fonction numérique d'une variable réelle, définie sur $\mathcal{D}_f$, de courbe représentative $\CC_f$.

 \begin{definition}
   $f$ est dite \textbf{paire} si
 \begin{itemize}[label=\textbullet]
     \item $\forall~x\in \DD_f,\quad -x\in \DD_f$ (le domaine de définition est symétrique par rapport à $0$).
     \item $\forall~x \in \DD_f, \quad f(-x)=f(x)$
 \end{itemize}
\end{definition}

\begin{remarque}
  $f$ est paire si et seulement si $\CC_f$ est symétrique par rapport à l'axe des ordonnées.
\begin{center}
	\includegraphics{tex/Chap03/mp/paire.mps}
\end{center}
\end{remarque}

\begin{definition}
$f$ est dite \textbf{impaire} si
 \begin{itemize}[label=\textbullet]
     \item $\forall~x\in \DD_f,\quad -x\in \DD_f$ (le domaine de définition est symétrique par rapport à $0$).
     \item $\forall~x \in \DD_f, \quad f(-x)=-f(x)$
 \end{itemize}
\end{definition}

 \begin{remarque}
$f$ est impaire si et seulement si $\CC_f$ est symétrique par rapport à l'origine.
 \begin{center}
 	\includegraphics{tex/Chap03/mp/impaire.mps}
\end{center}
\end{remarque}

 \begin{exemple}
La fonction carrée $f:x\mapsto x^2$ est une fonction paire; la fonction inverse $x \mapsto \frac{1}{x}$ est une fonction impaire. En effet,
\[\forall~x \in \R,\quad(-x)^2=x^2 \qeq \forall~x \in \R^*,\quad\frac{1}{-x}=-\frac{1}{x} \]
\end{exemple}

\begin{methode}
  Pour déterminer la parité d'une fonction, on procède en deux étapes :
\begin{itemize}[label=\textbullet]
    \item On vérifie que le domaine est bien symétrique par rapport à $0$.
    \item Pour un certain $x$ dans $\DD_f$ on calcule $f(-x)$ et on essaie de retrouver $f(x)$ ou $-f(x)$.
\end{itemize}
\end{methode}

\begin{exemple}
Soit $f:\R \rightarrow \R$ la fonction définie pour tout $x$ par \[f(x)=\eu{-x^2}\] Déterminer la parité de $f$.
\end{exemple}

\solution[5]{Le domaine de définition de $f$ est $\R$ qui est bien symétrique par rapport à $0$. Pour tout $x\in \R$, on a alors
\[f(-x)=\eu{-(-x)^2} = \eu{-x^2}=f(x)\]
La fonction $f$ est donc paire.}

\begin{methode}
  Pour démontrer qu'une fonction n'est ni paire, ni impaire, on exhibe un contre-exemple : on cherche deux réels $x$ et $y$ dans  $\DD_f$ tels que $f(-x)\neq f(x)$ et $f(-y) \neq -f(y)$ (selon le cas, cela peut être le même réel).
\end{methode}

\begin{exemple}
  Montrer que la fonction $f:\R \rightarrow \R$ définie pour tout réel $x$ par $f(x)=x^2+x$ n'est ni paire, ni impaire.
\end{exemple}

\solution[4]{En effet, on constate que, pour $x=1$, on a
\[f(-1)=(-1)^2+(-1)=0\]
alors que $f(1)=1^2+1=2$. Donc $f(-1)\neq f(1)$ et $f(-1)\neq -f(1)$ : la fonction n'est ni paire, ni impaire.}

    \subsection{Périodicité}

Soit $f$ une fonction numérique d'une variable réelle, de courbe représentative $\CC_f$.

\begin{definition}
  On dit que $f$ est \textbf{périodique} de période $T > 0$ si :
\begin{itemize}[label=\textbullet]
    \item $\forall~x \in \DD_f,\quad x+T \in \DD_f$
    \item $\forall~x \in \DD_f,\quad f(x+T)=f(x)$
\end{itemize}
\end{definition}

\begin{remarque}
La courbe représentative d'une fonction périodique n'est donc qu'une répétition de sa représentation sur $[0;T]$. La fonction suivante est périodique, de période $1$ :
\begin{center}
\includegraphics{tex/Chap03/mp/periodique.mps}
\end{center}
\end{remarque}

    \subsection{Sens de variation}

\begin{definition}
  Soit $f$ une fonction définie sur un intervalle $I$ de $\R$, de courbe représentative $\CC_f$ dans un repère.

  ~
  \begin{center}
  \begin{tabular}{p{7cm}|p{7cm}}

   On dit que $f$ est \textbf{croissante} sur $I$ si, pour tous nombres réels $u$ et $v$ de l'intervalle $I$ on a \[\text{Si } u < v \text{ alors } f(u) \leq f(v)\]

    Une fonction croissante conserve l'ordre &    On dit que $f$ est \textbf{décroissante} sur $I$ si, pour tous nombres réels $u$ et $v$ de l'intervalle $I$ on a
   \[\text{Si } u < v \text{ alors } f(u) \geq f(v)\]

   Une fonction décroissante change l'ordre \\
\begin{center}\includegraphics{tex/Chap03/pic/fonccroissante}\end{center} &
\begin{center}\includegraphics{tex/Chap03/pic/foncdecroi}\end{center}
  \end{tabular}
	\end{center}
\end{definition}


\begin{remarque}
 On définit également la croissance stricte et la décroissance stricte. Une fonction $f$ définie sur un intervalle $I$ est dite \textbf{strictement croissante} lorsque pour tout $u,v$ de l'intervalle $I$
\[\text{Si } u<v \text{ alors } f(u) < f(v)\]
\end{remarque}

\begin{propriete}
\begin{itemize}[label=\textbullet]
    \item La somme de deux fonctions croissantes (resp. décroissante) est croissante (resp. décroissante).
    \item La composée de deux fonctions ayant le même sens de variation est croissante.
    \item La composée de deux fonctions ayant des sens de variations contraires est décroissante.
\end{itemize}
\end{propriete}

\ifprof
\begin{demonstration}
 \begin{itemize}[label=\textbullet]
    \item Soit $x < y$. Si $f$ et $g$ sont croissantes, alors $f(x)\leq f(y)$ et $g(x)\leq g(y)$. En additionnant les inégalités, $f(x)+g(x) \leq f(y)+g(y)$ : $f+g$ est bien croissante.
    \item Soit $x< y$ et $f,g$ deux fonctions croissantes. Alors $f(x)\leq f(y)$. Puisque $g$ est croissante, on a également $g(f(x)) \leq g(f(y))$ : $g\circ f$ est bien croissante.
\end{itemize}
Les autres inégalités se démontrent de la même manière.
\end{demonstration}
\else
\lignes{5}
\fi

\begin{attention}
La somme d'une fonction croissante et d'une fonction décroissante peut tout donner !
\end{attention}

\begin{theoreme}
  Soit $f:I \rightarrow \R$ une application strictement monotone. Alors $f$ est bijective de $I$ sur $f(I)$.
\end{theoreme}

\ifprof
\begin{demonstration}
$f$ est par définition surjective sur $f(I)$. Enfin, si $x\neq x'$, $f(x)\neq f(x')$ car la fonction $f$ est strictement monotone : donc $f$ est injective, et donc bijective.
\end{demonstration}
\else
\lignes{4}
\fi

\afaire{Exercice \lienexo{1}.}

    \subsection{Majorant-Minorant}

 \begin{definition}
Soit $f$ une fonction définie sur un intervalle $I$ de $\R$.
 \begin{itemize}[label=\textbullet]
     \item $f$ est dite \textbf{majorée} sur $I$ s'il existe un réel $M$ tel que, \[\forall~x \in I,\quad f(x)\leq M\]
     $M$ est appelé un \textbf{majorant} de $f$ sur $I$.
     \item $f$ est dite \textbf{minorée} sur $I$ s'il existe un réel $m$ tel que, \[\forall~x \in I,\quad f(x)\geq m\]
     $m$ est appelé un \textbf{minorant} de $f$ sur $I$.
     \item $f$ est dite \textbf{bornée} sur $I$ si elle est à la fois majorée et minorée.
 \end{itemize}
\end{definition}
 \begin{center}
     \includegraphics[width=7cm]{tex/Chap03/pic/majorantminorant}
 \end{center}

    \subsection{Maximum-Minimum}

\label{objectif-04-2}
 \begin{definition}
 Soit $f$ une fonction définie sur un intervalle $I$. Soit $a$ un nombre réel de l'intervalle $I$.
 \begin{itemize}[label=\textbullet]
     \item On dit que $f$ admet un \textbf{maximum} en $a$  si, pour tout réel $x$ de $I$, on
 \[f(x) \leq f(a)\]
 On note $\ds{\max_{x\in I} f(x)= f(a)}$.
      \item On dit que $f$  admet un \textbf{minimum} en $a$ si, pour tout réel $x$ de $I$, on
 \[f(x) \geq f(a)\]
 On note $\ds{\min_{x\in I} f(x)= f(a)}$.
 \end{itemize}
\end{definition}
 \begin{center}
     \includegraphics[width=7cm]{tex/Chap03/pic/minmax}
 \end{center}

\begin{remarque}
On dit que $f(a)$ est un \textbf{extremum} de $f$ sur $I$ si $f(a)$ est un minimum ou un maximum de $f$ sur $I$.\\
Si $f(a)$ est un extremum sur un intervalle ouvert contenant $a$, mais pas sur $I$ tout entier, on dit que $f(a)$ est un \textbf{extremum local} en $a$.
\end{remarque}

\section{Fonctions de référence}

    	\subsection{Fonctions affines}
    
\label{objectif-04-3}

\begin{definition}
Une fonction affine est une fonction de la forme $f:x\donne ax+b$, où $a$ et $b$ sont des réels fixés. $a$ est appelé le \textbf{coefficient directeur} de $f$ et $b$ son \textbf{ordonnée à l'origine}.
\end{definition}


\begin{proposition}
Sa courbe représentative est une droite.\\On rappelle le tableau de signe et variations :
\begin{itemize}[label=\textbullet]
	\item Si $a>0$ :
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.3,lgt = 3]{$x$ / .75 , Signe de $f$ / .75, Variation de $f$ / 1.5}{$-\infty$, $-\frac{b}{a}$, $+\infty$}
   \tkzTabLine{, -, z, +, }
   \tkzTabVar{-/, R / , +/}
   \tkzTabIma{1}{3}{2}{$0$}
\end{tikzpicture}
\end{center}
	\item Si $a<0$ : 
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.3,lgt = 3]{$x$ / .75 , Signe de $f$ / .75, Variation de $f$ / 1.5}{$-\infty$, $-\frac{b}{a}$, $+\infty$}
   \tkzTabLine{, +, z, -, }
   \tkzTabVar{+/, R / , -/}
   \tkzTabIma{1}{3}{2}{$0$}
\end{tikzpicture}
\end{center}
\end{itemize}	
\end{proposition}

		\subsection{Fonction trinômes}
    
\begin{definition}
On appelle fonction \textbf{trinôme du second degré} à coefficients réels une fonction de la forme $f:x\donne ax^2+bx+c$, avec $a,b$ et $c$ trois réels fixes et $a\neq 0$.	
\end{definition}

\begin{proposition}
Sa courbe représentative est une parabole. \\On rappelle son tableau de variations :
\begin{itemize}[label=\textbullet]
	\item Si $a>0$ :
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.3,lgt = 3]{$x$ / .75 , Variation de $f$ / 2}{$-\infty$, $-\frac{b}{2a}$, $+\infty$}
   \tkzTabVar{+/, - / $f\left(-\frac{b}{2a}\right)$ , +/}
\end{tikzpicture}
\end{center}
	\item Si $a<0$ : 
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.3,lgt = 3]{$x$ / .75 , Variation de $f$ / 2}{$-\infty$, $-\frac{b}{2a}$, $+\infty$}
   \tkzTabVar{-/, + / $f\left(-\frac{b}{2a}\right)$ , -/}
\end{tikzpicture}
\end{center}
\end{itemize}	
\end{proposition}

\begin{proposition}
On appelle \textbf{discriminant} du trinôme $f:x\donne ax^2+bx+c$ ($a\neq 0$) le nombre $\Delta=b^2-4ac$.
\begin{itemize}[label=\textbullet]
	\item Si $\Delta>0$, $f$ admet deux racines : \[ x_1=\frac{-b-\sqrt{\Delta}}{2a} \qeq x_2=\frac{-b+\sqrt{\Delta}}{2a} \]
		Il se factorise en $f(x)=a(x-x_1)(x-x_2)$ et son tableau de signe est donné par (en supposant ici que $x_1<x_2$) :
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.7,lgt = 3]{$x$ / .75 , Signe de $f$ / .75}{$-\infty$, $x_1$, $x_2$, $+\infty$}
   \tkzTabLine{, \text{signe de } a  , z, \text{signe de } -a, z, \text{signe de } a, }
\end{tikzpicture}
\end{center}
	\item Si $\Delta=0$, $f$ admet une racine $x_0=-\frac{b}{2a}$. Il se factorise en $f(x)=a(x-x_0)^2$ et son tableau de signe est donné par
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.7,lgt = 3]{$x$ / .75 , Signe de $f$ / .75}{$-\infty$, $x_0$, $+\infty$}
   \tkzTabLine{, \text{signe de } a  , z, \text{signe de } a, }
\end{tikzpicture}
\end{center}
	\item Si $\Delta<0$, $f$ n'admet pas de racine réelle et ne se factorise pas sur $\R$. Son tableau de signe est donné par 
\begin{center}
\begin{tikzpicture}
   \tkzTabInit[espcl=2.7,lgt = 3]{$x$ / .75 , Signe de $f$ / .75}{$-\infty$, $+\infty$}
   \tkzTabLine{, \text{signe de } a  ,  }
\end{tikzpicture}
\end{center}
\end{itemize}	
\end{proposition}


        \subsection{Fonction valeur absolue}

\label{objectif-04-4}

\begin{definition}
La fonction \textbf{valeur absolue} est la fonction définie sur $\R$ par
\[|x|= \left\{ \begin{array}{rcl} x &\textrm{ si } & x\geq 0 \\
                                  -x &\textrm{ si }& x \leq 0 \end{array}\right.\]
\end{definition}

\begin{exemple}
  $|3|=3$ et $|-4|=4$.
\end{exemple}

\begin{remarque}
  Par construction, la valeur absolue est une fonction paire, décroissante sur $\R^{-}$ et
 croissante sur $\R^+$.
 \begin{center}
 \includegraphics[width=7cm]{tex/Chap03/pic/abs}
 \end{center}
\end{remarque}

\begin{propriete}\logoparcoeur
Soient $x,y$ deux réels, et $n\in \N$.
\begin{itemize}
  \item \( |-x|=|x| \textrm{ (parité) }\quad \quad|xy|=|x||y|\quad\quad|x^n|=|x|^n \quad \quad |x|^2 = x^2\)\vspace*{2mm}
  \item Si $y\neq 0$, \(\left|\frac{x}{y} \right|=\frac{|x|}{|y|}\)\vspace*{2mm}
  \item \(|x|=|y| \Leftrightarrow x=y \textrm{ ou } x=-y\)\vspace*{2mm}
  \item Si $y>0$, \( \quad |x| \leq y \Leftrightarrow -y \leq x \leq y\)\vspace*{2mm}
  \item Si $y>0$, \( \quad |x| \geq y \Leftrightarrow  x \leq -y \textrm{ ou } x \geq y\)
\end{itemize}
\end{propriete}

\begin{theoreme}[Inégalité triangulaire]\logoparcoeur
Pour tous réels $x$ et $y$,
\[|x+y|\leq |x|+|y|\qeq \left||x|-|y| \right|\leq |x-y|\]
\end{theoreme}

\begin{remarque}
  Si $x$ et $y$ sont des réels, $|x-y|$ représente la distance entre les points $A$ d'abscisse $x$ et $B$ d'abscisse $y$.

\begin{center}
\includegraphics[width=7cm]{tex/Chap03/pic/distance}
\end{center}

Ainsi $|4-1|=3$ car la distance entre le point $A(1)$ et le point $B(4)$ est de $3$.
\end{remarque}

\ifprof
\begin{demonstration}
Calculons $(|x|+|y|)^2-|x+y|^2$ :
\begin{align*}
  (|x|+|y|)^2-|x+y|^2 &=  (|x|^2 + 2|x|\cdot |y|+|y|^2)-(x+y)^2\\
    &=x^2+2|x|\cdot|y|-(x^2+2xy+y) \\
    &= 2( |xy| - xy)
\end{align*}
Or, par définition de la valeur absolue, $|xy|-xy \geq 0$. Donc $(|x|+|y|)^2-|x+y|^2\geq 0$, c'est-à-dire \[ (|x|+|y|)^2\geq |x+y|^2 \]
Puisque les deux nombres $|x|+|y|$ et $|x+y|$ sont positifs, cela implique que $|x|+|y|\geq |x+y|$.

La deuxième inégalité se traite de la même manière.
\end{demonstration}
\else
\lignes{10}
\fi

    \subsection{Fonctions racine carrée et inverse}
		\subsubsection{Fonction inverse}
\begin{definition}
La fonction \textbf{inverse} est la fonction $f$, définie sur $\R^*$, par $f(x)=\frac{1}{x}$.
\end{definition}

\begin{center}
\includegraphics{tex/Chap03/mp/fonction_inverse.mps}	
\end{center}

\begin{propriete}
\begin{itemize}[label=\textbullet]
    \item La fonction inverse est impaire : $\forall~x\in\R^*,\quad\frac{1}{-x}=-\frac{1}{x}$.
    \item Son tableau de variations est :
\begin{center}
\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f(x)$ / 2}{$-\infty$, $0$, $+\infty$}
   \tkzTabVar{-/  , +D-/ / ,  +/ }
\end{tikzpicture}	
\end{center}
   

    \item Pour tout réel $a \neq 0$, $\frac{1}{x}=a \Leftrightarrow x=\frac{1}{a}$.
\end{itemize}
\end{propriete}


	\subsubsection{Fonction racine carrée}
\begin{definition}
Pour tout $x\in \R^+$, $\sqrt{x}=x^{\frac{1}{2}}$ est l'unique réel positif dont le carré est $x$ :
\[\sqrt{x}>0 \qeq (\sqrt{x})^2=x\]
La fonction $x \mapsto \sqrt{x}$ est appelée \textbf{fonction racine carrée}.
\end{definition}

\begin{center}
    \includegraphics[width=7cm]{tex/Chap03/pic/rac}
\end{center}

\begin{propriete}\logoparcoeur
\begin{itemize}[label=\textbullet]
    \item $\sqrt{0}=0$, $\sqrt{1}=1$.
    \item Pour tous réels positifs $x$ et $y$ :
    \[\sqrt{xy}=\sqrt{x}\sqrt{y},\quad\quad(\sqrt{x})^2=x\quad\quad\textrm{ et si }y\neq 0, \quad \frac{\sqrt{x}}{\sqrt{y}}=\sqrt{\frac{x}{y}}\]
    \item Pour tout réel $x$, $\ds{\sqrt{x^2}=|x|}$.
\end{itemize}
\end{propriete}

    \subsection{Fonction logarithme népérien}

\begin{definition}
La fonction \textbf{logarithme népérien}, notée $\ln$, est l'unique fonction définie sur $\R^{*}_+$ telle que \[\forall~x>0,\quad \ln'(x)=\frac{1}{x} \qeq \ln(1)=0\]
\end{definition}

       \begin{center}
     \includegraphics[width=7cm]{tex/Chap03/pic/ln}
     \end{center}

\begin{propriete}\logoparcoeur
     \begin{itemize}[label=\textbullet]
     \item La fonction $\ln$ est strictement croissante sur $\R^{*}_+$.\\~\\
     \item Pour tous réels $x$ et $y$ de $\R^{*}_+$,
     \[\ln(xy)=\ln(x)+\ln(y)\qeq \ln\left(\frac{1}{y}\right)=-\ln(y)\]
     Ainsi,
    \[\ln\left(\frac{x}{y}\right)=\ln(x)-\ln(y) \quad\quad\quad \forall~n\in\Z,\quad \ln(x^n)=n\ln(x)\]
    \[\ln\left(\sqrt{x}\right)=\frac{1}{2}\ln(x)\]
     \item Grâce à la stricte croissance de $\ln$, pour tous réels $x,y$ strictement positifs :
     \[\ln(x)=\ln(y) \Leftrightarrow x=y\qeq \ln(x)<\ln(y) \Leftrightarrow x<y\]
    En particulier $\ln(x)=0\Leftrightarrow x=1$ et $\ln(x)=1\Leftrightarrow x=\E$ avec $\E\approx 2,718$.
    \end{itemize}
\end{propriete}

\begin{experiencehistorique}
John Neper inventa les logarithmes vers 1613. Le logarithme le plus utilisé (dit en ``base $\E$'') porte ainsi son nom.	
\end{experiencehistorique}


    \subsection{Fonction exponentielle}
\label{objectif-04-6}
\begin{definition}
  La fonction \textbf{exponentielle}, noté $\exp$, est l'unique fonction définie sur $\R$ dont la dérivée est elle-même, et telle que $\exp(0)=1$. On note $\exp(x)=\E^x$.
\end{definition}

   \begin{center}
     \includegraphics[width=7cm]{tex/Chap03/pic/exp}
     \end{center}

\begin{propriete}\logoparcoeur
\begin{itemize}[label=\textbullet]
    \item La fonction $\exp$ est strictement croissante et positive sur $\R$.
    \item Par définition, $\eu{0}=1$, et $\eu{1}=\E$.
    \item Pour tous réels $x$ et $y$, \[\exp(x+y)=\exp(x)\exp(y)\qeq\exp(-x)=\frac{1}{\exp(x)}\]
    Ainsi,
    \[\exp(x-y)=\frac{\exp(x)}{\exp(y)}\qeq \forall~n\in\Z,\quad(\exp(x))^n=\exp(nx)\]
    \item Grâce à la stricte croissance de $\exp$, pour tout réels $x$ et $y$ :
    \[\exp(x)=\exp(y) \Leftrightarrow x=y \qeq \exp(x)<\exp(y) \Leftrightarrow x<y\]
\end{itemize}
\end{propriete}

\begin{experiencehistorique}
Le mot ``exponentiel'' a été introduit pour la première fois par \textbf{Jean Bernoulli} en  1694, dans une correspondance avec \textbf{Leibniz}. La notation $\E$ est due à \textbf{Leonhard Euler}, utilisée pour la première fois en 1728.
\end{experiencehistorique}

\begin{remarque}
  La fonction exponentielle est la fonction réciproque de la fonction logarithme népérien :
\begin{itemize}[label=\textbullet]
    \item Pour tout réel $x$, $\ln(\E^x)=x$.
    \item Pour tout réel $x>0$, $\eu{\ln(x)}=x$.
    \item Pour tout réel $x$, et tout réel $y>0$, $\E^x=y \Leftrightarrow x=\ln(y)$.
\end{itemize}
Ainsi, on retiendra également que $\ln(\E)=1$.
\end{remarque}


\begin{propriete}[Comparaison $\ln$, $\exp$, $x\mapsto x$]
  Pour tout réel $x > 0$, on a
\[\ln(x) \leq x \leq \exp(x)\] et les courbes représentatives de $\exp$ et $\ln$ sont symétriques par rapport à la droite d'équation $y=x$.
\end{propriete}

  \begin{center}
\includegraphics[width=7cm]{tex/Chap03/pic/comparaison}
\end{center}

    \subsection{Fonctions puissances}

Les fonctions puissances généralisent les fonctions $x\mapsto x^n$ avec $n\in \N$.

\begin{definition}
  Soit $\alpha \in \R$. Pour tout réel $x \in \R^{*}_+$, on note \[x^\alpha := \eu{\alpha \ln(x)}\]
\end{definition}

\begin{remarque}
  Attention : l'écriture $x^\alpha$ n'est qu'une notation. En pratique, on repassera toujours à l'écrire $\eu{\alpha \ln(x)}$.
\end{remarque}

\begin{propriete}\logoparcoeur
Les fonctions puissances possèdent les mêmes règles de calcul que les puissances entières :
\begin{itemize}[label=\textbullet]
    \item $\ds{\forall~(\alpha,\beta)\in \R^2, \quad \forall~x\in \R^*_+,\quad  x^{\alpha+\beta}=x^\alpha x^\beta}$.
    \item $\ds{\forall~\alpha \in \R,\quad \forall~(x,y)\in (\R^*_+)^2,\quad (x\times y)^\alpha=x^\alpha y^\alpha \qeq x^{-\alpha}=\frac{1}{x^\alpha}}$.
    Ainsi,
    \[\left(x^\alpha\right)^\beta = x^{\alpha\beta}, \quad \quad x^{\alpha-\beta}=\frac{x^\alpha}{x^\beta} \qeq \left(\frac{x}{y}\right)^\alpha=\frac{x^\alpha}{y^\alpha}\]
\end{itemize}
\end{propriete}

\ifprof
\begin{demonstration}
Les démonstrations sont toutes sur le même modèle. Soit $(\alpha, \beta) \in \R^2$ et $x>0$. Alors
\[x^{\alpha+\beta} = \eu{(\alpha+\beta)\ln (x)} = \eu{\alpha \ln (x) + \beta \ln(x)} = \eu{\alpha \ln(x)} \eu{\beta \ln(x)}=x^\alpha x^\beta\]
où on utilise les propriétés de la fonction exponentielle.
\end{demonstration}
\else
\lignes{5}
\fi

\begin{remarque}
  Ainsi, pour tout $x>0$ et $n\in \N^*$, \[\left(x^{\frac{1}{n}}\right)^n=x\]
En particulier, $\ds{\sqrt{x}=x^{\frac{1}{2}}}$ pour $x>0$. Par abus d'écriture, on confondra les deux écritures pour $x\geq 0$.
\end{remarque}

La représentation graphique des fonctions puissances dépend de $\alpha$ :

\begin{propriete}\logoparcoeur
  \begin{center}
\begin{tabular}{c|c|c}
\includegraphics[width=4.5cm]{tex/Chap03/pic/puissg} & \includegraphics[width=4.5cm]{tex/Chap03/pic/puissp} & \includegraphics[width=4.5cm]{tex/Chap03/pic/puissn} \\
Cas $\alpha > 1$ & Cas $0 < \alpha < 1$ & Cas $\alpha <0$
\end{tabular}
\end{center}
\end{propriete}

\begin{remarque}
  On dispose d'autres fonctions puissances, dans le cas où la variable $x$ est un exposant. \\Pour tout réel $a>0$, on définit la fonction $x \mapsto a^x$ par
\[\forall~x\in \R,~a^x= \eu{x\ln(a)}\]
Ainsi, les fonctions du type $x\mapsto x^\alpha$ sont définies sur $\R^*_+$, alors que les fonctions du type $x\mapsto a^x$ sont définies sur $\R$. Dans les deux cas, elles vérifient les propriétés sur les puissances.
\end{remarque}

\afaire{Exercices \lienexo{2}, \lienexo{3}, \lienexo{4} et \lienexo{5}.}

    \subsection{Fonction partie entière}
\label{objectif-04-5}
\begin{definition}
Soit $x$ un nombre réel. On appelle \textbf{partie entière} de $x$, et on note $E(x)$, $[x]$ ou $\lfloor x\rfloor$, le plus grand nombre entier inférieur ou égal à $x$.
\end{definition}

\begin{remarque}
  On utilisera en général la notation $[x]$, voire $\lfloor x \rfloor$.
\end{remarque}

\begin{exemple}
  Ainsi, $[3,2]=3,~[\pi]=3,~[-3,2]=-4$.
\end{exemple}

     \begin{center}
     \includegraphics{tex/Chap03/mp/partie_entiere.mps}
         \end{center}

\begin{propriete}\logoparcoeur
\begin{itemize}[label=\textbullet]
    \item $\ds{\forall~x\in \R,\quad [x]\in \Z}$.
    \item $\ds{\forall~x\in \R,\quad [x]\leq x}$ par définition.
    \item $\ds{\forall~x\in \R,\quad x<[x]+1}$ aussi par définition.
    Ainsi, \[\forall~x\in \R,\quad [x]\leq x < [x]+1\]
    On a également la relation
    \[\forall~x\in \R,\quad x-1<[x]\leq x\]
    \item La fonction partie entière est constante par morceaux : pour tout $x \in [n;n+1[$ (où $n \in \Z$), $[x] = n$.\\~\\
\end{itemize}
\end{propriete}

\begin{definition}
  Soit $x$ un nombre réel. On appelle $\textbf{partie entière supérieure}$, et on note $\lceil x \rceil$, le plus petit nombre entier supérieur ou égal à $x$.
\end{definition}

\begin{remarque}
  On dispose également d'une inégalité pratique : $\forall~x\in \R,~\lceil x \rceil -1 < x \leq \lceil x \rceil$.
\end{remarque}

\afaire{Exercice \lienexo{8}.}

	\subsection{Maximum, minimum de deux fonctions}

\begin{definition}
Soient $f$ et $g$ deux fonctions définies sur un même intervalle $I$.
\begin{itemize}[label=\textbullet]
	\item On appelle \textbf{maximum} de $f$ et $g$, et on note $\max(f,g)$ l'application qui à tout réel $x\in I$ associe le plus grand des deux nombres $f(x)$ et $g(x)$.
	\item On appelle \textbf{minimum} de $f$ et $g$, et on note $\min(f,g)$ l'application qui à tout réel $x\in I$ associe le plus petit des deux nombres $f(x)$ et $g(x)$.
\end{itemize}
\end{definition}

\begin{exemple}
  Si $f:x\mapsto x^2$ et $g:x\mapsto \E^x$, on obtient la figure suivante :
\begin{center}
\includegraphics{tex/Chap03/pic/maxmin}
\end{center}
\end{exemple}

\begin{theoreme}
  Soient $f$ et $g$ deux fonctions définies sur un même intervalle $I$. Alors, pour tout réel $x\in I$ :
\[\max(f,g)(x)=\frac{f(x)+g(x)+|f(x)-g(x)|}{2} \qeq
\min(f,g)(x)=\frac{f(x)+g(x)-|f(x)-g(x)|}{2}
\]
\end{theoreme}

\ifprof
\begin{demonstration}
Si $f(x)\geq g(x)$, alors $|f(x)-g(x)|=f(x)-g(x)$ et donc
\[\frac{f(x)+g(x)+|f(x)-g(x)|}{2}=\frac{f(x)+g(x)+f(x)-g(x)}{2}=f(x)=\max(f,g)(x)\]
\[\frac{f(x)+g(x)-|f(x)-g(x)|}{2}=\frac{f(x)+g(x)-(f(x)-g(x))}{2}=g(x)=\min(f,g)(x)\]
Si cette fois-ci $f(x)<g(x)$ alors $|f(x)-g(x)|=|g(x)-f(x)|=g(x)-f(x)$. Ainsi :
\[\frac{f(x)+g(x)+|f(x)-g(x)|}{2}=\frac{f(x)+g(x)+g(x)-f(x)}{2}=g(x)=\max(f,g)(x)\]
\[\frac{f(x)+g(x)-|f(x)-g(x)|}{2}=\frac{f(x)+g(x)-(g(x)-f(x))}{2}=f(x)=\min(f,g)(x)\]
\end{demonstration}
\else
\lignes{5}
\fi
