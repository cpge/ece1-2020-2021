input TEX;

verbatimtex
%&latex
 \documentclass{article}
 \usepackage[latin1]{inputenc}
 \usepackage[frenchb]{babel}
 \usepackage{amsmath}
 \begin{document}
etex

vardef func (expr X) = 
  -1/2.496*X*X+0.7/2.496*X+5
enddef;

outputtemplate := "courbe_fonction.mps";
beginfig(1)
  u:=1cm;

  path parabole; numeric N;
  
  parabole=(-3.8, func (-3.8)) for i=-3.8 step .1 until 4.6: ..(i, func (i)) endfor;

  draw parabole scaled u withcolor red withpen pencircle scaled 2bp;

  pair O,A,B,C;
  O:=(0,0);
  A:=(1.6u,0);
  B:=(1.6u,func(1.6)*u);
  C:=(0,func(1.6)*u);
  draw A--B dashed evenly; draw B--C dashed evenly;

  for i=-5 upto 5: draw (i*u,-0.1u)--(i*u,0.1u); endfor;
  for j=-1 upto 5: draw (-0.1u,j*u)--(0.1u,j*u); endfor;
  drawarrow (-5u,0)--(5u,0);
  drawarrow (0,-2u)--(0,5.5u);

  draw (-3.8u,0)--(4.6u,0) withcolor blue withpen pencircle scaled 1.2bp;

  dotlabel.bot (btex $x$ etex, A);
  dotlabel.rt (btex $(x,f(x))$ etex, B);
  dotlabel.lft (btex $f(x)$ etex,C);

  label.urt(btex $\mathcal{C}_f$ etex, (3.2u,(func (3.2))*u)) withcolor red;
  drawdblarrow (-3.8u,func(-3.8)*u)--(4.5u,func(-3.8)*u) withcolor blue;
  label.top(btex $\mathcal{D}_f$ etex, (-u,func(-3.8)*u));

%  pair O,A,B,C,D,E,F,G,H;
%  label.bot (btex $a$ etex, (xpart H,0));
%  label.lft (btex $f(x)$ etex, (0,ypart B));
%  label.lft (btex $f(x+h)$ etex, (0,ypart C));
%  label.urt(btex $\mathcal{C}_f$ etex, (3.4u,2.3u));
endfig;

end
