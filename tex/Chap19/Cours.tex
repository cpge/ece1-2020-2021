\chapter{Applications linéaires}

\objectifintro{
Ce chapitre est très important et tombe régulièrement au concours. Il est abstrait, mais pas difficile. Il sera approfondi l'année prochaine. \\Il doit être maitrisé dans son ensemble.
}


\begin{extrait}{Laurence Tardieu (1972 -- ). \emph{Le Jugement de Léa}}
Durant des années le temps paraît linéaire, malgré les écueils, les soubresauts, les détours. Jusqu'au jour où une chute plus profonde fait voler en éclats les moindres repères.
\end{extrait}

\begin{objectifs}
\begin{numerote}
  \item \hyperref[objectif-18-9]{Savoir montrer qu'une application est linéaire}\dotfill $\Box$
  \item \hyperref[objectif-18-10]{Savoir déterminer la matrice d'une application linéaire de $\MM_{n,1}(\R)$ dans $\MM_{p,1}(\R)$}\dotfill $\Box$
  \item \hyperref[objectif-18-11]{Savoir déterminer le noyau d'une application linéaire}\dotfill $\Box$
  \item \hyperref[objectif-18-12]{Savoir déterminer l'image d'une application linéaire}\dotfill $\Box$
  \item \hyperref[objectif-18-13]{Savoir démontrer qu'une application linéaire est injective, surjective, bijective}\dotfill $\Box$
\end{numerote}
\end{objectifs}

%%% Début du cours %%%

\section{Applications linéaires}

\subsection{Définition}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels. On appelle \textbf{application linéaire} de $E$ dans $F$ toute application $f:E\rightarrow F$ telle que
\begin{itemize}
  \item $\forall~(x,y)\in E^2, f(x+y)=f(x)+f(y)$
  \item $\forall~x\in E,\forall~\lambda\in \R, f(\lambda x)=\lambda f(x)$
\end{itemize}
\end{definition}

\begin{remarque}
On peut réunir les deux propriétés en une seule : $f:E\rightarrow F$ est une application linéaire si et seulement si
\[\forall~(x,y)\in E^2, \forall~\lambda\in \R, f(\lambda x+y)=\lambda f(x)+f(y)\]
\end{remarque}

\begin{methode}
\label{objectif-18-9}
Pour montrer qu'une application $f$ est une application linéaire, on prendra $x$ et $y$ dans $E$, $\lambda \in \R$, et on montre que $f(\lambda x + y) = \lambda f(x) + f(y)$.
\end{methode}

\begin{exo}
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{3,1}(\R)$ définie pour tout $(a, b)\in \R^2$ par
\[f\left( \left(\begin{array}{c} a\\b\end{array} \right) \right)=   \left(\begin{array}{c} b\\0\\a\end{array} \right)\]
Montrer que $f$ est une application linéaire.
\end{exo}

\ifprof
\begin{soluce}
En effet, si $A=\left(\begin{array}{c} a_1\\a_2\end{array} \right)$ et $B=\left(\begin{array}{c} b_1\\b_2\end{array} \right)$, et si $\lambda \in \R$, alors
\[f(\lambda A+B)=f\left( \left( \begin{array}{c} \lambda a_1+b_1\\\lambda a_2+b_2 \end{array}\right) \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
et
\[\lambda f(A)+f(B)=\lambda \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)+\left(\begin{array}{c} b_2\\0\\b_1\end{array} \right)=\left(\begin{array}{c} \lambda a_2+b_2\\0\\\lambda a_1+b_1\end{array} \right)\]
Donc $f(\lambda A + B)=\lambda f(A)+f(B)$ : $f$ est une application linéaire.
\end{soluce}
\else
\lignes{12}
\fi

 \afaire{Exercice \lienexo{1}.}

\begin{notation}
L'ensemble des applications linéaires de $E$ dans $F$ est noté $\mathcal{L}(E,F)$.
\end{notation}

\begin{definition}
\begin{itemize}
  \item Si $F=E$, et si $f:E\rightarrow E$ est une application linéaire, on dit que $f$ est un \textbf{endomorphisme}.
  \item Si $F=\R$, et si $f\in \mathcal{L}(E,\R)$, on dit que $f$ est une \textbf{forme linéaire}.
\end{itemize}
\end{definition}

\begin{propriete}
Soient $E$ et $F$ deux espaces vectoriels, et $f:E\rightarrow F$ une application linéaire. Alors $f(0_E)=0_F$.
\end{propriete}

\begin{proposition}
Soient $E$, $F$ et $G$ trois espaces vectoriels, et soit $f\in \mathcal{L}(E,F)$ et $g \in \mathcal{L}(F,G)$. Alors $g~\circ~f~\in~\mathcal{L}(E,G)$.
\end{proposition}

\ifprof
\begin{demonstration}
Soient $x, y \in E^2$ et $\lambda \in \R$. Alors, en utilisant respectivement la linéarité de $f$ puis de $g$ :
\[g\circ f (\lambda x + y) = g( f(\lambda x + y)) = g(\lambda f(x)+f(y)) = \lambda g(f(x))+g(f(y))=\lambda g\circ f(x)+g \circ f(y)\]
\end{demonstration}
\else
\lignes{3}
\fi

\subsection{Cas de $\mathcal{L}(\MM_{n,1}(\R),\MM_{p,1}(\R))$}

\begin{theoreme}
Soit $f:\MM_{n,1}(\R)\rightarrow \MM_{p,1}(\R)$ une application linéaire. Alors, il existe une matrice \\$M\in \MM_{p,n}(\R)$ telle que
\[\forall~X \in \MM_{n,1}(\R), f(X)=MX\]
Cette matrice est appelée matrice \textbf{associée} à $f$ dans les bases canoniques respectives et est notée $\mathrm{Mat}_{\mathcal{B}}~f$.
\end{theoreme}

\begin{methode}
\label{objectif-18-10}
Pour déterminer la matrice associée à une application linéaire $f:\MM_{n,1}(\R) \rightarrow \MM_{p,1}(\R)$, on prend la base canonique $(e_1,\cdots, e_n)$ de $\MM_{n,1}(\R)$, et on calcule les images $f(e_i)$ par $f$ qu'on range dans une matrice.
\end{methode}

\begin{exo}
Soit $f$ l'application définie dans l'exemple précédent :
 \[f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)\]
 Déterminer la matrice associée à $f$ dans les bases canoniques.
\end{exo}

\ifprof
\begin{soluce}
Soit $e_1=\left(\begin{array}{c}1\\0\end{array}\right)$ et $e_2=\left(\begin{array}{c}0\\1\end{array}\right)$ la base canonique de $\MM_{2,1}(\R)$. On a
\[f(e_1)=\left(\begin{array}{c}0\\0\\1\end{array}\right) \qeq f(e_2)=\left(\begin{array}{c}1\\0\\0\end{array}\right)\]
Ainsi,
 \[   f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=
 \left( \begin{array}{cc} 0&1\\0&0 \\ 1&0\end{array}\right) \left(\begin{array}{c} a_1\\a_2\end{array} \right) \]
 donc $\forall~X\in \MM_{2,1}(\R),~f(X)=MX$ avec $M=\left( \begin{array}{cc} 0&1\\0&0 \\ 1&0\end{array}\right) $.
\end{soluce}
\else
\lignes{8}
\fi

 \afaire{Exercice \lienexo{2}.}

\section{Noyau, image}

\subsection{Noyau d'une application linéaire}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{noyau} de $f$, et on note $\Ker f$, le sous-ensemble de $E$ défini par
     \[\Ker f = \left \{ x\in E,~f(x)=0_F \right\}\]
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Ker f$ est un sous-espace vectoriel de $E$.
\end{theoreme}

\ifprof
\begin{demonstration}
Puisque $f(0_E)=0_F$, on a déjà $0_E \in \Ker f$. Ensuite, si $x$ et $y$ sont deux vecteurs de $\Ker f$, et si $\lambda \in \R$, alors
\[f(\lambda x + y) = \lambda f(x) +f(y) \underbrace{=}_{x\textrm{ et }y \textrm{ dans } \ker f} \lambda 0_F+0_F=0_F\]
donc $\lambda x + y \in \Ker f$ : $\Ker f$ est bien un sous-espace vectoriel de $E$.
\end{demonstration}
\else
\lignes{6}
\fi

\begin{methode}
\label{objectif-18-11}
Pour déterminer le noyau d'une application linéaire, on résout l'équation $f(x)=0_F$ d'inconnue $x$. On se ramène en général à un système.
\end{methode}

\begin{exo}
Déterminer le noyau de l'application linéaire précédente.
\end{exo}

\ifprof
\begin{soluce}
Si $f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)$, alors $X=\left( \begin{array}{c} x_1\\x_2 \end{array} \right) \in \Ker f$ si et seulement si
\[f\left( X \right)=   \left(\begin{array}{c} x_2\\0\\x_1\end{array} \right)=\left( \begin{array}{c} 0\\0\\0 \end{array} \right)\]
ce qui donne le système
\[\left \{ \begin{array}{ccc} x_2 &=& 0 \\ 0 &=& 0 \\ x_1 &=& 0 \end{array} \right. \Leftrightarrow \left \{ \begin{array}{ccc} x_2 &=& 0 \\x_1 &=& 0 \end{array} \right. \Leftrightarrow X=0_E\]
Donc, $\Ker f = \{ 0_E \}$.
\end{soluce}
\else
\lignes{8}
\fi

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. $f$ est injective si et seulement si $\Ker f = \{ 0_E \}$.
\end{theoreme}

\ifprof
\begin{demonstration}
Supposons $f$ injective. Soit $x$ tel que $f(x)=0_F$ alors $f(x)=f(0_E)$. Par injectivité de $f$, $x=0_E$. Donc $\Ker f = \{ 0_E \}$. \\
Réciproquement, supposons que $\Ker f = \{ 0_E \}$. Soient $x$ et $x'$ tels que $f(x)=f(x')$. Par linéarité de $f$, $f(x-x')=0_F$. Puisque $\Ker f = \{ 0_E \}$, alors $x-x'=0_E$ c'est-à-dire $x=x'$ : $f$ est injective.
\end{demonstration}
\else
\lignes{7}
\fi

\subsection{Image d'une application linéaire}

\subsubsection{Définition}

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathcal{L}(E,F)$. On appelle \textbf{image} de $f$, et on note $\Img f$, le sous-ensemble de $F$ défini par
     \[\Img f = \left \{ y \in F, ~\exists~x\in E,~y=f(x) \right \}\]
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $\Img f$ est un sous-espace vectoriel de $F$. Si $\mathcal{B}=(e_1,\cdots, e_n)$ est une base de $E$, alors
\[\Img f = \Vect(f(e_1),\cdots, f(e_n))\]
\end{theoreme}

\ifprof
\begin{demonstration}
Puisque $O_F=f(0_E)$, on a $O_F \in \Img f$. Soient $a$ et $b$ deux éléments de $\Img f$, et $\lambda \in \R$. Puisque $a$ et $b$ sont dans $\Img f$, il existe $x$ et $y$ dans $E$ tels que
\[a=f(x) \qeq  b=f(y)\]
Mais alors
\[\lambda a + b = \lambda f(x) + f(y)  \underbrace{=}_{\textrm{linéarité de }f} f(\lambda x + y)\]
Donc $\lambda a + b$ s'écrit $f(z)$ pour un certain $z\in E$ : $\lambda a + b \in \Img f$. $\Img f$ est donc bien un sous-espace vectoriel de $F$.
\end{demonstration}
\else
\lignes{8}
\fi

\subsubsection{Rang}

Puisque $\Img f$ est un sous-espace vectoriel, on peut s'intéresser à sa dimension :

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels de dimension finie. Soit $f\in \mathcal{L}(E,F)$. On appelle \textbf{rang} de $f$, et on note $\rg(f)$, la dimension de $\Img f$.
\end{definition}

\begin{methode}
\label{objectif-18-12}
Pour déterminer l'image d'une application linéaire, on prend une base $\mathcal{B}=(e_1,\cdots,e_n)$ de $E$ et on écrit $\Img f = \Vect(f(e_1),\cdots,f(e_n))$. On essaie ensuite d'en déterminer une base.
\end{methode}

\begin{exo}
Déterminer l'image de l'application linéaire précédente.
\end{exo}

\ifprof
\begin{soluce}
Si $f\left( \left(\begin{array}{c} a_1\\a_2\end{array} \right) \right)=   \left(\begin{array}{c} a_2\\0\\a_1\end{array} \right)$, on prend $\mathcal{B}=(e_1,e_2)$ la base canonique de $\MM_{2,1}(\R)$. On a donc $\Img f = \Vect(f(e_1),f(e_2))$.
\[f(e_1)=f\left( \left( \begin{array}{c}1\\0 \end{array} \right) \right) = \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right) \qeq
f(e_2)=f\left( \left( \begin{array}{c}0\\1 \end{array} \right) \right) = \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\]
La famille $\left( \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right), \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\right)$ est libre car les vecteurs ne sont pas colinéaires.
Donc
\[\Img f = \Vect \left(\left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right),\left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array}  \right) \right)
= \left \{ \left( \begin{array}{c} \lambda \\0\\\mu \end{array} \right) , \lambda \in \R, \mu \in \R \right\}\]
et $\left( \left( \begin{array}{c} 0 \\ 0 \\ 1 \end{array} \right), \left( \begin{array}{c} 1 \\ 0 \\ 0 \end{array} \right)\right)$  forme une base de $\Img f$. Ainsi, $\rg(f) = 2$.
\end{soluce}
\else
\lignes{16}
\fi

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est surjective si et seulement si $\Img f = F$.
\end{theoreme}

\ifprof
\begin{demonstration}
Par définition, $f$ est surjective si et seulement si $f(E)=F$. Or, par définition, $f(E)=\Img f$. Donc $f$ est surjective si et seulement si $\Img f = F$.
\end{demonstration}
\else
\lignes{3}
\fi

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et  $f \in \mathcal{L}(E,F)$. Alors $f$ est bijective si et seulement si $\Ker f = \{0_E \}$ et $\Img f = F$.
\end{theoreme}

\begin{methode}
\label{objectif-18-13}
Pour montrer qu'une application linéaire $f:E\rightarrow F$ est bijective, on montrera que $\Ker f=\{0_E\}$ et que $\Img f = F$.
\end{methode}

\begin{exo}
Soit $f:\MM_{2,1}(\R)\rightarrow \MM_{2,1}(\R)$ l'application définie par
\[f\left( \left(\begin{array}{c}x_1\\x_2
\end{array}\right) \right) = \left( \begin{array}{c} 2x_2\\x_1 \end{array}\right)\]
est bijective.
\end{exo}

\ifprof
\begin{soluce}
$f$ est bien linéaire (c'est un endomorphisme de $\MM_{2,1}(\R)$. Déterminons noyau et image.
\begin{itemize}[label=\textbullet]
	\item Soit $X=\left(\begin{array}{c}x\\y\end{array}\right) \in \Ker f$. Alors
		\[f(X)=\left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow \left(\begin{array}{c}2y\\x\end{array}\right) = \left(\begin{array}{c}0\\0\end{array}\right) \Leftrightarrow x=y=0\]
		ainsi, $\Ker f =\left\{ \left(\begin{array}{c}0\\0\end{array}\right) \right\}$ et $f$ est injective.
	\item Soit $e_1=\left(\begin{array}{c}1\\0\end{array}\right)$ et $e_2=\left(\begin{array}{c}0\\1\end{array}\right)$ la base canonique de $\MM_{2,1}(\R)$. Alors,
		\[f(e_1)=\left(\begin{array}{c}0\\1\end{array}\right)=e_2 \qeq f(e_2)=\left(\begin{array}{c}2\\0\end{array}\right)=2e_1\]
		La famille $(2e_1,e_2)$ est libre (car les vecteurs ne sont pas colinéaires) donc forme une base de $\Img f$ :
		\[\Img f = \Vect(2e_1,e_2) = \Vect(e_1,e_2) = \MM_{2,1}(\R)\]
		Ainsi, $f$ est surjective.
\end{itemize}
\textbf{Bilan} : $f$ est bien bijective.
\end{soluce}
\else
\lignes{15}
\fi

\begin{definition}
Soient $E$ et $F$ deux espaces vectoriels.
\begin{itemize}
  \item Si $f \in \mathcal{L}(E,F)$ est bijective, on dit que $f$ est un \textbf{isomorphisme} de $E$ dans $F$.
  \item Si $f \in \mathcal{L}(E)$ est bijective, on dit que $f$ est un \textbf{automorphisme}.
  \item On note $\mathrm{Isom}(E,F)$ l'ensemble des isomorphismes de $E$ dans $F$, et $\mathrm{Aut}(E)$ ou $GL(E)$ l'ensemble des automorphismes de $E$.
\end{itemize}
\end{definition}

\begin{theoreme}
Soient $E$ et $F$ deux espaces vectoriels, et $f\in \mathrm{Isom}(E,F)$. Alors $f^{-1}$ est également une application linéaire et $f^{-1}\in \mathrm{Isom}(F,E)$.
\end{theoreme}

\ifprof
\begin{demonstration}
Soient $x,y$ deux éléments de $F$ et $\lambda$ un réel. Puisque $f$ est bijective, il existe un unique $a\in E$ tel que $f(a)=x$ et un unique $b\in E$ tel que $f(b)=y$. Donc $f(\lambda a + b)=\lambda f(a)+f(b) = \lambda x + y$ donc, par unicité de bijectivité de $f$, $\lambda a + b = f^{-1}(\lambda f(a)+f(b))$, c'est-à-dire
\[\lambda f^{-1}(x)+f^{-1}(y)=f^{-1}(\lambda x + y)\]
$f^{-1}$ est bien linéaire.
\end{demonstration}
\else
\lignes{6}
\fi

 \afaire{Exercices \lienexo{3}, \lienexo{4}, \lienexo{5}, \lienexo{6} et \lienexo{7}.}

%%% Fin du cours %%%
