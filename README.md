# Cours de mathématiques ECE 1ère année (2020-2021) Ancien programme

Ce dépôt contient le cours, avec les exercices, de mathématiques enseigné par Antoine Crouzet au lycée Rabelais (Saint Brieuc). 

* Répertoire Doc : des documents (programmes, ...)
* Répertoire scripts et Makefile : outils permettant de créer les fichiers avec mon fichier style
* Répertoire tex : les sources, par chapitre, et répertoires particuliers (pic, script)
* Répertoire PDF : les fichiers compilés (version elève, sans démonstration et correction; version publi, avec démonstration et sans correction, version prof)
* Répertoire classe : ma classe personnelle, basée sur **doc-vuiprep**

-> des erreurs, coquille... sont présents. N'hésitez pas à remonter les erreurs potentielles.

### Sous licence CC-BY-NC-SA4